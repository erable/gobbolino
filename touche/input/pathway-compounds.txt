ID=PROPIONATE;NAME=propionate
ID=NADPH;NAME=NADPH
ID=FAD;NAME=FAD
ID=PHOSPHORIBOSYL__45__FORMAMIDO__45__CARBOXAMIDE;NAME=phosphoribosyl-formamido-carboxamide
ID=_5__45__P__45__RIBOSYL__45__N__45__FORMYLGLYCINEAMIDE;NAME=5&amp;#039;-phosphoribosyl-N-formylglycineamide
ID=GLC__45__1__45__P;NAME=glucose-1-phosphate
ID=GDP;NAME=GDP
ID=APS;NAME=APS
ID=_3__45__P__45__HYDROXYPYRUVATE;NAME=3-phospho-hydroxypyruvate
ID=PHOSPHO__45__ENOL__45__PYRUVATE;NAME=phosphoenolpyruvate
ID=DIHYDROFOLATE;NAME=7,8-dihydrofolate
ID=ILE;NAME=L-iso-leucine
ID=L__45__ORNITHINE;NAME=L-ornithine
ID=OROTIDINE__45__5__45__PHOSPHATE;NAME=orotidine-5&amp;#039;-phosphate
ID=P__45__HYDROXY__45__PHENYLPYRUVATE;NAME=p-hydroxyphenylpyruvate
ID=N__45__ALPHA__45__ACETYLORNITHINE;NAME=N-&amp;alpha;-acetylornithine
ID=CPD__45__171;NAME=a dolichyl &amp;beta;-D-mannosyl phosphate
ID=B__45__KETOACYL__45__ACP;NAME=a &amp;beta;-ketoacyl-[acp]
ID=ARG;NAME=L-arginine
ID=PROTOHEME;NAME=protoheme
ID=_3__45__HYDROXY__45__ANTHRANILATE;NAME=3-hydroxy-anthranilate
ID=CPD__45__177;NAME=a 1-phosphatidyl-1D-myo-inositol 3-phosphate
ID=IMP;NAME=IMP
ID=BUTANEDIOL;NAME=butanediol
ID=HIS;NAME=L-histidine
ID=GLYCOLALDEHYDE;NAME=glycolaldehyde
ID=_3__45__CARBOXY__45__3__45__HYDROXY__45__ISOCAPROATE;NAME=2-isopropylmalate
ID=DEAMIDO__45__NAD;NAME=deamido-NAD
ID=COPROPORPHYRINOGEN_III;NAME=coproporphyrinogen III
ID=_4__45__PHOSPHOPANTOTHENOYLCYSTEINE;NAME=4&amp;#039;-phospho-N-pantothenoylcysteine
ID=L__45__1__45__PHOSPHATIDYL__45__GLYCEROL__45__P;NAME=an L-1-phosphatidylglycerol-phosphate
ID=PALMITOYL__45__ACP;NAME=palmitoyl-ACP
ID=L__45__GLUTAMATE_GAMMA__45__SEMIALDEHYDE;NAME=L-glutamate &amp;gamma;-semialdehyde
ID=P__45__RIBOSYL__45__4__45__SUCCCARB__45__AMINOIMIDAZOLE;NAME=5&amp;#039;-phosphoribosyl-4-(N-succinocarboxamide)-5-aminoimidazole
ID=PYRIDOXAL;NAME=pyridoxal
ID=PHE;NAME=L-phenylalanine
ID=ACYL__45__COA;NAME=an acyl-CoA
ID=CPD__45__190;NAME=an &lt;i&gt;N&lt;/i&gt;-acetylglucosaminyl-diphosphodolichol
ID=IPC;NAME=inositol-P-ceramide
ID=DEHYDROQUINATE;NAME=3-dehydroquinate
ID=RIBITYLAMINO__45__AMINO__45__DIHYDROXY__45__PYR;NAME=4-(1-D-ribitylamino)-5-amino-2,6-dihydroxypyrimidine
ID=DIPHOSPHO__45__1D__45__MYO__45__INOSITOL__45__TETRAKISPHOSPH;NAME=a diphospho-1D-myo-inositol tetrakisphosphate
ID=FRUCTOSE__45__16__45__DIPHOSPHATE;NAME=fructose-1,6-bisphosphate
ID=ATP;NAME=ATP
ID=phospholipid;NAME=NA
ID=METHYL__45__GLYOXAL;NAME=methyl-glyoxal
ID=__124__2__45__Hexadecenoyl__45__ACPs__124__;NAME=a &lt;i&gt;trans&lt;/i&gt; hexadecenoyl-[acp]
ID=OH__45__ACYL__45__ACP;NAME=a (3&lt;i&gt;R&lt;/i&gt;)-3-hydroxyacyl-[acp]
ID=_1__45__PHOSPHATIDYL__45__1D__45__MYO__45__INOSITOL__45__35__45__BISPH;NAME=1-phosphatidyl-1D-myo-inositol 3,5-bisphosphate
ID=PHOSPHORYL__45__ETHANOLAMINE;NAME=phosphoryl-ethanolamine
ID=PALMITYL__45__COA;NAME=palmityl-CoA
ID=GALACTOSE;NAME=&amp;beta;-D-galactose
ID=CIS__45__ACONITATE;NAME=cis-aconitate
ID=NAD;NAME=NAD
ID=PHOSPHORIBOSYL__45__FORMIMINO__45__AICAR__45__P;NAME=phosphoribosylformiminoAICAR-phosphate
ID=CPD__45__499;NAME=mevalonate-5P
ID=TRIMETHYLSULFONIUM;NAME=trimethyl sulfonium
ID=CDPDIACYLGLYCEROL;NAME=a CDP-diacylglycerol
ID=XANTHOSINE__45__5__45__PHOSPHATE;NAME=xanthosine-5-phosphate
ID=URACIL;NAME=uracil
ID=CPD__45__280;NAME=glutarate semialdehyde
ID=CIS__45__DELTA3__45__ENOYL__45__COA;NAME=a &lt;i&gt;cis&lt;/i&gt;-3-enoyl-CoA
ID=BCCP__45__BIOTIN;NAME=BCCP-biotin
ID=ACETOACETYL__45__COA;NAME=acetoacetyl-CoA
ID=CARBON__45__DIOXIDE;NAME=CO&lt;SUB&gt;2&lt;/SUB&gt;
ID=L__45__CYSTATHIONINE;NAME=cystathionine
ID=TYR;NAME=L-tyrosine
ID=SUC;NAME=succinate
ID=ERYTHROSE__45__4P;NAME=erythrose-4-phosphate
ID=_5__45__METHYLTHIOADENOSINE;NAME=5&amp;#039;-methylthioadenosine
ID=L__45__DELTA1__45__PYRROLINE_5__45__CARBOXYLATE;NAME=L-&amp;Delta;&lt;SUP&gt;1&lt;/SUP&gt;-pyrroline-5-carboxylate
ID=_2__45__OXOBUTANOATE;NAME=2-oxobutanoate
ID=_2__45__PG;NAME=2-phosphoglycerate
ID=_5__45__ACETAMIDOVALERATE;NAME=5-acetamidovalerate
ID=PHOSPHATIDYLCHOLINE__45__CMPD;NAME=phosphatidylcholine
ID=__124__Acceptor__124__;NAME=an oxidized electron acceptor
ID=PHYTOCERAMIDE;NAME=phytoceramide
ID=O__45__PHOSPHO__45__L__45__HOMOSERINE;NAME=O-phospho-L-homoserine
ID=LYS;NAME=L-lysine
ID=DIHYDRONEOPTERIN__45__P;NAME=dihydroneopterin phosphate
ID=ADENYLOSUCC;NAME=adenylo-succinate
ID=PYRIDOXAL_PHOSPHATE;NAME=pyridoxal 5&amp;#039;-phosphate
ID=DUMP;NAME=dUMP
ID=_5__45__PHOSPHORIBOSYL__45__5__45__AMINOIMIDAZOLE;NAME=5&amp;#039;-phosphoribosyl-5-aminoimidazole
ID=CPD3O__45__144;NAME=[PP]&lt;sub&gt;2&lt;/sub&gt;-IP&lt;sub&gt;4&lt;/sub&gt;
ID=PROTOPORPHYRIN_IX;NAME=protoporphyrin IX
ID=COUMARATE;NAME=4-coumarate
ID=DIOH__45__ISOVALERATE;NAME=2,3-dihydroxy-isovalerate
ID=protein__45__acetyllysine;NAME=NA
ID=_44__45__DIMETHYL__45__824__45__CHOLESTADIENOL;NAME=4,4-dimethylzymosterol
ID=CPD__45__166;NAME=a dolichyl &amp;beta;-D-glucosyl phosphate
ID=CPD__45__160;NAME=a phosphatidyl-&lt;i&gt;N&lt;/i&gt;-dimethylethanolamine
ID=ACETYL__45__COA;NAME=acetyl-CoA
ID=CPD3O__45__292;NAME=Man(5)GlcNAc(2)-PP-Dol
ID=XANTHINE;NAME=xanthine
ID=ACYL__45__SN__45__GLYCEROL__45__3P;NAME=a 1-acyl-&lt;i&gt;sn&lt;/i&gt;-glycerol-3-phosphate
ID=UDP__45__GLUCOSE;NAME=UDP-D-glucose
ID=CPD__45__250;NAME=L-Formylkynurenine
ID=_2__45__D__45__THREO__45__HYDROXY__45__3__45__CARBOXY__45__ISOCAPROATE;NAME=3-isopropylmalate
ID=NADH;NAME=NADH
ID=CYS;NAME=L-cysteine
ID=AICAR;NAME=AICAR
ID=ADENOSINE;NAME=adenosine
ID=_2K__45__ADIPATE;NAME=&amp;alpha;-ketoadipate
ID=NADP;NAME=NADP
ID=_3__45__AMINOPROPANAL;NAME=3-aminopropanal
ID=THREO__45__DS__45__ISO__45__CITRATE;NAME=isocitrate
ID=HYDROXYMETHYLBILANE;NAME=hydroxymethylbilane
ID=GLC;NAME=&amp;beta;-D-glucose
ID=SQUALENE;NAME=squalene
ID=FORMYL__45__THF__45__GLU__45__N;NAME=an &lt;i&gt;N&lt;/i&gt;&lt;sup&gt;10&lt;/sup&gt;-formyl-tetrahydrofolate
ID=HISTIDINAL;NAME=histidinal
ID=CPD__45__641;NAME=mevalonate-5-PP
ID=CPD__45__444;NAME=5-methylthioribose-1-phosphate
ID=PYRIDOXAMINE;NAME=pyridoxamine
ID=QUINOLINATE;NAME=quinolinate
ID=MI__45__HEXAKISPHOSPHATE;NAME=phytate
ID=CARBOXYPHENYLAMINO__45__DEOXYRIBULOSE__45__P;NAME=1-(o-carboxyphenylamino)-1&amp;#039;-deoxyribulose-5&amp;#039;-phosphate
ID=AMINO__45__OXOBUT;NAME=2-amino-3-oxobutanoate
ID=SACCHAROPINE;NAME=saccharopine
ID=CPD__45__229;NAME=(S)-2,3-Epoxysqualene
ID=CARBAMOYL__45__P;NAME=carbamoyl-phosphate
ID=_3__45__KETOACYL__45__COA;NAME=a 3-oxoacyl-CoA
ID=DI__45__CH3__45__ALLYL__45__PPI;NAME=dimethylallyl-pyrophosphate
ID=DEPHOSPHO__45__COA;NAME=dephospho-CoA
ID=HOMO__45__I__45__CIT;NAME=homo-isocitrate
ID=_7__45__8__45__DIHYDROPTEROATE;NAME=7,8-dihydropteroate
ID=VAL;NAME=L-valine
ID=PUTRESCINE;NAME=putrescine
ID=MALONYL__45__ACP;NAME=a malonyl-[acp]
ID=UDP;NAME=UDP
ID=PAP;NAME=adenosine 3&amp;#039;,5&amp;#039;-bisphosphate
ID=HCO3;NAME=HCO&lt;SUB&gt;3&lt;/SUB&gt;&lt;SUP&gt;-&lt;/SUP&gt;
ID=PORPHOBILINOGEN;NAME=porphobilinogen
ID=INOSITOL__45__PHOSPHATE;NAME=inositol phosphate
ID=_5__45__METHYL__45__THF__45__GLU__45__N;NAME=an &lt;i&gt;N&lt;/i&gt;&lt;sup&gt;5&lt;/sup&gt;-methyl-tetrahydrofolate
ID=TDP;NAME=dTDP
ID=__124__Ubiquinones__124__;NAME=a ubiquinone
ID=HOMO__45__SER;NAME=homoserine
ID=AMINO__45__ACETONE;NAME=aminoacetone
ID=NIACINAMIDE;NAME=nicotinamide
ID=GLC__45__6__45__P;NAME=glucose-6-phosphate
ID=CPD3O__45__29;NAME=Man(4)GlcNAc(2)-PP-Dol
ID=NICOTINAMIDE_RIBOSE;NAME=nicotinamide ribose
ID=CPD3O__45__28;NAME=Man(3)GlcNAc(2)-PP-Dol
ID=CPD3O__45__27;NAME=Man(2)GlcNAc(2)-PP-Dol
ID=CHOLINE;NAME=choline
ID=_5__45__AMINO__45__LEVULINATE;NAME=5-amino-levulinate
ID=L__45__ASPARTATE;NAME=L-aspartate
ID=MET;NAME=L-methionine
ID=_57222428__45__ERGOSTATETRAENOL;NAME=5,7,22,24(28)-ergostatetraenol
ID=_2__45__ACETO__45__LACTATE;NAME=2-aceto-lactate
ID=DIHYDRO__45__SPHINGOSINE;NAME=dihydrosphingosine
ID=CPD3O__45__10;NAME=Man(8)GlcNAc(2)-PP-Dol
ID=PANTOTHENATE;NAME=pantothenate
ID=OXALACETIC_ACID;NAME=oxaloacetic acid
ID=CPD__45__405;NAME=a phosphatidyl-&lt;i&gt;N&lt;/i&gt;-methylethanolamine
ID=N__45__ACETYLGLUTAMATE_SEMIALDEHYDE;NAME=N-acetylglutamate semialdehyde
ID=_1__45__ACYL__45__DIHYDROXYACETONE__45__PHOSPHATE;NAME=1-acyl-dihydroxyacetone-phosphate
ID=HOMO__45__CYS;NAME=homocysteine
ID=ACET;NAME=acetate
ID=OROTATE;NAME=orotate
ID=CPD__45__8259;NAME=nicotinate riboside
ID=HS;NAME=H&lt;SUB&gt;2&lt;/SUB&gt;S
ID=CPD__45__482;NAME=1-phosphatidyl-D-myo-inositol
ID=MYO__45__INOSITOL;NAME=&lt;I&gt;myo&lt;/I&gt;-inositol
ID=O__45__SUCCINYL__45__L__45__HOMOSERINE;NAME=O-succinyl-L-homoserine
ID=GLYCEROL__45__3P;NAME=glycerol-3-phosphate
ID=AMMONIA;NAME=NH&lt;SUB&gt;3&lt;/SUB&gt;
ID=DGDP;NAME=dGDP
ID=_25__45__DIAMINO__45__6__45__RIBOSYLAMINO__45__PYR;NAME=2,5-diamino-6-(ribosylamino)-4-(3H)-pyrimidinone 5&amp;#039;-phosphate
ID=DUDP;NAME=dUDP
ID=CPD__45__201;NAME=4-Hydroxybenzoyl-CoA
ID=SUC__45__COA;NAME=succinyl-CoA
ID=_3__45__HEXAPRENYL__45__45__45__DIHYDROXYBENZOATE;NAME=3,4-dihydroxy-5-hexaprenylbenzoate
ID=DCDP;NAME=dCDP
ID=CPD__45__479;NAME=2-oxo-4-methylthiobutanoate
ID=GAP;NAME=glyceraldehyde-3-phosphate
ID=_44__45__DIMETHYL__45__CHOLESTA__45__812__45__24__45__TRIENOL;NAME=4,4-dimethyl-cholesta-8,14,24-trienol
ID=PYRIDOXINE;NAME=pyridoxine
ID=PROPIONYL__45__COA;NAME=propionyl-CoA
ID=L__45__3__45__HYDROXYACYL__45__COA;NAME=a (3&lt;i&gt;S&lt;/i&gt;)-3-hydroxyacyl-CoA
ID=DADP;NAME=dADP
ID=HYDROGEN__45__PEROXIDE;NAME=H&lt;SUB&gt;2&lt;/SUB&gt;O&lt;SUB&gt;2&lt;/SUB&gt;
ID=S__45__ADENOSYLMETHIONINE;NAME=S-adenosyl-L-methionine
ID=CPD__45__468;NAME=L-2-Aminoadipate
ID=SPERMIDINE;NAME=spermidine
ID=CPD__45__465;NAME=presqualene diphosphate
ID=PROTOPORPHYRINOGEN;NAME=protoporphyrinogen
ID=CPD__45__4702;NAME=4&amp;alpha;-carboxy-5&amp;alpha;-cholesta-8,24-dien-3&amp;beta;-ol
ID=_4__45__METHYL__45__824__45__CHOLESTADIENOL;NAME=4-&amp;alpha;-methylzymosterol
ID=CPD__45__1302;NAME=5-methyltetrahydropteroyltri-L-glutamate
ID=CPD__45__1301;NAME=tetrahydropteroyltri-L-glutamate
ID=ADENOSYL__45__HOMO__45__CYS;NAME=S-adenosyl-homocysteine
ID=D__45__ERYTHRO__45__IMIDAZOLE__45__GLYCEROL__45__P;NAME=D-erythro-imidazole-glycerol-phosphate
ID=PREPHENATE;NAME=prephenate
ID=FRUCTOSE__45__6P;NAME=fructose-6-phosphate
ID=DI__45__H__45__OROTATE;NAME=dihydroorotate
ID=_5__45__FORMYL__45__THF;NAME=5-formyl-THF
ID=NICOTINAMIDE_NUCLEOTIDE;NAME=nicotinamide nucleotide
ID=EPISTEROL;NAME=episterol
ID=_4__45__P__45__PANTOTHENATE;NAME=4&amp;#039;-phosphopantothenate
ID=_3__45__METHOXY__45__4__45__HYDROXY__45__5__45__HEXAPRENYLBENZOATE;NAME=3-methoxy-4-hydroxy-5-hexaprenylbenzoate
ID=URIDINE;NAME=uridine
ID=GLUTARATE;NAME=glutarate
ID=_25__45__DIAMINO__45__6__45__RIBITYLAMINO__45__43H__45__PYRIMID;NAME=2,5-diamino-6-ribitylamino-4(3H)-pyrimidinone 5&amp;#039;-phosphate
ID=__124__Saturated__45__Fatty__45__Acyl__45__CoA__124__;NAME=a 2,3,4-saturated fatty acyl CoA
ID=INDOLE__45__3__45__GLYCEROL__45__P;NAME=indole-3-glycerol-phosphate
ID=PYRUVATE;NAME=pyruvate
ID=MAL;NAME=malate
ID=DEHYDROSPHINGANINE;NAME=3-ketodihydrosphingosine
ID=L__45__BETA__45__ASPARTYL__45__P;NAME=L-aspartyl-4-P
ID=DEOXYURIDINE;NAME=Deoxyuridine
ID=L__45__ARGININO__45__SUCCINATE;NAME=L-arginino-succinate
ID=PYRIDOXINE__45__5P;NAME=pyridoxine-5&amp;#039;-phosphate
ID=LEU;NAME=L-leucine
ID=CPD__45__3842;NAME=5-diphospho-1D-myo-inositol pentakisphosphate
ID=CPD__45__1063;NAME=5-methylthioribulose-1-phosphate
ID=_2__45__DEOXYRIBOSE;NAME=deoxyribose
ID=ACETYL__45__ACP;NAME=an acetyl-[acp]
ID=L__45__PHOSPHATIDATE;NAME=a 1,2-diacylglycerol-3-phosphate
ID=MEVALONATE;NAME=mevalonate
ID=CARBAMYUL__45__L__45__ASPARTATE;NAME=carbamoyl-L-aspartate
ID=SPERMINE;NAME=spermine
ID=_2__45__KETO__45__ISOVALERATE;NAME=2-keto-isovalerate
ID=FARNESYL__45__PP;NAME=trans, trans-farnesyl diphosphate
ID=_2__45__HEXAPRENYL__45__3__45__METHYL__45__5__45__HYDROXY__45__6__45__METHOX;NAME=2-hexaprenyl-3-methyl-5-hydroxy-6-methoxy-1,4-benzoquinol
ID=ACYL__45__ACP;NAME=an acyl-[acp]
ID=SHIKIMATE;NAME=shikimate
ID=SIROHEME;NAME=siroheme
ID=L__45__HISTIDINOL__45__P;NAME=L-histidinol-phosphate
ID=_3__45__DEOXY__45__D__45__ARABINO__45__HEPTULOSONATE__45__7__45__P;NAME=3-deoxy-D-arabino-heptulosonate-7-phosphate
ID=__124__4__45__hydroxybenzoate__124__;NAME=p-hydroxybenzoate
ID=L__45__ASPARTATE__45__SEMIALDEHYDE;NAME=L-aspartate-semialdehyde
ID=TMP;NAME=dTMP
ID=G3P;NAME=3-phosphoglycerate
ID=CPD__45__1079;NAME=L-2-Aminoadipate 6-semialdehyde
ID=DIHYDRO__45__NEO__45__PTERIN;NAME=dihydroneopterin
ID=PANTETHEINE__45__P;NAME=pantetheine 4&amp;#039;-phosphate
ID=CO__45__A;NAME=Coenzyme A
ID=ALLANTOIN;NAME=allantoin
ID=CYTOSINE;NAME=cytosine
ID=L__45__1__45__PHOSPHATIDYL__45__GLYCEROL;NAME=an L-1-phosphatidyl-glycerol
ID=L__45__KYNURENINE;NAME=kynurenine
ID=_2__45__DEHYDROPANTOATE;NAME=2-dehydropantoate
ID=_5__45__PHOSPHO__45__RIBOSYL__45__GLYCINEAMIDE;NAME=5-phospho-ribosyl-glycineamide
ID=PHOSPHORIBULOSYL__45__FORMIMINO__45__AICAR__45__P;NAME=phosphoribulosylformimino-AICAR-P
ID=CDP__45__ETHANOLAMINE;NAME=CDP-ethanolamine
ID=CPD__45__8999;NAME=5-(methylthio)-2,3-dioxopentyl phosphate
ID=CDP;NAME=CDP
ID=CPD__45__578;NAME=Urea-1-carboxylate
ID=DIACETYL;NAME=diacetyl
ID=ACP;NAME=a holo-[acp]
ID=CARBOXYHEXENEDIOATE;NAME=Homoaconitate
ID=__124__Red__45__Thioredoxin__124__;NAME=a reduced thioredoxin
ID=PYRIDOXAMINE__45__5P;NAME=pyridoxamine 5&amp;#039;-phosphate
ID=ALPHA__45__HYDROXYPHYTOCERAMIDE;NAME=alpha-hydroxyphytoceramide
ID=GTP;NAME=GTP
ID=B__45__ALANINE;NAME=&amp;beta;-alanine
ID=SULFATE;NAME=SO&lt;SUB&gt;4&lt;/SUB&gt;&lt;SUP&gt;2-&lt;/SUP&gt;
ID=FUM;NAME=fumarate
ID=__124__Saturated__45__Fatty__45__Acyl__45__ACPs__124__;NAME=a 2,3,4-saturated fatty acyl-[acp]
ID=CYTIDINE;NAME=cytidine
ID=_4__45__HYDROXY__45__PHENYLLACTATE;NAME=4-hydroxy-phenyllactate
ID=FECOSTEROL;NAME=fecosterol
ID=CPD__45__649;NAME=dihydrosphingosine 1-phosphate
ID=THR;NAME=L-threonine
ID=THF;NAME=THF
ID=METHYLENE__45__THF__45__GLU__45__N;NAME=a 5,10-methylene-tetrahydrofolate
ID=HISTIDINOL;NAME=histidinol
ID=CPD__45__506;NAME=D-&lt;i&gt;myo&lt;/i&gt;-inositol (1,3,4,5)-tetra&lt;i&gt;kis&lt;/i&gt;phosphate
ID=INOSITOL__45__1__45__4__45__5__45__TRISPHOSPHATE;NAME=inositol 1,4,5-trisphosphate
ID=__124__Ubiquinols__124__;NAME=a ubiquinol
ID=_2__45__KETO__45__3__45__METHYL__45__VALERATE;NAME=2-keto-3-methyl-valerate
ID=ETHANOL__45__AMINE;NAME=ethanol-amine
ID=ALLANTOATE;NAME=allantoate
ID=L__45__CITRULLINE;NAME=citrulline
ID=DIHYDROSIROHYDROCHLORIN;NAME=precorrin-2
ID=BUTRYL__45__ACP;NAME=butryl-ACP
ID=_2__45__ACETO__45__2__45__HYDROXY__45__BUTYRATE;NAME=2-aceto-2-hydroxy-butyrate
ID=PHYTOSPINGOSINE;NAME=phytosphingosine
ID=ERGOSTEROL;NAME=ergosterol
ID=INOSINE;NAME=inosine
ID=RIBOSE;NAME=D-ribose
ID=__124__Reduced__45__ferredoxins__124__;NAME=a reduced ferredoxin
ID=CPD3O__45__410;NAME=Glc(3)Man(9)GlcNAc(2)-PP-Dol
ID=CHORISMATE;NAME=chorismate
ID=GLYOX;NAME=glyoxylate
ID=DIMETHYL__45__D__45__RIBITYL__45__LUMAZINE;NAME=6,7-dimethyl-8-(1-D-ribityl)lumazine
ID=L__45__ALPHA__45__ALANINE;NAME=L-alanine
ID=ACETOACETYL__45__S__45__ACP;NAME=acetoacetyl-ACP
ID=ADP;NAME=ADP
ID=CPD__45__567;NAME=N6-Acetyl-L-lysine
ID=UMP;NAME=UMP
ID=DIHYDROPTERIN__45__CH2OH__45__PP;NAME=2-amino-4-hydroxy-6-hydroxymethyl-7,8-dihydropteridine diphosphate
ID=IMIDAZOLE__45__ACETOL__45__P;NAME=imidazole acetol-phosphate
ID=DCMP;NAME=dCMP
ID=CPD__45__667;NAME=O-Acetyl-L-homoserine
ID=HYPOXANTHINE;NAME=hypoxanthine
ID=CPD3O__45__409;NAME=Glc(2)Man(9)GlcNac(2)-PP-Dol
ID=CPD3O__45__408;NAME=Glc(1)Man(9)GlcNAc(2)-PP-Dol
ID=PHOSPHORIBOSYL__45__ATP;NAME=phosphoribosyl-ATP
ID=PAPS;NAME=PAPS
ID=CDP__45__CHOLINE;NAME=CDP-choline
ID=SER;NAME=L-serine
ID=AMINO__45__RIBOSYLAMINO__45__1H__45__3H__45__PYR__45__DIONE;NAME=5-amino-6-ribitylamino-2,4(1H,3H)-pyrimidinedione 5&amp;#039;-phosphate
ID=__124__Fatty__45__Acids__124__;NAME=a fatty acid
ID=UDP__45__N__45__ACETYL__45__D__45__GLUCOSAMINE;NAME=UDP-GlcNAc
ID=SHIKIMATE__45__5P;NAME=shikimate-3-phosphate
ID=RIBULOSE__45__5P;NAME=ribulose-5-phosphate
ID=S__45__ADENOSYLMETHIONINAMINE;NAME=S-adenosylmethioninamine
ID=PHOSPHATIDYL__45__MYO__45__INOSITOL__45__45__45__BISPHOSPHA;NAME=a 1-phosphatidyl-1D-&lt;i&gt;myo&lt;/i&gt;-inositol 4,5-bisphosphate
ID=_2__45__HEXAPRENYL__45__6__45__METHOXY__45__14__45__BENZOQUINOL;NAME=2-hexaprenyl-6-methoxy-1,4-benzoquinol
ID=D__45__6__45__P__45__GLUCONO__45__DELTA__45__LACTONE;NAME=D-6-phospho-glucono-&amp;delta;-lactone
ID=GUANINE;NAME=guanine
ID=CPD__45__1091;NAME=(S)-Ureidoglycolate
ID=NIACINE;NAME=niacine
ID=ALPHA__45__D__45__MANNOSYLCHITOBIOSYLDIPHOSPHODOLI;NAME=Man(1)GlcNAc(2)-PP-Dol
ID=_3__45__HYDROXY__45__3__45__METHYL__45__GLUTARYL__45__COA;NAME=3-hydroxy-3-methyl-glutaryl-CoA
ID=_5__45__METHYL__45__THF;NAME=5-methyl-THF
ID=CIT;NAME=citrate
ID=_3__45__DEHYDRO__45__SHIKIMATE;NAME=3-dehydro-shikimate
ID=METHYLENE__45__THF;NAME=5,10-methylene-THF
ID=NICOTINATE_NUCLEOTIDE;NAME=nicotinic acid mononucleotide
ID=DEOXY__45__RIBOSE__45__1P;NAME=deoxyribose-1-phosphate
ID=WATER;NAME=H&lt;SUB&gt;2&lt;/SUB&gt;O
ID=TTP;NAME=dTTP
ID=DIHYDROFOLATE__45__GLU__45__N;NAME=a 7,8-dihydrofolate
ID=CPD3O__45__258;NAME=Man(9)GlcNAc(2)-PP-Dol
ID=GDP__45__MANNOSE;NAME=GDP-mannose
ID=_1__45__AMINO__45__PROPAN__45__2__45__OL;NAME=1-amino-propan-2-ol
ID=MIPC;NAME=MIPC
ID=CPD__45__85;NAME=1,2-dihydroxy-3-keto-5-methylthiopentene
ID=_10__45__FORMYL__45__THF;NAME=10-formyl-THF
ID=UROPORPHYRINOGEN__45__III;NAME=uroporphyrinogen-III
ID=SIROHYDROCHLORIN;NAME=sirohydrochlorin
ID=XYLULOSE__45__5__45__PHOSPHATE;NAME=xylulose-5-phosphate
ID=ALPHA__45__D__45__GALACTOSE;NAME=&amp;alpha;-D-galactose
ID=NADH__45__P__45__OR__45__NOP;NAME=NAD(P)H
ID=GALACTOSE__45__1P;NAME=&amp;alpha;-D-galactose-1-phosphate
ID=CPD3O__45__250;NAME=4- or 6-diphosphoinositol pentakisphosphate
ID=PROTON;NAME=H&lt;SUP&gt;+&lt;/SUP&gt;
ID=DIHYDROXY__45__BUTANONE__45__P;NAME=3,4-dihydroxy-2-butanone-4-P
ID=TRIACYLGLYCEROL;NAME=triacylglycerol
ID=MIP2C;NAME=MIP2C
ID=_2__45__AMINO__45__3__45__CARBOXYMUCONATE_SEMIALDEHYDE;NAME=2-amino-3-carboxymuconate semialdehyde
ID=L__45__1__45__PHOSPHATIDYL__45__SERINE;NAME=an L-1-phosphatidylserine
ID=GLYCEROL;NAME=glycerol
ID=_3__45__ENOLPYRUVYL__45__SHIKIMATE__45__5P;NAME=5-enolpyruvyl-shikimate-3-phosphate
ID=RIBOSE__45__1P;NAME=ribose-1-phosphate
ID=ACETALD;NAME=acetaldehyde
ID=FE__43__2;NAME=Fe&lt;SUP&gt;2+&lt;/SUP&gt;
ID=_5__45__AMINO__45__VALERATE;NAME=5-aminovalerate
ID=CPD3O__45__768;NAME=diphosphoinositol pentakisphosphate
ID=MALONYL__45__COA;NAME=malonyl-CoA
ID=ACETYL__45__GLU;NAME=N-acetyl-L-glutamate
ID=__124__Ox__45__Thioredoxin__124__;NAME=an oxidized thioredoxin
ID=__124__Amino__45__Acids__45__20__124__;NAME=a standard &amp;alpha; amino acid
ID=GMP;NAME=GMP
ID=FMN;NAME=FMN
ID=__124__R__45__3__45__Hydroxypalmitoyl__45__ACPs__124__;NAME=an (&lt;i&gt;R&lt;/i&gt;)-3-hydroxypalmitoyl-[acp]
ID=DIACYLGLYCEROL;NAME=a 1,2-diacylglycerol
ID=PROPIONYL__45__P;NAME=propionyl-P
ID=N__45__5__45__PHOSPHORIBOSYL__45__ANTHRANILATE;NAME=N-(5&amp;#039;-phosphoribosyl)-anthranilate
ID=PPI;NAME=pyrophosphate
ID=TRP;NAME=L-tryptophan
ID=_1__45__KETO__45__2__45__METHYLVALERATE;NAME=2,3-dihydroxy-3-methylvalerate
ID=THYMINE;NAME=thymine
ID=ACETOIN;NAME=acetoin
ID=D__45__SEDOHEPTULOSE__45__7__45__P;NAME=sedoheptulose-7-phosphate
ID=_2__45__KETO__45__6__45__ACETAMIDOCAPROATE;NAME=2-keto-6-acetamidocaproate
ID=RIBOFLAVIN;NAME=riboflavin
ID=_3__45__HYDROXY__45__L__45__KYNURENINE;NAME=3-hydroxy-L-kynurenine
ID=PHOSPHORYL__45__CHOLINE;NAME=phosphoryl-choline
ID=PHENYL__45__PYRUVATE;NAME=phenylpyruvate
ID=lysophospholipid;NAME=NA
ID=_4__45__AMINO__45__4__45__DEOXYCHORISMATE;NAME=4-amino-4-deoxychorismate
ID=ANTHRANILATE;NAME=anthranilate
ID=PRA;NAME=5-phosphoribosylamine
ID=L__45__1__45__PHOSPHATIDYL__45__ETHANOLAMINE;NAME=an L-1-phosphatidyl-ethanolamine
ID=L__45__PANTOATE;NAME=pantoate
ID=_2__45__HEXAPRENYL__45__6__45__METHOXYPHENOL;NAME=2-hexaprenyl-6-methoxyphenol
ID=GLY;NAME=L-glycine
ID=CMP;NAME=CMP
ID=GLN;NAME=L-glutamine
ID=CPD__45__7670;NAME=dimethylsulfide
ID=PALMITALDEHYDE;NAME=palmitaldehyde
ID=GLT;NAME=L-glutamate
ID=PRPP;NAME=PRPP
ID=_3__45__HEXAPRENYL__45__4__45__HYDROXYBENZOATE;NAME=3-hexaprenyl-4-hydroxybenzoate
ID=DELTA3__45__ISOPENTENYL__45__PP;NAME=&amp;Delta;&lt;SUP&gt;3&lt;/SUP&gt;-isopentenyl-PP
ID=GERANYL__45__PP;NAME=geranyl-PP
ID=AMP;NAME=AMP
ID=ETOH;NAME=ethanol
ID=GERANYLFARNESYL__45__DIPHOSPHATE;NAME=geranylfarnesyl-diphosphate
ID=UTP;NAME=UTP
ID=_3__45__P__45__SERINE;NAME=3-phospho-serine
ID=THYMIDINE;NAME=thymidine
ID=GLCNAC2PPDOL;NAME=GlcNAc(2)-PP-Dol
ID=GERANYLGERANYL__45__PP;NAME=geranylgeranyl-PP
ID=FORMATE;NAME=formate
ID=DIHYDRONEOPTERIN__45__P3;NAME=dihydroneopterin triphosphate
ID=PRO;NAME=L-proline
ID=__124__Pi__124__;NAME=phosphate
ID=protein__45__lysine;NAME=NA
ID=DEOXYCYTIDINE;NAME=deoxycytidine
ID=DUTP;NAME=dUTP
ID=FADH2;NAME=FADH&lt;SUB&gt;2&lt;/SUB&gt;
ID=P__45__COUMAROYL__45__COA;NAME=p-coumaroyl-CoA
ID=_5__45__PHOSPHORIBOSYL__45__N__45__FORMYLGLYCINEAMIDINE;NAME=5-phosphoribosyl-n-formylglycineamidine
ID=DPG;NAME=3-phospho-D-glyceroyl-phosphate
ID=_2__45__KETOGLUTARATE;NAME=2-oxoglutarate
ID=DIHYDROXY__45__ACETONE__45__PHOSPHATE;NAME=dihydroxy-acetone-phosphate
ID=N__45__ACETYL__45__GLUTAMYL__45__P;NAME=N-acetylglutamyl-phosphate
ID=AMINO__45__OH__45__HYDROXYMETHYL__45__DIHYDROPTERIDINE;NAME=2-amino-4-hydroxy-6-hydroxymethyl-7,8-dihydropteridine
ID=BCCP__45__BIOTIN__45__CO2;NAME=BCCP-biotin-CO&lt;SUB&gt;2&lt;/SUB&gt;
ID=CARDIOLIPIN;NAME=a cardiolipin
ID=TRANS__45__D2__45__ENOYL__45__COA;NAME=a &lt;i&gt;trans&lt;/i&gt;-2,3-dehydroacyl-CoA
ID=UDP__45__GLACTOSE;NAME=UDP-galactose
ID=CTP;NAME=CTP
ID=PHOSPHATIDYLCHOLINE;NAME=a phosphatidylcholine
ID=__124__Oxidized__45__ferredoxins__124__;NAME=an oxidized ferredoxin
ID=PHOSPHORIBOSYL__45__AMP;NAME=phosphoribosyl-AMP
ID=DGTP;NAME=deoxy-GTP
ID=UBIQUINONE__45__6;NAME=ubiquinone(30)
ID=CPD__45__1107;NAME=D-&lt;i&gt;myo&lt;/i&gt;-inositol (1,3,4,5,6)-penta&lt;i&gt;kis&lt;/i&gt;phosphate
ID=_6__45__P__45__GLUCONATE;NAME=6-phospho-gluconate
ID=ADENINE;NAME=adenine
ID=PHOSPHORIBOSYL__45__CARBOXY__45__AMINOIMIDAZOLE;NAME=phosphoribosyl-carboxy-aminoimidazole
ID=DOLICHOLP;NAME=a dolichyl phosphate
ID=RIBOSE__45__5P;NAME=ribose-5-phosphate
ID=HEXAPRENYL__45__DIPHOSPHATE;NAME=hexaprenyl-diphosphate
ID=ZYMOSTEROL;NAME=zymosterol
ID=CPD3O__45__340;NAME=Man(6)GlcNAc(2)-PP-Dol
ID=LANOSTEROL;NAME=lanosterol
ID=P__45__AMINO__45__BENZOATE;NAME=p-aminobenzoate
ID=__124__Donor__45__H2__124__;NAME=a reduced electron acceptor
ID=CPD__45__4575;NAME=4&amp;alpha;-hydroxymethyl-4&amp;beta;-methyl-5&amp;alpha;-cholesta-8,24-dien-3&amp;beta;-ol
ID=HOMO__45__CIT;NAME=homocitrate
ID=SO3;NAME=SO&lt;SUB&gt;3&lt;/SUB&gt;
ID=TRANS__45__D2__45__ENOYL__45__ACP;NAME=a &lt;i&gt;trans&lt;/i&gt;-&amp;Delta;&lt;sup&gt;2&lt;/sup&gt;-enoyl-acyl-[acp]
ID=_5__45__10__45__METHENYL__45__THF;NAME=5,10-methenyl-THF
ID=THF__45__GLU__45__N;NAME=a tetrahydrofolate-glutamate
ID=__124__Keto__45__Acid__124__;NAME=a keto acid
ID=CPD__45__4577;NAME=4&amp;alpha;-carboxy-4&amp;beta;-methyl-5&amp;alpha;-cholesta-8,24-dien-3&amp;beta;-ol
ID=CPD__45__4576;NAME=4&amp;alpha;-formyl-4&amp;beta;-methyl-5&amp;alpha;-cholesta-8,24-dien-3&amp;beta;-ol
ID=CPD__45__4579;NAME=4&amp;alpha;-hydroxymethyl-5&amp;alpha;-cholesta-8,24-dien-3&amp;beta;-ol
ID=UBIQUINOL__45__30;NAME=ubiquinol(30)
ID=CPD__45__4578;NAME=3-keto-4-methylzymosterol
ID=PHTYOSPHINGOSINE__45__1__45__P;NAME=phytosphingosine-1-P
ID=_572428__45__ERGOSTATRIENOL;NAME=5,7,24(28)-ergostatrienol
ID=UREA;NAME=urea
ID=ACETYL__45__ADP__45__RIBOSE;NAME=acetyl-ADP-ribose
ID=NAD__45__P__45__OR__45__NOP;NAME=NAD(P)&lt;sup&gt;+&lt;/sup&gt;
ID=OXYGEN__45__MOLECULE;NAME=O&lt;SUB&gt;2&lt;/SUB&gt;
ID=DATP;NAME=deoxy-ATP
ID=CPD3O__45__2;NAME=Man(7)GlcNAc(2)-PP-Dol
ID=DCTP;NAME=dCTP
ID=CPD__45__4581;NAME=5&amp;alpha;-cholesta-8,24-dien-3-one
ID=CPD__45__4580;NAME=4&amp;alpha;-formyl-5&amp;alpha;-cholesta-8,24-dien-3&amp;beta;-ol
ID=CPD__45__1108;NAME=a 1-phosphatidyl-1D-&lt;i&gt;myo&lt;/i&gt;-inositol 4-phosphate
ID=_2__45__HEXAPRENYL__45__3__45__METHYL__45__6__45__METHOXY__45__14__45__BENZ;NAME=2-hexaprenyl-3-methyl-6-methoxy-1,4-benzoquinol
ID=INOSITOL__45__1456__45__TETRAKISPHOSPHATE;NAME=D-&lt;i&gt;myo&lt;/i&gt;-inositol (1,4,5,6)-tetra&lt;i&gt;kis&lt;/i&gt;phosphate
ID=_2K__45__4CH3__45__PENTANOATE;NAME=&amp;alpha;-ketoisocaproate