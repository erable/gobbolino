ID=CPD3O__45__292;NAME=Man(5)GlcNAc(2)-PP-Dol;DbId=
ID=_1__45__PHOSPHATIDYL__45__1D__45__MYO__45__INOSITOL__45__35__45__BISPH;NAME=1-phosphatidyl-1D-myo-inositol 3,5-bisphosphate;DbId=
ID=INOSITOL__45__PHOSPHATE;NAME=inositol phosphate;DbId=
ID=BUTRYL__45__ACP;NAME=butryl-ACP;DbId=
ID=UDP__45__GLACTOSE;NAME=UDP-galactose;DbId=
ID=lysophospholipid;NAME=NA;DbId=
ID=protein__45__acetyllysine;NAME=NA;DbId=
ID=GLCNAC2PPDOL;NAME=GlcNAc(2)-PP-Dol;DbId=
ID=DIHYDRO__45__SPHINGOSINE;NAME=dihydrosphingosine;DbId=
ID=CPD3O__45__28;NAME=Man(3)GlcNAc(2)-PP-Dol;DbId=
ID=_3__45__AMINOPROPANAL;NAME=3-aminopropanal;DbId=
ID=PHOSPHATIDYLCHOLINE__45__CMPD;NAME=phosphatidylcholine;DbId=
ID=NAD__45__P__45__OR__45__NOP;NAME=NAD(P)&lt;sup&gt;+&lt;/sup&gt;;DbId=
ID=UDP__45__GLUCOSE;NAME=UDP-D-glucose;DbId=
ID=N__45__ACETYLGLUTAMATE_SEMIALDEHYDE;NAME=N-acetylglutamate semialdehyde;DbId=
ID=CPD3O__45__2;NAME=Man(7)GlcNAc(2)-PP-Dol;DbId=
ID=CPD3O__45__27;NAME=Man(2)GlcNAc(2)-PP-Dol;DbId=
ID=_2__45__AMINO__45__3__45__CARBOXYMUCONATE_SEMIALDEHYDE;NAME=2-amino-3-carboxymuconate semialdehyde;DbId=
ID=CPD3O__45__408;NAME=Glc(1)Man(9)GlcNAc(2)-PP-Dol;DbId=
ID=RIBOSE;NAME=D-ribose;DbId=
ID=_6__45__P__45__GLUCONATE;NAME=6-phospho-gluconate;DbId=
ID=RIBITYLAMINO__45__AMINO__45__DIHYDROXY__45__PYR;NAME=4-(1-D-ribitylamino)-5-amino-2,6-dihydroxypyrimidine;DbId=
ID=CPD3O__45__410;NAME=Glc(3)Man(9)GlcNAc(2)-PP-Dol;DbId=
ID=CPD__45__250;NAME=L-Formylkynurenine;DbId=
ID=TRIACYLGLYCEROL;NAME=triacylglycerol;DbId=
ID=_25__45__DIAMINO__45__6__45__RIBOSYLAMINO__45__PYR;NAME=2,5-diamino-6-(ribosylamino)-4-(3H)-pyrimidinone 5&amp;#039;-phosphate;DbId=
ID=BCCP__45__BIOTIN__45__CO2;NAME=BCCP-biotin-CO&lt;SUB&gt;2&lt;/SUB&gt;;DbId=
ID=CPD3O__45__144;NAME=[PP]&lt;sub&gt;2&lt;/sub&gt;-IP&lt;sub&gt;4&lt;/sub&gt;;DbId=
ID=protein__45__lysine;NAME=NA;DbId=
ID=CPD3O__45__768;NAME=diphosphoinositol pentakisphosphate;DbId=
ID=DEOXY__45__RIBOSE__45__1P;NAME=deoxyribose-1-phosphate;DbId=
ID=_1__45__ACYL__45__DIHYDROXYACETONE__45__PHOSPHATE;NAME=1-acyl-dihydroxyacetone-phosphate;DbId=
ID=phospholipid;NAME=NA;DbId=
ID=PHYTOCERAMIDE;NAME=phytoceramide;DbId=
ID=CPD3O__45__340;NAME=Man(6)GlcNAc(2)-PP-Dol;DbId=
ID=_4__45__HYDROXY__45__PHENYLLACTATE;NAME=4-hydroxy-phenyllactate;DbId=
ID=NADH__45__P__45__OR__45__NOP;NAME=NAD(P)H;DbId=
ID=ALPHA__45__D__45__MANNOSYLCHITOBIOSYLDIPHOSPHODOLI;NAME=Man(1)GlcNAc(2)-PP-Dol;DbId=
ID=CPD3O__45__258;NAME=Man(9)GlcNAc(2)-PP-Dol;DbId=
ID=DIOH__45__ISOVALERATE;NAME=2,3-dihydroxy-isovalerate;DbId=
ID=CPD3O__45__10;NAME=Man(8)GlcNAc(2)-PP-Dol;DbId=
ID=_4__45__PHOSPHOPANTOTHENOYLCYSTEINE;NAME=4&amp;#039;-phospho-N-pantothenoylcysteine;DbId=
ID=_25__45__DIAMINO__45__6__45__RIBITYLAMINO__45__43H__45__PYRIMID;NAME=2,5-diamino-6-ribitylamino-4(3H)-pyrimidinone 5&amp;#039;-phosphate;DbId=
ID=_5__45__AMINO__45__VALERATE;NAME=5-aminovalerate;DbId=
ID=CARBOXYHEXENEDIOATE;NAME=Homoaconitate;DbId=
ID=CPD__45__3842;NAME=5-diphospho-1D-myo-inositol pentakisphosphate;DbId=
ID=CPD3O__45__250;NAME=4- or 6-diphosphoinositol pentakisphosphate;DbId=
ID=PALMITOYL__45__ACP;NAME=palmitoyl-ACP;DbId=
ID=CPD__45__1079;NAME=L-2-Aminoadipate 6-semialdehyde;DbId=
ID=CPD3O__45__409;NAME=Glc(2)Man(9)GlcNac(2)-PP-Dol;DbId=
ID=CPD3O__45__29;NAME=Man(4)GlcNAc(2)-PP-Dol;DbId=
ID=BCCP__45__BIOTIN;NAME=BCCP-biotin;DbId=
