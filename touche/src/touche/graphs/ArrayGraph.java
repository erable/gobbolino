/*
 * Copyright (c) 2012, LASAGNE and/or its affiliates. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * - Neither the name of LASAGNE or the names of its contributors may be used to
 * endorse or promote products derived from this software without specific prior
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package touche.graphs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.StringTokenizer;

/*
 * This class implements the graph data structure by means of adjacency and
 * incidency lists. Each list is implemented as an array: for this reason, the
 * graph file format specifies for each node its in-degree and its out-degree.
 * The graph can be weighted: for this reason, each edge includes both its head
 * and its weight.
 */
public class ArrayGraph {
	public class Edge {
		public int head;
		public int weight;

		Edge(int h, int w) {
			head = h;
			weight = w;
		}
	}

	public class Element {
		public int id;
		public int weight;
	}

	/*
	 * This class is used by the Dijkstra algorithm implementation (see
	 * Crescenzi, Gambosi, Grossi, 'Strutture di Dati e Algoritmi')
	 */
	public class Heap {
		int heapSize;
		Element[] heapArray;
		int[] heapArrayPosition;

		public Heap(int n) {
			heapArray = new Element[n];
			heapSize = 0;
			heapArrayPosition = new int[n];
		}

		public void decreaseKey(int id, int weight) {
			int i = heapArrayPosition[id];
			heapArray[i].weight = weight;
			heapify(i);
		}

		public boolean isEmpty() {
			return heapSize == 0;
		}

		public void enqueue(Element e) {
			heapArray[heapSize] = e;
			heapArrayPosition[e.id] = heapSize;
			heapSize = heapSize + 1;
			heapify(heapSize - 1);
		}

		public Element dequeue() {
			Element minimum = heapArray[0];
			heapArray[0] = heapArray[heapSize - 1];
			heapSize = heapSize - 1;
			heapify(0);
			return minimum;
		}

		private void heapify(int i) {
			while (i > 0 && heapArray[i].weight < heapArray[father(i)].weight) {
				swap(i, father(i));
				i = father(i);
			}
			while (left(i) < heapSize && i != minimumFatherSons(i)) {
				int son = minimumFatherSons(i);
				swap(i, son);
				i = son;
			}
		}

		private int minimumFatherSons(int i) {
			int j = left(i);
			int k = j;
			if (k + 1 < heapSize) {
				k = k + 1;
			}
			if (heapArray[k].weight < heapArray[j].weight) {
				j = k;
			}
			if (heapArray[i].weight < heapArray[j].weight) {
				j = i;
			}
			return j;
		}

		private int left(int i) {
			return 2 * i + 1;
		}

		private int father(int i) {
			return (i - 1) / 2;
		}

		private void swap(int i, int j) {
			Element tmp = heapArray[i];
			heapArray[i] = heapArray[j];
			heapArray[j] = tmp;
			heapArrayPosition[heapArray[i].id] = i;
			heapArrayPosition[heapArray[j].id] = j;
		}
	}

	public Edge[][] adjacencyLists;
	public Edge[][] incidencyLists;
	private int n;
	private int m;
	private boolean isOriented;
	private boolean isWeighted;
	// These fields are used by some of the class methods for computing
	// topological properties of the graph
	
	public ArrayGraph() {
		n = 0;
		m = 0;
	}


	public int getM() {
		return m;
	}

	public int getN() {
		return n;
	}

	public boolean isOriented() {
		return isOriented;
	}

	public boolean isWeighted() {
		return isWeighted;
	}

	public boolean readFile(String fileName) {
		File inFile = new File(fileName);
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			StringTokenizer lineTokens = new StringTokenizer(line, " ");
			n = Integer.parseInt(lineTokens.nextToken());
			isOriented = false;
			isWeighted = false;
			if (lineTokens.hasMoreTokens()) {
				if (lineTokens.nextToken().equals("1")) {
					isOriented = true;
				}
				if (lineTokens.hasMoreTokens()) {
					if (lineTokens.nextToken().equals("1")) {
						isWeighted = true;
					}
				}
			}
			adjacencyLists = new Edge[n][];
			incidencyLists = new Edge[n][];
			m = 0;
			for (int i = 0; i < n; i++) {
				line = br.readLine();
				lineTokens = new StringTokenizer(line, " ");
				int u = Integer.parseInt(lineTokens.nextToken());
				int od = Integer.parseInt(lineTokens.nextToken());
				adjacencyLists[u] = new Edge[od + 1];
				adjacencyLists[u][0] = new Edge(u, 0);
				if (isOriented) {
					int id = Integer.parseInt(lineTokens.nextToken());
					incidencyLists[u] = new Edge[id + 1];
					incidencyLists[u][0] = new Edge(u, 0);
				}
				m = m + od;
			}
			if (!isOriented) {
				m = m / 2;
			}
			line = br.readLine();
			while (line != null && line.length() > 0) {
				lineTokens = new StringTokenizer(line, " ");
				int s = Integer.parseInt(lineTokens.nextToken());
				int t = Integer.parseInt(lineTokens.nextToken());
				adjacencyLists[s][0].weight++;
				int w = 1;
				if (isWeighted) {
					w = Integer.parseInt(lineTokens.nextToken());
				}
				adjacencyLists[s][adjacencyLists[s][0].weight] = new Edge(t, w);
				if (!isOriented) {
					adjacencyLists[t][0].weight++;
					adjacencyLists[t][adjacencyLists[t][0].weight] = new Edge(
							s, w);
				} else {
					incidencyLists[t][0].weight++;
					incidencyLists[t][incidencyLists[t][0].weight] = new Edge(
							s, w);
				}
				line = br.readLine();
			}
			br.close();
			return true;
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			return false;
		}
	}
}