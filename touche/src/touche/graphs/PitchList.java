package touche.graphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class PitchList {
	public ArrayList<Integer>[] adjacencyList;
	public ArrayList<Integer>[] incidencyList;
	public int nn = 0;
	public int ne = 0;
	public int correctNumbers[];

	@SuppressWarnings("unchecked")
	public PitchList(int n) {
		adjacencyList = new ArrayList[n];
		incidencyList = new ArrayList[n];
		nn = n;
		
		correctNumbers = new int[nn];
		
		for (int i = 0; i < n; i++)
		{
			correctNumbers[i] = i;
			adjacencyList[i] = new ArrayList<Integer>();
			incidencyList[i] = new ArrayList<Integer>();
		}
	}
	
	public PitchList copy() {
		PitchList p = new PitchList(nn);
		
		for (int i = 0; i < nn; i++)
		{
			for (int j = 0; j < adjacencyList[i].size(); j++)
				p.adjacencyList[i].add(this.adjacencyList[i].get(j));
			
			for (int j = 0; j < incidencyList[i].size(); j++)
				p.incidencyList[i].add(this.incidencyList[i].get(j));
		}
		
		p.correctNumbers = this.correctNumbers;
		
		return p;
	}
	
	@SuppressWarnings("unchecked")
	public PitchList(PitchList c) {
		adjacencyList = new ArrayList[c.nn];
		incidencyList = new ArrayList[c.nn];
		nn=c.nn;
		
		for (int i = 0; i < nn; i++)
		{
			adjacencyList[i] = new ArrayList<Integer>();
			for(int j = 0; j < c.adjacencyList[i].size(); j++) {
				adjacencyList[i].add(c.adjacencyList[i].get(j));
			}
			incidencyList[i] = new ArrayList<Integer>();
			for(int j = 0; j < c.incidencyList[i].size(); j++) {
				incidencyList[i].add(c.incidencyList[i].get(j));
			}
		}
	}
	
	public String toString() {
		
		String toReturn = "";
		boolean doneSomething = false;
		int k = 0;
		for (int i = 0; i < nn; i++)
			for (int j = 0; j < adjacencyList[i].size(); j++) {
				toReturn += "(" + correctNumbers[i] + ", " + correctNumbers[adjacencyList[i].get(j)] + "); ";
				doneSomething = true;
				k++;
				if (k % 10000 == 0)
					toReturn += "\n";
			}
		if (doneSomething)
			if (k % 10000 == 0)
				toReturn = toReturn.substring(0, toReturn.length() - 3) + ".";
			else
				toReturn = toReturn.substring(0, toReturn.length() - 2) + ".";
		return toReturn;
	}
	
	public void addEdge(int t, int h) {
		adjacencyList[t].add(h); 
		incidencyList[h].add(t); 
		ne++;
	}
	
	public void removeEdge(int t, int h) {

		adjacencyList[t].remove((Object)h); 
		incidencyList[h].remove((Object)t); 
		ne--;
	}

	public void merge(PitchList p) {
		for (int i = 0; i < p.nn; i++) {
			for (int j = 0; j < p.adjacencyList[i].size(); j++) {
				addEdge(p.correctNumbers[i], p.correctNumbers[p.adjacencyList[i].get(j)]);
			}
		}
	}
	
	public void addPath(LinkedList<Integer> r) {
	
		ListIterator<Integer> iter = r.listIterator();
		int prev = iter.next(), last;
		
		while (iter.hasNext()) {
			last = iter.next();
			if (!this.adjacencyList[prev].contains(last))
				addEdge(prev, last);
			prev = last;
		}
	}

	public void removePath(LinkedList<Integer> r) {
		for (int i = 0; i < r.size()-1; i++) {
			removeEdge(r.get(i), r.get(i+1));
		}
	}
	public boolean isInPitch(int i) {
		return !(adjacencyList[i].isEmpty() && incidencyList[i].isEmpty());
	}
	public boolean isInPitch(int t, int h) {
		return adjacencyList[t].contains(h);
	}
}
