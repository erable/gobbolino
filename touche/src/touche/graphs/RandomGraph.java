package touche.graphs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Collections;
import java.util.Vector;

import touche.application.Main;
import touche.preprocessing.SCCComputer;




public class RandomGraph {
	private int numberOfNodes;
	private int numberOfSources;
	private int numberOfTargets;
	private boolean[][] adjacencyMatrix;
	private Vector<Integer> blacks;

	private void writeNELFile(double p, int e) {
		int numberOfBlacks = numberOfTargets + numberOfSources;
		File outFile = new File("./input/random/rg_" + numberOfNodes + "_"
				+ (int) (p * 100) + "_" + numberOfSources + "_"
				+ numberOfTargets + "_" + e + ".nel");
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			bw.write(numberOfNodes + " " + numberOfBlacks + "\n");
			for (int i = 0; i < numberOfBlacks; i++) {
				bw.write(blacks.elementAt(i) + "\n");
			}
			int[] inDegree = new int[numberOfNodes];
			int[] outDegree = new int[numberOfNodes];
			for (int i = 0; i < numberOfNodes; i++) {
				for (int j = 0; j < numberOfNodes; j++) {
					if (adjacencyMatrix[i][j]) {
						inDegree[j]++;
						outDegree[i]++;
					}
				}
			}
			for (int i = 0; i < numberOfNodes; i++) {
				bw.write(i + " " + outDegree[i] + " " + inDegree[i] + " L\n");
			}
			for (int i = 0; i < numberOfNodes; i++) {
				for (int j = 0; j < numberOfNodes; j++) {
					if (adjacencyMatrix[i][j]) {
						bw.write(i + " " + j + " L\n");
					}
				}
			}
			bw.close();
		} catch (Exception exc) {
			exc.printStackTrace();
			System.exit(-1);
		}
	}

	private void createAdjMatrix(double p) {
		adjacencyMatrix = new boolean[numberOfNodes][numberOfNodes];
		for (int i = 0; i < numberOfNodes; i++) {
			for (int j = 0; j < numberOfNodes; j++) {
				double r = Math.random();
				if (r <= p && i != j) {
					adjacencyMatrix[i][j] = true;
				}
			}
		}
	}

	private void createBlacks() {
		int numberOfBlacks = numberOfTargets + numberOfSources;
		Vector<Integer> all = new Vector<Integer>();
		for (int i = 0; i < numberOfNodes; i++) {
			all.add(i);
		}
		Collections.shuffle(all);
		blacks = new Vector<Integer>();
		for (int i = 0; i < numberOfBlacks; i++) {
			blacks.add(all.elementAt(i));
		}
	}

	public RandomGraph(int n, int s, int t) {
		numberOfNodes = n;
		numberOfSources = s;
		numberOfTargets = t;
	}

	private void createGraph(double p) {
		createAdjMatrix(p);
		createBlacks();
	}

	private void enumerate(double p, int e) {
		String fn = "rg_" + numberOfNodes + "_" + (int) (p * 100) + "_"
				+ numberOfSources + "_" + numberOfTargets + "_" + e;
		System.out.print(fn);
		Main.main(new String[] {
				"-verb=0",
				"-f=random/n" + numberOfNodes + "p" + (int) (p * 100) + "/"
						+ fn, "-bc" });
	}

	private void checkSC(double p, int e) {
		String fn = "./input/random/rg_" + numberOfNodes + "_"
				+ (int) (p * 100) + "_" + numberOfSources + "_"
				+ numberOfTargets + "_" + e + ".nel";
		System.out.print("rg_" + numberOfNodes + "_" + (int) (p * 100) + "_"
				+ numberOfSources + "_" + numberOfTargets + "_" + e);
		if ((new File(fn)).exists()) {
			SourceTargetNetwork g = new SourceTargetNetwork(fn);
			SCCComputer scc = new SCCComputer(g);
			scc.findSCC();
			System.out.println("\t" + scc.getLastComponent());
		} else {
			System.out.println("\t NA");
		}
	}

	public static void main(String[] args) {
		int n = 8;
		double p = 0.39;
		for (int s = 1; s < 4; s++) {
			for (int t = 1; t < 4; t++) {
				for (int e = 0; e < 10; e++) {
					RandomGraph rg = new RandomGraph(n, s, t);
					// rg.createGraph(p);
					// rg.writeNELFile(p, e);
					rg.enumerate(p, e);
					// rg.checkSC(p, e);
				}
			}
		}
	}
}
