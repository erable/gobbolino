/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package touche.graphs;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;

/*
 * This class models a metabolic network by means of adjacency and incidency lists.
 */
public class SourceTargetNetwork {
	public ArrayList<Integer>[] adjacencyList;
	public ArrayList<Integer>[] incidencyList;
	public LinkedList<String>[]  outLabels;
	public LinkedList<String>[]  inLabels;

	public ArrayList<Integer> arcsToAdd = new ArrayList<Integer>();
	public int component = -1;
	
	protected int[] permutation;
	protected int[] inDegree;
	protected int[] outDegree;
	public boolean[] isSource;
	public boolean[] isTarget;
	public String[] nodeName;
	public int nn, ne;
	public int correctNumbers[];

	protected int vertexToConsider = -1;
	

	public SourceTargetNetwork(int nn) {
		set(nn);
	}
	
	public SourceTargetNetwork(SourceTargetNetwork g) {
		adjacencyList = g.adjacencyList;
		incidencyList = g.incidencyList;
		inDegree = g.inDegree;
		outDegree = g.outDegree;
		isSource = g.isSource;
		isTarget = g.isTarget;
		nn = g.nn;
		ne = g.ne;
		correctNumbers = g.correctNumbers;
		component = g.component;
		arcsToAdd = g.arcsToAdd;
	}
	
	public SourceTargetNetwork(String inputFile) {
		readFile(inputFile);
	}
	/*
	 * It computes the number of edges.
	 */
	int numberOfEdges() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			result = result + outDegree[i];
		}
		return result;
	}

	/*
	 * It computes the number of source nodes.
	 */
	int numberOfSourceNodes() {
		int b = 0;
		for (int i = 0; i < nn; i++) {
			if (isSource[i]) {
				b++;
			}
		}
		return b;
	}
	
	/*
	 * It computes the number of target nodes.
	 */

	int numberOfTargetNodes() {
		int b = 0;
		for (int i = 0; i < nn; i++) {
			if (isTarget[i]) {
				b++;
			}
		}
		return b;
	}

	/*
	 * It prints some basic statistics of the network.
	 */
	void printStatistics(boolean verbose, String method) {
		if (verbose) {
			System.out.println("=== Graph statistics after " + method + " ===");
			System.out.println("Number of nodes: " + nn);
			System.out.println("Number of edges: " + ne);
		}
	}

	@SuppressWarnings("unchecked")
	public void set(int nn) {
		
		this.nn = nn;
		isSource = new boolean[nn];
		isTarget = new boolean[nn];
		correctNumbers = new int[nn];
		inDegree = new int[nn];
		outDegree = new int[nn];

		for (int i = 0; i < nn; i++) {
			isSource[i] = false;
			isTarget[i] = false;
			inDegree[i] = 0;
			outDegree[i] = 0;
			correctNumbers[i] = i;
		}
		
		adjacencyList = new ArrayList[nn];
		outLabels = new LinkedList[nn];
		incidencyList = new ArrayList[nn];
		inLabels = new LinkedList[nn];
		for (int i = 0; i < nn; i++) {
			adjacencyList[i] = new ArrayList<Integer>();
			outLabels[i] = new LinkedList<String>();
			incidencyList[i] = new ArrayList<Integer>();
			inLabels[i] = new LinkedList<String>();
		}
		
		permutation = new int[nn];
		for (int i = 0; i < nn; i++) {
			permutation[i] = i;
		}
	}
	

	/*
	 * It reads the graph from a file in NEL format. This format is the
	 * following. The first line contains the number of nodes and the number of
	 * black nodes. The list of black nodes follows. Then, for each node, there
	 * is a line with the node index, its out-degree, its in-degree, and its
	 * name. Finally, the list of all edges follows.
	 */
	protected void readFile(String inPath) {
		File inFile = new File(inPath);
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			int index = line.indexOf(' ');
			int secondIndex;
			int numberOfSourceNodes;
		
			if (line.length() <= index + 2)
				secondIndex = -1;
			else
				secondIndex = line.indexOf(' ', index + 1);
					
			set(Integer.parseInt(line.substring(0, index)));
			
			if (secondIndex == -1) {
				numberOfSourceNodes = Integer.parseInt(line.substring(index + 1));
				
				for (int i = 0; i < numberOfSourceNodes; i++) {
					int j = Integer.parseInt(br.readLine());
					isSource[j] = true;
					isTarget[j] = true;
				}
			}
			else {
				numberOfSourceNodes = Integer.parseInt(line.substring(index + 1, secondIndex));
				int numberOfTargetNodes = Integer.parseInt(line.substring(secondIndex + 1));
				for (int i = 0; i < numberOfSourceNodes; i++) {
					int j = Integer.parseInt(br.readLine());
					isSource[j] = true;
				}
				for (int i = 0; i < numberOfTargetNodes; i++) {
					int j = Integer.parseInt(br.readLine());
					isTarget[j] = true;
				}
			}
			
			nodeName = new String[nn];
			for (int i = 0; i < nn; i++) {
				line = br.readLine();
				index = line.indexOf(' ');
				secondIndex = line.indexOf(' ', index + 1);
				outDegree[i] = Integer.parseInt(line.substring(index + 1,
						secondIndex));
				index = line.indexOf(' ', secondIndex + 1);
				inDegree[i] = Integer.parseInt(line.substring(secondIndex + 1,
						index));
				nodeName[i] = line.substring(index + 1);
			}
			ne = numberOfEdges();

			line = br.readLine();
			while (line != null) {
				index = line.indexOf(' ');
				secondIndex = line.indexOf(' ', index + 1);				
				int s = Integer.parseInt(line.substring(0, index));
				int t = Integer.parseInt(line.substring(index + 1, secondIndex));
				adjacencyList[s].add(t);
				incidencyList[t].add(s);
				outLabels[s].add(line.substring(secondIndex+1));
				inLabels[t].add(line.substring(secondIndex+1));
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	@SuppressWarnings("unchecked")
	public void permuteNodes(int[] permutation) {
		this.permutation = permutation;

		ArrayList<Integer>[] newIncidencyList = new ArrayList[nn];
		ArrayList<Integer>[] newAdjacencyList = new ArrayList[nn];
		

		int[] newInDegree = new int[nn];
		int[] newOutDegree = new int[nn];
		boolean[] newSource = new boolean[nn];
		boolean[] newTarget = new boolean[nn];
		
		for (int i = 0; i < nn; i++) {
			newSource[i] = false;
			newTarget[i] = false;
		}
		
		for (int i = 0; i < nn; i++) {
			newSource[permutation[i]] = isSource[i];
			newTarget[permutation[i]] = isTarget[i];
			newInDegree[permutation[i]] = inDegree[i];
			newOutDegree[permutation[i]] = outDegree[i];
			newIncidencyList[permutation[i]] = new ArrayList<Integer>();
			newAdjacencyList[permutation[i]] = new ArrayList<Integer>();
			
			for (int j = 0; j < adjacencyList[i].size(); j++) {
				newAdjacencyList[permutation[i]].add(permutation[adjacencyList[i].get(j)]);
			}
			newAdjacencyList[permutation[i]] = sort(newAdjacencyList[permutation[i]]);

			for (int j = 0; j < incidencyList[i].size(); j++) {
				newIncidencyList[permutation[i]].add(permutation[incidencyList[i].get(j)]);
			}
			newIncidencyList[permutation[i]] = sort(newIncidencyList[permutation[i]]);
		}
		this.incidencyList = newIncidencyList;
		this.adjacencyList = newAdjacencyList;
		this.isSource = newSource;
		this.isTarget = newTarget;
		this.inDegree = newInDegree;
		this.outDegree = newOutDegree;
	}
	
	public ArrayList<Integer> sort(ArrayList<Integer> toSort) {
		int minPos;
		ArrayList<Integer> sorted = new ArrayList<Integer>();
		
		while (!toSort.isEmpty()) {
			minPos = 0;
			for (int j = 1; j < toSort.size(); j++) {
				if (toSort.get(j) < toSort.get(minPos)) {
					minPos = j;
				}
			}
			sorted.add(toSort.get(minPos));
			toSort.remove(minPos);
		}
		return sorted;
	}
	
	
	public void addArc(int t, int h) {
		if (!adjacencyList[t].contains(h)) {
			adjacencyList[t].add(h);
			incidencyList[h].add(t);
			inDegree[h]++;
			inDegree[t]++;
			ne++;
		}
	}
	
	protected SourceTargetNetwork exportInducedSubgraph(boolean[] nodes, int[] newNumbers) {
		
		int nodeNumber = 0;
		
		for (int i = 0; i < nn; i++) {
			if (nodes[i])
			{
				newNumbers[i] = nodeNumber;
				nodeNumber++;
			}
			else
				newNumbers[i] = -1;
		}
	
		SourceTargetNetwork g = new SourceTargetNetwork(nodeNumber);

		g.correctNumbers = correctNumbers;
		g.component = component;
		g.arcsToAdd = arcsToAdd;
		
		for (int i = 0; i < nn; i++) {
			if (nodes[i]) {
				for (int j = 0; j < adjacencyList[i].size(); j++) {
					if (nodes[adjacencyList[i].get(j)]) {
						g.addArc(newNumbers[i], newNumbers[adjacencyList[i].get(j)]);					
					}
				}
				if (isSource[i]) {
					g.isSource[newNumbers[i]] = true;
				}
				if (isTarget[i]) {
					g.isTarget[newNumbers[i]] = true;
				}
			}
		}
		
		return g;
	}
	
protected SourceTargetNetwork exportInducedSubgraph(boolean[] nodes) {
	
	int[] newNumbers = new int[nn];
		int nodeNumber = 0;
		
		for (int i = 0; i < nn; i++) {
			if (nodes[i])
			{
				newNumbers[i] = nodeNumber;
				nodeNumber++;
			}
		}
	
		SourceTargetNetwork g = new SourceTargetNetwork(nodeNumber);
	
		g.correctNumbers = correctNumbers;
		g.component = component;
		g.arcsToAdd = arcsToAdd;

		for (int i = 0; i < nn; i++) {
			if (nodes[i]) {
				for (int j = 0; j < adjacencyList[i].size(); j++) {
					if (nodes[adjacencyList[i].get(j)]) {
						g.addArc(newNumbers[i], newNumbers[adjacencyList[i].get(j)]);					
					}
				}
				if (isSource[i]) {
					g.isSource[newNumbers[i]] = true;
				}
				if (isTarget[i]) {
					g.isTarget[newNumbers[i]] = true;
				}
			}
		}
		
		return g;
	}
	
	public String toString() {
		
		String toReturn = "Number of nodes: " + nn + ".\nNumber of edges: " + ne +".\nSources: ";
		
		boolean changed = false;
		
		for (int i = 0; i < nn; i++)
			if (isSource[i]) {
				changed = true;
				toReturn = toReturn + correctNumbers[i] + ", ";
			}
		
		if (changed)
			toReturn = toReturn.substring(0, toReturn.length() - 2) + ".";
		
		toReturn = toReturn + "\nTargets: ";
		changed = false;
		
		for (int i = 0; i < nn; i++)
			if (isTarget[i]) {
				changed = true;
				toReturn = toReturn + correctNumbers[i] + ", ";
			}
		
		if (changed)
			toReturn = toReturn.substring(0, toReturn.length() - 2) + ".";

		changed = false;
		
		toReturn =  toReturn + "\nEdges: ";
		int k = 0;
		for (int i = 0; i < nn; i++)
			for (int j = 0; j < adjacencyList[i].size(); j++) {
				toReturn += "(" + correctNumbers[i]  + ", " + correctNumbers[adjacencyList[i].get(j)] + "); ";
				k++;
				if (k % 10 == 0)
					toReturn += "\n";
			}
		if (ne != 0)
			if (k % 10 == 0)
				toReturn = toReturn.substring(0, toReturn.length() - 3) + ".";
			else
				toReturn = toReturn.substring(0, toReturn.length() - 2) + ".";
		return toReturn;
	}

	public int getIdFromNodeName(String name) {
		for(int i = 0; i < nodeName.length; i++) {
			String[] names = nodeName[i].split(";");
			for(int j = 0; j < names.length; j++)
			if( names[j].equals(name) ) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean isBlack(int i) {
		return isSource[i] || isTarget[i];
	}
	
}
