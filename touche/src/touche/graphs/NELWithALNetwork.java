/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package touche.graphs;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;

/*
 * This class models a metabolic network by means of adjacency and incidency lists.
 */
public class NELWithALNetwork {
	protected LinkedList<Integer>[] adjacencyList;
	protected LinkedList<Integer>[] incidencyList;
	protected LinkedList<String>[]  outLabels;
	protected LinkedList<String>[]  inLabels;
	
	protected int[] inDegree;
	protected int[] outDegree;
	protected boolean[] isBlack;
	protected String[] nodeName;
	protected int nn, ne;

	/*
	 * It computes the number of edges.
	 */
	int numberOfEdges() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			result = result + outDegree[i];
		}
		return result;
	}

	/*
	 * It computes the number of black nodes.
	 */
	int numberOfBlackNodes() {
		int b = 0;
		for (int i = 0; i < nn; i++) {
			if (isBlack[i]) {
				b++;
			}
		}
		return b;
	}

	/*
	 * It prints some basic statistics of the network.
	 */
	void printStatistics(boolean verbose, String method) {
		if (verbose) {
			System.out.println("=== Graph statistics after " + method + " ===");
			System.out.println("Number of nodes: " + nn);
			System.out.println("Number of edges: " + ne);
		}
	}

	/*
	 * It reads the graph from a file in NEL format. This format is the
	 * following. The first line contains the number of nodes and the number of
	 * black nodes. The list of black nodes follows. Then, for each node, there
	 * is a line with the node index, its out-degree, its in-degree, and its
	 * name. Finally, the list of all edges follows.
	 */
	@SuppressWarnings("unchecked")
	protected void readFile(String inPath) {
		File inFile = new File(inPath);
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			int index = line.indexOf(' ');
			nn = Integer.parseInt(line.substring(0, index));
			int numberOfBlackNodes = Integer
					.parseInt(line.substring(index + 1));
			isBlack = new boolean[nn];
			for (int i = 0; i < nn; i++) {
				isBlack[i] = false;
			}
			for (int i = 0; i < numberOfBlackNodes; i++) {
				isBlack[Integer.parseInt(br.readLine())] = true;
			}
			inDegree = new int[nn];
			outDegree = new int[nn];
			nodeName = new String[nn];
			for (int i = 0; i < nn; i++) {
				line = br.readLine();
				index = line.indexOf(' ');
				int secondIndex = line.indexOf(' ', index + 1);
				outDegree[i] = Integer.parseInt(line.substring(index + 1,
						secondIndex));
				index = line.indexOf(' ', secondIndex + 1);
				inDegree[i] = Integer.parseInt(line.substring(secondIndex + 1,
						index));
				nodeName[i] = line.substring(index + 1);
			}
			ne = numberOfEdges();
			adjacencyList = new LinkedList[nn];
			outLabels = new LinkedList[nn];
			incidencyList = new LinkedList[nn];
			inLabels = new LinkedList[nn];
			for (int i = 0; i < nn; i++) {
				adjacencyList[i] = new LinkedList<Integer>();
				outLabels[i] = new LinkedList<String>();
				incidencyList[i] = new LinkedList<Integer>();
				inLabels[i] = new LinkedList<String>();
			}
			line = br.readLine();
			while (line != null) {
				index = line.indexOf(' ');
				int secondIndex = line.indexOf(' ', index + 1);				
				int s = Integer.parseInt(line.substring(0, index));
				int t = Integer.parseInt(line.substring(index + 1, secondIndex));
				adjacencyList[s].add(t);
				incidencyList[t].add(s);
				outLabels[s].add(line.substring(secondIndex+1));
				inLabels[t].add(line.substring(secondIndex+1));
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
