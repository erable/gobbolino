package touche.enumerator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Queue;
import java.util.Stack;

import touche.application.InputParameters;
import touche.graphs.PitchList;
import touche.graphs.SourceTargetNetwork;

public class EnumeratorSourceTargetAll extends SourceTargetNetwork implements Runnable {

	public boolean finished = false;
	public int valueSCC = 1;
	public long timeBetweenPitchOutput = 5000000;
	public long numberOfOutputs = 0;
	protected boolean lastPath = true;
	protected long numberOfLeaves = 0;
	protected long numberOfPitches = 0;
	protected PitchList currentPitch;
	public long time = 0;
	protected LinkedList<Integer> r;
	public int depth = 0;
	protected int[] frontier;
	private EnumeratorWithBCC father;
	public int thread;
	public int startDepth = 0;
	public boolean needToStop = false;
	protected EndThread endThread;
	public boolean started = false;

	

	public EnumeratorSourceTargetAll(SourceTargetNetwork g, EnumeratorWithBCC father, EndThread endThread) {
		super(g);
		frontier = new int[0];
		this.father = father;
		this.endThread = endThread;
	}

	protected void backwardBFSOK(Queue<Integer> queue, int[] l) {

		ListIterator<Integer> iter;
		int x, y;

		while (!queue.isEmpty()) {
			x = queue.poll();

			iter = incidencyList[x].listIterator();

			while (iter.hasNext()) {
				y = iter.next();

				if (l[y] == 0) {
					l[y] = l[x];
					queue.add(y);
				}
			}
		}
	}

	public synchronized void setHelpingThread(EnumeratorSourceTargetAll helpingThread) {

		if (!this.started) {
			try {
				this.needToStop = true;
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
		}

		
		helpingThread.depth = depth;
		helpingThread.startDepth = startDepth;
		helpingThread.finished = finished;
		helpingThread.currentPitch = currentPitch.copy();
		startDepth++;

		this.needToStop = false;
		this.notifyAll();

	}
	
	protected synchronized void enumeratePitchesNotRecursive() {
		this.started = true;
		nextPath();
		String gen = "";

		while (depth != this.startDepth || !r.isEmpty()) {
			
			if (this.needToStop && depth > this.startDepth + 1) {
				this.notifyAll();
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			if (r.isEmpty()) {

				if (lastPath) {
					if (isStory())
						this.storyFound();
					numberOfLeaves++;
				}
				lastPath = false;
				// System.out.println("Depth: " + depth);
				father();
				depth--;
			}

			else {
				if (InputParameters.verbosity > 3) {
					System.out.println("Pitch: " + currentPitch.toString());
					System.out.println("Generated from: " + gen);
					System.out.println("Adding path: " + r.toString() + "\n");
				}
				// System.out.println("Depth: " + depth);
				lastPath = true;
				numberOfPitches++;
				if (numberOfPitches % this.timeBetweenPitchOutput == 0) {
					System.out.println("Number of pitches considered by thread " + (thread + 1) + ": "
							+ numberOfPitches + ".");
				}
			}
			if (InputParameters.verbosity > 3) {
				gen = currentPitch.toString();
			}

			nextPath();

		}
		this.finished = true;
		this.notifyAll();
	}

	protected void father() {
		int v, i;

		for (v = nn - 1; v >= 0; v--)
			if (currentPitch.incidencyList[v].size() == 0
					&& currentPitch.adjacencyList[v].size() > 0) {
				break;
			}
		r.addLast(v);

		while (currentPitch.incidencyList[v].size() <= 1
				&& currentPitch.adjacencyList[v].size() > 0
				&& (!isSource[v] || v >= r.getFirst())) {
			v = currentPitch.adjacencyList[v].get(currentPitch.adjacencyList[v]
					.size() - 1);
			r.addLast(v);
		}

		for (i = r.size() - 2; i > 0
				&& currentPitch.adjacencyList[r.get(i)].size() == 1
				&& !isTarget[r.get(i)]; i--) {
			currentPitch.removeEdge(r.get(i), r.get(i + 1));
		}

		currentPitch.removeEdge(r.get(i), r.get(i + 1));

	}

	protected int[] findFirstArcOut(int[] label, int i) {

		int toReturn[] = new int[2];
		int v = 0, j = 0;
		int old = r.getLast();

		ListIterator<Integer> iter;

		if (r.size() > i + 1) {
			iter = r.listIterator(i + 2);
			old = iter.previous();
		} else {
			iter = r.listIterator(i + 1);
			old = -1;
		}

		while (iter.hasPrevious()) {
			v = iter.previous();

			for (j = adjacencyList[v].indexOf(old) + 1; j < adjacencyList[v]
					.size(); j++) {

				if (label[adjacencyList[v].get(j)] > label[v]) {
					toReturn[0] = v;
					toReturn[1] = adjacencyList[v].get(j);
					return toReturn;
				}
			}
			old = v;
		}

		toReturn[0] = -1;
		toReturn[1] = -1;
		return toReturn;
	}

	protected boolean isStory() {

		boolean labels[] = new boolean[nn];

		for (int i = 0; i < nn; i++) {

			if (currentPitch.isInPitch(i) || isSource[i]) {

				for (int j = 0; j < nn; j++) {
					labels[j] = false;
				}

				labels[i] = true;
				storyBBFS(i, labels);

				if (storyBFS(i, labels)) {
					return false;
				}

			}
		}
		return true;
	}

	private void label(int[] l) {
		Queue<Integer> q = new LinkedList<Integer>();
		int v;

		for (int i = 0; i < nn; i++) {

			if (isTarget[i] || currentPitch.isInPitch(i)) {
				l[i] = nn;
				q.add(i);
			} else if (isSource[i] && i < r.getFirst())
				l[i] = -1;
		}

		ListIterator<Integer> iter = r.listIterator();

		while (iter.hasNext()) {
			v = iter.next();
			l[v] = iter.nextIndex();
			q.remove(v);
		}

		backwardBFSOK(q, l);

		while (iter.hasPrevious()) {
			q.add(iter.previous());
			backwardBFSOK(q, l);
		}
	}

	protected void lexDFS(int[] l, int lastr, int w, int labelmin) {

		int[] pred = new int[nn];
		boolean[] reached = new boolean[nn];

		for (int i = 0; i < nn; i++) {
			reached[i] = false;
			pred[i] = -1;
		}

		Stack<Integer> p = new Stack<Integer>();
		reached[w] = true;
		p.push(w);

		int u = w;
		while (!p.empty()) {
			u = p.pop();
			reached[u] = true;
			if ((currentPitch.isInPitch(u) || isTarget[u])
					&& (labelmin >= 0 || u != w)) {
				p.clear();
			} else {
				for (int j = adjacencyList[u].size() - 1; j >= 0; j--) {
					int v = adjacencyList[u].get(j);
					if (!reached[v] && l[v] > labelmin) {

						pred[v] = u;
						p.push(v);
					}
				}
			}
		}

		LinkedList<Integer> extension = new LinkedList<Integer>();

		while (pred[u] >= 0) {
			extension.addFirst(u);
			u = pred[u];
		}
		extension.addFirst(u);
		extension.addFirst(lastr);
		r.addAll(extension);

		currentPitch.addPath(r);
	}

	private void nextPath() {

		int[] l = new int[nn];
		int[] firstArcOut = new int[2];

		int i = r.size() - 1;

		if (currentPitch.isInPitch(r.getLast()) && i > 0
				&& !currentPitch.isInPitch(r.get(i - 1), r.getLast()))
			i--;

		else if ((isTarget[r.getLast()] && i > 0 && !currentPitch.isInPitch(
				r.get(i - 1), r.getLast()))
				|| (isSource[r.getLast()] && r.getLast() < r.get(0))
				|| currentPitch.incidencyList[r.getLast()].size() > 1)
			i--;

		label(l);
	
		
		firstArcOut = findFirstArcOut(l, i);

		if (firstArcOut[0] == -1) {
			nextPathFromNewSource();
		} else {
			while (r.pollLast() != firstArcOut[0]) {
			}

			if (currentPitch.isInPitch(firstArcOut[1])
					|| isTarget[firstArcOut[1]]) {
				r.addLast(firstArcOut[0]);
				r.addLast(firstArcOut[1]);
				currentPitch.addPath(r);
			} else
				lexDFS(l, firstArcOut[0], firstArcOut[1], r.size() + 1);
			depth++;
		}
	}

	private void nextPathFromNewSource() {

		int u = 0;
		int source = r.getFirst() + 1;
		int[] pred = new int[nn];
		boolean[] reached = new boolean[nn];
		boolean found = false;
		Stack<Integer> p = new Stack<Integer>();

		for (int i = 0; i < nn; i++) {
			reached[i] = false;
			pred[i] = -1;
		}

		while (source < nn && !found) {

			for (; source < nn; source++) {
				if (isSource[source] && !reached[source]
						&& !currentPitch.isInPitch(source)) {
					reached[source] = true;
					p.push(source);
					break;
				}
			}

			while (!p.empty()) {
				u = p.pop();
				reached[u] = true;
				if (currentPitch.isInPitch(u) || (isTarget[u] && u != source)) {
					p.clear();
					found = true;
				} else {
					for (int j = adjacencyList[u].size() - 1; j >= 0; j--) {
						int v = adjacencyList[u].get(j);
						if (!reached[v]
								&& (!isSource[v] || v > source || isTarget[v] || currentPitch
										.isInPitch(v))) {
							pred[v] = u;
							p.push(v);
						}
					}
				}
			}
			if (source < nn && isTarget[source]) {
				reached[source] = false;
			}
			source++;
		}

		r.clear();
		if (found) {
			LinkedList<Integer> extension = new LinkedList<Integer>();

			while (pred[u] >= 0) {
				extension.addFirst(u);
				u = pred[u];
			}
			extension.addFirst(u);
			r = extension;
			depth++;
			currentPitch.addPath(r);
		}

	}

	public void prepareToRun() {
		currentPitch = new PitchList(nn);
		currentPitch.correctNumbers = this.correctNumbers;
		this.numberOfLeaves = 0;
		this.numberOfPitches = 0;
		
		r = new LinkedList<Integer>();

		int i;

		for (i = 0; i < nn && !isSource[i]; i++) {
		}

		if (i != nn)
			r.add(i);
			
	}
	
	
	public void run() {

		enumeratePitchesNotRecursive();		
		this.endThread.threadFinished(this.thread);

	}



	protected void storyBBFS(int i, boolean[] l) {
		int x, y;
		Iterator<Integer> iter;
		Queue<Integer> queue = new LinkedList<Integer>();
		queue.add(i);

		while (!queue.isEmpty()) {
			x = queue.poll();

			iter = currentPitch.incidencyList[x].iterator();

			while (iter.hasNext()) {
				y = iter.next();
				if (l[y] == false) {
					l[y] = true;
					queue.add(y);
				}
			}
		}
	}

	private boolean storyBFS(int start, boolean l[]) {

		int x, y;
		Queue<Integer> queue = new LinkedList<Integer>();
		queue.add(start);

		while (!queue.isEmpty()) {
			x = queue.poll();
			Iterator<Integer> iter = adjacencyList[x].iterator();

			while (iter.hasNext()) {
				y = iter.next();

				if (l[y] == false && !currentPitch.isInPitch(x, y)
						&& (currentPitch.isInPitch(y) || isTarget[y]))
					return true;
				else if (l[y] == false && !currentPitch.isInPitch(x, y)) {
					l[y] = true;
					queue.add(y);
				}
			}
		}
		return false;
	}

	protected void storyFound() {

		father.storyFound(currentPitch, thread);
	}

	public void start() {
		Thread t = new Thread(this);
        t.start();
	}
}