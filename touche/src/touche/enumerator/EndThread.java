package touche.enumerator;

public class EndThread {
	EnumeratorThread father;
	
	public EndThread(EnumeratorThread father) {
		this.father = father;
	}
	
	public synchronized void threadFinished(int nThread) {
		father.threadFinished(nThread);
	}
}
