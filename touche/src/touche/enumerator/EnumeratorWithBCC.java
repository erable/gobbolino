/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
 */
package touche.enumerator;

import java.util.ArrayList;

import touche.application.InputParameters;
import touche.graphs.PitchList;
import touche.graphs.SourceTargetNetwork;
import touche.preprocessing.BCCComputer;

public class EnumeratorWithBCC extends BCCComputer {

	public long totalNumberOfStories = 1;
	public int CC = 1;
	private int nCurrentBCC;
	public EnumeratorWithSCC father;
	private int vertexToConsiderInSCC = -1;
	public int currentThread = 1;
	private int vertexToConsiderInBCC = -1;
	public int numberOfSons;
	public int currentSon;
	public SourceTargetNetwork currentBCC;

	
	
	public ArrayList<PitchList> oldStoriesS[];
	public ArrayList<PitchList> oldStoriesT[];
	public ArrayList<PitchList> oldStories[];
	public ArrayList<PitchList> newStoriesS;
	public ArrayList<PitchList> newStoriesT;
	public ArrayList<PitchList> newStories;
	public int[] newNumbers = new int[0];



	public EnumeratorWithBCC(SourceTargetNetwork g, EnumeratorWithSCC father) {
		super(g);
		this.father = father;
	}

	
	public void runNextBCC() {

		this.totalNumberOfStories = 0;
		newNumbers = new int[nn];
		currentBCC = getBiconnectedComponent(nCurrentBCC, newNumbers);

		if (getTotalNBCC() > 1) {

			if (this.verticesNotCompleted[nCurrentBCC] == -1) {
				vertexToConsider = -1;
			}
			else {
				vertexToConsiderInSCC = frontier.get(this.verticesNotCompleted[nCurrentBCC]);
				vertexToConsiderInBCC = newNumbers[vertexToConsiderInSCC];
				vertexToConsider = currentBCC.correctNumbers[vertexToConsiderInBCC];
			}

			if (InputParameters.verbosity > 0) {
				System.out.println("\n\nBICONNECTED COMPONENT " + (CC + 1) + "-" + (nCurrentBCC + 1) + ":\n");
				if (vertexToConsider >= 0)
					System.out.println("Linking vertex: " + vertexToConsider + ".");
				else
					System.out.println("Linking vertex: none.");

				System.out.println(currentBCC.toString());
			}
		}
		if (InputParameters.verbosity > 0) {
			System.out.println();
		}

		for (int k = 0; k < this.frontierComponent[nCurrentBCC].size(); k++) {
			currentBCC.isSource[newNumbers[frontierComponent[nCurrentBCC].get(k)]] = true;
			currentBCC.isTarget[newNumbers[frontierComponent[nCurrentBCC].get(k)]] = true;
		}
		new EnumeratorThread(currentBCC, this).run();
	
		if (InputParameters.verbosity > 0 && this.nCurrentBCC < this.biconnectedComponents.size() - 1)
			System.out.println("Stories found in BCC " + (this.nCurrentBCC + 1) + ": " + this.totalNumberOfStories + ".");
	}
	
	

	


	@SuppressWarnings("unchecked")
	public void run(int CC) {

		oldStoriesS = new ArrayList[nn];
		oldStoriesT = new ArrayList[nn];
		oldStories = new ArrayList[nn];
		newStoriesS = new ArrayList<PitchList>();
		newStoriesT = new ArrayList<PitchList>();
		newStories = new ArrayList<PitchList>();

		for (int i = 0; i < nn; i++) {
			oldStoriesS[i] = new ArrayList<PitchList>();
			oldStoriesT[i] = new ArrayList<PitchList>();
			oldStories[i] = new ArrayList<PitchList>();
		}


		this.CC = CC;

		if (InputParameters.optimizeBiconnected) {
			computeBCC();

			if (InputParameters.verbosity > 0) {
				System.out.println("\n" +getStatistics());
			}
		}
		else
			onlyOneBCC();


		if (getNBCC() == 0) {
			if (InputParameters.verbosity > 0)
				System.out.println("\nThere is only the empty story, because there are no useful BCC.\n");
			this.totalNumberOfStories = 1;
		}

		for (nCurrentBCC = 0; nCurrentBCC < this.biconnectedComponents.size(); nCurrentBCC++) {
			runNextBCC();
			
			
			if (vertexToConsider >= 0) {
				oldStoriesS[vertexToConsiderInSCC] = newStoriesS;
				oldStoriesT[vertexToConsiderInSCC] = newStoriesT;
				oldStories[vertexToConsiderInSCC] = newStories;
			}
			newStoriesS = new ArrayList<PitchList>();
			newStoriesT = new ArrayList<PitchList>();
			newStories = new ArrayList<PitchList>();
		}
	}
	
	public synchronized void completeStoryRecursive(PitchList story, int front, int thread) {
		if (front == frontierComponent[this.nCurrentBCC].size()) {

			if (this.nCurrentBCC == this.biconnectedComponents.size() - 1)
				father.storyFound(story, this.totalNumberOfStories, thread);
			else {
				if (story.incidencyList[vertexToConsider].isEmpty() && !isSource[vertexToConsiderInSCC]) {
					this.newStoriesS.add(story);
				}
				else if (story.adjacencyList[vertexToConsider].isEmpty() && !isTarget[vertexToConsiderInSCC]) {
					this.newStoriesT.add(story);
				}
				else
					this.newStories.add(story);
			}
		}
		else {
			PitchList s;

			if (story.incidencyList[correctNumbers[frontierComponent[this.nCurrentBCC].get(front)]].size() != 0 || frontierComponent[this.nCurrentBCC].get(front) == vertexToConsider) {

				for (int i = 0; i < this.oldStoriesS[frontierComponent[this.nCurrentBCC].get(front)].size(); i++) {

					s = story.copy();
					s.merge(this.oldStoriesS[frontierComponent[this.nCurrentBCC].get(front)].get(i));
					completeStoryRecursive(s, front + 1, thread);

				}
			}

			if (story.adjacencyList[correctNumbers[frontierComponent[this.nCurrentBCC].get(front)]].size() != 0 || frontierComponent[this.nCurrentBCC].get(front) == vertexToConsider) {

				for (int i = 0; i < this.oldStoriesT[frontierComponent[this.nCurrentBCC].get(front)].size(); i++) {

					s = story.copy();
					s.merge(this.oldStoriesT[frontierComponent[this.nCurrentBCC].get(front)].get(i));
					completeStoryRecursive(s, front + 1, thread);

				}
			}

			for (int i = 0; i < this.oldStories[frontierComponent[this.nCurrentBCC].get(front)].size(); i++) {

				s = story.copy();
				s.merge(this.oldStories[frontierComponent[this.nCurrentBCC].get(front)].get(i));
				completeStoryRecursive(s, front + 1, thread);

			}
			if (this.oldStoriesS[frontierComponent[this.nCurrentBCC].get(front)].size() == 0 && this.oldStoriesT[frontierComponent[this.nCurrentBCC].get(front)].size() == 0 && this.oldStories[frontierComponent[this.nCurrentBCC].get(front)].size() == 0) {
				completeStoryRecursive(story, front + 1, thread);
			}	
		}

	}


	public synchronized void storyFound(PitchList story, int thread) {

		PitchList completeStory = new PitchList(this.father.nn);
		completeStory.merge(story);
		totalNumberOfStories++;
		this.completeStoryRecursive(completeStory, 0, thread);

		if (InputParameters.verbosity > 2 && !(this.nCurrentBCC == this.biconnectedComponents.size() - 1)) {
			System.out.println("TOUCHE! BCC story " + totalNumberOfStories + " found by thread " + thread + ": " + story.toString());
		}
	}
}
