package touche.enumerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Queue;
import java.util.Stack;

import touche.application.InputParameters;
import touche.graphs.SourceTargetNetwork;


//IMPROVEMENTS:
// - SUBSTITUTE NEXTPATHFROMNEWSOURCE WITH SIMPLIFIED VERSION
// - ADD FINDPRUNING
// - USE i-=2 AND NOT i-- IN NEXTPATH
// - USE BICONNECTED COMPONENTS (RUN WITH -bc)

public class EnumeratorSourceTarget extends EnumeratorSourceTargetAll {

	public EnumeratorSourceTarget(SourceTargetNetwork g, EnumeratorWithBCC father, EndThread endThread) {
		super(g, father, endThread);
		findPaths();
	}

	private ArrayList<Integer>[] paths;

	private void openNodesBFS(Queue<Integer> queue, boolean[] visited,
			int labelMin, int[] label) {

		ListIterator<Integer> iter;
		int x, y;

		while (!queue.isEmpty()) {
			x = queue.poll();

			iter = adjacencyList[x].listIterator();

			while (iter.hasNext()) {
				y = iter.next();

				if (!visited[y] && label[y] > labelMin) {
					visited[y] = true;
					queue.add(y);
				}
			}
		}
	}

	private int[] labelSimplified() {
		Queue<Integer> q = new LinkedList<Integer>();
		int v;
		int[] l = new int[nn];

		for (int i = 0; i < nn; i++) {

			if (isTarget[i] || currentPitch.isInPitch(i)) {
				l[i] = nn;
				q.add(i);
			}
		}

		ListIterator<Integer> iter = r.listIterator();

		while (iter.hasNext()) {
			v = iter.next();
			l[v] = iter.nextIndex();
			q.remove(v);
		}

		backwardBFSOK(q, l);

		while (iter.hasPrevious()) {
			q.add(iter.previous());
			backwardBFSOK(q, l);
		}
		return l;
	}

	private void nextPath() {

		int[] l = labelSimplified();
		int[] firstArcOut = new int[2];

		int i = r.size() - 1;

		
		if (currentPitch.isInPitch(r.getLast()) && i > 0
				&& !currentPitch.isInPitch(r.get(i - 1), r.getLast()))
			i -= 2;
		// i-=2; to improve.
		else if ((isTarget[r.getLast()] && i > 0 && !currentPitch.isInPitch(
				r.get(i - 1), r.getLast()))
				|| (isSource[r.getLast()] && r.getLast() < r.get(0))
				|| currentPitch.incidencyList[r.getLast()].size() > 1)
			i--;

		if (i < 0) {
			nextPathFromNewSource();
			return;
		}
		
		if (pruning(l, i)) {
			lastPath = false;
			r.clear();
			return;
		}

		firstArcOut = findFirstArcOut(l, i);

		if (firstArcOut[0] == -1) {
			nextPathFromNewSource();
		} else {
			while (r.pollLast() != firstArcOut[0]) {
			}

			if (currentPitch.isInPitch(firstArcOut[1])
					|| isTarget[firstArcOut[1]]) {
				r.addLast(firstArcOut[0]);
				r.addLast(firstArcOut[1]);
				currentPitch.addPath(r);
			} else
				lexDFS(l, firstArcOut[0], firstArcOut[1], r.size() + 1);
			depth++;
		}
	}

	
	private boolean[] findOpenNodes(int[] label, int i) {

		Queue<Integer> q = new LinkedList<Integer>();
		ListIterator<Integer> iter = r.listIterator();
		int v = iter.next(), j, old = v;

		boolean open[] = new boolean[nn];

		for (j = 0; j < nn; j++)
			open[j] = currentPitch.isInPitch(j);

		for (j = r.getFirst() + 1; j < nn; j++) {
			if (isSource[j] && !open[j]) {
				q.add(j);
				open[j] = true;
				openNodesBFS(q, open, 0, label);
			}
		}

		while (iter.previousIndex() < i) {
			old = v;
			v = iter.next();
			open[old] = true;
			for (j = adjacencyList[old].indexOf(v) + 1; j < adjacencyList[old]
					.size(); j++) {

				if ((label[adjacencyList[old].get(j)] > iter.previousIndex())
						&& !open[adjacencyList[old].get(j)]) {
					q.add(adjacencyList[old].get(j));
					open[adjacencyList[old].get(j)] = true;
				}
			}
			openNodesBFS(q, open, iter.previousIndex(), label);
		}

		open[v] = true;
		if (!iter.hasNext())
			j = 0;
		else
			j = adjacencyList[v].indexOf(iter.next()) + 1;

		for (; j < adjacencyList[v].size(); j++) {

			if ((label[adjacencyList[v].get(j)] > i + 1)
					&& !open[adjacencyList[v].get(j)]) {
				q.add(adjacencyList[v].get(j));
				open[adjacencyList[v].get(j)] = true;
			}
		}

		openNodesBFS(q, open, i + 1, label);
		return open;
	}

	private boolean pruning(int[] label, int i) {
		
		switch (InputParameters.pruning) {
		case 1:
			return findPruning1(label, i);
		case 2:
			boolean open2[] = findOpenNodes(label, i);

			for (int j = 0; j < nn; j++) {
				if (!open2[j] && isTarget[j]) {
					return true;
				}
			}
			return findPruning2(open2, i);
		default:		
			boolean open3[] = findOpenNodes(label, i);

			for (int j = 0; j < nn; j++) {
				if (!open3[j] && isTarget[j]) {
					return true;
				}
			}
			return findPruning3(open3, i);
		}
	}

	private boolean findPruning1(int[] label, int lastR) {
		
		ListIterator<Integer> iterR = r.listIterator();
		boolean isInR[] = new boolean[nn];
		int y = iterR.next(), x, z;
		isInR[y] = true;
		
		while (iterR.hasNext()) {
			x = y;
			y = iterR.next();
			
			isInR[y] = true;
			
			Iterator<Integer> iter = adjacencyList[x].iterator();
			
			while (iter.hasNext()) {
				z = iter.next();
				if (y == z)
					break;
			
				if (currentPitch.isInPitch(z) && (!r.contains(z) || (r.getLast() == z && lastR < r.size() - 1)) && !currentPitch.isInPitch(x, z))
					return true;
			}
		}
		
		for (x = 0; x < nn; x++) {

			boolean previous[] = new boolean[nn];

			if (!isInR[x] && currentPitch.isInPitch(x)) {
				previous[x] = true;
				this.storyBBFS(x, previous);


				Iterator<Integer> iter = adjacencyList[x].iterator();

				while (iter.hasNext()) {
					y = iter.next();

					if (!previous[y] && !isInR[y] && currentPitch.isInPitch(y) && !currentPitch.isInPitch(x, y))
						return true;
				}
			}
		}
		return false;
		
	}
	
	private boolean findPruning2(boolean[] open, int lastR) {

		boolean openCopy[] = new boolean[nn];
		boolean previous[] = new boolean[nn];

		for (int i = 0; i < nn; i++) {
			if (currentPitch.isInPitch(i)
					&& (r.getLast() != i || lastR < r.size() - 1)) {

				for (int j = 0; j < nn; j++) {
					openCopy[j] = open[j];
					previous[j] = false;
				}

				previous[i] = true;
				storyBBFS(i, previous);

				if (pruningBFS2(i, openCopy, previous, lastR)) {
					return true;
				}

			}
		}
		return false;
	}
	
	private boolean findPruning3(boolean[] open, int lastR) {

		boolean openCopy[] = new boolean[nn];
		boolean previous[] = new boolean[nn];
		
		for (int i = 0; i < nn; i++) {
			if (currentPitch.isInPitch(i)
					&& (r.getLast() != i || lastR < r.size() - 1)) {

				
				for (int j = 0; j < nn; j++) {
					openCopy[j] = open[j];
					previous[j] = false;
				}

				findPreviousNodes(i, previous, open);
				
				if (pruningBFS3(i, openCopy, previous, lastR)) {
					return true;
				}

			}
		}
		return false;
	}
	
	private void nextPathFromNewSource() {

		int v;

		if (!isTarget[r.get(0)] && !currentPitch.isInPitch(r.get(0))) {
			r.clear();
			return;
		}

		for (v = r.getFirst() + 1; v < nn; v++)
			if (isSource[v] && !currentPitch.isInPitch(v) && paths[v].size() > 0)
				break;

		r.clear();

		if (v < nn) {
			r.add(v);

			for (int k = 0; !currentPitch.isInPitch(r.getLast())
					&& k < paths[v].size(); k++) {
				r.add(paths[v].get(k));
			}
			currentPitch.addPath(r);
			depth++;
		}
	}

	private void findPreviousNodes(int start, boolean[] previous, boolean[] open) {
		ListIterator<Integer> iter;
		int v;
		
		previous[start] = true;
		storyBBFS(start, previous);
		
		Queue<Integer> q = new LinkedList<Integer>();

		for (int i = 0; i < nn; i++) {
			if (previous[i])
				q.add(i);
		}
		pruningBBFS(q, previous, open);
		iter = r.listIterator();
		
		while (iter.hasNext()) {
			v = iter.next();
			q.add(v);
			previous[v] = true;
		}
		
		
		pruningBBFS(q, previous, open);
	}
	
	private boolean pruningBFS2(int start, boolean open[], boolean previous[], int lastR) {
		
		int x, y = -1, z;
		Queue<Integer> queue = new LinkedList<Integer>();
		int i = r.indexOf(start);
		
		if (i > -1 && i != r.size() - 1) {
		
			Iterator<Integer> iter = adjacencyList[start].iterator();
			
			z = r.get(i + 1);
			
			while (iter.hasNext()) {
				y = iter.next();
				if (y == z)
					break;
			
				if (!previous[y] && currentPitch.isInPitch(y) && (!r.contains(y) || (r.getLast() == y && lastR < r.size() - 1)) && !currentPitch.isInPitch(start, y))
					return true;
				if (!open[y]) {
					open[y] = true;
					queue.add(y);
				}
			}
			
		}
		else
			queue.add(start);
		
		while (!queue.isEmpty()) {
			x = queue.poll();
			Iterator<Integer> iter = adjacencyList[x].iterator();
			
			while (iter.hasNext()) {
				y = iter.next();
			
				if (!previous[y] && !r.contains(y) && currentPitch.isInPitch(y) && !currentPitch.isInPitch(x, y))
					return true;
				if (!open[y]) {
					open[y] = true;
					queue.add(y);
				}
			}
		}
		return false;
	}

	private int pruningBBFS(Queue<Integer> queue, boolean[] previous, boolean[] open) {
		int[] differentR = new int[nn];
		int x, y, toReturn = 0;
		Iterator<Integer> iter;
		int lastRInPitch = r.size() - 1;
		
		ListIterator<Integer> iterR = r.listIterator();
		
		while (iterR.hasNext()) {
			x = iterR.next();
			differentR[x] = iterR.nextIndex();
			if (!currentPitch.isInPitch(x) && lastRInPitch == r.size() - 1)
				lastRInPitch = iterR.previousIndex();
		}
		
		while (!queue.isEmpty()) {
			x = queue.poll();

			iter = incidencyList[x].iterator();

			while (iter.hasNext()) {
				y = iter.next();
				if (!previous[y] && !currentPitch.isInPitch(y) && open[y]) {
					previous[y] = true;
					queue.add(y);
				}
				
				toReturn = Math.max(toReturn, differentR[y]);
			}
		}
		
		
		return Math.min(lastRInPitch, toReturn);
	}

	private boolean pruningBFS3(int start, boolean open[], boolean previous[], int lastR) {
		
		int x, y = -1, z;
		Queue<Integer> queue = new LinkedList<Integer>();
		int i = r.indexOf(start);
		
		if (i > -1 && i != r.size() - 1) {
		
			Iterator<Integer> iter = adjacencyList[start].iterator();
			
			z = r.get(i + 1);
			
			while (iter.hasNext()) {
				y = iter.next();
				if (y == z)
					break;
				
				if (!open[y]) {
					open[y] = true;
					previous[y] = true;
					queue.add(y);
				}
				else if (!previous[y] && !currentPitch.isInPitch(start, y))
					return true;
			
			}
			
		}
		else
			queue.add(start);
		
		while (!queue.isEmpty()) {
			x = queue.poll();
			Iterator<Integer> iter = adjacencyList[x].iterator();
			
			while (iter.hasNext()) {
				y = iter.next();
				
				if (!open[y]) {
					open[y] = true;
					previous[y] = true;
					queue.add(y);
				}
				else if (!previous[y] && !currentPitch.isInPitch(x, y))
					return true;
			
			}
		}
		return false;
	}


	private void sourcesLexDFS(int start) {
		boolean[] reached = new boolean[nn];
		boolean solved = false;
		int[] pred = new int[nn];

		for (int i = 0; i < nn; i++) {
			reached[i] = false;
			pred[i] = -1;
		}

		Stack<Integer> p = new Stack<Integer>();
		reached[start] = true;
		p.push(start);

		int u = 0;
		while (!p.empty()) {
			u = p.pop();
			reached[u] = true;
			if ((isTarget[u] && u != start) || (isSource[u] && u < start)) {
				p.clear();
				solved = true;
			} else {
				for (int j = adjacencyList[u].size() - 1; j >= 0; j--) {
					int v = adjacencyList[u].get(j);
					if (!reached[v]) {
						pred[v] = u;
						p.push(v);
					}
				}
			}
		}
		paths[start] = new ArrayList<Integer>();

		if (solved) {
			while (pred[u] >= 0) {
				paths[start].add(0, u);
				u = pred[u];
			}
		}
	}

	protected synchronized void enumeratePitchesNotRecursive() {
		this.started = true;
		nextPath();
		String gen = "";
	
		while (depth != this.startDepth || !r.isEmpty()) {

			if (this.needToStop && depth > this.startDepth + 1) {
				this.notifyAll();
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			if (r.isEmpty()) {

				if (lastPath) {

					if (isStory())
						this.storyFound();
					numberOfLeaves++;
				}
				lastPath = false;
				// System.out.println("Depth: " + depth);
				father();
				depth--;
			}

			else {
				if (InputParameters.verbosity > 3) {
					System.out.println("Pitch: " + currentPitch.toString());
					System.out.println("Generated from: " + gen);
					System.out.println("Adding path: " + r.toString() + "\n");
				}
				// System.out.println("Depth: " + depth);
				lastPath = true;
				numberOfPitches++;
				if (numberOfPitches % this.timeBetweenPitchOutput == 0) {
					System.out.println("Number of pitches considered by thread " + (thread + 1) + ": "
							+ numberOfPitches + ".");
				}
			}
			if (InputParameters.verbosity > 3) {
				gen = currentPitch.toString();
			}

			// if
			// (gen.equals("(0, 1); (0, 2); (1, 5); (2, 1); (3, 4); (4, 8); (5, 9); (9, 3)."))
			// System.out.print("");

			nextPath();

		}

		this.finished = true;
		notifyAll();
	}
	

	@SuppressWarnings("unchecked")
	public void findPaths() {
		paths = new ArrayList[nn];

		for (int i = 0; i < nn; i++) {
			sourcesLexDFS(i);
		}
	}
	public void start() {
		Thread t = new Thread(this);
        t.start();
	}
	
	public void run() {
		
		enumeratePitchesNotRecursive();
		this.endThread.threadFinished(this.thread);
		
	}
}