/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
 */
package touche.enumerator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import touche.application.InputParameters;
import touche.graphs.PitchList;
import touche.graphs.SourceTargetNetwork;

public class EnumeratorThread extends SourceTargetNetwork {

	public long totalNumberOfStories = 1;
	public int currentThread = 0;
	public EnumeratorWithBCC father;
	public int numberOfSons;
	public int currentSon;
	public int numberOfThreadsFinished;
	private Queue<EnumeratorSourceTargetAll> threadsNeedingHelp = new LinkedList<EnumeratorSourceTargetAll>();
	private EndThread endThread;


	public ArrayList<PitchList> oldStoriesS[];
	public ArrayList<PitchList> oldStoriesT[];
	public ArrayList<PitchList> oldStories[];
	public ArrayList<PitchList> newStoriesS;
	public ArrayList<PitchList> newStoriesT;
	public ArrayList<PitchList> newStories;
	public int[] newNumbers = new int[0];



	public EnumeratorThread(SourceTargetNetwork g, EnumeratorWithBCC father) {
		super(g);
		this.father = father;
		this.endThread = new EndThread(this);
	}



	private EnumeratorSourceTargetAll setEnumerator() {

		EnumeratorSourceTargetAll storiesEnumerator = new EnumeratorSourceTargetAll(this, father, endThread);

		if (InputParameters.pruning > 0) {
			storiesEnumerator = new EnumeratorSourceTarget(this, father, endThread);
		}

		storiesEnumerator.prepareToRun();		
		storiesEnumerator.thread = currentThread;

		if (InputParameters.verbosity == 0) {
			storiesEnumerator.timeBetweenPitchOutput = Long.MAX_VALUE;
		}

		return storiesEnumerator;
	}




	public synchronized void run() {

		EnumeratorSourceTargetAll thread = setEnumerator();
		this.threadsNeedingHelp.add(thread);
		this.currentThread++;

		this.father.father.storiesFoundByThread.clear();
		this.father.father.partialStoriesFoundByThread.clear();
		this.father.father.storiesFoundByThread.add(0);
		this.father.father.partialStoriesFoundByThread.add(0);
		if (InputParameters.verbosity > 0)
			System.out.println("Thread 1 started!");
		thread.start();

		while (currentThread < InputParameters.nThreads && !threadsNeedingHelp.isEmpty()) {
			newThread();
		}

		while (this.numberOfThreadsFinished < this.currentThread) {

			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}		
			newThread();
		}
	}


	public synchronized void threadFinished(int thread) {
		if (InputParameters.verbosity > 0)
			System.out.println("Thread " + (thread + 1) + " finished!");
		this.numberOfThreadsFinished++;
		notifyAll();
	}


	public void newThread() {

		EnumeratorSourceTargetAll helpingThread = this.setEnumerator();
		EnumeratorSourceTargetAll threadToHelp;

		if (!threadsNeedingHelp.isEmpty()) {
			do {

				threadToHelp = threadsNeedingHelp.poll();
				threadToHelp.needToStop = true;
				threadToHelp.setHelpingThread(helpingThread);

			} while (helpingThread.finished && !threadsNeedingHelp.isEmpty());

			if (!helpingThread.finished) {
				if (InputParameters.verbosity > 0)
					System.out.println("Thread " + (helpingThread.thread + 1) + " started helping thread " + (threadToHelp.thread + 1) + " at depth " + helpingThread.startDepth + ".");

				while (helpingThread.depth > helpingThread.startDepth) {
					helpingThread.r.clear();
					helpingThread.father();
					helpingThread.depth--;
				}

				if (helpingThread.startDepth < ne / 10) {
					threadsNeedingHelp.add(helpingThread);
					threadsNeedingHelp.add(threadToHelp);
				}
				this.father.father.storiesFoundByThread.add(0);
				this.father.father.partialStoriesFoundByThread.add(0);
				this.currentThread++;
				helpingThread.start();
			}
		}
	}
}