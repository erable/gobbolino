/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
 */
package touche.enumerator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

import touche.application.ComputeStories;
import touche.application.InputParameters;
import touche.application.InputParameters.EnumStopCondition;
import touche.graphs.PitchList;
import touche.graphs.SourceTargetNetwork;
import touche.preprocessing.SCCComputer;
import touche.score.ComputeScore;
import touche.story.Story;
import touche.story.StoryWriter;

public class EnumeratorWithSCC extends SCCComputer {

	public long timeOutput = 10000;
	public long totalNumberOfStories = 0;
	public ArrayList<PitchList> oldStories = new ArrayList<PitchList>();
	public ArrayList<PitchList> newStories = new ArrayList<PitchList>();
	public int currentComponent;
	public long numberOfOutputs = 0;
	public long startTime;
	public ComputeStories father;
	public ArrayList<Integer> partialStoriesFoundByThread = new ArrayList<Integer>();
	public ArrayList<Integer> storiesFoundByThread = new ArrayList<Integer>();
	
	private SourceTargetNetwork graph = null;
	private ComputeScore score;
	private StoryWriter writer;
	private BufferedWriter scoresWriter;
	private DecimalFormat df = new DecimalFormat("0.000");
	private long numberStories = 1;
	
	public void startStoriesOutput(ComputeScore score, StoryWriter writer, BufferedWriter scoresWriter, SourceTargetNetwork graph) {
		this.score = score;
		this.writer = writer;
		this.scoresWriter = scoresWriter;
		this.graph = graph;
	}	
	
	public EnumeratorWithSCC(SourceTargetNetwork g, ComputeStories father) {
		super(g);
		this.father = father;
		if (InputParameters.verbosity > 1) {
			this.timeOutput = 10000;
		}
		else
			this.timeOutput = 60000;
	}

	
	public void runNextComponent() {

		SourceTargetNetwork h;

		totalNumberOfStories = 0;
		h = getConnectedComponent(currentComponent);

		if (lastComponent > 1 && InputParameters.verbosity > 0) {
			System.out.println("\n\nSTRONGLY CONNECTED COMPONENT " + (currentComponent + 1) + ":");
			System.out.println("\n" + h.toString());
		}

		new EnumeratorWithBCC(h, this).run(currentComponent + 1);

		if (this.currentComponent < lastComponent - 1 && InputParameters.verbosity > 0)
			System.out.println("\nStories found in SCC " + (currentComponent + 1) + ": " + this.totalNumberOfStories + ".");
	}

	public void run() {

		if (InputParameters.optimizeStronglyConnected || InputParameters.optimizeBiconnected || InputParameters.pruning > 0) {

			if (!InputParameters.optimizeStronglyConnected && InputParameters.verbosity > 0)
				if (InputParameters.optimizeBiconnected)
					System.out.println("\nThe biconnected components optimization does not work without the strongly connected component optimization.\nI use both of them.");
				else
					System.out.println("\nThe pruning improvement can be done only on a strongly connected graph.\nI add the strongly connected components improvement.");

			this.findSCC();

			if (InputParameters.verbosity > 0) {
				System.out.println("\n" + getStatistics());
			}
		}
		else
			this.onlyOneSCC();

		startTime = System.currentTimeMillis();

		for (currentComponent = 0; currentComponent < this.lastComponent; currentComponent++) {
			runNextComponent();
			
			if (newStories.size() > 0) {
				oldStories = newStories;
				newStories = new ArrayList<PitchList>();
			}
		}
		
			if (this.totalNumberOfStories == 0) {
				storyFound(new PitchList(nn), 1, -1);
			}

			System.out.println("\n\nTotal time: " + ((float)(System.currentTimeMillis() - this.startTime) / 1000) + " sec.");
	}

	public synchronized void storyFound(PitchList story, long NBCC, int thread) {

		PitchList completeStory;

		if (currentComponent == lastComponent - 1) {
			for (int j = 0; j < this.arcsToAdd.size(); j += 2) {
				story.addEdge(arcsToAdd.get(j), arcsToAdd.get(j + 1));
			}
		}


		for (int i = 0; i < oldStories.size(); i++) {
			completeStory = story.copy();

			totalNumberOfStories++;
			if (thread > -1)
				this.storiesFoundByThread.set(thread, this.storiesFoundByThread.get(thread) + 1);

			completeStory.merge(oldStories.get(i));

			if (InputParameters.verbosity > 2) {
				if (InputParameters.optimizeBiconnected)
					System.out.println("TOUCHE! Story " + totalNumberOfStories + " (made with story: " + NBCC + " from last BCC) found by thread " + thread + ": " + completeStory.toString());
				else
					System.out.println("TOUCHE! Story " + totalNumberOfStories +" found by thread " + thread + ": " + completeStory.toString());
			}

			if (currentComponent < lastComponent - 1) {
				newStories.add(completeStory);
			}
			else
				CompleteStoryFound(completeStory, NBCC);
		}
		
		if (oldStories.size() == 0) {
			completeStory = story.copy();

			totalNumberOfStories++;
			
			if (thread > -1)
				this.storiesFoundByThread.set(thread, this.storiesFoundByThread.get(thread) + 1);

			if (InputParameters.verbosity > 2) {
				if (InputParameters.optimizeBiconnected)
					System.out.println("TOUCHE! Story " + totalNumberOfStories + " (made with story: " + NBCC + " from last BCC) found by thread " + thread + ": " + completeStory.toString());
				else
					System.out.println("TOUCHE! Story " + totalNumberOfStories +" found by thread " + thread + ": " + completeStory.toString());
			}

			if (currentComponent != lastComponent - 1) {
				newStories.add(completeStory);
			}
			else
				CompleteStoryFound(completeStory, NBCC);
		}
	}
	

	protected void storyBBFS(int i, boolean[] l, PitchList currentPitch) {
		int x, y;
		Iterator<Integer> iter;
		Queue<Integer> queue = new LinkedList<Integer>();
		queue.add(i);

		while (!queue.isEmpty()) {
			x = queue.poll();

			iter = currentPitch.incidencyList[x].iterator();

			while (iter.hasNext()) {
				y = iter.next();
				if (l[y] == false) {
					l[y] = true;
					queue.add(y);
				}
			}
		}
	}

	private boolean storyBFS(int start, boolean l[], PitchList currentPitch) {

		int x, y;
		Queue<Integer> queue = new LinkedList<Integer>();
		queue.add(start);

		while (!queue.isEmpty()) {
			x = queue.poll();
			Iterator<Integer> iter = adjacencyList[x].iterator();

			while (iter.hasNext()) {
				y = iter.next();

				if (l[y] == false && !currentPitch.isInPitch(x, y)
						&& (currentPitch.isInPitch(y) || isTarget[y]))
					return true;
				else if (l[y] == false && !currentPitch.isInPitch(x, y)) {
					l[y] = true;
					queue.add(y);
				}
			}
		}
		return false;
	}

	
	// this function is useful only to verify that what we obtained is in fact a story.
	
	protected boolean isStory(PitchList currentPitch) {

		boolean labels[] = new boolean[nn];

		for (int i = 0; i < nn; i++) {

			if (currentPitch.isInPitch(i) || isSource[i]) {

				for (int j = 0; j < nn; j++) {
					labels[j] = false;
				}

				labels[i] = true;
				storyBBFS(i, labels, currentPitch);

				if (storyBFS(i, labels, currentPitch)) {
					return false;
				}

			}
		}
		return true;
	}

	
	
	private synchronized void CompleteStoryFound(PitchList story, long NBCC) {
		
		// TODO: add what you need to do when a story is found. 
		// note that the subroutine is synchronized, so you must pay attention if
		// you modify global variables.
//		if (!isStory(story)) {
//			System.out.println("There is a mistake in the program!!!!");
//			System.exit(0);
//		}
		
		if ((InputParameters.verbosity > 0 && (long) ((System.currentTimeMillis() - startTime) / timeOutput) > numberOfOutputs)) {
			numberOfOutputs = (System.currentTimeMillis() - startTime) / timeOutput;
			System.out.println("TOUCHE! Stories found after "
							+ (float) Math
									.round(((float) numberOfOutputs * timeOutput) / 600)
							/ 100 + " minutes: " + totalNumberOfStories +
							". Stories in last BCC: "
							+ NBCC + ".");
			
			
			for (int i = 0; i < this.storiesFoundByThread.size(); i++) {
				if (this.storiesFoundByThread.get(i) - partialStoriesFoundByThread.get(i) > 0) {
					System.out.println("        Stories found by thread "
							+ (i + 1) + ": " + this.storiesFoundByThread.get(i) + " (new: " + (this.storiesFoundByThread.get(i) - partialStoriesFoundByThread.get(i)) + ").");
					partialStoriesFoundByThread.set(i, this.storiesFoundByThread.get(i));
				}
			}
		}
		
		if( (InputParameters.stop == EnumStopCondition.TIME) && (System.currentTimeMillis() - startTime) >= InputParameters.stopTime ) {
			System.out.println("\t" + this.totalNumberOfStories + "\t"
					+ ((float) (System.currentTimeMillis() - startTime) / 1000));
			System.exit(0);
		}		
		
		// computes the score of the new story
		if( InputParameters.outputStories ) {
			Story s = new Story(story);
			Vector<Double> storyScore = score.computeScore(s);
			Double[] value = storyScore.toArray(new Double[0]);
			try {
				scoresWriter.write(++numberStories+") "+df.format(value[0])+"\n");
			} catch (IOException e) {
				System.out.println("Error writing in the scores file");
				e.printStackTrace();
			}
			
			// writes the story on the output file
			writer.writeFile(numberStories, graph, s, value);
		}
	}
	
	
	
}
