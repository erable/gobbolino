package touche.preprocessing;

import java.util.ArrayList;
import java.util.Stack;

import touche.graphs.SourceTargetNetwork;



public class BCCComputer extends SourceTargetNetwork{

	public ArrayList<boolean[]> biconnectedComponents;
	public int componentSize[];
	public ArrayList<Integer> frontier = new ArrayList<Integer>();
	protected ArrayList<Integer>[] frontierComponent;
	public int[] multiplicities;
	public int[] numberOfBiconnectedComponents;
	public int[] verticesNotCompleted;
	private int nBCC;
	
	
	public String getStatistics() {
		String toReturn = "";

		toReturn += "Number of biconnected components: " + nBCC + ".\nNumber of useful biconnected components: " + this.biconnectedComponents.size() + ".\nList of useful biconnected component sizes: ";
		for (int i = 0; i < biconnectedComponents.size() - 1; i++) {
			toReturn += componentSize[i] + ", ";
		}
		if (biconnectedComponents.size() != 0) {
			toReturn += componentSize[biconnectedComponents.size() - 1] + ".";
		}
		return toReturn;
	}
	
	
	
	public BCCComputer(SourceTargetNetwork g) {
		super(g);
	}
	
	private void findBCC()
	{
		int[] time = {0};
		int[] p;
		int[] b;
		int[] dtime;
		Stack<Integer> start = new Stack<Integer>();
		Stack<Integer> end = new Stack<Integer>();
		
		dtime = new int[nn];
		b = new int[nn];
		p = new int[nn];
		
		for (int i = 0; i < nn; i++) {
			p[i] = 0;
			b[i] = nn + 1;
			dtime[i] = 0;
		}
		for (int i = 0; i < nn; i++) {
			if (dtime[i] == 0) {		
				biDFS(i, p, b, dtime, start, end, time);
			}
		}
	}
	
	private void biDFS(int v, int[] p, int[] b, int[] dtime, Stack<Integer> start, Stack<Integer> end, int time[]) {
		time[0]++;
		dtime[v] = time[0];
		b[v] = dtime[v];
		
		for (int i = 0; i < adjacencyList[v].size(); i++) {
			int w = adjacencyList[v].get(i);
			if (dtime[w] == 0) {
				start.add(v);
				end.add(w);
				p[w] = v;
				
				biDFS(w, p, b, dtime, start, end, time);
				if (b[w] >= dtime[v]) {
					boolean[] BCC = new boolean[nn];
					int x = start.pop(), y = end.pop();
					BCC[x] = true;
					BCC[y] = true;
					while (x != v || y != w) {
						x = start.pop();
						y = end.pop();
						BCC[x] = true;
						BCC[y] = true;
					}
					this.biconnectedComponents.add(BCC);
				}
				else
					b[v] = Math.min(b[w], b[v]);
			}
			else if (dtime[w] < dtime[v] && w != p[v]) {
				start.add(v);
				end.add(w);
				b[v] = Math.min(b[v], dtime[w]);
			}
		}
		for (int i = 0; i < incidencyList[v].size(); i++) {
			int w = incidencyList[v].get(i);
			if (!adjacencyList[v].contains(w)) {
			if (dtime[w] == 0) {
				start.add(v);
				end.add(w);
				p[w] = v;
				
				biDFS(w, p, b, dtime, start, end, time);
				if (b[w] >= dtime[v]) {
					boolean[] BCC = new boolean[nn];
					int x = start.pop(), y = end.pop();
					BCC[x] = true;
					BCC[y] = true;
					while (x != v || y != w) {
						x = start.pop();
						y = end.pop();
						BCC[x] = true;
						BCC[y] = true;
					}
					this.biconnectedComponents.add(BCC);
				}
				else
					b[v] = Math.min(b[w], b[v]);
			}
			else if (dtime[w] < dtime[v] && w != p[v]) {
				start.add(v);
				end.add(w);
				b[v] = Math.min(b[v], dtime[w]);
			}
		}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void computeFrontier()
	{
		frontier = new ArrayList<Integer>();
		boolean found = false;
		for (int i = 0; i < nn; i++) {
			found = false;
			for (int j = 0; j < biconnectedComponents.size(); j++) {
				if (biconnectedComponents.get(j)[i]) {
					if (found) {
						frontier.add(i);
						break;
					}
					else
						found = true;
				}
			}
		}
		
		numberOfBiconnectedComponents = new int[frontier.size()];
		frontierComponent = new ArrayList[biconnectedComponents.size()];
		
		for (int i = 0; i < frontierComponent.length; i++) {
			frontierComponent[i] = new ArrayList<Integer>();
			
			for (int j = 0; j < frontier.size(); j++) {
				if (biconnectedComponents.get(i)[frontier.get(j)]) {
					frontierComponent[i].add(j);
					numberOfBiconnectedComponents[j]++;
				}
			}	
		}
	}
	
	private void modifyFrontierComponent() {
		for (int i = 0; i < frontierComponent.length; i++) {
			
			for (int j = 0; j < frontierComponent[i].size(); j++) {
				frontierComponent[i].set(j, frontier.get(frontierComponent[i].get(j)));
			}	
		}
	}
	
	public int getNBCC() {
		return this.biconnectedComponents.size();
	}
	

	public int getTotalNBCC() {
		return this.nBCC;
	}

	@SuppressWarnings("unchecked")
	public void onlyOneBCC() {

		biconnectedComponents = new ArrayList<boolean[]>();
		
		boolean[] bcc = new boolean[nn];
		
		for (int i = 0; i < nn; i++)
			bcc[i] = true;
		
		this.biconnectedComponents.add(bcc);
		this.componentSize = new int[1];
		this.componentSize[0] = nn;
		this.nBCC = 1;
		this.frontier = new ArrayList<Integer>();
		frontierComponent = new ArrayList[1];
		this.verticesNotCompleted = new int[1];
		verticesNotCompleted[0] = -1;
		frontierComponent[0] = new ArrayList<Integer>();
	}
	
	
	@SuppressWarnings("unchecked")
	public void computeBCC() {
		biconnectedComponents = new ArrayList<boolean[]>();
		
		if (nn == 1) {
			boolean[] bcc = {true};
			this.biconnectedComponents.add(bcc);
			this.componentSize = new int[1];
			this.componentSize[0] = 1;
			this.nBCC = 1;
			this.frontier = new ArrayList<Integer>();
			frontierComponent = new ArrayList[1];
			this.verticesNotCompleted = new int[1];
			verticesNotCompleted[0] = -1;
			frontierComponent[0] = new ArrayList<Integer>();
			return;
		}
		
		findBCC();
		nBCC = this.biconnectedComponents.size();
		computeComponentSizes();
		computeFrontier();
		removeUnusefulBCC();
		sortBiconnectedComponents();
		computeComponentSizes();
		computeFrontier();
		modifyFrontierComponent();
	}
	
	private void removeUnusefulBCC() {
		boolean foundSource;
		boolean tmp;
		boolean foundTarget;
		boolean changedSth = true;

		while (changedSth && biconnectedComponents.size() > 0) {
			
			for (int i = 0; i < biconnectedComponents.size(); i++) {
				foundSource = foundTarget = false;
				changedSth = true;
				for (int j = 0; j < nn; j++) {
					tmp = false;
					if(biconnectedComponents.get(i)[j] && (isSource[j] || frontier.contains(j))) {
						if (!foundSource)
							tmp = true;
						
						foundSource = true;
						if (foundTarget) {
							changedSth = false;
							break;
						}
					}
					if(biconnectedComponents.get(i)[j] && (isTarget[j] || frontier.contains(j))) {
						foundTarget = true;
						if (foundSource && !tmp) {
							changedSth = false;
							break;
						}
					}
				}
				if (changedSth) {
					biconnectedComponents.remove(i);
					computeComponentSizes();
					computeFrontier();
					break;
				}
				
			}
		}
	}
	
	
	private void computeComponentSizes() {
		this.componentSize = new int[biconnectedComponents.size()];
		for (int i = 0; i < biconnectedComponents.size(); i++) {
			componentSize[i] = 0;
			for (int j = 0; j < nn; j++) {
				if (biconnectedComponents.get(i)[j])
					componentSize[i]++;
			}
		}
	}
	

	private void sortBiconnectedComponents() {
		int oldComponent = 0;
		int newComponentSize[] = new int[this.biconnectedComponents.size()];
		int visited[] = new int[frontier.size()];
		int frontierFound = 0;
		int vertex, vertexOK;
		ArrayList<boolean[]> newComponents = new ArrayList<boolean[]>();
		
		this.verticesNotCompleted = new int[this.biconnectedComponents.size()];
		
		for (int i = 0; i < this.biconnectedComponents.size(); i++) {
			newComponentSize[i] = nn + 1;
		}
		
		for (int i = 0; i < this.biconnectedComponents.size(); i++) {
			vertex = vertexOK = -1;
			for (int j = 0; j < this.biconnectedComponents.size(); j++) {
				if (componentSize[j] < newComponentSize[i]) {
					frontierFound = 0;
					for (int k = 0; k < frontierComponent[j].size(); k++) {
						if (visited[frontierComponent[j].get(k)] < numberOfBiconnectedComponents[frontierComponent[j].get(k)] - 1) {
							vertex = frontierComponent[j].get(k);
							frontierFound++;
						}
					}
					if (frontierFound <= 1) {
						vertexOK = vertex;
						newComponentSize[i] = componentSize[j];
						oldComponent = j;
					}
				}
			}
			componentSize[oldComponent] = nn + 1;
			for (int j = 0; j < frontierComponent[oldComponent].size(); j++)
				visited[frontierComponent[oldComponent].get(j)]++;
			
			this.verticesNotCompleted[newComponents.size()] = vertexOK;
			newComponents.add(biconnectedComponents.get(oldComponent));
				
		}
		biconnectedComponents = newComponents;
		componentSize = newComponentSize;
	}

	public SourceTargetNetwork getBiconnectedComponent(int comp, int[] newNumbers)
	{
		SourceTargetNetwork g = exportInducedSubgraph(biconnectedComponents.get(comp), newNumbers);
		g.correctNumbers = new int[g.nn];
		
		int j = 0;
		for (int i = 0; i < nn; i++)
			if (newNumbers[i] >= 0) {
				g.correctNumbers[j] = correctNumbers[i];
				j++;
			}
		
		return g;
	}	
	
}
