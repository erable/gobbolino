/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package touche.preprocessing;
import java.util.ArrayList;
import java.util.Stack;

import touche.graphs.SourceTargetNetwork;


/*
 * This class allows us to compute the strongly connected components of a graph stored in a NEL file. The components are made of black nodes only.
 */
public class SCCComputer extends SourceTargetNetwork {

	public SCCComputer(SourceTargetNetwork g) {
		super(g);
	}

	private int nSCC;
	boolean[] complete;
	int counter;
	int[] component, componentSize;
	int[] dfsNumber;
	public int[] newNumbers;
	public int[] correctNumbers;
	protected int lastComponent;
	Stack<Integer> partial;
	Stack<Integer> representative;
	public ArrayList<Integer> arcsToAdd = new ArrayList<Integer>();

	/*
	 * For each component, computes the number of black nodes in it.
	 */
	void computeComponentSizes() {
		componentSize = new int[lastComponent];
		for (int i = 0; i < lastComponent; i++) {
			componentSize[i] = 0;
		}
		for (int i = 0; i < nn; i++) {
			if (component[i] != -1)
				componentSize[component[i]] = componentSize[component[i]] + 1;
		}
	}

	/*
	 * This is a recursive version of the depth-first search which is used in
	 * order to compute the strongly connected components of the graph. See the
	 * book by Crescenzi, Gambosi, Grossi.
	 */
	void extendedRecursiveDFS(int u) {
		dfsNumber[u] = counter;
		counter = counter + 1;
		partial.push(u);
		representative.push(u);
		for (int v = 0; v < nn; v++) {
			if (adjacencyList[u].contains(v)) {
				if (dfsNumber[v] == -1) {
					extendedRecursiveDFS(v);
				} else if (!complete[v]) {
					while (dfsNumber[representative.peek()] > dfsNumber[v]) {
						representative.pop();
					}
				}
			}
		}
		if (u == representative.peek()) {
			int z = partial.pop();
			component[z] = lastComponent;
			complete[z] = true;
			while (z != u) {
				z = partial.pop();
				component[z] = lastComponent;
				complete[z] = true;
			}
			representative.pop();
			lastComponent = lastComponent + 1;
		}
	}

	/*
	 * It prints the list of nodes in a specific strongly connected components.
	 */
	void printSCC(int c) {
		System.out.print("Component " + c + ": ");
		for (int i = 0; i < nn; i++) {
			if (component[i] == c) {
				System.out.print(i + " ");
			}
		}
		System.out.print(" numbers ");
		for (int i = 0; i < nn; i++) {
			if (component[i] == c) {
				System.out.print(i + " ");
			}
		}
		System.out.println("");
	}

	/*
	 * It prints the list of the sizes of all strongly connected components.
	 */
	public String getStatistics() {
		String toReturn = "";
		
		toReturn += "Number of strongly connected components: " + nSCC + "\nNumber of useful strongly connected components: " + lastComponent + ".\nList of useful strongly connected component sizes: ";
		if (lastComponent >= 1) {
			for (int i = 0; i < lastComponent - 1; i++) {
				toReturn += componentSize[i] + ", ";
			}
			toReturn += componentSize[lastComponent - 1] + ".";
		}
		return toReturn;
	}

	/*
	 * It reads the graph NEL file, computes the black strongly connected
	 * components, and outputs the number and the sizes of the components.
	 */
	public void findSCC(){
		stronglyConnectedComponents();
		this.nSCC = lastComponent;
		eliminateUnusefulCC();
		computeComponentSizes();
		sortConnectedComponents();
		//printSCCSizes();
		modifySourceTargets();
	}
	
	public void onlyOneSCC() {
		component = new int[nn];
		for (int i = 0; i < nn; i++) {
			component[i] = 0;
		}
		lastComponent = 1;
	}
	

	private void findArcsToAdd(SourceTargetNetwork g) {
		
		this.arcsToAdd = new ArrayList<Integer>();
		
		for (int i = 0; i < nn; i++) {
			if (component[i] != -1) {
				for (int j = 0; j < adjacencyList[i].size(); j++) {
					if (component[adjacencyList[i].get(j)] != component[i] && component[adjacencyList[i].get(j)] != -1) {
						arcsToAdd.add(i);
						arcsToAdd.add(adjacencyList[i].get(j));
					}
				}
			}
		}
				
	}
	
	
	
	private void eliminateUnusefulCC() {
		boolean doneSth = true;
		boolean source, target;
		while (doneSth) {
			doneSth = false;
			for (int i = 0; i < lastComponent; i++)	{
				source = target = false;
				for (int j = 0; j < nn; j++) {
					if (component[j] == i) {
						if (isSource[j])
							source = true;
						
						if (isTarget[j])
							target = true;
						
						for (int k = 0; k < adjacencyList[j].size(); k++) {
							if (component[adjacencyList[j].get(k)] != component[j] && component[adjacencyList[j].get(k)] != -1) {
								target = true;
								break;
							}
						}
						
						for (int k = 0; k < incidencyList[j].size(); k++) {
							if (component[incidencyList[j].get(k)] != component[j] && component[incidencyList[j].get(k)] != -1) {
								source = true;
								break;
							}
						}
					}
				}
				if (!source || !target) {
					doneSth = true;
					lastComponent--;
					for (int j = 0; j < nn; j++) {
						if (component[j] > i)
							component[j]--;
						else if (component[j] == i) {
							component[j] = -1;
						}
					}
				}
			}
		}
	}
	
	/*
	 * It computes the strongly connected components of the graph containing
	 * only black nodes. See the book by Crescenzi, Gambosi, Grossi.
	 */
	private void stronglyConnectedComponents() {
		dfsNumber = new int[nn];
		complete = new boolean[nn];
		component = new int[nn];
		for (int s = 0; s < nn; s++) {
			dfsNumber[s] = -1;
			complete[s] = false;
			component[s] = -1;
		}
		partial = new Stack<Integer>();
		representative = new Stack<Integer>();
		lastComponent = 0;
		counter = 0;
		for (int s = 0; s < nn; s++) {
			if (dfsNumber[s] == -1) {
				extendedRecursiveDFS(s);
			}
		}
	}
	
	private void modifySourceTargets()
	{
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < adjacencyList[i].size(); j++) {
				if (component[adjacencyList[i].get(j)] != component[i] && component[adjacencyList[i].get(j)] != -1) {
					//System.out.println("New target: " + i + ".");
					isTarget[i] = true;
				}
			}
			for (int j = 0; j < incidencyList[i].size(); j++) {
				if (component[incidencyList[i].get(j)] != component[i] && component[incidencyList[i].get(j)] != -1) {
					//System.out.println("New source: " + i + ".");
					isSource[i] = true;
				}
			}
		}
	}
	
	private void sortConnectedComponents() {
		int oldComponent = 0;
		
		int newComponent[] = new int[nn];
		int newComponentSize[] = new int[lastComponent];
		
		for (int i = 0; i < this.lastComponent; i++) {
			newComponentSize[i] = nn + 1;
		}
		
		for (int i = 0; i < this.lastComponent; i++) {
			for (int j = 0; j < this.lastComponent; j++) {
				if (newComponentSize[i] > componentSize[j]) {
					newComponentSize[i] = componentSize[j];
					oldComponent = j;
				}
			}
			componentSize[oldComponent] = nn + 1;
			for (int j = 0; j < nn; j++) {
				if (component[j] == oldComponent) {
					newComponent[j] = i;
				}
			}
				
		}
		for (int j = 0; j < nn; j++) {
			if (component[j] == -1) {
				newComponent[j] = -1;
			}
		}
		
		component = newComponent;
		componentSize = newComponentSize;
	}
	
	public SourceTargetNetwork getConnectedComponent(int comp) {
		boolean nodesInComponent[] = new boolean[nn];
		int j = 0;
		
		for (int i = 0; i < nn; i++) {
			nodesInComponent[i] = (component[i] == comp);
		}
		newNumbers = new int[nn];
		SourceTargetNetwork g = exportInducedSubgraph(nodesInComponent, newNumbers);
		
		g.correctNumbers = new int[g.nn];
		g.component = comp + 1;
		
		for (int i = 0; i < nn; i++)
			if (newNumbers[i] >= 0) {
				g.correctNumbers[j] = i;
				j++;
			}
		if (g.component == lastComponent) {
			this.findArcsToAdd(g);
		}
			
		return g;
	}

	public int getLastComponent() {
		return lastComponent;
	}
	
	public int getTotalNCC() {
		return nSCC;
	}
	
	public int getComponentSize(int c) {
		return componentSize[c];
	}
	
	public int getComponent(int c) {
		return component[c];
	}
}
