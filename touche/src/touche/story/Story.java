/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package touche.story;
import java.io.BufferedWriter;
import java.io.IOException;

import touche.graphs.PitchList;
import touche.graphs.SourceTargetNetwork;

public class Story {

	private PitchList pitch;
	protected String[] role;
	protected int[] b;
	protected int nodesInPitch;
	
	public Story(PitchList c) {
		pitch = c;
	}

	private void extractInfo(SourceTargetNetwork graph) {
		// count the black nodes
		int nb = 0;
		nodesInPitch = 0;
		for(int i = 0; i < graph.nn; i++) {
			if( isInStory(i) ) {
				nodesInPitch++;
				if( graph.isBlack(i) ) {
					nb++;
				}
			}
		}
		// save them and assign their role
		b = new int[nb];
		role = new String[nb];
		int bn = 0;
		for(int i = 0; i < graph.nn; i++) {
			if( graph.isSource[i] || graph.isTarget[i] ) {
				b[bn] = i;
				if( graph.isSource[i] && !graph.isTarget[i] ) {
					role[bn] = "S";
				} else if (!graph.isSource[i] && graph.isTarget[i]) {
					role[bn] = "T";
				} else {
					role[bn] = "I";
				}
				bn++;
			}
		}
	}
	
	public void writeStory(SourceTargetNetwork graph, BufferedWriter bw) throws IOException {
		extractInfo(graph);
		int nb = b.length;
		bw.write(nodesInPitch + " " + nb + "\n");
		
		for (int i = 0; i < b.length; i++) {
			bw.write(b[i] + " " + role[i] + "\n");
		}
		for (int i = 0; i < graph.nn; i++) {
			if (isInStory(i)) {
				bw.write(i + " " + pitch.adjacencyList[i].size() + " " + pitch.incidencyList[i].size()
						+ " " + graph.nodeName[i] + "\n");
			}
		}
		for (int i = 0; i < nn(); i++) {
			for (int j = 0; j < nn(); j++) {
				if (isInStory(i,j)) {
					bw.write(i + " " + j + " " + (i + "->" + j) + "\n");
				}
			}
		}
	}

	public boolean isInStory(int i, int j) {
		return pitch.isInPitch(i,j);
	}
	
	public boolean isInStory(int i) {
		return pitch.isInPitch(i);
	}

	public int nn() {
		return pitch.nn;
	}
}
