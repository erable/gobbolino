package touche.application;

import java.text.DecimalFormat;

import touche.utils.ArgumentParser;


public class Main {
	
	public static long totalNumberOfStories = 0;
	
	
	public static void main(String[] args) {
		
		long start = System.currentTimeMillis();
		ArgumentParser p = new ArgumentParser(args);
	
        processOptions(p);
        
		ComputeStories cs = new ComputeStories();
		cs.run();
		
		
		totalNumberOfStories = cs.totalNumberOfStories;
		
		if (InputParameters.pruning == -1) {
			DecimalFormat df = new DecimalFormat("0.000");
			double elapsed = ((System.currentTimeMillis() - start)/1000.0);
			System.out.println("\t"+totalNumberOfStories+"\t"+df.format(elapsed));
		}
	}

	
	private static void processOptions(ArgumentParser p) {
		
        if( p.hasOption("h") || p.hasOption("help") ) { 
        	System.out.println("Unfortunately, no help file is available yet.");
        	System.exit(0);
        }
        
        if( p.hasOption("verb") ) {
        	InputParameters.verbosity = Integer.parseInt(p.getOption("verb"));
        }
		
        if (p.hasOption("f"))
			InputParameters.inputFile = "./input/" + p.getOption("f");
		else if (p.hasOption("path"))
			InputParameters.inputFile = p.getOption("path");
		else
			InputParameters.inputFile = "./input/figura3_16.nel";
		
		if (!InputParameters.inputFile.endsWith(".nel"))
			InputParameters.inputFile = InputParameters.inputFile + ".nel";
		
		if (p.hasOption("onlypitches"))
			InputParameters.onlyPitches = true;

		if (p.hasOption("nst"))
			InputParameters.onlyBlack = true;
		
		if (p.hasOption("sc")) {
			InputParameters.optimizeStronglyConnected = true;
			InputParameters.stronglyConnectedComponents = true;			
		}
		
		if (p.hasOption("bc"))
			InputParameters.optimizeBiconnected = true;
		
		if (p.hasOption("pruning"))
			InputParameters.pruning = Integer.parseInt(p.getOption("pruning"));

		if (p.hasOption("perm"))
			InputParameters.perm = true;
		
		if (p.hasOption("o"))
			InputParameters.outputFile = p.getOption("o");
		
		if (p.hasOption("threads"))
			InputParameters.nThreads = Integer.parseInt(p.getOption("threads"));
		
		if (p.hasOption("score")) {
			String score = p.getOption("score");
			if( score.equalsIgnoreCase("enzyme_activation") ) {
				InputParameters.scoreMode[0] = InputParameters.EnumScoreMode.ENZYME_ACTIVATION;	// to simplify, assuming a single score function
			}
			if( score.equalsIgnoreCase("enzyme_concentration") ) {
				InputParameters.scoreMode[0] = InputParameters.EnumScoreMode.ENZYME_ACTIVATION_OR_CONCENTRATION_CHANGE;	// to simplify, assuming a single score function
			}
			if( score.equalsIgnoreCase("concentration") ) {
				InputParameters.scoreMode[0] = InputParameters.EnumScoreMode.CONCENTRATION_CHANGE;	// to simplify, assuming a single score function
			}
			if( score.equalsIgnoreCase("enzyme_inhibition") ) {
				InputParameters.scoreMode[0] = InputParameters.EnumScoreMode.ENZYME_INHIBITION;	// to simplify, assuming a single score function
			}
		}
		
	}
}

