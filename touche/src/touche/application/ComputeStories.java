/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
 */
package touche.application;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import touche.enumerator.EnumeratorWithSCC;
import touche.graphs.SourceTargetNetwork;
import touche.score.ComputeScore;
import touche.story.StoryWriter;

public class ComputeStories {
	
	public long totalNumberOfStories = 0;
	private ComputeScore score = null;
	private StoryWriter writer = null;
	private BufferedWriter scoresWriter;	

	private void initStoriesOutput(SourceTargetNetwork g) {
		if( writer == null ) {
			score = new ComputeScore(g);
			writer = new StoryWriter();
			try {
				scoresWriter = new BufferedWriter(new FileWriter(new File("./output/scores.txt")));
			} catch (IOException e) {
				System.out.println("Error creating the scores file");
				e.printStackTrace();
			}
		}
	}	

	private void finishStoriesOutput() {
		if( writer != null) {
			writer.closeFile();
			try {
				scoresWriter.close();
			} catch (IOException e) {
				System.out.println("Error closing the scores file.");
				e.printStackTrace();
			}
		}
	}
	
	public void run() {

		File NELFileCheck = new File(InputParameters.inputFile);

		if (!NELFileCheck.exists()) {
			System.out.println(InputParameters.inputFile + " not found.\n");
			System.exit(0);
		}

		SourceTargetNetwork g = new SourceTargetNetwork(InputParameters.inputFile);
		// assigns the same name of the file +'.bn' for the file with the intensities for the score function
		InputParameters.scoreFile = InputParameters.inputFile.replace(".nel", ".bn");
		
		if (InputParameters.verbosity > 0) {
			System.out.println("ORIGINAL GRAPH:\n");
			System.out.println(g.toString());
		}
		if( InputParameters.outputStories ) {
			initStoriesOutput(g);
		}
		EnumeratorWithSCC e = new EnumeratorWithSCC(g, this);
		e.startStoriesOutput(score, writer, scoresWriter, g);
		e.run();
		
		this.totalNumberOfStories = e.totalNumberOfStories;
		
		System.out.println("Touch�! This graph has " + this.totalNumberOfStories + " stories.");
		
		if( InputParameters.outputStories ) {
			finishStoriesOutput();
		}
	}
}
