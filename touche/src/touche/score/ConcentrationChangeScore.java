/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package touche.score;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

import touche.graphs.SourceTargetNetwork;
import touche.utils.SBMLTools;

public class ConcentrationChangeScore extends ScoreEval {

	protected static String filename;
	protected static HashMap<String, Double> blackMap;
	protected static double min = Double.MAX_VALUE, max = Double.MIN_VALUE;
	
	protected static double RED_OUTGOING_RED = 1;
	protected static double RED_OUTGOING_GREEN = -1.0;
	protected static double GREEN_OUTGOING_RED = -1.0;
	protected static double GREEN_OUTGOING_GREEN = 1;

	public ConcentrationChangeScore(SourceTargetNetwork graph) {
		super(graph);
	}	
	
	private static int readBlackNodesNamesFile(File blackFile) {
		int nbLines = 0;
		try {
			BufferedReader bbr = new BufferedReader(new FileReader(blackFile));
			String line = bbr.readLine();
			blackMap = new HashMap<String, Double>();
			while (line != null) {
				nbLines++;
				String[] lineWords = line.split(" ");
				String nodeName = SBMLTools.sbmlEncode(lineWords[0].trim());
				if( lineWords.length > 1 ) {
					double intensity = 0.0;
					try {
						intensity = new Double(lineWords[1]);
						if( intensity > max ) {
							max = intensity;
						}
						if( intensity < min ) {
							min = intensity;
						}
					}
					catch(Exception e) {
						System.err.println("Error reading black nodes file. Expected compound intensity. Found: "+lineWords[1]);
					}
					blackMap.put(nodeName, intensity);					
				} else
				{
					blackMap.put(nodeName, 0.0);
				}
				line = bbr.readLine();
			}
			bbr.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return nbLines;
	}	
	
	public static void readIntensities(String blackNodeFileName) {
		if( blackNodeFileName == null ) {
			System.err.println("No black nodes enriched with intensities file was supplied/found.");
			System.exit(0);
		}
		if( !blackNodeFileName.equalsIgnoreCase(filename) ) {
			File blackNodesFile = new File(blackNodeFileName);
			readBlackNodesNamesFile(blackNodesFile);
			filename = blackNodeFileName;
		}
	}
	
	public double[] computeScore() {
		double score = 0.0;
		boolean isRed = false;
		// for each black node of the story
		for (String name : blackMap.keySet()) {
			double intensity = blackMap.get(name);
			// normalization
			if( intensity > 1.0 ) { // production 
				intensity = intensity / max;
				isRed = false;
			} else {	// consumption
				intensity = min / intensity;
				isRed = true;
			}
			
			int node = graph.getIdFromNodeName(name);
			if( node >= 0 ) { // if the node is found
				// for each outgoing arc, compute the intensity of the node times the intensity of the target node
				// and add it to the score multiplying by the qualified adjacency factor 
				
				double nodeContribution = 0;
				for(int i = 0; i < story.nn(); i++) {
					if( story.isInStory(node,  i) ) {
						nodeContribution += getArcContribution(node, isRed, intensity, i);
					}
				}
				score += nodeContribution;
			}
		}
		return new double[] { score };
	}
	
	protected double getArcContribution(int node, boolean isRed, double intensity, int targetNode) {
		if( graph.isBlack(targetNode) ) {
			double target_intensity = getIntensity(graph.nodeName[targetNode]);
/*			double factor;
			if( intensity > target_intensity ) // explanation by concentration change!!
			{
				factor = 1.0;
			} else {
				factor = 0.5;
			}*/
			if( target_intensity > 1.0 ) { // green
				// normalization
				target_intensity = target_intensity / max;
				if( isRed ) {
					return RED_OUTGOING_GREEN * (intensity * target_intensity);
				} else {
					return GREEN_OUTGOING_GREEN * (intensity * target_intensity);					
				}
			} else if (target_intensity > 0.0 ) {	// red
				// normalization
				target_intensity = min / target_intensity;
				if( isRed ) {
					return RED_OUTGOING_RED * (intensity * target_intensity);
				} else {
					return GREEN_OUTGOING_RED * (intensity * target_intensity);					
				}
			} 
		}
		double nodeContribution = 0.0;
		// if it is not black, then recursively identify the neighbours
		for(int i = 0; i < story.nn(); i++) {
			if( story.isInStory(targetNode, i) ) {
				nodeContribution += getArcContribution(node, isRed, intensity, i);
			}
		}
		return nodeContribution;
	}

	protected static double getIntensity(String nodeNames) {
		String[] names = nodeNames.split(";");
		for(int i = 0; i < names.length; i++) {
			if( blackMap.containsKey(names[i]) ) {
				return blackMap.get(names[i]);
			}
		}
		return -1.0;
	}
	
}
