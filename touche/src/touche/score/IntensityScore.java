/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package touche.score;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

import touche.graphs.SourceTargetNetwork;
import touche.utils.SBMLTools;


public class IntensityScore extends ScoreEval {

	protected static HashMap<String, Double> blackMap;
	protected static double min = Double.MAX_VALUE, max = Double.MIN_VALUE;
	
	protected static double RED_OUTGOING_RED = 0;
	protected static double RED_OUTGOING_GREEN = 1.0;
	protected static double RED_OUTGOING_WHITE = 1.0;
	
	protected static double GREEN_OUTGOING_RED = -1.0;
	protected static double GREEN_OUTGOING_GREEN = 0;
	protected static double GREEN_OUTGOING_WHITE = -1.0;
	
	protected static double WHITE_OUTGOING_RED = -1.0;
	protected static double WHITE_OUTGOING_GREEN = 1.0;
	protected static double WHITE_OUTGOING_WHITE = 0.0;

	public IntensityScore(SourceTargetNetwork graph) {
		super(graph);
	}	
	
	private static int readBlackNodesNamesFile(File blackFile) {
		int nbLines = 0;
		try {
			BufferedReader bbr = new BufferedReader(new FileReader(blackFile));
			String line = bbr.readLine();
			blackMap = new HashMap<String, Double>();
			while (line != null) {
				nbLines++;
				String[] lineWords = line.split(" ");
				String nodeName = SBMLTools.sbmlEncode(lineWords[0].trim());
				if( lineWords.length > 1 ) {
					double intensity = 0.0;
					try {
						intensity = new Double(lineWords[1]);
						if( intensity > max ) {
							max = intensity;
						}
						if( intensity < min ) {
							min = intensity;
						}
					}
					catch(Exception e) {
						System.err.println("Error reading black nodes file. Expected compound intensity. Found: "+lineWords[1]);
					}
					blackMap.put(nodeName, intensity);					
				} else
				{
					blackMap.put(nodeName, 0.0);
				}
				line = bbr.readLine();
			}
			bbr.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return nbLines;
	}	
	
	public static void readIntensities(String blackNodeFileName) {
		if( blackNodeFileName == null ) {
			System.err.println("No black nodes enriched with intensities file was supplied/found.");
			System.exit(0);
		}
		
		File blackNodesFile = new File(blackNodeFileName);
		readBlackNodesNamesFile(blackNodesFile);		
	}
	
	public double[] computeScore() {
		double score = 0.0;
		boolean isRed = false;
		// for each black node of the story
		for (String name : blackMap.keySet()) {
			double intensity = blackMap.get(name);
			// normalization
			if( intensity > 1.0 ) { // production 
				intensity = intensity / max;
				isRed = false;
			} else {	// consumption
				intensity = min / intensity;
				isRed = true;
			}
			
			int node = graph.getIdFromNodeName(name);
			if( node >= 0 ) { // if the node is found
				// compute its consumption/production profile
				// (in-out)/(in+out)
				/*
				 * First approach: 
				
				double profile = ((double)story.getInDegree(node) - story.getOutDegree(node)) / (story.getInDegree(node) + story.getOutDegree(node));
				boolean profileSign = profile > 0;
				boolean expectedSign = intensity > 1.0;		// values below 1.0 mean consumption
				int contribution = profileSign == expectedSign ? 1 : 0;

				score += contribution * Math.abs(profile) * intensity;
				*/
				// check all edges incident to the node and compute its profile accordingly
				double profile = 0;
				for(int i = 0; i < story.nn(); i++) {
					if( story.isInStory(node,  i) ) {
						if( graph.isBlack(i) ) {
							double target_intensity = getIntensity(graph.nodeName[i]);
							if( target_intensity > 1.0 ) { // green
								profile += isRed ? RED_OUTGOING_GREEN : GREEN_OUTGOING_GREEN;
							} else if (target_intensity > 0.0 ) {	// red
								profile += isRed ? RED_OUTGOING_RED : GREEN_OUTGOING_RED;							
							}
						}
						else { // WHITE
							profile += isRed ? RED_OUTGOING_WHITE : GREEN_OUTGOING_WHITE;
						}
					}
					
					if( story.isInStory(i,  node) ) {
						if( !graph.isBlack(i) ) { 						// WHITE
							profile += isRed ? WHITE_OUTGOING_RED : WHITE_OUTGOING_GREEN;							
						}
					}
				}
				score += profile * intensity;
			}
		}
		return new double[] { score };
	}
	
	protected static double getIntensity(String nodeNames) {
		String[] names = nodeNames.split(";");
		for(int i = 0; i < names.length; i++) {
			if( blackMap.containsKey(names[i]) ) {
				return blackMap.get(names[i]);
			}
		}
		return -1.0;
	}

}
