/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package touche.score;

import java.util.ArrayList;
import java.util.Vector;

import touche.application.InputParameters;
import touche.graphs.SourceTargetNetwork;
import touche.story.Story;

public class ComputeScore {

	static ArrayList<ScoreEval> scoreFunctions = new ArrayList<ScoreEval>();

	public ComputeScore(SourceTargetNetwork graph) {
		clearScoreFunctions();
		for(int i = 0; i < InputParameters.scoreMode.length; i++) {
			ScoreEval eval = null;
			switch(InputParameters.scoreMode[i]) {
				case ENZYME_ACTIVATION: {
					eval = new EnzymeActivationScore(graph);
					EnzymeActivationScore.readIntensities(InputParameters.scoreFile);
					break;
				}
				case ENZYME_INHIBITION: {
					eval = new EnzymeInhibitionScore(graph);
					EnzymeInhibitionScore.readIntensities(InputParameters.scoreFile);
					break;
				}				
				case CONCENTRATION_CHANGE: {
					eval = new ConcentrationChangeScore(graph);
					ConcentrationChangeScore.readIntensities(InputParameters.scoreFile);
					break;
				}
				case ENZYME_ACTIVATION_OR_CONCENTRATION_CHANGE: {
					eval = new EnzymeActivationOrConcentrationChangeScore(graph);
					EnzymeActivationOrConcentrationChangeScore.readIntensities(InputParameters.scoreFile);
					break;
				}
			}
			scoreFunctions.add(eval);
		}
	}
	
	public static void addScoreFunction(ScoreEval scoreFunction) {
		ComputeScore.scoreFunctions.add(scoreFunction);
	}
	
	public static void clearScoreFunctions() {
		ComputeScore.scoreFunctions.clear();
	}
	
	public Vector<Double> computeScore(Story story) {
		// computing scores
		Vector<Double> dynScore = new Vector<Double>();
		for(int i = 0; i < scoreFunctions.size(); i++) {
			ScoreEval scoreFunction = scoreFunctions.get(i);
			scoreFunction.setStory(story);
			double s[] = scoreFunction.computeScore();
			for(int j = 0; j < s.length; j++)
				dynScore.add(s[j]);
		}
		return dynScore;
	}
	
}
