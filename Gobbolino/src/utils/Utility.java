/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils;
import java.util.LinkedList;

import application.InputParameters;

public class Utility {
	/*
	 * Check whether two arrays are different.
	 */
	static public boolean areDifferent(int[] a, int[] b) {
		boolean result = false;
		if (a.length != b.length) {
			return true;
		}
		for (int i = 0; i < a.length; i++) {
			if (a[i] != b[i]) {
				return true;
			}
		}
		return result;
	}

	/*
	 * Check whether two matrices are different.
	 */
	static boolean areDifferent(int[][] X, int[][] Y, int nn) {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (X[i][j] != Y[i][j]) {
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * Copy the first matrix into the second one.
	 */
	static public void copy(int[][] X, int[][] Y, int nn) {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				Y[i][j] = X[i][j];
			}
		}
	}

	/*
	 * Multiply the first matrix times the second one and return the resulting
	 * matrix.
	 */
	static public int[][] multiply(int[][] X, int[][] Y, int nn) {
		int[][] R = new int[nn][nn];
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				R[i][j] = 0;
				for (int k = 0; k < nn; k++) {
					R[i][j] = R[i][j] + X[i][k] * Y[k][j];
				}
				if (R[i][j] > 0) {
					R[i][j] = 1;
				}
			}
		}
		return R;
	}

	/*
	 * It prints a boolean matrix by representing the true (false) value by 1
	 * (0).
	 */
	static public void print(boolean[][] X) {
		for (int i = 0; i < X.length; i++) {
			for (int j = 0; j < X[0].length; j++) {
				if (X[i][j]) {
					System.out.print(1 + " ");
				} else {
					System.out.print(0 + " ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	/*
	 * prints a vector of booleans
	 */
	void print(boolean[] x) {
		for (int i = 0; i < x.length; i++) {
			System.out.print(x[i] + " ");
		}
		System.out.println();
	}
	
	
	/*
	 * It prints an integer array.
	 */
	static public void print(int[] x) {
		for (int i = 0; i < x.length; i++) {
			System.out.print(x[i] + " ");
		}
		System.out.println();
		System.out.println();
	}

	static public void print(LinkedList<Integer> x) {
		for (Integer i : x) {
			System.out.print(i + " ");
		}
		System.out.println();
		System.out.println();
	}

	/*
	 * It prints an integer matrix.
	 */
	static public void print(int[][] X) {
		for (int i = 0; i < X.length; i++) {
			for (int j = 0; j < X[0].length; j++) {
				System.out.print(X[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

	/*
	 * It prints an integer matrix, by substituting positive numbers with 1 and
	 * negative numbers with -1.
	 */
	static public void printCompare(int[][] compare) {
		System.out.println("Compare matrix");
		for (int i = 0; i < compare.length; i++) {
			for (int j = 0; j < compare[0].length; j++) {
				if (compare[i][j] > 0) {
					System.out.print("+1 ");
				} else if (compare[i][j] < 0) {
					System.out.print("-1 ");
				} else {
					System.out.print(" 0 ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	/*
	 * Insertion sort.
	 */
	static public void sort(int[] a) {
		for (int i = 1; i < a.length; i++) {
			int current = a[i];
			int j = i;
			while (j > 0 && a[j - 1] > current) {
				a[j] = a[j - 1];
				j = j - 1;
			}
			a[j] = current;
		}
	}

	/*
	 * Insertion sort of both a and b with respect only to the elements of a.
	 */
	static public void sort(double[] a, String[] b) {
		for (int i = 1; i < a.length; i++) {
			double current = a[i];
			String tmp = b[i];
			int j = i;
			while (j > 0 && a[j - 1] > current) {
				a[j] = a[j - 1];
				b[j] = b[j - 1];
				j = j - 1;
			}
			a[j] = current;
			b[j] = tmp;
		}
	}

	/*
	 * Compute the transitive closure of an adjacency matrix of an acyclic graph
	 * and then return the corresponding partial order.
	 */
	static public int[][] partialOrder(boolean[][] adj, int nn) {
		int[][] compare = new int[nn][nn];
		int[][] A = new int[nn][nn];
		int[][] AStar = new int[nn][nn];
		int[][] B = new int[nn][nn];
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (adj[i][j]) {
					A[i][j] = 1;
					AStar[i][j] = 1;
				} else {
					A[i][j] = 0;
					AStar[i][j] = 0;
				}
			}
			A[i][i] = 1;
			AStar[i][i] = 1;
		}
		do {
			Utility.copy(AStar, B, nn);
			AStar = Utility.multiply(B, B, nn);
		} while (Utility.areDifferent(AStar, B, nn));
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				compare[i][j] = 0;
			}
		}
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (AStar[i][j] == 1) {
					compare[i][j] = -1;
					compare[j][i] = 1;
				}
			}
		}
		return compare;
	}
	
	/*
	 * System out that checks for verbose mode
	 */
	public static void printMsg(String msg) {
		if( InputParameters.verbose ) {
			System.out.println(msg);
		}
	}

}
