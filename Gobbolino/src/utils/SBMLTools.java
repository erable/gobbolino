/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SBMLTools {

	static public String sbmlDecode(String in) {
		String out = new String(in);
		
		String REGEX = ".*__(\\d+)__.*";
		
		Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(out);
        
        while(matcher.find()) {
        	String str = matcher.group(1);        	
        	String specialCharacter = new Integer(str).toString();        	
        	out = out.replace("__"+str+"__", specialCharacter);
        	matcher = pattern.matcher(out);
        }
        REGEX = "^_(\\d*).*";
        
        pattern = Pattern.compile(REGEX);
        matcher = pattern.matcher(out);
        if(matcher.find()) {
        	String str = matcher.group(1);   	
        	out = out.replaceFirst("^_"+str, str);
        }
		
        return out;
	}

	static public String sbmlEncode(String in) {
		String out = new String(in);
		String REGEX = "^\\d";
		
    		Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(out);
        
        if(matcher.find()) {
			out = "_".concat(out);
		}
        
        REGEX = "[^0-9A-Za-z_]";
        
        pattern = Pattern.compile(REGEX);
        matcher = pattern.matcher(out);
        
        while(matcher.find()) {
        	String specialCharacter = matcher.group(0);
        	Integer value = specialCharacter.codePointAt(0);
        	String code = "__"+value+"__";
        	
        	out = out.replace(specialCharacter,code);
        }
		
		return out;
	}

}
