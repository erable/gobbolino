/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package cytoscape;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class CytoscapeFilesCreator {
	/**
	 * Will create : and edge file to import in cytoscape and two node attribue files
	 * Can be used in the main :
	 * 		if (fromXML) {
			(new XML2NEL()).run(fn, verbose, oneWay);
			CytoscapeFilesCreator.createFiles(fn,".nel");
		}
	 * @param fn
	 * @param suffix by default ".nl" but then could be "LP.nel" or "S.nel"
	 */
	public static void createFiles(String fn,String suffix)
	{
		File bnOutFile = new File("./"+fn+suffix+"cy_BN.txt");
		File edgesOutFile = new File("./"+fn+suffix+"cy_Edges.txt");
		File nodesOutFile = new File("./"+fn+suffix+"cy_Nodes.txt");

		File inFile = new File(fn+suffix);
		
		try {
			BufferedWriter bnOutFileW = new BufferedWriter(new FileWriter(bnOutFile));
			BufferedWriter edgesOutFileW = new BufferedWriter(new FileWriter(edgesOutFile));
			BufferedWriter nodesOutFileW = new BufferedWriter(new FileWriter(nodesOutFile));
			
				BufferedReader br = new BufferedReader(new FileReader(inFile));
				String line = br.readLine();
				int nn = new Integer(line.split(" ")[0]);
				int numberOfBlackNodes = new Integer(line.split(" ")[1]);
				
				//Black nodes attributes
				bnOutFileW.write("ID\tbn\n");
				for (int i=1;i<numberOfBlackNodes+1;i++)
				{
					line = br.readLine();
					bnOutFileW.write(line+"\tbn\n");
				}
				bnOutFileW.close();
				
				//Node name Attributes
				nodesOutFileW.write("ID\tName\n");
				for (int i=numberOfBlackNodes+1;i<numberOfBlackNodes+nn+1;i++)
				{
					line = br.readLine();
					nodesOutFileW.write(line.split(" ")[0]+"\t"+line.split(" ")[3]+"\n");
				}
				nodesOutFileW.close();
				
				//Edges
				line = br.readLine();
				while(line!=null)
				{
					edgesOutFileW.write(line.split(" ")[0]+"\t"+line.split(" ")[1]+"\n");
					line = br.readLine();
				}
				edgesOutFileW.close();
				
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
