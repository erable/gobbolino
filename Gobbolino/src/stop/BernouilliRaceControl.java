/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package stop;
import java.util.Random;

public class BernouilliRaceControl extends RaceControl {

	int rep;
	int n;
	double percSolutionsLeft;
	double sum;
	
	int[] experiment;
	int index;
	int nsas;
	int countdownToRestart;
	
	boolean control;
	
	public BernouilliRaceControl(int rep, double percSolutionsLeft) {
		this.rep = rep;
		this.percSolutionsLeft = percSolutionsLeft;
		control = false;
		init();
	}
	
	public void init() {
		experiment = new int[rep];
		for(int i = 0; i < rep; i++)
			experiment[i] = 0;
		index = 0;
		n = 0;
		sum = 0.0;		
	}
	
	public void newStoryGenerated(boolean newOne, int nsas) {
		if( control && countdownToRestart > 0)
			countdownToRestart--;

		int x = newOne ? 1 : 0;
		
		this.nsas = nsas;
		// update the sum of the last rep experiments
		if( n < rep ) {
			sum += x;			
		}
		else {
			// removes the oldest value (index) that is going to be replaced and adds x instead
			sum = sum - experiment[index] + x;
		}
		// adds the new experiment
		experiment[index++] = x;
		// increases the number of runs
		n++;
		// makes the experiments vector circular
		if( index == rep ){
			index = 0;
			//System.out.println("Computed "+nsas+" stories. New stories seen in the last "+rep+": "+sum);			
		}
	}

	public boolean stop() {
		if( control && countdownToRestart > 0 )
			return false;
		
		if( n > rep ) 
		{
			if( (sum / (double)rep) <= (percSolutionsLeft/(double)rep) ) 
			{
				// the control "verifies" that we are close to the desired percentage
				if( control )
				{
					return true;
				}
				else
				{
					init();
					control = true;
					countdownToRestart = new Random().nextInt(100) * new Random().nextInt(rep);
					System.out.println("Starting control experiment. Computed "+nsas+" stories. Expected = "+(int)(nsas/(1-percSolutionsLeft)));
					return false;
				}
			}
			else
				control = false;
		}
		return false;
	}
	
	public String explanation() {
		if( !stop() ) {
			return "Why do you want an explanation for something that did not happened yet?";
		}
		return "...we are close to "+((1-percSolutionsLeft)*100)+" %";			
	}
	
}
