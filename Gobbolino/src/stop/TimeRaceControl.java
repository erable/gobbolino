/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package stop;

public class TimeRaceControl extends RaceControl {

	long timeout = 5 * 60 * 1000;	// 5 minutes in milliseconds
	long elapsed = 0;
	boolean no_new = true;
	
	public TimeRaceControl(long timeout, boolean no_new) {
		this.timeout = timeout;
		this.no_new = no_new;
		elapsed = System.currentTimeMillis();
	}
	
	public void newStoryGenerated(boolean newOne, int nsas) {
		if( elapsed == 0 ) {
			elapsed = System.currentTimeMillis();			
		}
		else if( newOne && no_new)
		{
			// calculate how much of the stop condition was reached until the new story was found
			double waitingTime = (System.currentTimeMillis() - elapsed) / (double)timeout; 
			if( waitingTime > 0.5 ) {			
				System.out.println("Not finished yet... restarted after "+(System.currentTimeMillis() - elapsed)/1000+" seconds");
			}
			elapsed = 0; // restart
		}
	}

	public boolean stop() {
		if( elapsed == 0 )
			return false;
		
		return (System.currentTimeMillis() - elapsed) > timeout;
	}

	public String explanation() {
		if( !stop() ) {
			return "Why do you want an explanation for something that did not happened yet?";
		}
		String msg = "...we are waiting for "+((timeout/60)/1000)+" minutes";
		if( no_new ) {
			return msg + " for a new story and nothing new appeared.";
		}
		return msg;
	}
	
}
