/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package preprocessing;
import java.util.LinkedList;

import network.BlackSCCComputer;
import network.NELNetwork;

public class GraphSimplifier extends NELNetwork {
	
	LinkedList<LinkedList<Integer>> subSCCList;
	
	/*
	 * A backward bottleneck is a white node whose in-degree is 1. This method
	 * logically deletes all backward bottlenecks and directly connects their
	 * unique predecessor p to all their successors s (if p is not equal to s
	 * and if the edge (p,s) does not exist).
	 */
	protected boolean bb() {
		boolean answer = false;
		for (int i = 0; i < nn; i++) {
			if (!isBlack[i] && inDegree[i] == 1) {
				int p;
				for (p = 0; p < nn; p++) {
					if (adjacency[p][i]) {
						adjacency[p][i] = false;
						inDegree[i] = inDegree[i] - 1;
						outDegree[p] = outDegree[p] - 1;
						break;
					}
				}
				for (int s = 0; s < nn; s++) {
					if (adjacency[i][s]) {
						adjacency[i][s] = false;
						outDegree[i] = outDegree[i] - 1;
						inDegree[s] = inDegree[s] - 1;
						labels[p][s] += labels[p][i] + ";" + labels[i][s]; 
						if (!adjacency[p][s]) {
							adjacency[p][s] = true;
							outDegree[p] = outDegree[p] + 1;
							inDegree[s] = inDegree[s] + 1;
						}
					}
				}
				nodeAlive[i] = false;
				answer = true;
			}
		}
		return answer;
	}

	/*
	 * It logically deletes all black nodes which are targets or sources and are connect only to black nodes
	 */
	boolean deleteBlackEarrings() {
		boolean answer = false;
		for (int i = 0; i < nn; i++) {
			if( isBlack[i] && outDegree[i] == 0 && inDegree[i] > 0) {
				boolean onlyBlack = true;
				for(int j = 0; j < nn; j++) {
					if( adjacency[j][i] && !isBlack[j] )
					{
						onlyBlack = false;
						break;
					}
				}
				if( onlyBlack ) {
					//System.out.println(nodeName[i]);
					delete(i);
					answer = true;
				}
			}
		}

		for (int i = 0; i < nn; i++) {
			if( isBlack[i] && inDegree[i] == 0 && outDegree[i] > 0) {
				boolean onlyBlack = true;
				for(int j = 0; j < nn; j++) {
					if( adjacency[i][j] && !isBlack[j] )
					{
						onlyBlack = false;
						break;
					}
				}
				if( onlyBlack ) {					
					//System.out.println(nodeName[i]);
					delete(i);
					answer = true;
				}
			}
			
		}
		
		return answer;		
	}
	
	/*
	 * Delete isolated nodes (in-degree and out-degree equal to zero) from the graph
	 * 	
	 */
	void deleteIsolatedNodes() {
		for(int i = 0; i < nn; i++) {
			if( nodeAlive[i] && inDegree[i] == 0 && outDegree[i] == 0 )
				delete(i);
		}
	}
	
	/*
	 * It logically deletes all self-loops, that is, all edges (i,i).
	 */
	protected boolean deleteSelfLoops() {
		boolean answer = false;
		for (int i = 0; i < nn; i++) {
			if (adjacency[i][i]) {
				adjacency[i][i] = false;
				inDegree[i] = inDegree[i] - 1;
				outDegree[i] = outDegree[i] - 1;
				answer = true;
			}
		}
		return answer;
	}
	
	/*
	 * A forward bottleneck is a white node whose out-degree is 1. This method
	 * logically deletes all forward bottlenecks and directly connects all their
	 * predecessors p to their unique successor s (if p is not equal to s and if
	 * the edge (p,s) does not exist).
	 */
	protected boolean fb() {
		boolean answer = false;
		for (int i = 0; i < nn; i++) {
			if (!isBlack[i] && outDegree[i] == 1) {
				int s;
				for (s = 0; s < nn; s++) {
					if (adjacency[i][s]) {
						adjacency[i][s] = false;
						inDegree[s] = inDegree[s] - 1;
						outDegree[i] = outDegree[i] - 1;
						break;
					}
				}
				for (int p = 0; p < nn; p++) {
					if (adjacency[p][i]) {
						adjacency[p][i] = false;
						outDegree[p] = outDegree[p] - 1;
						inDegree[i] = inDegree[i] - 1;
						labels[p][s] += labels[p][i] + ";" + labels[i][s];
						if (!adjacency[p][s]) {
							adjacency[p][s] = true;
							outDegree[p] = outDegree[p] + 1;
							inDegree[s] = inDegree[s] + 1;
						}
					}
				}
				nodeAlive[i] = false;
				answer = true;
			}
		}
		return answer;
	}
	
	/*
	 * This method computes the strongly connected components of the graph. If the SCC contain only
	 * black nodes and has only incoming edges or only outgoing edges, i.e, it is a target or source SCC
	 * then all black nodes inside the SCC are merged (=considered one meta-node).
	 */
	boolean mergeBlackSCC() {
		boolean changed = false;
		BlackSCCComputer bSCC = new BlackSCCComputer(this);
		bSCC.run(0, true);
		//System.out.println("Number of components = " +bSCC.lastComponent);
		for(int c = 0; c < bSCC.getLastComponent(); c++) {
			boolean localChanged = false;
			int counter = 0;
			int[] scc = new int[bSCC.getComponentSize(c)];
			for(int i = 0; i < nn; i++) {
				if( bSCC.getComponent(i) == c )
				{
					scc[counter++] = i;
				}
			}
			
			int componentSize = bSCC.getComponentSize(c);
			while(!localChanged && componentSize > 1)
			{
				subSCCList = new LinkedList<LinkedList<Integer>>();
				computeSubSCC(scc, 0, componentSize, new LinkedList<Integer>());
				//System.out.println("Merge component "+c);
				//Utility.print(scc);
				
				for(LinkedList<Integer> subSCC: subSCCList) {
					//System.out.println("subSCC with "+componentSize+" nodes.");
					//Utility.print(subSCC);
					
					if( compactSCC(subSCC) ) {
						localChanged = true;
						break;
					}
				}
				componentSize = componentSize - 1;
			}
			if( localChanged )
				changed = true;
		}
		return changed;
	}
	
	private void computeSubSCC(int[] scc, int start, int count, LinkedList<Integer> subSCC)
	{
		if( count > 0 ) {
			for(int j = start; j < scc.length; j++) {
				LinkedList<Integer> result = new LinkedList<Integer>(subSCC);
				result.add(scc[j]);
				computeSubSCC(scc, j+1, count-1, result);
			}
		}
		else
			subSCCList.add(subSCC);
	}
	
	class Edge
	{
		Edge(int s, int t) {
			this.s = s;
			this.t = t;
		}
		
		int s;
		int t;
	}
	
	private boolean compactSCC(LinkedList<Integer> scc)
	{
		LinkedList<Edge> incoming = new LinkedList<Edge>();
		LinkedList<Edge> outgoing = new LinkedList<Edge>();

		for(Integer i: scc) {
			for(int j = 0; j < nn; j++) {
				if(adjacency[i][j] && !scc.contains(j))
				{
					outgoing.add(new Edge(i,j));
				}
				if(adjacency[j][i] && !scc.contains(j))
				{
					incoming.add(new Edge(j, i));
				}
			}
		}
	
		// target/source case
		if( scc.size() > 1 && ((incoming.size() == 0 && outgoing.size() > 0) || 
			(incoming.size() > 0 && outgoing.size() == 0)))
		{
			mergeComponent(scc, incoming, outgoing);
			return true;
		}
		
		// common hitting point case
		if( scc.size() > 1 &&
			incoming.size() > 0 && outgoing.size() > 0 && 
			commonHittingPoint(incoming, outgoing) ) {
			mergeComponent(scc, incoming, outgoing);
			return true;		
		}
		return false;
	}

	
	private void mergeComponent(LinkedList<Integer> sccNodes, LinkedList<Edge> incoming, LinkedList<Edge> outgoing)
	{
		// delete all nodes inside the SCC
		String newLabel = "";
		for(int i = 0; i < sccNodes.size(); i++) {
			newLabel = newLabel + nodeName[sccNodes.get(i)];
			delete(sccNodes.get(i));
		}
		
		int metaNode = sccNodes.get(0);
		nodeAlive[metaNode] = true;
		nodeName[metaNode] = newLabel;
	
		for (int p = 0; p < incoming.size(); p++) {
			adjacency[incoming.get(p).s][metaNode] = true;
			labels[incoming.get(p).s][metaNode] = labels[incoming.get(p).s][incoming.get(p).t] + ";" + nodeName[metaNode];
			inDegree[metaNode]++;
			outDegree[incoming.get(p).s]++;
		}
			
		for (int s = 0; s < outgoing.size(); s++) {
			adjacency[metaNode][outgoing.get(s).t] = true;
			labels[metaNode][outgoing.get(s).t] = labels[outgoing.get(s).s][outgoing.get(s).t] + ";" + nodeName[metaNode];
			inDegree[outgoing.get(s).t]++;
			outDegree[metaNode]++;
		}
		
	}
	
	private boolean commonHittingPoint(LinkedList<Edge> incoming, LinkedList<Edge> outgoing) {
		int hittingPoint = incoming.get(0).t;
		for(int i = 1; i < incoming.size(); i++)
			if( incoming.get(i).t != hittingPoint )
				return false;
		for(int i = 0; i < outgoing.size(); i++)
			if( outgoing.get(i).s != hittingPoint )
				return false;
		return true;
	}
	
	/*
	 * It reads the graph file, and, until nothing changes, it logically deletes
	 * forward bottlenecks, self-loops, and backward bottlenecks. Successively,
	 * it updates the set of alive nodes, recomputes the degrees of these nodes,
	 * and computes a mapping between the alive nodes and the number between 1
	 * and the number of alive nodes. Finally, it writes the graph on a file
	 * with the added suffix 'S'.
	 */
	public String run(String nn, boolean createMetaNodes) {
		readFile(nn + ".nel");
		printStatistics("readFile");
		boolean changed = true;
		while(changed) {
			
			while (changed) {
				changed = false;
				if (fb()) {
					printStatistics("fb");
					changed = true;
				}
				if (deleteSelfLoops()) {
					printStatistics("deleteSelfLoops");
					changed = true;
				}
				if (bb()) {
					printStatistics("bb");
					changed = true;
				}
				if (deleteSelfLoops()) {
					printStatistics("deleteSelfLoops");
					changed = true;
				}
				/*if( deleteBlackEarrings()){
					printStatistics(verbose, "deleteBlackEarrings");
					changed = true;
				}*/
			}
	
			if( createMetaNodes && mergeBlackSCC()){
				printStatistics("mergeBlackSCC");
				changed = true;				
			}
		}
		deleteIsolatedNodes();
		printStatistics("deleteIsolatedNodes");
		
		computeAliveNodes();
		computeDegrees();
		simplifyLabels();
		int numberOfAlive = reMap();
		String outputFile = nn+"_S";
		writeFile(outputFile + ".nel", numberOfAlive);
		return outputFile;
	}

	
}
