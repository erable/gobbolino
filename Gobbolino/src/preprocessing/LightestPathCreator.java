/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package preprocessing;

import network.NELNetwork;

public class LightestPathCreator extends NELNetwork {

	LightestPath lightestPath;
	
	double[][] dist;
	boolean[][] edgeAlive;

	/*
	 * For any edge (u,v) it checks whether it belongs to at least one shortest
	 * path or a lightest path between two black nodes s and t. To this aim, for
	 * each pair s and t of black nodes, (u,v) belongs to a shortest path from s
	 * to t if and only if dist[s][u]+dist[v][t]+1 is equal to dist[s][t], and
	 * (u,v) belongs to a lightest path from s to t if and only if
	 * dist[s][u]+dist[v][t] is equal to dist[s][t]. While checking this
	 * equality, it has to take into account the fact that some distances are
	 * equal to the integer maximum value, which represents an infinite value.
	 */
	void cruzAlgorithm(boolean weighted) 
	{
		initEdgeAlive();		
		for (int s = 0; s < nn; s++) {
			if (isBlack[s]) {
				for (int t = 0; t < nn; t++) {
					if (t != s && isBlack[t]) {
						for (int u = 0; u < nn; u++) {
							for (int v = 0; v < nn; v++) {
								if (adjacency[u][v]
										&& dist[s][u] < Integer.MAX_VALUE
										&& dist[v][t] < Integer.MAX_VALUE) {
									double distanceThroughUV = dist[s][u]
											+ dist[v][t];
									if (!weighted) {
										distanceThroughUV = distanceThroughUV + 1;
									}
									if (distanceThroughUV == dist[s][t]) {
										edgeAlive[u][v] = true;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	/*
	 * Set all the edges to be initially dead.
	 */
	void initEdgeAlive() {
		edgeAlive = new boolean[nn][nn];
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				edgeAlive[u][v] = false;
			}
		}
	}
	

	/*
	 * It reads the graph file, sets the weight of the edges, and computes the
	 * distance between any pair of nodes.
	 */
	public String run(String fn, boolean weighted, WeightFunction weightFunction) {
		readFile(fn + ".nel");
		lightestPath = new LightestPath(this, weighted, weightFunction);
		dist = lightestPath.computeDistanceMatrix(weighted);
		cruzAlgorithm(weighted);
		updateAdjacency();
		computeAliveNodes();
		computeDegrees();
		int numberOfAlive = reMap();
		String outputFile = fn + (weighted ? "_LP" : "_SP");
		writeFile(outputFile + ".nel", numberOfAlive);
		return outputFile;
	}

	/*
	 * Compute the adjacency matrix induced by the alive edges.
	 */
	private void updateAdjacency() {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				adjacency[i][j] = edgeAlive[i][j];
			}
		}
	}
	
	public double diameter() {
		double diameter = 0;
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				if( edgeAlive[u][v] && dist[u][v] > diameter )
					diameter = dist[u][v];
			}
		}
		return diameter;
	}
}
