package preprocessing;

public interface WeightFunction {

	// weights for nodes
	double weight(String nodename);
	// weights for arcs
	double weight(String sourceNodeName, String targetNodeName);
	
}
