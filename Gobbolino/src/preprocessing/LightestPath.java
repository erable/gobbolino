/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package preprocessing;

import network.NELNetwork;

public class LightestPath {

	NELNetwork network;
	WeightFunction function;
	boolean weighted;
	
	public LightestPath(NELNetwork network, boolean weighted, WeightFunction function) {
		this.network = network;
		this.weighted = false;
		this.function = function;
		initMatrices(weighted);
	}
	
	/*
	 * Inner class used for the Dijkstra algorithm in order to represent
	 * elements of the priority queue.
	 */
	class Element {
		int id;
		double weight;
	}

	/*
	 * Priority queue used by Dijkstra algorithm. See book by Crescenzi,
	 * Gambosi, Grossi.
	 */
	class Heap {
		int heapSize;
		Element[] heapArray;
		int[] heapArrayPosition;

		Heap(int n) {
			heapArray = new Element[n];
			heapSize = 0;
			heapArrayPosition = new int[n];
		}

		void decreaseKey(int id, double weight) {
			int i = heapArrayPosition[id];
			heapArray[i].weight = weight;
			heapify(i);
		}

		boolean isEmpty() {
			return heapSize == 0;
		}

		void enqueue(Element e) {
			heapArray[heapSize] = e;
			heapArrayPosition[e.id] = heapSize;
			heapSize = heapSize + 1;
			heapify(heapSize - 1);
		}

		Element dequeue() {
			Element minimum = heapArray[0];
			heapArray[0] = heapArray[heapSize - 1];
			heapSize = heapSize - 1;
			heapify(0);
			return minimum;
		}

		void heapify(int i) {
			while (i > 0 && heapArray[i].weight < heapArray[father(i)].weight) {
				swap(i, father(i));
				i = father(i);
			}
			while (left(i) < heapSize && i != minimumFatherSons(i)) {
				int son = minimumFatherSons(i);
				swap(i, son);
				i = son;
			}
		}

		int minimumFatherSons(int i) {
			int j = left(i);
			int k = j;
			if (k + 1 < heapSize) {
				k = k + 1;
			}
			if (heapArray[k].weight < heapArray[j].weight) {
				j = k;
			}
			if (heapArray[i].weight < heapArray[j].weight) {
				j = i;
			}
			return j;
		}

		int left(int i) {
			return 2 * i + 1;
		}

		int father(int i) {
			return (i - 1) / 2;
		}

		void swap(int i, int j) {
			Element tmp = heapArray[i];
			heapArray[i] = heapArray[j];
			heapArray[j] = tmp;
			heapArrayPosition[heapArray[i].id] = i;
			heapArrayPosition[heapArray[j].id] = j;
		}
	}

	double[][] weight, dist;

	/*
	 * Set the weight of the edges. If the graph is not weighted the weight of
	 * any edge is 1. Otherwise, the weight of edge (u,v) is equal to the
	 * out-degree of v.
	 */
	void initMatrices(boolean weighted) {
		weight = new double[network.getNumNodes()][network.getNumNodes()];
		for (int u = 0; u < network.getNumNodes(); u++) {
			for (int v = 0; v < network.getNumNodes(); v++) {
				if (!network.areAdjacent(u,v)) {
					weight[u][v] = Integer.MAX_VALUE;
				} else {
					if (weighted) {
						if( function != null ) { // asks for the weight of the arc
							weight[u][v] = function.weight(network.getNodeName(u), network.getNodeName(v));
						} else {	// uses the default weight, which is the out-degree of the target node
							weight[u][v] = network.getOutDegree(v);
						}
					} else {
						weight[u][v] = 1;
					}
				}
			}
		}
	}

	/*
	 * For each node u, it applies Dijkstra algorithm starting from u and sets
	 * the row of the distance matrix corresponding to u.
	 */
	public double[][] computeDistanceMatrix(boolean weighted) {
		dist = new double[network.getNumNodes()][network.getNumNodes()];
		for (int u = 0; u < network.getNumNodes(); u++) {
			dist[u] = dijkstra(u, weighted);
		}
		return dist;
	}

	/*
	 * Dijkstra algorithm. See book by Crescenzi, Gambosi, Grossi.
	 */
	double[] dijkstra(int s, boolean weighted) {
		Heap pq = new Heap(network.getNumNodes());
		double[] dist = new double[network.getNumNodes()];
		for (int u = 0; u < network.getNumNodes(); u = u + 1) {
			dist[u] = Integer.MAX_VALUE;
		}
		if (weighted) {
			if( function != null ) { // asks for the weight of the node
				dist[s] = function.weight(network.getNodeName(s));
			} else {	// uses the out-degree as weight
				dist[s] = network.getOutDegree(s);
			}
		} else {
			dist[s] = 0;
		}
		for (int i = 0; i < network.getNumNodes(); i = i + 1) {
			Element e = new Element();
			e.id = i;
			e.weight = dist[i];
			pq.enqueue(e);
		}
		while (!pq.isEmpty()) {
			Element e = pq.dequeue();
			int v = e.id;
			if (dist[v] < Integer.MAX_VALUE) {
				for (int u = 0; u < network.getNumNodes(); u = u + 1) {
					if (network.areAdjacent(v,u)) {
						if (dist[u] > dist[v] + weight[v][u]) {
							dist[u] = dist[v] + weight[v][u];
							pq.decreaseKey(u, dist[u]);
						}
					}
				}
			}
		}
		return dist;
	}
	
	public double diameter() {
		initMatrices(weighted);
		computeDistanceMatrix(weighted);
		double diameter = 0;
		for (int u = 0; u < network.getNumNodes(); u++) {
			for (int v = 0; v < network.getNumNodes(); v++) {
				if( dist[u][v] < Integer.MAX_VALUE && dist[u][v] > diameter )
					diameter = dist[u][v];
			}
		}
		return diameter;
	}
	

}
