package preprocessing;

//import fr.univ_lyon1.baobab.metabolicNetwork.GetMostFrequentAtomExchange;

public class AtomExchangeWeight implements WeightFunction {

	public double weight(String nodename) {
		return 0;
	}

	public double weight(String sourceNodeName, String targetNodeName) {
		sourceNodeName = sourceNodeName.replace(";", "");
		targetNodeName = targetNodeName.replace(";", "");
		int weight = 0; //GetMostFrequentAtomExchange.getMostFrequentAtomExchange(sourceNodeName, targetNodeName, "C");
		if( weight < 0 ) {
			System.out.println("No exchange atoms identified for "+sourceNodeName+" and "+targetNodeName);
			System.exit(0);
		}
		System.out.println("C atoms exchanged between "+sourceNodeName+" and "+targetNodeName+" is "+weight);
		if( weight == 0 ) {
			return Integer.MAX_VALUE;
		}
		return 1/(double)weight;
	}

}
