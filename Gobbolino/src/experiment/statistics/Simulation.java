/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package experiment.statistics;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import network.Convert2NEL;
import network.EL2NEL;
import network.NELNetwork;
import network.WCCPartition;
import network.XML2NEL;

import org.xml.sax.SAXException;

import preprocessing.GraphSimplifier;
import preprocessing.LightestPath;
import preprocessing.LightestPathCreator;
import story.Story;
import application.ComputeStories;
import application.InputParameters;
import application.InputParameters.EnumRandomBlackNodesMode;
import enumerator.OrderBasedEnumerator;
import enumerator.StoriesEnumeratorListener;
import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.Hypergraph;
import fr.univ_lyon1.baobab.metabolicNetwork.MetabolicNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkSBMLReader;


public class Simulation implements StoriesEnumeratorListener {

	public enum InputFormat { SBML, EL };
	
	private String simulationFile;

	private Random rnd = new Random();
	private HashMap<String, Integer> blackNodesSet = new HashMap<String, Integer>();
	private HashMap<String, Integer> reactionFrequency = new HashMap<String, Integer>();
	
	private int numberOfStories;
	private NELNetwork bns;
	LightestPathCreator allPairsShortestPath;	
	private WCCPartition scc;
	private Hypergraph metabolicNetwork;
	
	private BufferedWriter bwStoriesStatistics;
	
	private Convert2NEL inputFile;
	
	private String pickBlackNodes(int nbn) {
		// initialize all nodes as white
		HashMap<String, Double> blackMap = new HashMap<String, Double>();
		
		// chooses a new set of black nodes
		List<Integer> bn = new ArrayList<Integer>();
		for(int i = 0; i < nbn; i++) {
			int nextNode = rnd.nextInt(inputFile.getNumNodes());
			while( bn.contains(nextNode)) {
				nextNode = rnd.nextInt(inputFile.getNumNodes());
			}
			bn.add(nextNode);
			// make it black
			blackMap.put(inputFile.getNodeName(nextNode), 0.0);
		}
		inputFile.setBlackMap(blackMap);
		
		// generates an "id" to the set (concatenation of the ids of the nodes in the set)
		Collections.sort(bn);
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < bn.size(); i++)
			sb.append(bn.get(i)+";");
		
		return sb.toString();
	}
	
	public String buildLigthestPathNetwork(String fn, boolean weighted) {
		allPairsShortestPath = new LightestPathCreator();
		return allPairsShortestPath.run(fn, true, null);
	}
	
	public String buildBlackNodeScenario(String fn) {
		return (new GraphSimplifier()).run(fn,true);
	}
	
	public void generateStatisticsOnBNS(String lightestPathFile, String fn, int run) {
		File outFile = new File(fn+".cn");
		DecimalFormat df = new DecimalFormat("#.###");
		try {
			NELNetwork lightestPath = new NELNetwork();
			lightestPath.readFile(lightestPathFile+".nel");			
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			bw.write("ID " + fn + "\n");
			bw.write("nbBN_input " + inputFile.getBlackMap().size() + "\n");
			bw.write("nbVertices_input " + inputFile.getNumNodes() + "\n");
			bw.write("nbEdges_input " + inputFile.numberOfEdges() + "\n");

			bw.write("nbRandomSimulation " + run + "\n");
			
			bw.write("NbVertices " + bns.numberOfAlives() + "\n");
			bw.write("NbEdges " + bns.numberOfEdges() + "\n");
			bw.write("Density " + ((double)bns.numberOfEdges() / (double)(bns.numberOfAlives() * bns.numberOfAlives())) + "\n");

			bw.write("NbVerticesLP " + lightestPath.numberOfAlives() + "\n");
			bw.write("NbEdgesLP " + lightestPath.numberOfEdges() + "\n");
			
			//bw.write("Diameter " + allPairsShortestPath.diameter() + "\n");
			//bw.write("NbConnectedComponents " + scc.getNumComponents() + "\n");
			
			bw.write("NbBN " + bns.numberOfBlackNodes() + "\n");
			bw.write("NbWN " + (bns.numberOfAlives() - bns.numberOfBlackNodes()) + "\n");

			bw.write("NbCompressedVertices " + bns.totalNumberOfNodes() + "\n");
			
			bw.write("NodeCompression " + df.format(1-((double)bns.numberOfAlives()/inputFile.getNumNodes())) + "\n");
			bw.write("EdgeCompression " + df.format(1-((double)bns.numberOfEdges()/inputFile.numberOfEdges())) + "\n");
			
			
			//bw.write("NbMS " + numberOfStories + "\n");
			//
			//for(String reaction: reactionFrequency.keySet()) {
			//	bw.write("REAC "+reaction+" "+reactionFrequency.get(reaction)+"\n");
			//}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}		
	}

	public void analyseResultFiles(File[] files) {
		/*
		 * format of the file
		 * 
		 * 
		 * 
		 * 
		ID ./input/XML-Networks/ACYPI.xml_10_138_1.sim_LP_S
		nbBN_input 138
		nbVertices_input 921
		nbEdges_input 2050
		nbRandomSimulation 1
		NbVertices 246
		NbEdges 737
		Density 0.012178597395730056
		NbVerticesLP 355
		NbEdgesLP 848
		NbBN 117
		NbWN 129
		NbCompressedVertices 356
		NodeCompression 0,733
		EdgeCompression 0,64
		 * 
		 */
		if( files.length == 0 ) {
			System.out.println("No files to analyse.");
			return;
		}
		double minNodeCompressionBNS = Double.MAX_VALUE, minArcCompressionBNS = Double.MAX_VALUE, maxNodeCompressionBNS = Double.MIN_VALUE, maxArcCompressionBNS = Double.MIN_VALUE;
		double sumNodeCompressionBNS = 0.0, sumArcCompressionBNS = 0.0;

		double minNodeCompressionLP = Double.MAX_VALUE, minArcCompressionLP = Double.MAX_VALUE, maxNodeCompressionLP = Double.MIN_VALUE, maxArcCompressionLP = Double.MIN_VALUE;
		double sumNodeCompressionLP = 0.0, sumArcCompressionLP = 0.0;
		
		for(File cn: files) {
			try {
				System.out.println("Analysing file "+cn.getName());
				BufferedReader br = new BufferedReader(new FileReader(cn));
				String id = br.readLine();
				String nbBNs = br.readLine().split(" ")[1];
				String nbVertices = br.readLine().split(" ")[1];
				String nbArcs = br.readLine().split(" ")[1];
				String nbSim = br.readLine();
				String nbVertBNS = br.readLine().split(" ")[1];
				String nbArcsBNS = br.readLine().split(" ")[1];
				String density = br.readLine();
				String nbVertLP = br.readLine().split(" ")[1];
				String nbArcsLP = br.readLine().split(" ")[1];
				
				double nodeCompressionBNS = 1.0 - ((double)Integer.parseInt(nbVertBNS) / Integer.parseInt(nbVertices));
				double arcCompressionBNS  = 1.0 - ((double)Integer.parseInt(nbArcsBNS) / Integer.parseInt(nbArcs));
				
				double nodeCompressionLP = 1.0 - ((double)Integer.parseInt(nbVertLP) / Integer.parseInt(nbVertices));
				double arcCompressionLP  = 1.0 - ((double)Integer.parseInt(nbArcsLP) / Integer.parseInt(nbArcs));

				sumNodeCompressionBNS += nodeCompressionBNS;
				sumArcCompressionBNS += arcCompressionBNS;
				
				sumNodeCompressionLP += nodeCompressionLP;
				sumArcCompressionLP += arcCompressionLP;

				if( nodeCompressionBNS > maxNodeCompressionBNS) {
					maxNodeCompressionBNS = nodeCompressionBNS;
					if( maxNodeCompressionBNS == 1.0) {
						System.out.println("100% compression: "+cn.getName());
					}
				}
				if( arcCompressionBNS > maxArcCompressionBNS ) {
					maxArcCompressionBNS = arcCompressionBNS;
				}
				
				if( minNodeCompressionBNS > nodeCompressionBNS ) {
					minNodeCompressionBNS = nodeCompressionBNS;
				}
				if( minArcCompressionBNS > arcCompressionBNS ) {
					minArcCompressionBNS = arcCompressionBNS;
				}
				
				if( nodeCompressionLP > maxNodeCompressionLP) {
					maxNodeCompressionLP = nodeCompressionLP;
				}
				if( arcCompressionLP > maxArcCompressionLP ) {
					maxArcCompressionLP = arcCompressionLP;
				}
				
				if( minNodeCompressionLP > nodeCompressionLP ) {
					minNodeCompressionLP = nodeCompressionLP;
				}
				if( minArcCompressionLP > arcCompressionLP ) {
					minArcCompressionLP = arcCompressionLP;
				}				
				
				br.close();
			} catch (FileNotFoundException e) {
				System.out.println("File "+cn.getName()+" could not be opened because it does not exist.");
			} catch (IOException e) {
				System.out.println("Unexpected error while processing file "+cn.getName()+". Exception: "+e.getMessage());
				System.exit(0);
			}
		}
		
		// Presents the results
		System.out.println("---- Lightest path compression ---- ");
		System.out.println("Max node compression: "+maxNodeCompressionLP);
		System.out.println("Min node compression: "+minNodeCompressionLP);
		System.out.println("Max  arc compression: "+maxArcCompressionLP);
		System.out.println("Min  arc compression: "+minArcCompressionLP);
		System.out.println("Avg node compression: "+(sumNodeCompressionLP/files.length));
		System.out.println("Avg  arc compression: "+(sumArcCompressionLP/files.length));

		System.out.println("\n\n---- Preprocessing compression ---- ");
		System.out.println("Max node compression: "+maxNodeCompressionBNS);
		System.out.println("Min node compression: "+minNodeCompressionBNS);
		System.out.println("Max  arc compression: "+maxArcCompressionBNS);
		System.out.println("Min  arc compression: "+minArcCompressionBNS);
		System.out.println("Avg node compression: "+(sumNodeCompressionBNS/files.length));
		System.out.println("Avg  arc compression: "+(sumArcCompressionBNS/files.length));
	
	}
	
	public void run(String fn, InputFormat format) {		
		if( format == InputFormat.SBML) {
			// builds the graph from the xml file
			inputFile = new XML2NEL();	
			((XML2NEL)inputFile).readXMLFile(fn);
		}
		else if (format == InputFormat.EL ) {
			// builds the graph from the EL file
			inputFile = new EL2NEL();	
			((EL2NEL)inputFile).readELFile(fn);
		}
				
		if( numberOfStories > 0 ) {
			// Loads also as a metabolic network hold as an hypergraph 
		    metabolicNetwork = new Hypergraph();
			try 
			{
				metabolicNetwork.loadNetwork(new MetabolicNetworkSBMLReader(fn+".xml"), true);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			} catch (SAXException e) {
				e.printStackTrace();
				System.exit(0);
			}
		}
		ComputeStories storiesEnumerator = new ComputeStories();
		
		for(int blackNodes = InputParameters.randomBlackNodesMin; blackNodes <= InputParameters.randomBlackNodesMax; blackNodes = blackNodes + InputParameters.randomBlackNodesIncrement) {
			
			// For each run
			for(int run = 1; run <= InputParameters.simulationRuns; run++) {
				// Randomly choose the black nodes
				int nbn = blackNodes;
				if( InputParameters.randomblackNodesMode == EnumRandomBlackNodesMode.PERCENTAGE ) {
					nbn = (int) ( (blackNodes/100.0) * inputFile.getNumNodes() );
				}
				String blackNodesHash = pickBlackNodes(nbn);
				while( blackNodesSet.containsKey(blackNodesHash) ) {
					blackNodesHash = pickBlackNodes(nbn);
				}
				blackNodesSet.put(blackNodesHash, 1);
				inputFile.initDegrees();
				inputFile.deleteSelfLoops();
				
				reactionFrequency.clear();
				numberOfStories = 0;
	
				System.out.println("Simulation for "+fn+" network with "+InputParameters.simulationRuns+" runs and "+nbn+" black nodes producing at most "+InputParameters.stories+" stories.");
				simulationFile = fn+"_"+InputParameters.stories+"_"+nbn+"_"+run+".sim"; 
				inputFile.writeFile(simulationFile+".nel");
				
				String lightestPathFile = buildLigthestPathNetwork(simulationFile, true);
				simulationFile = buildBlackNodeScenario(lightestPathFile);
				
				bns = new NELNetwork();
				bns.readFile(simulationFile+".nel");
	
				if( numberOfStories > 0 ) {
					scc = new WCCPartition(bns);
					scc.run();
					if( scc.getNumComponents() == 1 ) {
						File storiesStatisticsFile = new File(simulationFile+".ms");
						try {
							bwStoriesStatistics = new BufferedWriter(new FileWriter(storiesStatisticsFile));
							InputParameters.inputFile = simulationFile;
							storiesEnumerator.run(null, this);
							bwStoriesStatistics.write("//");
							bwStoriesStatistics.close();
						} catch (Exception e) {
							e.printStackTrace();
							System.exit(-1);
						}						
					}
				}
				generateStatisticsOnBNS(lightestPathFile, simulationFile, run);
			}
		}
	}

	/*
		IDMS	1		# First metabolic story
		IDCN	3		# ID of the compressed metabolic network 
		NbSources	4
		NbTargets	4
		NbIntermediates	5
		ScoreCompression	0.8	# Compression Score
		NbVertices	45
		NbEdges	58
		Density	0.5
		Diameter	5
		NbBN	5		# nb of black nodes
		NbWN	45		# nb of white nodes
		NbCompressedVertices	28	# Number of compressed metabolites 
		NbVerticesBP	58	# Stats in bipartite network
		NbEdgesBP	89
		DensityBP	90
		DiameterBP	50
		NbReactions	30
		NbMetabolites	28
		NbBN_BP	5		# nb of black nodes
		NbWN_BP	45		# nb of white nodes
		//
	 */
	public void onNewStoryGenerated(int numStory, OrderBasedEnumerator enumerator, Story story, Double[] score) {
		numberOfStories = numberOfStories + 1;
		try {
			if( numberOfStories > 1 )
				bwStoriesStatistics.write("//\n");
			bwStoriesStatistics.write("IDMS " + numStory + "\n");  // id of the metabolic story
			bwStoriesStatistics.write("IDCN " + simulationFile + "\n");   // id of the simulation
			bwStoriesStatistics.write("NbSources "+story.numberOfSources(true) +"\n");
			bwStoriesStatistics.write("NbTargets "+story.numberOfTargets(true) +"\n");
			bwStoriesStatistics.write("NbIntermediates "+(story.numberOfAlives()-(story.numberOfSources(true)+story.numberOfTargets(true)))+"\n");
			bwStoriesStatistics.write("ScoreCompression "+score+"\n");
			bwStoriesStatistics.write("NbVertices "+story.numberOfAlives()+"\n");
			bwStoriesStatistics.write("NbEdges " + story.numberOfEdges() + "\n");
			bwStoriesStatistics.write("Density " + ((double)story.numberOfEdges() / (double)(story.numberOfAlives() * story.numberOfAlives())) + "\n");

			LightestPath allPairsShortestPath = new LightestPath(story, false, null);
			bwStoriesStatistics.write("Diameter " + allPairsShortestPath.diameter() + "\n");
			bwStoriesStatistics.write("NbBN " + story.numberOfBlackNodes() + "\n");
			bwStoriesStatistics.write("NbWN " + (story.numberOfAlives()-story.numberOfBlackNodes()) + "\n");
			bwStoriesStatistics.write("NbCompressedVertices " + story.totalNumberOfNodes() + "\n");
			
			MetabolicNetwork subgraph = metabolicNetwork.subgraphFromCompounds(buildListOfCompounds(story));
			bwStoriesStatistics.write("NbVerticesBP " + (subgraph.getReactions().size()+subgraph.getCompounds().size()) + "\n");
			int numberOfEdges = numberOfEdges(subgraph);
			bwStoriesStatistics.write("NbEdgesBP " + numberOfEdges + "\n");
			bwStoriesStatistics.write("DensityBP " + ((double)numberOfEdges / (double)(subgraph.getReactions().size()+subgraph.getCompounds().size())) + "\n");
			bwStoriesStatistics.write("NbMetabolites " + subgraph.getCompounds().size() + "\n");
			bwStoriesStatistics.write("NbReactions " + subgraph.getReactions().size() + "\n");
			bwStoriesStatistics.write("NbBN " + story.totalNumberOfBlackNodes() + "\n");
			bwStoriesStatistics.write("NbWN " + (subgraph.getCompounds().size()-story.totalNumberOfBlackNodes()) + "\n");
			//subgraph.exportAsXML("story"+story.getId()+".xml", null);
			
			for(Reaction reaction: subgraph.getReactions()) {
				if( reactionFrequency.containsKey(reaction.getId())) {
					int freq = reactionFrequency.get(reaction.getId());
					reactionFrequency.put(reaction.getId(), freq+1);
				}
				else
					reactionFrequency.put(reaction.getId(), 1);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}				
	}
	
	private List<Compound> buildListOfCompounds(Story story) {
		List<Compound> compounds = new ArrayList<Compound>();
		for(String cpd: story.allNodeNames()) {
			compounds.add(new Compound(cpd, cpd, null));
		}
		return compounds;
	}
	
	private int numberOfEdges(MetabolicNetwork graph) {
		int numberOfEdges = 0;
		for(Reaction r: graph.getReactions()) {
			numberOfEdges += r.getSubstrates().size() + r.getProducts().size();
		}
		return numberOfEdges;
	}
	
}
