package experiment.pathways;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import network.NELNetwork;
import utils.Utility;

public class ImportStoriesSubgraph {

	public static List<NELNetwork> stories = new ArrayList<NELNetwork>();
	
	public static void importStories(String inFile)
	{
		stories = new ArrayList<NELNetwork>();
		try {
			StringBuffer strStory = new StringBuffer();
			
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			
			// 1st line - id of the story (ignoring - not canonical NEL)
			int processing = 1;
			String line = br.readLine();
			while(line != null)
			{
				// 2nd line - order that produced the story ignoring (not canonical NEL format)
				line = br.readLine();
				// 3rd line - beginning of canonial NEL format (except for the role of the black nodes in the story, that have to be supressed)
				line = br.readLine();
				boolean start = true;
				while(line != null && line.charAt(0) != '#')
				{
					strStory.append(line+"#");

					if( start ) {
						Utility.printMsg("Processing story #"+processing++);
						int numberOfBlackNodes = Integer.parseInt(line.split(" ")[1]);
						for(int i = 0; i < numberOfBlackNodes; i++) {
							line = br.readLine();
							strStory.append(line.split(" ")[0]+"#");
						}
						start = false;
					}
					line = br.readLine();
				}
				
				NELNetwork story = new NELNetwork();
				String[] NELInStrings = strStory.toString().split("#");
				story.readStrings(NELInStrings);
				stories.add(story);
				
				strStory = new StringBuffer();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
