/* 
Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package experiment.pathways;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import network.NELNetwork;
import network.XML2NEL;

import org.jgrapht.graph.DirectedSubgraph;

import preprocessing.LightestPathCreator;
import preprocessing.WeightFunction;
import story.UncompressStory;
import utils.SBMLTools;
import utils.Utility;
import application.InputParameters;
import application.InputParameters.EnumBlackNodes;
import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.Hypergraph;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.Stoichiometry;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkSBMLReader;
import fr.univ_lyon1.baobab.metabolicNetwork.topological.SeedSearcher;

public class PathwayCoveringSimulationAccuracy extends XML2NEL implements WeightFunction {

	private static int TP;
	private static int FP;
	private static int FN;
	
	private static int bestTPArcs;
	private static int bestFPArcs;
	private static int bestFNArcs;
	private static int bestStoryIndexArcs;

	private static int bestTPNodes;
	private static int bestFPNodes;
	private static int bestFNNodes;
	private static int bestStoryIndexNodes;
	
	private XML2NEL network;
	
	public void run(String networkFile, String pathwaysPath) {
		System.out.println("Computing the accuracy of "+networkFile+" pathways.");
		
		File[] pathwayFiles = new File(pathwaysPath+"/pathways").listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".xml"); 
			}
		});
		
		File[] bnFiles = new File(pathwaysPath+"/BNs").listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".bn"); 
			}
		});
		
		// builds the graph from the xml file
		network = new XML2NEL();
		network.readXMLFile(networkFile + ".xml");
		network.initDegrees();
		network.deleteSelfLoops();

		// result file of the experiment
		PathwayCoveringOutput results = new PathwayCoveringOutput("pc-reaction-graph");		
		
		for(int i = 0; i < pathwayFiles.length; i++) {
			// Check if we have for the i-th xml file the information on the black nodes and order of nodes
			String xmlFileName = pathwayFiles[i].getName().substring(0, pathwayFiles[i].getName().length()-4);			
			String storiesFileName = xmlFileName+".sto";
			//Utility.printMsg();
			System.out.println("Processing Pathway "+xmlFileName);

			// loads the NEL Network with lightest paths between black nodes built for the given pathway
			// (and from which the stories were computed)
			String[] networkFilePath = networkFile.split("/");
			String pathwayNetworkFile = "./output/pathway/pathways/"+networkFilePath[networkFilePath.length-1]+"_for_"+xmlFileName+"_LP.nel";
			NELNetwork pathwayNetwork = new NELNetwork();
			pathwayNetwork.readFile(pathwayNetworkFile);
			
			// builds the graph for the current pathway
			readXMLFile(pathwaysPath+"/pathways/"+pathwayFiles[i].getName());
			initDegrees();
			deleteSelfLoops();
			
			File blackNodesFile = null;
			
			if( InputParameters.blackNodesMode == EnumBlackNodes.FILE) {
				blackNodesFile = findFile(bnFiles, xmlFileName);
			}
			else {
				// for each pathway, automatically extract the list of black nodes 
				extractBlackNodes("./"+pathwaysPath+"/"+pathwayFiles[i].getName(), "./output/automaticBNs/"+xmlFileName);
				blackNodesFile = new File("./output/automaticBNs/"+xmlFileName+".bn");
			}

			// read and check if the black nodes were identified
			int nbLines = readBlackNodesNamesFile(blackNodesFile);
			if( numberOfIdentifiedBlackNodes() != nbLines )
			{
				System.out.println("Not all black nodes identified. Expected = "+nbLines+" found = "+numberOfIdentifiedBlackNodes());
			}
			else
			{
				initDegrees();
				deleteSelfLoops();
				
				// log the black nodes chosen (verbose mode)
				for (String name : blackMap.keySet()) {
					Utility.printMsg("Black node: "+name+" inDegree: "+inDegree[map.get(name)]+". outDegree: "+outDegree[map.get(name)]);
				}									
				
				// generating the compressed network for the pathway (with its black nodes) on the output folder
				String simulationFile = "./output/pathway/convertedPathways/"+xmlFileName+"_storyVersion";
				writeFile(simulationFile+".nel");
				
				// defines the .NEL file to be preprocessed
				// computes the lightest paths but providing as weights the out-degrees from the original network
				// and not the ones in the pathway subgraph
				simulationFile = (new LightestPathCreator()).run(simulationFile, true, this);
				
				// bns correspond to the "story version" of the pathway
				NELNetwork pathwayNEL = new NELNetwork();
				pathwayNEL.readFile(simulationFile+".nel");
				
				// now we have to compare bns with each one of the stories computed for this pathway
				// Finds the stories file associated with the pathway
				ImportStoriesSubgraph.importStories("./output/stories/"+storiesFileName);
				bestTPArcs = 0; bestFPArcs = 0; bestFNArcs = 0; bestStoryIndexArcs = -1; NELNetwork bestStoryArcs = null;
				bestTPNodes = 0; bestFPNodes = 0; bestFNNodes = 0; bestStoryIndexNodes = -1; NELNetwork bestStoryNodes = null;
				for(int story=0; story < ImportStoriesSubgraph.stories.size(); story++)
				{
					NELNetwork compressedStoryNEL = ImportStoriesSubgraph.stories.get(story);
					NELNetwork storyNEL = UncompressStory.uncompress(pathwayNetwork, compressedStoryNEL);
					
					compareNetworkArcs(pathwayNEL, storyNEL);
					if( bestStoryArcs == null || getAcc() > getBestArcsAcc() ) {
						updateBestArcs(story);
						bestStoryArcs = storyNEL;
						bestStoryIndexArcs = story;
					}
					
					compareNetworkNodes(pathwayNEL, storyNEL);
					if( bestStoryNodes == null || getAcc() > getBestNodesAcc() ) {
						updateBestNodes(story);
						bestStoryNodes = storyNEL;
						bestStoryIndexNodes = story;
					}
				}
				if( bestStoryArcs != null && bestStoryNodes != null ) {
					results.register(xmlFileName, pathwayNEL.numberOfAlives(), pathwayNEL.numberOfEdges(), 
									 ""+bestStoryIndexArcs, bestStoryArcs.numberOfAlives(), bestStoryArcs.numberOfEdges(), bestTPArcs, bestFPArcs, bestFNArcs, getBestArcsSn(), getBestArcsPPV(), getBestArcsAcc(), bestStoryArcs.numberOfBlackNodes(), 
									 ""+bestStoryIndexNodes, bestStoryNodes.numberOfAlives(), bestStoryNodes.numberOfEdges(), bestTPNodes, bestFPNodes, bestFNNodes, getBestNodesSn(), getBestNodesPPV(), getBestNodesAcc(), bestStoryNodes.numberOfBlackNodes() );
				}
				System.out.println("\n%%%%%%\nFinished.\n%%%%%%%");
			}
		}
		results.finish();
	}
	
	private int numberOfIdentifiedBlackNodes() {
		int result = 0;
		for (String name : blackMap.keySet()) {
			if( map.get(name) != null )
				result = result + 1;
		}		
		return result;
	}
	
	private File findFile(File[] files, String name)
	{
		for(int i = 0; i < files.length; i++) {
			if( files[i].getName().contains(name) ) {
				return files[i];
			}
		}
		return null;
	}

	private int readBlackNodesNamesFile(File blackFile) {
		int nbLines = 0;
		try {
			BufferedReader bbr = new BufferedReader(new FileReader(blackFile));
			String line = bbr.readLine();
			blackMap = new HashMap<String, Double>();
			while (line != null) {
				nbLines++;
				String[] lineWords = line.split(" ");
				String nodeName = SBMLTools.sbmlEncode(lineWords[0].trim());
				if( lineWords.length > 1 ) {
					double intensity = 0.0;
					try {
						intensity = new Double(lineWords[1]);
					}
					catch(Exception e) {
						System.err.println("Error reading black nodes file. Expected compound intensity. Found: "+lineWords[1]);
					}
					blackMap.put(nodeName, intensity);					
				} else
				{
					blackMap.put(nodeName, 0.0);
				}
				line = bbr.readLine();
			}
			bbr.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return nbLines;
	}	
	
	private void extractBlackNodes(String pathwayFileName, String blackNodeFileName) {
		try
		{
			Hypergraph pathway = new Hypergraph();
			pathway.loadNetwork(new MetabolicNetworkSBMLReader(pathwayFileName), false);
			
			File bnFile = new File(blackNodeFileName+".bn");
			BufferedWriter bw = new BufferedWriter(new FileWriter(bnFile));
			List<Compound> seedsAndTargets = new ArrayList<Compound>();
			if( InputParameters.blackNodesMode == EnumBlackNodes.AUTO_BORENSTEIN_ALL )
				seedsAndTargets.addAll( SeedSearcher.findSeedsAndTargets(pathway, null) );
			else if( InputParameters.blackNodesMode == EnumBlackNodes.AUTO_BORENSTEIN_ONE )
			{
				List<DirectedSubgraph<Compound, Reaction>> sccs = SeedSearcher.getStronglyConnectedComponents(pathway);
				for(DirectedSubgraph<Compound,Reaction> scc: sccs) {
					// for each scc, compute the min degree (out or in-degree, depending on the source/target property of the scc)
					int minDegree = Integer.MAX_VALUE;
					for(Compound c: scc.vertexSet()) {
						int nodeDegree = 0;
						if( c.isSeed() )
							nodeDegree = inDegree(c);
						else
							nodeDegree = outDegree(c);
						if( minDegree > nodeDegree )
							minDegree = nodeDegree;
					}
					// and add all the minDegree nodes to the list of seeds/targets
					for(Compound c: scc.vertexSet()) {
						int nodeDegree = 0;
						if( c.isSeed() )
							nodeDegree = inDegree(c);
						else
							nodeDegree = outDegree(c);
						if( minDegree == nodeDegree )
							seedsAndTargets.add(c);
					}
				}
			}
			else if( InputParameters.blackNodesMode == EnumBlackNodes.AUTO_SOURCE_TARGET ) 
				seedsAndTargets.addAll( SeedSearcher.findTopologicalSeedsAndTargets(pathway) );
			else if( InputParameters.blackNodesMode == EnumBlackNodes.AUTO_REACTION_GRAPH_BORENSTEIN ) {
				for(Reaction r: SeedSearcher.findSeedsAndTargetsConvertingToReactionGraph(pathway) ) {
					if( r.getTopologicalSeed() ) {
						for(Stoichiometry s: r.getSubstrates()) {
							if( !seedsAndTargets.contains(s.getCompound())) {
								seedsAndTargets.add(s.getCompound());
								s.getCompound().setSeed(true);
							}
						}
						if( r.getReversible() ) {
							for(Stoichiometry s: r.getProducts()) {
								if( !seedsAndTargets.contains(s.getCompound())) {
									seedsAndTargets.add(s.getCompound());
									s.getCompound().setTarget(true);
								}
							}
						}
					}
					else if( r.getTopologicalTarget() ) {
						for(Stoichiometry s: r.getProducts()) {
							if( !seedsAndTargets.contains(s.getCompound())) {
								seedsAndTargets.add(s.getCompound());
								s.getCompound().setTarget(true);
							}
						}
						if( r.getReversible() ) {
							for(Stoichiometry s: r.getSubstrates()) {
								if( !seedsAndTargets.contains(s.getCompound())) {
									seedsAndTargets.add(s.getCompound());
									s.getCompound().setSeed(true);
								}
							}
						}
					}

				}
			}
			
			for(int i = seedsAndTargets.size()-1; i >= 0; i--)
			{
				if( validBlackNode(seedsAndTargets.get(i), 60, 60) )
					bw.write(seedsAndTargets.get(i).getId()+"\n");
				else
					seedsAndTargets.remove(i);
			}
			bw.close();

			// define the ordering of the identified black nodes
			File orderFile = new File(blackNodeFileName+".order");
			bw = new BufferedWriter(new FileWriter(orderFile));
			for(Compound s: seedsAndTargets)
			{
				if( s.isSeed() && !s.isTarget())
				{
					for(Compound t: seedsAndTargets) {
						if( t.isTarget() && !t.isSeed() ) {
							bw.write(SBMLTools.sbmlEncode(s.getId()) + " < "+SBMLTools.sbmlEncode(t.getId())+"\n");
						}
					}
				}
				
			}
			bw.close();
		
		
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error creating file with extracted black nodes.\n"+e.getMessage());
		}
	}
	
	private boolean validBlackNode(Compound c, int inThreshold, int outThreshold) {
		for(int i = 0; i < nodeName.length; i++) {
			if( nodeName[i].equals(c.getId()) ) {
				//System.out.println("Validating "+(c.isSeed() ? "seed ":"target ")+c.getId()+". InDegree="+inDegree[i]+", outDegree="+outDegree[i]);
				if( (c.isSeed() && inDegree[i] <= inThreshold) || (c.isTarget() && outDegree[i] <= outThreshold) )
				{
					return true;
				}
				else
				{
					System.out.println(c.getId()+" rejected");
					return false;
				}
			}
		}
		return true;
	}
	
	private int inDegree(Compound c) {
		for(int i = 0; i < nodeName.length; i++) {
			if( nodeName[i].equals(c.getId()) ) {
				return inDegree[i];
			}
		}	
		return Integer.MAX_VALUE;
	}
	
	private int outDegree(Compound c) {
		for(int i = 0; i < nodeName.length; i++) {
			if( nodeName[i].equals(c.getId()) ) {
				return outDegree[i];
			}
		}	
		return Integer.MAX_VALUE;
	}
	
	public static double getSn() {
		if( (TP + FN) == 0)
			return 0;
		
		return (double)TP / (TP + FN);
	}
	
	public static double getPPV() {
		if( (TP + FP) == 0)
			return 0;

		return (double)TP / (TP + FP);
	}
	
	public static double getAcc() { 
		return Math.sqrt(getSn() * getPPV());
	}

	public static double getBestArcsSn() {
		if( (bestTPArcs + bestFNArcs) == 0)
			return 0;
		
		return (double)bestTPArcs / (bestTPArcs + bestFNArcs);
	}
	
	public static double getBestArcsPPV() {
		if( (bestTPArcs + bestFPArcs) == 0)
			return 0;

		return (double)bestTPArcs / (bestTPArcs + bestFPArcs);
	}
	
	public static double getBestNodesSn() {
		if( (bestTPNodes + bestFNNodes) == 0)
			return 0;
		
		return (double)bestTPNodes / (bestTPNodes + bestFNNodes);
	}
	
	public static double getBestNodesPPV() {
		if( (bestTPNodes + bestFPNodes) == 0)
			return 0;

		return (double)bestTPNodes / (bestTPNodes + bestFPNodes);
	}	
	
	public static double getBestArcsAcc() {
		return Math.sqrt(getBestArcsSn() * getBestArcsPPV());
	}
	
	public static double getBestNodesAcc() {
		return Math.sqrt(getBestNodesSn() * getBestNodesPPV());
	}

	private static void updateBestArcs(int story) {
		bestTPArcs = TP;
		bestFPArcs = FP;
		bestFNArcs = FN;
	}
	
	private static void updateBestNodes(int story) {
		bestTPNodes = TP;
		bestFPNodes= FP;
		bestFNNodes = FN;
	}	

	private static void compareNetworkArcs(NELNetwork pathway, NELNetwork story) {
		Utility.printMsg("\n\nStarting arc comparison between pathway and story.");
		TP = 0; FP = 0;
		
		// check if each arc present in the story corresponds to an arc on the pathway
		for(int i = 0; i < story.getNumNodes(); i++) {
			
			if( story.isAlive(i) ) {
			
				for(int j = 0; j < story.getNumNodes(); j++) {
					if( story.isAlive(j) && story.areAdjacent(i,  j) ) {
						// check if this arc is also present in the pathway
						int k = pathway.getIdFromNodeName(story.getNodeName(i));
						int l = pathway.getIdFromNodeName(story.getNodeName(j));
						
						if( k == -1 || l == -1 || !pathway.isAlive(k) || !pathway.isAlive(l) || !pathway.areAdjacent(k, l)) {
							Utility.printMsg("Arc "+story.getNodeName(i)+" -> "+story.getNodeName(j)+" found in story but not in pathway. FP!");
							FP++;
						}
						else {
							Utility.printMsg("Arc "+story.getNodeName(i)+" -> "+story.getNodeName(j)+" found both in story and in pathway. TP!");
							TP++;
						}
					}
				}
			}
		}
		
		FN = 0;
		// check if each arc present in the pathway corresponds to an arc on the story
		for(int i = 0; i < pathway.getNumNodes(); i++) {
			
			if( pathway.isAlive(i) ) {
				for(int j = 0; j < pathway.getNumNodes(); j++) {
					if( pathway.isAlive(j) && pathway.areAdjacent(i, j)  ) {
						// check if this arc is also present in the story
						// check if this arc is also present in the pathway
						String[] nodeI = pathway.getNodeName(i).split(";");
						String[] nodeJ = pathway.getNodeName(j).split(";");
						
						int k = story.getIdFromNodeName(nodeI[0]);
						int l = story.getIdFromNodeName(nodeJ[0]);
						
						if( k == -1 || l == -1 || !story.isAlive(k) || !story.isAlive(l) || !story.areAdjacent(k, l)) {
							Utility.printMsg("Arc "+pathway.getNodeName(i)+" -> "+pathway.getNodeName(j)+" found in pathway but not in story. FN!");
							FN++;
						}
					}
				}
			}
		}		
		
	}
	
	private static void compareNetworkNodes(NELNetwork pathway, NELNetwork story) {
		Utility.printMsg("\n\nStarting node comparison between pathway and story.");
		TP = 0; FP = 0;
		
		// check if each node present in the story corresponds to node on the pathway
		for(int i = 0; i < story.getNumNodes(); i++) {
			int k = pathway.getIdFromNodeName(story.getNodeName(i));
			if( k == -1 || !pathway.isAlive(k) ) {
				Utility.printMsg("Node "+story.getNodeName(i)+" found in story but not in pathway. FP!");
				FP++;
			}
			else {
				Utility.printMsg("Node "+story.getNodeName(i)+" found both in story and in pathway. TP!");
				TP++;
			}
		}
		
		FN = 0;
		// check if each node present in the pathway corresponds to a node on the story
		for(int i = 0; i < pathway.getNumNodes(); i++) {
			
			if( pathway.isAlive(i) ) {
				for(int j = 0; j < pathway.getNumNodes(); j++) {
					if( pathway.isAlive(j) && pathway.areAdjacent(i, j)  ) {
						// check if this arc is also present in the story
						// check if this arc is also present in the pathway
						String[] nodeI = pathway.getNodeName(i).split(";");
						String[] nodeJ = pathway.getNodeName(j).split(";");
						
						int k = story.getIdFromNodeName(nodeI[0]);
						int l = story.getIdFromNodeName(nodeJ[0]);
						
						if( k == -1 || l == -1 || !story.isAlive(k) || !story.isAlive(l) || !story.areAdjacent(k, l)) {
							Utility.printMsg("Arc "+pathway.getNodeName(i)+" -> "+pathway.getNodeName(j)+" found in pathway but not in story. FN!");
							FN++;
						}
					}
				}
			}
		}		
		
	}

	public double weight(String nodename) {
		String[] node = nodename.split(";");
		int i = network.getIdFromNodeName(node[0]);
		if( i > -1 ) {
			return network.getOutDegree(i);
		}
		else {
			return 0;
		}
	}

	public double weight(String sourceNodeName, String targetNodeName) {
		String[] node = targetNodeName.split(";");
		int i = network.getIdFromNodeName(node[0]);
		if( i > -1 ) {
			return network.getOutDegree(i);
		}
		else {
			return 0;
		}
	}
	
}
