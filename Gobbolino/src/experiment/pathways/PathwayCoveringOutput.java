package experiment.pathways;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;

public class PathwayCoveringOutput {

	private File outputFile = null;
	private BufferedWriter writer = null;
	private String experimentName;
	
	public PathwayCoveringOutput(String experimentName) {
		this.experimentName = experimentName;
	}

	public void finish() {
		if( writer != null ) {
			try {
				writer.close();
			}
			catch(Exception e) {
				System.out.println("Error trying to close file.");
			}
		}
	}
	
	private void writeHeader() {
		try {
			writer.write("experiment;" +
					"pathway;" +
					"#C;" +
					"#A;" +
					"bestStoryArcs;" +
					"#C;" +
					"#A;" +
					"TP;" +
					"FP;" +
					"FN;" +
					"Sn;" +
					"PPV;" +
					"Acc;" +
					"#BN;" +
					"bestStoryNodes;" +
					"#C;" +
					"#A;" +
					"TP;" +
					"FP;" +
					"FN;" +
					"Sn;" +
					"PPV;" +
					"Acc;" +
					"#BN\n");
		}
		catch(Exception e) {
			System.out.println("Error trying to write to the output file.");
		}		
	}
	
	public void register(String pathway, int nCompPathway, int nReacPathway, String storyArcsId, int nCompStoryArcs, int nReacStoryArcs,
			int TPArcs, int FPArcs, int FNArcs, double SnArcs, double PPVArcs, double AccArcs, int bnArcs, String storyNodesId, int nCompStoryNodes, 
			int nReacStoryNodes, int TPNodes, int FPNodes, int FNNodes, double SnNodes, double PPVNodes, double AccNodes, int bnNodes) {
		if( writer == null ) {
			prepareFile();
		}
		
		DecimalFormat df = new DecimalFormat("0.000");
		try {
			writer.write(experimentName + ";" +
					pathway + ";" +
					nCompPathway + ";" +
					nReacPathway + ";" +
					storyArcsId + ";" +
					nCompStoryArcs + ";" +
					nReacStoryArcs + ";" +
					TPArcs + ";" +
					FPArcs + ";" +
					FNArcs + ";" +
					df.format(SnArcs) + ";" +
					df.format(PPVArcs) + ";" +
					df.format(AccArcs) + ";" +
					bnArcs + ";" +
					storyNodesId + ";" +
					nCompStoryNodes + ";" +
					nReacStoryNodes + ";" +
					TPNodes + ";" +
					FPNodes + ";" +
					FNNodes + ";" +
					df.format(SnNodes) + ";" +
					df.format(PPVNodes) + ";" +
					df.format(AccNodes) + ";" +
					bnNodes + "\n"					
					);
		}
		catch(Exception e) {
			System.out.println("Error trying to write to the output file.");
		}
	}
	
	private void prepareFile() {
		outputFile = new File(experimentName+".csv");
		try {
			writer = new BufferedWriter(new FileWriter(outputFile));
			writeHeader();
		}
		catch(Exception e) {
			System.out.println("Error trying to create output file "+experimentName+".csv");
		}
	}
	
}
