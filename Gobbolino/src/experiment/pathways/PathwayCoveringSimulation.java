/* 
Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package experiment.pathways;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import network.NELNetwork;
import network.WCCPartition;
import network.XML2NEL;

import org.jgrapht.graph.DirectedSubgraph;

import score.ComputeScore;
import score.PartialOrder;
import score.PartialOrderScore;
import utils.SBMLTools;
import utils.Utility;
import application.ComputeBlackNodeScenario;
import application.ComputeStories;
import application.InputParameters;
import application.InputParameters.EnumBlackNodes;
import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.Hypergraph;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.Stoichiometry;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkSBMLReader;
import fr.univ_lyon1.baobab.metabolicNetwork.topological.SeedSearcher;

public class PathwayCoveringSimulation extends XML2NEL {

	public void run(String networkFile, String pathwaysPath) {
		System.out.println("Trying to cover "+networkFile+" pathways.");
		
		File[] bnFiles = new File(pathwaysPath+"/BNs").listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".bn"); 
			}
		});

		File[] orderFiles = new File(pathwaysPath+"/order").listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".order"); 
			}
		});

		File[] pathwayFiles = new File(pathwaysPath+"/pathways").listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".xml"); 
			}
		});

		// builds the graph from the xml file 
		readXMLFile(networkFile + ".xml");
		initDegrees();
		deleteSelfLoops();

		// object to create black node scenarios (network simplifications)
		ComputeBlackNodeScenario bnsCreator = new ComputeBlackNodeScenario();
		InputParameters.createMetaNode = false;
		
		// object to compute stories
		ComputeScore computeScore = new ComputeScore();
		ComputeStories storiesEnumerator = new ComputeStories();
		
		for(int i = 0; i < pathwayFiles.length; i++) {
			// Check if we have for the i-th xml file the information on the black nodes and order of nodes
			String xmlFileName = pathwayFiles[i].getName().substring(0, pathwayFiles[i].getName().length()-4);			
			
			String storiesFileName = xmlFileName+".sto";
			File blackNodesFile = null;
			File partialOrderFile = null;
			
			if( InputParameters.blackNodesMode == EnumBlackNodes.FILE) {
				blackNodesFile = findFile(bnFiles, xmlFileName);
				partialOrderFile = findFile(orderFiles, xmlFileName);				
			}
			else {
				// for each pathway, automatically extract the list of black nodes 
				extractBlackNodes("./"+pathwaysPath+"/"+pathwayFiles[i].getName(), "./output/automaticBNs/"+xmlFileName);
				blackNodesFile = new File("./output/automaticBNs/"+xmlFileName+".bn");
				partialOrderFile = new File("./output/automaticOrder/"+xmlFileName+".order");
			}

			if( blackNodesFile == null )
			{
				System.out.println("File with list of black nodes not found for pathway file "+xmlFileName);
			}
			else if( partialOrderFile == null  )
			{
				System.out.println("File with partial order information not found for pathway file "+xmlFileName);
			}
			else
			{
				// read and check if the black nodes were identified
				int nbLines = readBlackNodesNamesFile(blackNodesFile);
				if( numberOfIdentifiedBlackNodes() != nbLines )
				{
					System.out.println("Not all black nodes identified. Expected = "+nbLines+" found = "+numberOfIdentifiedBlackNodes());
				}
				else
				{
					initDegrees();
					deleteSelfLoops();
					
					// log the black nodes chosen (verbose mode)
					for (String name : blackMap.keySet()) {
						Utility.printMsg("Black node: "+name+" inDegree: "+inDegree[map.get(name)]+". outDegree: "+outDegree[map.get(name)]);
					}									
					
					// generating the compressed network for the pathway (with its black nodes) on the output folder
					String[] networkFilePath = networkFile.split("/");
					String simulationFile = "./output/pathway/pathways/"+networkFilePath[networkFilePath.length-1]+"_for_"+xmlFileName;
					writeFile(simulationFile+".nel");
					
					// defines the .NEL file to be preprocessed
					InputParameters.inputFile = simulationFile;
					simulationFile = bnsCreator.run();
					
					NELNetwork bns = new NELNetwork();
					bns.readFile(simulationFile+".nel");
					WCCPartition scc = new WCCPartition(bns);
					scc.run();
					if( scc.getNumComponents() == 1 ) {
						System.out.println("%%%%%%\nSearching stories for the input file "+simulationFile+"\n%%%%%%%");
						
						// prepare the partial order score function
						ComputeScore.clearScoreFunctions();
						PartialOrderScore pos = new PartialOrderScore();
						pos.setPartialOrder(new PartialOrder(bns, partialOrderFile));
						ComputeScore.addScoreFunction(pos);

						InputParameters.outputFile = "./output/stories/"+storiesFileName;
						
						// enumerate stories
						InputParameters.inputFile = simulationFile;
						if( bns.numberOfAlives() > InputParameters.thresholdForComputingAllStories ) {
							System.out.println("Number of nodes on the BNS: "+bns.numberOfAlives()+". Computing randomly stories until stop condition is reached.");
							InputParameters.generateAllOrders = false;
						}
						else {
							System.out.println("Number of nodes on the BNS: "+bns.numberOfAlives()+". Computing all stories.");
							InputParameters.generateAllOrders = true;							
						}
						storiesEnumerator.run(computeScore, null);
					}
					else
					{
						System.out.println("More than 1 WCC found. Skipping the validation for "+pathwayFiles[i].getName());
						// identify where the black nodes are
						for(int c = 0; c < scc.getNumComponents(); c++) {
							int tam = 0;
							for(int j = 0; j < bns.getNumNodes(); j++) {
								if( scc.getComponent(j) == c) {
									tam++;
								}
							}
							System.out.println("Component "+c+" of size "+tam);
						}
						
					}
					System.out.println("\n%%%%%%\nFinished.\n%%%%%%%");
				}
			}
		}
	}
	
	private int numberOfIdentifiedBlackNodes() {
		int result = 0;
		for (String name : blackMap.keySet()) {
			if( map.get(name) != null )
				result = result + 1;
		}		
		return result;
	}
	
	private File findFile(File[] files, String name)
	{
		for(int i = 0; i < files.length; i++) {
			if( files[i].getName().contains(name) ) {
				return files[i];
			}
		}
		return null;
	}

	private int readBlackNodesNamesFile(File blackFile) {
		int nbLines = 0;
		try {
			BufferedReader bbr = new BufferedReader(new FileReader(blackFile));
			String line = bbr.readLine();
			blackMap = new HashMap<String, Double>();
			while (line != null) {
				nbLines++;
				String[] lineWords = line.split(" ");
				String nodeName = SBMLTools.sbmlEncode(lineWords[0].trim());
				if( lineWords.length > 1 ) {
					double intensity = 0.0;
					try {
						intensity = new Double(lineWords[1]);
					}
					catch(Exception e) {
						System.err.println("Error reading black nodes file. Expected compound intensity. Found: "+lineWords[1]);
					}
					blackMap.put(nodeName, intensity);					
				} else
				{
					blackMap.put(nodeName, 0.0);
				}
				line = bbr.readLine();
			}
			bbr.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return nbLines;
	}	
	
	private void extractBlackNodes(String pathwayFileName, String blackNodeFileName) {
		try
		{
			Hypergraph pathway = new Hypergraph();
			pathway.loadNetwork(new MetabolicNetworkSBMLReader(pathwayFileName), false);
			
			File bnFile = new File(blackNodeFileName+".bn");
			BufferedWriter bw = new BufferedWriter(new FileWriter(bnFile));
			List<Compound> seedsAndTargets = new ArrayList<Compound>();
			if( InputParameters.blackNodesMode == EnumBlackNodes.AUTO_BORENSTEIN_ALL )
				seedsAndTargets.addAll( SeedSearcher.findSeedsAndTargets(pathway, null) );
			else if( InputParameters.blackNodesMode == EnumBlackNodes.AUTO_BORENSTEIN_ONE )
			{
				List<DirectedSubgraph<Compound, Reaction>> sccs = SeedSearcher.getStronglyConnectedComponents(pathway);
				for(DirectedSubgraph<Compound,Reaction> scc: sccs) {
					// for each scc, compute the min degree (out or in-degree, depending on the source/target property of the scc)
					int minDegree = Integer.MAX_VALUE;
					for(Compound c: scc.vertexSet()) {
						int nodeDegree = 0;
						if( c.isSeed() )
							nodeDegree = inDegree(c);
						else
							nodeDegree = outDegree(c);
						if( minDegree > nodeDegree )
							minDegree = nodeDegree;
					}
					// and add all the minDegree nodes to the list of seeds/targets
					for(Compound c: scc.vertexSet()) {
						int nodeDegree = 0;
						if( c.isSeed() )
							nodeDegree = inDegree(c);
						else
							nodeDegree = outDegree(c);
						if( minDegree == nodeDegree )
							seedsAndTargets.add(c);
					}
				}
			}
			else if( InputParameters.blackNodesMode == EnumBlackNodes.AUTO_SOURCE_TARGET ) 
				seedsAndTargets.addAll( SeedSearcher.findTopologicalSeedsAndTargets(pathway) );
			else if( InputParameters.blackNodesMode == EnumBlackNodes.AUTO_REACTION_GRAPH_BORENSTEIN ) {
				for(Reaction r: SeedSearcher.findSeedsAndTargetsConvertingToReactionGraph(pathway) ) {
					if( r.getTopologicalSeed() ) {
						for(Stoichiometry s: r.getSubstrates()) {
							if( !seedsAndTargets.contains(s.getCompound())) {
								seedsAndTargets.add(s.getCompound());
								s.getCompound().setSeed(true);
							}
						}
						if( r.getReversible() ) {
							for(Stoichiometry s: r.getProducts()) {
								if( !seedsAndTargets.contains(s.getCompound())) {
									seedsAndTargets.add(s.getCompound());
									s.getCompound().setTarget(true);
								}
							}
						}
					}
					else if( r.getTopologicalTarget() ) {
						for(Stoichiometry s: r.getProducts()) {
							if( !seedsAndTargets.contains(s.getCompound())) {
								seedsAndTargets.add(s.getCompound());
								s.getCompound().setTarget(true);
							}
						}
						if( r.getReversible() ) {
							for(Stoichiometry s: r.getSubstrates()) {
								if( !seedsAndTargets.contains(s.getCompound())) {
									seedsAndTargets.add(s.getCompound());
									s.getCompound().setSeed(true);
								}
							}
						}
					}

				}
			}
			
			for(int i = seedsAndTargets.size()-1; i >= 0; i--)
			{
				if( validBlackNode(seedsAndTargets.get(i), 60, 60) )
					bw.write(seedsAndTargets.get(i).getId()+"\n");
				else
					seedsAndTargets.remove(i);
			}
			bw.close();

			// define the ordering of the identified black nodes
			File orderFile = new File(blackNodeFileName+".order");
			bw = new BufferedWriter(new FileWriter(orderFile));
			for(Compound s: seedsAndTargets)
			{
				if( s.isSeed() && !s.isTarget())
				{
					for(Compound t: seedsAndTargets) {
						if( t.isTarget() && !t.isSeed() ) {
							bw.write(SBMLTools.sbmlEncode(s.getId()) + " < "+SBMLTools.sbmlEncode(t.getId())+"\n");
						}
					}
				}
				
			}
			bw.close();
		
		
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error creating file with extracted black nodes.\n"+e.getMessage());
		}
	}
	
	private boolean validBlackNode(Compound c, int inThreshold, int outThreshold) {
		for(int i = 0; i < nodeName.length; i++) {
			if( nodeName[i].equals(c.getId()) ) {
				//System.out.println("Validating "+(c.isSeed() ? "seed ":"target ")+c.getId()+". InDegree="+inDegree[i]+", outDegree="+outDegree[i]);
				if( (c.isSeed() && inDegree[i] <= inThreshold) || (c.isTarget() && outDegree[i] <= outThreshold) )
				{
					return true;
				}
				else
				{
					System.out.println(c.getId()+" rejected");
					return false;
				}
			}
		}
		return true;
	}
	
	private int inDegree(Compound c) {
		for(int i = 0; i < nodeName.length; i++) {
			if( nodeName[i].equals(c.getId()) ) {
				return inDegree[i];
			}
		}	
		return Integer.MAX_VALUE;
	}
	
	private int outDegree(Compound c) {
		for(int i = 0; i < nodeName.length; i++) {
			if( nodeName[i].equals(c.getId()) ) {
				return outDegree[i];
			}
		}	
		return Integer.MAX_VALUE;
	}
}
