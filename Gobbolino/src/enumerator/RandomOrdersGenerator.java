package enumerator;

import java.util.Random;

public class RandomOrdersGenerator extends OrderGenerator {

	protected Random rnd = new Random();
	
	/*
	 * Generates a random order of the nodes
	 */	
	protected void nextOrder() {
		for (int i = 0; i < order.length; ++i) {
			int j = rnd.nextInt(i + 1);
			order[i] = order[j];
			order[j] = i;
		}	
	}
	
}
