package enumerator;

import java.io.BufferedReader;
import java.io.FileReader;

public class FileFirstRandomOrdersGenerator extends RandomOrdersGenerator {

	private boolean fileIsOver = false;
	private BufferedReader file;
	
	public FileFirstRandomOrdersGenerator(String ordersFilename) {
		try {
			file = new BufferedReader(new FileReader(ordersFilename));
			fileIsOver = false;
		} catch (Exception e) {
			System.out.println("Problems opening orders file "+ordersFilename);
			e.printStackTrace();
			System.exit(-1);
		}		
	}
	
	private void extractOrderFromFile() {
		try {
			String line = file.readLine();
			while( line != null && line.isEmpty() ) {
				line = file.readLine();
			}
			if (line != null) {
				String[] lineWords = line.split(" ");
				if( lineWords.length != order.length ) {
					System.out.println("The number of expected nodes is different than found in the orders file.");
					System.exit(0);
				}
				for(int i = 0; i < lineWords.length; i++) {
					order[i] = Integer.parseInt(lineWords[i]);
				}
			} 
			else {
				fileIsOver = true;
				file.close();
			}
		}
		catch(Exception e) {
			System.out.println("Problems dealing with orders file.");
			e.printStackTrace();
			System.exit(-1);
		}
	}	
	
	/*
	 * Generates a random order of the nodes
	 */	
	protected void nextOrder() {
		if( fileIsOver ) {
			super.nextOrder();
		}
		else {
			extractOrderFromFile();
		}
	}
	
}
