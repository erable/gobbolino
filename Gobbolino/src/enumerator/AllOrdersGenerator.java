package enumerator;

public class AllOrdersGenerator extends OrderGenerator {

	char[] direction;
	boolean bootstrap = false;
	
	public void init(int size) {
		super.init(size);
		bootstrap = false;
		direction = new char[size];
		for(int i = 0; i < direction.length; i++) {
			direction[i] = '<';
		}
	}
	
	/*
	 * Johnson-Trotter algorithm (H. F. Trotter[1962], S. M. Johnson[1963])
	 * 
	 */
	protected void nextOrder() {
		if( !bootstrap ) {
			bootstrap = true; 	// use the initial ordering of the nodes
			return;
		}
		
		int k = largestMobile();

		if( k < 0 ) {
			finished = true;
			return;
		}
		
		if( direction[k] == '<' ) {
			swap(k, k-1);
			k = k - 1;
		}
		else {
			swap(k, k+1);
			k = k+1;
		}

		// reverse the direction of all integers larger than rank[k]
		for(int i = 0; i < order.length; i++) {
			if( order[i] > order[k] ) {
				if( direction[i] == '<' ) {
					direction[i] = '>';
				} else {
					direction[i] = '<';					
				}
			}
		}
	}
	
	
	/*
	 * swap also the direction
	 */
	protected void swap(int i, int j) {
		super.swap(i, j);
		char t = direction[i];
		direction[i] = direction[j];
		direction[j] = t;
	}	
	
	protected int largestMobile() {
		int currentLargest = -1, currentLargestIndex = -1;
		for(int i = 0; i < order.length; i++) {
			if( direction[i] == '<' ) {
				// it looks to i-1
				if( (i-1) >= 0 && order[i] > order[i-1] ) {
					// it is mobile
					if( order[i] > currentLargest ) {
						currentLargest = order[i];
						currentLargestIndex = i;
					}
				}
			} else {
				// it looks to i+1
				if( (i+1) < order.length && order[i] > order[i+1] ) {
					// it is mobile
					if( order[i] > currentLargest ) {
						currentLargest = order[i];
						currentLargestIndex = i;
					}
				}
				
			}
		}
		return currentLargestIndex;
	}
}
