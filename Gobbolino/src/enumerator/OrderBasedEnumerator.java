/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package enumerator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;

import network.NELWithALNetwork;
import score.ComputeScore;
import stop.RaceControl;
import story.Story;
import story.StoryWriter;
import utils.Utility;
import application.InputParameters;

public class OrderBasedEnumerator extends NELWithALNetwork {
	protected int[][] compare;
	protected boolean[][] pitchAdj;
	protected boolean[] isInPitch;
	protected OrderGenerator orderGenerator;
	protected int[] rank, order;
	protected HashMap<String, Integer> sasSet = new HashMap<String, Integer>();
	protected HashMap<String, Integer> edgeIndex = new HashMap<String, Integer>();
	protected int nsas = 0, maxNSAS;
	protected int[][] storyIncidency;
	protected String[][] nodeKind;
	protected boolean pitchChanged;
	protected int numberOldSAS;
	protected long timeBetween;
	
	private ComputeScore score;
	private StoryWriter writer;
	
	// holds the listener that may be informed once a new story is generated.
	private StoriesEnumeratorListener listener = null;
	// holds the race control object that determines if the computation should be finished.
	protected RaceControl control;
	
	public OrderBasedEnumerator(RaceControl rc, OrderGenerator orderGenerator) {
		control = rc;
		this.orderGenerator = orderGenerator;
	}
	
	/*
	 * Add a path from v to u, using the predecessors information (v could be discarded as a parameters, it is here only for clarity)
	 */
	private void addPath(int v, int u, int[] pred) {
		while (pred[u] != u) {
			pitchAdj[pred[u]][u] = true;
			isInPitch[u] = true;
			u = pred[u];
		}
	}

	/*
	 * computes the bfs starting from the node v and considering only arcs not in the pitch. 
	 * Any path starting from v and going back to the pitch is added to the it. The partial order corresponding to the pitch is also extended.
	 * If at least one path is added, pitchChanged flag is set. 
	 */
	private void bfs(int v) {
		int[] pred = new int[nn];
		boolean[] visited = new boolean[nn];
		for (int i = 0; i < nn; i++) {
			pred[i] = -1;
			visited[i] = false;
		}
		LinkedList<Integer> queue = new LinkedList<Integer>();
		pred[v] = v;
		queue.offer(v);
		while (!queue.isEmpty()) {
			int x = queue.poll();
			visited[x] = true;
			for (int u = 0; u < nn; u++) {
				if (adjacencyList[x].contains(u) && !pitchAdj[x][u]
						&& compare[x][u] <= 0) {
					if (!visited[u] && isInPitch[u] && compare[u][v] >= 0) {
						pred[u] = x;
						addPath(v, u, pred);
						compare = Utility.partialOrder(pitchAdj, nn);
						pitchChanged = true;
					}
					if (!isInPitch[u] && !visited[u]) {
						pred[u] = x;
						queue.offer(u);
					}
				}
			}
		}
	}

	/*
	 * Computes a pitch that respects the current ordering of the nodes (rank) (consistent_arcs)
	 * and then cleans up the pitch by removing any white source/target remaining (clean)
	 */
	void computePitch() {
		// consistent arcs
		for (int u = 0; u < nn; u++) {
			for (int v : adjacencyList[u]) {
				if (rank[u] > rank[v]) {
					pitchAdj[u][v] = false;
				} else {
					isInPitch[u] = true;
					isInPitch[v] = true;
					pitchAdj[u][v] = true;
				}
			}
		}
		// clean
		boolean changed = true;
		while (changed) {
			changed = false;
			for (int u = 0; u < nn; u++) {
				if (!isBlack[u]
						&& isInPitch[u]
						&& (successors(u, pitchAdj).size() == 0 || predecessors(
								u, pitchAdj).size() == 0)) {
					isInPitch[u] = false;
					for (int v = 0; v < nn; v++) {
						pitchAdj[u][v] = false;
						pitchAdj[v][u] = false;
					}
					changed = true;
				}
			}
		}
	}

	/*
	 * computeSAS: computes the pitch extending the current order of the nodes and then completes it into a story.
	 */
	private boolean computeSAS() {
		
		// finds a new story arc set (SAS).
		// Initialization: empty pitch containing only the black nodes and no arc
		isInPitch = new boolean[nn];
		pitchAdj = new boolean[nn][nn];
		for (int i = 0; i < nn; i++) {
			isInPitch[i] = false;
			if (isBlack[i]) {
				isInPitch[i] = true;
			}
			for (int j = 0; j < nn; j++) {
				pitchAdj[i][j] = false;
			}
		}
		// Compute pitchs computes the pitch based on the current order of the nodes
		computePitch();
		
		// Initializes a partial order infered by the pitch
		compare = Utility.partialOrder(pitchAdj, nn);
		
		// tries to add more paths to the pitch while it is possible
		pitchChanged = true;
		while (pitchChanged) {
			pitchChanged = false;
			for (int v = 0; v < nn; v++) {
				if (isInPitch[order[v]]) {
					bfs(order[v]);
				}
			}
		}
		
		// now, pitchAdj contains a new story. Computes the id of the story.
		String sas = sasFromPitchAdj();
		// checks if this is a new story
		if (!sasSet.containsKey(sas)) {
			
			// registers the elapsed time since the last new story was found.
			long currentTime = System.currentTimeMillis();
			timeBetween = currentTime - timeBetween;

			// records the new story on the map
			sasSet.put(sas, 1);			
			if( ! InputParameters.countStoriesMode ) { 	// if it is not only in COUNT mode (SEA)
				// Builds the story object and simplifies it (applying simplification rules: bottleneck removals)
				Story s = new Story(pitchAdj, isBlack, nodeName, adjacencyList, outLabels);
				s.simplify();
				
				// computes the score of the new story
				Vector<Double> storyScore = score.computeScore(s);
				Double[] score = storyScore.toArray(new Double[0]);
				
				// writes the story on the output file
				writer.writeFile(nsas, s, score, rank);
								
				// invoke a possible listener on the generation process
				if( listener != null ) {
					listener.onNewStoryGenerated(nsas+1, this, s, score);
				}
				
				if( maxNSAS > 0) {
					newSAS();
				}
				
				// prints using the verbose option
				Utility.printMsg(nsas+" generated after "+timeBetween+" ms. id: "+sas);
			}

			// resets the control variables
			nsas = nsas + 1;
			numberOldSAS = 0;
			timeBetween = currentTime;
			return true;
			// System.out.println(nsas);
		} else {
			// if this is a story that was already computed before, only updates the number of times it has been found.
			int m = sasSet.get(sas);
			m = m + 1;
			sasSet.put(sas, m);
			numberOldSAS = numberOldSAS + 1;
			return false;
		}
	}

	/*
	 * Initializes the output matrices (wow!!)
	 */
	private void initOutputMatrices() {
		storyIncidency = new int[ne][maxNSAS];
		nodeKind = new String[nn][maxNSAS];
		for (int i = 0; i < ne; i++) {
			for (int j = 0; j < maxNSAS; j++) {
				storyIncidency[i][j] = 0;
			}
		}
		int e = 0;
		for (int i = 0; i < nn; i++) {
			for (int j : adjacencyList[i]) {
				edgeIndex.put(toString(new int[] { i, j }), e);
				e = e + 1;
			}
		}
	}

	/*
	 * 
	 * 
	 */
	private void newSAS() {
		for (int i = 0; i < nn; i++) {
			for (int j : adjacencyList[i]) {
				if (pitchAdj[i][j]) {
					storyIncidency[edgeIndex.get(toString(new int[] { i, j }))][nsas] = 1;
				}
			}
		}
		for (int i = 0; i < nn; i++) {
			if (!isBlack[i]) {
				nodeKind[i][nsas] = "W";
			} else {
				boolean isSource = true;
				for (int j = 0; j < nn; j++) {
					if (adjacencyList[j].contains(i) && pitchAdj[j][i]) {
						isSource = false;
						break;
					}
				}
				if (isSource) {
					nodeKind[i][nsas] = "S";
				} else {
					boolean isTarget = true;
					for (int j = 0; j < nn; j++) {
						if (adjacencyList[i].contains(j) && pitchAdj[i][j]) {
							isTarget = false;
							break;
						}
					}
					if (isTarget) {
						nodeKind[i][nsas] = "T";
					} else {
						nodeKind[i][nsas] = "I";
					}
				}
			}
		}
	}

	/*
	 * Computes a list of predecessors of a node from a matrix representation
	 */
	private LinkedList<Integer> predecessors(int u, boolean[][] adj) {
		LinkedList<Integer> rst = new LinkedList<Integer>();
		for (int v = 0; v < nn; v++) {
			if (adj[v][u]) {
				rst.add(v);
			}
		}
		return rst;
	}

	/*
	 * Prints all stories computed so far (considering the verbose parameter)
	 */
	private void printSASSet() {
		Utility.printMsg("\n\nStories generated:\n");
		
		for (String sas : sasSet.keySet()) {
			Utility.printMsg(sas + ": " + sasSet.get(sas));
		}
	}

	/*
	 * Computes stories for all the orders of the nodes (according to the order generator chosen by the user)
	 */
	public void run(String networkName, int maxNSAS) {
		prepareToRun(networkName, maxNSAS);

		while( (maxNSAS < 0 || nsas < maxNSAS) && !orderGenerator.finished() ) {			
			orderGenerator.computeNextOrder();
			rank = orderGenerator.getRank();
			order = orderGenerator.getOrder();

			if( computeSAS() )
				control.newStoryGenerated(true, nsas);
			else
				control.newStoryGenerated(false, nsas);

			if( control.stop() )
			{
				//System.out.println("Stopping stories computation since "+control.explanation()+". Stories computed so far " + nsas);
				break;
			}
		}
		// finishes 
		prepareToClose();
		System.out.println("#Stories found: " + nsas);
	}

	/*
	 * Prepares to run
	 * 
	 */
	protected void prepareToRun(String networkName, int maxNSAS) {
		this.maxNSAS = maxNSAS;
		readFile(networkName + ".nel");
		timeBetween = System.currentTimeMillis();
		if( maxNSAS > 0 ) {
			initOutputMatrices();
		}
		numberOldSAS = 0;	
		// the writer and score evaluator ars needed only if it is not in the COUNTING mode		
		if( !InputParameters.countStoriesMode ) {	
			if( score == null ) {
				score = new ComputeScore();
			}
			writer = new StoryWriter();
		}
		// initializes the order generator with the size of the graph (number of nodes)
		orderGenerator.init(nn);
	}
	
	public void setScoreEvaluator(ComputeScore score) {
		this.score = score;
	}
	
	
	/*
	 * Prepares to close
	 */
	protected void prepareToClose() {
		// finishes the processing (IF not in counting mode)
		if( !InputParameters.countStoriesMode ) {
			writer.closeFile();
			printSASSet();
		}
	}
	
	/*
	 * Computes stories up to maxNSAS
	 */
	public void runRandomOrders(String networkName, int maxNSAS) {
		//randomOrder = true;
		prepareToRun(networkName, maxNSAS);

		// while the maximum defined 
		while (maxNSAS < 0 || nsas < maxNSAS) {
			//randomOrder();
			
		
			if( computeSAS() )
				control.newStoryGenerated(true, nsas);
			else
				control.newStoryGenerated(false, nsas);

			if( control.stop() )
			{
				System.out.println("Stopping stories computation since "+control.explanation()+". Stories computed so far " + nsas);
				break;
			}
		}
		// finishes it
		prepareToClose();
	}

	/*
	 * Computes a story arc set from the pitch adjacency matrix
	 */
	private String sasFromPitchAdj() {
		boolean[][] isInSAS = new boolean[nn][nn];
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				if (adjacencyList[u].contains(v) && !pitchAdj[u][v]) {
					isInSAS[u][v] = true;
				} else {
					isInSAS[u][v] = false;
				}
			}
		}
		return toString(isInSAS);
	}

	/*
	 * Computes a list of predecessors of a node from a matrix representation
	 */
	private LinkedList<Integer> successors(int u, boolean[][] adj) {
		LinkedList<Integer> rst = new LinkedList<Integer>();
		for (int v = 0; v < nn; v++) {
			if (adj[u][v]) {
				rst.add(v);
			}
		}
		return rst;
	}

	/*
	 * toString of a matrix of booleans (adjacency matrix)
	 */
	private String toString(boolean[][] a) {
		String rst = "";
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				if (adjacencyList[u].contains(v) && a[u][v]) {
					String s = toString(new int[] { u, v });
					rst = rst + s;
				}
			}
		}
		return rst;
	}

	/*
	 * toString of a list of arcs (integers)
	 * 
	 */
	private String toString(int[] edge) {
		return "[" + edge[0] + "," + edge[1] + "]";
	}

	/*
	 * Method for the listener of events (newSAS) register itself
	 */
	public void setListener(StoriesEnumeratorListener listener) {
		this.listener = listener;
	}

}
