package enumerator;

public abstract class OrderGenerator {

	protected int[] rank, order;
	protected boolean finished = false;
	
	OrderGenerator(int size) {
		init(size);
	}
	
	OrderGenerator() {
		
	}
	
	public int[] getRank() {
		return rank;
	}
	
	public int[] getOrder() {
		return order;
	}	
	
	public void init(int size) {
		rank = new int[size];
		order = new int[size];
		for(int i = 0; i < order.length; i++) {
			order[i] = i;
		}
		completeRankVector();
	}
	
	protected void nextOrder() {
	}
	
	public boolean computeNextOrder() {
		nextOrder();
		completeRankVector();
		return finished;
	}
	
	/*
	 * Computes the rank vector based on the order vector.
	 * 
	 * For instance, if you have 5 nodes, stored on the vector n as n[0] = 1, n[1] = 2, n[2] = 3, n[3] = 4 and n[4] = 5 
	 *  and you want to consider the following order of the nodes: 3,5,1,2,4
	 * 
	 * The order vector o will have exactly o[0] = 3, o[1] = 5, o[2] = 1, o[3] = 2 and o[4] = 4.
	 * The rank vector r will have r[0] = 2, r[1] = 2, r[2] = 3, r[3] = 0, r[4] = 4  
	 * 
	 */
	protected void completeRankVector() {
		for (int i = 0; i < order.length; i++) {
			rank[order[i]] = i;
		}		
	}
	
	/*
	 * swap
	 */
	protected void swap(int i, int j) {
		int t = order[i];
		order[i] = order[j];
		order[j] = t;
	}	
	
	public boolean finished() {
		return finished;
	}
	
}
