/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package network;
import java.util.Stack;


/*
 * This class allows us to compute the strongly connected components of a graph stored in a NEL file. The components are made of black nodes only.
 */
public class BlackSCCComputer {// extends NELNetwork {
	boolean[] complete;
	int counter;
	int[] component, componentSize;
	int[] dfsNumber;
	int lastComponent;
	Stack<Integer> partial;
	Stack<Integer> representative;

	NELNetwork network;
	
	public BlackSCCComputer(NELNetwork network){
		this.network = network;
	}
	
	/*
	 * For each component, computes the number of black nodes in it.
	 */
	void computeComponentSizes() {
		componentSize = new int[lastComponent];
		for (int i = 0; i < lastComponent; i++) {
			componentSize[i] = 0;
		}
		for (int i = 0; i < network.nn; i++) {
			if (network.isBlack[i]) {
				componentSize[component[i]] = componentSize[component[i]] + 1;
			}
		}
	}

	/*
	 * This is a recursive version of the depth-first search which is used in
	 * order to compute the strongly connected components of the graph. See the
	 * book by Crescenzi, Gambosi, Graossi.
	 */
	void extendedRecursiveDFS(int u) {
		dfsNumber[u] = counter;
		counter = counter + 1;
		partial.push(u);
		representative.push(u);
		for (int v = 0; v < network.nn; v++) {
			if (network.isBlack[v] && network.adjacency[u][v]) {
				if (dfsNumber[v] == -1) {
					extendedRecursiveDFS(v);
				} else if (!complete[v]) {
					while (dfsNumber[representative.peek()] > dfsNumber[v]) {
						representative.pop();
					}
				}
			}
		}
		if (u == representative.peek()) {
			int z = partial.pop();
			component[z] = lastComponent;
			complete[z] = true;
			while (z != u) {
				z = partial.pop();
				component[z] = lastComponent;
				complete[z] = true;
			}
			representative.pop();
			lastComponent = lastComponent + 1;
		}
	}

	/*
	 * It prints the list of nodes in a specific strongly connected components.
	 */
	void printSCC(int c) {
		System.out.print("Component " + c + ": ");
		for (int i = 0; i < network.nn; i++) {
			if (component[i] == c) {
				System.out.print(network.nodeName[i] + " ");
				if( !network.isBlack(i) ) {
					System.out.print(" - white node");
				}
			}
		}
		System.out.print(" numbers ");
		for (int i = 0; i < network.nn; i++) {
			if (component[i] == c) {
				System.out.print(i + " ");
			}
		}
		System.out.println("");
	}

	/*
	 * It prints the list of the sizes of all strongly connected components.
	 */
	void printSCCSizes() {
		System.out.println("=== Number of black SCC: " + lastComponent);
		System.out.print("=== List of black SCC sizes: ");
		for (int i = 0; i < lastComponent - 1; i++) {
			System.out.print(componentSize[i] + ", ");
		}
		System.out.println(componentSize[lastComponent - 1] + "\n");
	}

	/*
	 * It reads the graph NEL file, computes the black strongly connected
	 * components, and outputs the number and the sizes of the components.
	 */
	public void run(int component, boolean onlyBlack){
		stronglyBlackConnectedComponents(onlyBlack);
		computeComponentSizes();
		printSCCSizes();
		if (component>=0 && component<lastComponent) {
			printSCC(component);
		}
		else if (component < 0)
		{
			for(int i = 0; i < lastComponent; i++)
				printSCC(i);
		}
	}

	/*
	 * It computes the strongly connected components of the graph containing
	 * only black nodes. See the book by Crescenzi, Gambosi, Graossi.
	 */
	void stronglyBlackConnectedComponents(boolean onlyBlack) {
		dfsNumber = new int[network.nn];
		complete = new boolean[network.nn];
		component = new int[network.nn];
		for (int s = 0; s < network.nn; s++) {
			dfsNumber[s] = -1;
			complete[s] = false;
			component[s] = -1;
		}
		partial = new Stack<Integer>();
		representative = new Stack<Integer>();
		lastComponent = 0;
		counter = 0;
		for (int s = 0; s < network.nn; s++) {
			if (dfsNumber[s] == -1 && (!onlyBlack || network.isBlack[s]) ) {
				extendedRecursiveDFS(s);
			}
		}
	}
	
	public int getLastComponent() {
		return lastComponent;
	}
	
	public int getComponentSize(int c) {
		return componentSize[c];
	}
	
	public int getComponent(int c) {
		return component[c];
	}
}
