/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package network;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Vector;

import application.InputParameters;

public class XML2NEL extends Convert2NEL {
	
	public void readXMLFile(String inPath) {
		File inFile = new File(inPath);
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine().trim();
			while (!line.equals("<listOfSpecies>")) {
				line = br.readLine().trim();
			}
			line = br.readLine().trim();
			numNodes = 0;
			while (!line.equals("</listOfSpecies>")) {
				if( line.contains("<species") ) {	// new species found
					numNodes = numNodes + 1;
				}
				line = br.readLine().trim();
			}
			br.close();
			adjacency = new boolean[numNodes][numNodes];
			edgeLabels=new String[numNodes][numNodes];
			inDegree = new int[numNodes];
			outDegree = new int[numNodes];
			map = new HashMap<String, Integer>();
			nodeName = new String[numNodes];
			br = new BufferedReader(new FileReader(inFile));
			line = br.readLine().trim();
			while (!line.equals("<listOfSpecies>")) {
				line = br.readLine().trim();
			}
			line = br.readLine().trim();
			int index = 0;
			while (!line.equals("</listOfSpecies>")) {
				if( line.contains("<species") ) {	// new species found
					int firstIndex = line.indexOf('=');
					int secondIndex = line.indexOf(' ', firstIndex);
					String species = line
							.substring(firstIndex + 2, secondIndex - 1);
					map.put(species, index);
					nodeName[index] = species;
					index = index + 1;
				}
				line = br.readLine().trim();
			}
			System.out.println(map.size());
			line = br.readLine();
			line = br.readLine();
			while (line.indexOf("/listOfReactions") == -1) {
				//get the id of the reaction
				int index1=line.indexOf("reaction id=");
				index1 = index1+ "reaction id=".length() + 1;
				int index2= line.indexOf("name=");
				String reactionID = line.substring(index1,index2 - 2);
				index = line.indexOf("reversible=");
				index = index + "reversible=".length() + 1;
				boolean reversible = false;
				if (line.charAt(index) == 't') {
					reversible = true;
				}
				while (line.indexOf("listOfReactants") == -1) {
					line = br.readLine();
				}
				line = br.readLine();
				Vector<String> reactants = new Vector<String>();
				while (line.indexOf("/listOfReactants") == -1) {
					int firstIndex = line.indexOf('=');
					int secondIndex = line.indexOf(' ', firstIndex);
					String species = line.substring(firstIndex + 2,
							secondIndex - 1);
					reactants.add(species);
					line = br.readLine();
				}
				line = br.readLine();
				line = br.readLine().trim();
				Vector<String> products = new Vector<String>();
				while (line.indexOf("/listOfProducts") == -1) {
					int firstIndex = line.indexOf('=');
					int secondIndex = line.indexOf(' ', firstIndex);
					String species = line.substring(firstIndex + 2,
							secondIndex - 1);
					products.add(species);
					line = br.readLine();
				}
				for (String reactant : reactants) {
					for (String product : products) {
						if( map.get(reactant) == null) 
						{
							System.out.println("reactant "+reactant+" not found in the species list.");
							System.exit(0);
						}						
						if( map.get(product) == null) 
						{
							System.out.println("product "+product+" not found in the species list.");
							System.exit(0);
						}						
						adjacency[map.get(reactant)][map.get(product)] = true;
						edgeLabels[map.get(reactant)][map.get(product)] = reactionID;
						if (reversible && !InputParameters.ignoreReversibility) {
							adjacency[map.get(product)][map.get(reactant)] = true;
							edgeLabels[map.get(product)][map.get(reactant)] = reactionID;

						}
					}
				}
				while(line.indexOf("/reaction") == -1) {
					line = br.readLine();
				}
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public String run(String nn) {
		readBlackNodesFile(nn + ".bn");
		readXMLFile(nn + ".xml");
		initDegrees();
		deleteSelfLoops();
		String outputFile = nn; 
		if (InputParameters.ignoreReversibility) {
			outputFile = nn + "_ONEWAY"; 
		}
		writeFile(outputFile+".nel");
		return outputFile;
	}

}
