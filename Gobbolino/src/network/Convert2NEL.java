/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package network;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Convert2NEL {
	protected HashMap<String, Double> blackMap;
	protected HashMap<String, Integer> map;
	protected String nodeName[];
	protected boolean[][] adjacency;
	protected String[][] edgeLabels;
	protected int[] inDegree;
	protected int[] outDegree;
	protected int numNodes;

	public void deleteSelfLoops() {
		for (int i = 0; i < numNodes; i++) {
			if (adjacency[i][i]) {
				adjacency[i][i] = false;
				inDegree[i] = inDegree[i] - 1;
				outDegree[i] = outDegree[i] - 1;
			}
		}
	}

	public void initDegrees() {
		for (int i = 0; i < numNodes; i++) {
			outDegree[i] = 0;
			inDegree[i] = 0;
			for (int j = 0; j < numNodes; j++) {
				if (adjacency[i][j]) {
					outDegree[i] = outDegree[i] + 1;
				}
				if (adjacency[j][i]) {
					inDegree[i] = inDegree[i] + 1;
				}
			}
		}
	}

	protected void readBlackNodesFile(String blackPath) {
		File blackFile = new File(blackPath);
		try {
			BufferedReader bbr = new BufferedReader(new FileReader(blackFile));
			String line = bbr.readLine();
			blackMap = new HashMap<String, Double>();
			while (line != null) {
				String[] lineWords = line.split(" ");
				if( lineWords.length > 1 ) {
					double intensity = 0.0;
					try {
						intensity = new Double(lineWords[1]);
					}
					catch(Exception e) {
						System.err.println("Error reading black nodes file. Expected compound intensity. Found: "+lineWords[1]);
					}
					blackMap.put(lineWords[0], intensity);					
				} else
				{
					blackMap.put(lineWords[0], 0.0);
				}
				line = bbr.readLine();
			}
			bbr.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public String run(String nn) { 
		// do your trick and return the name of the generated file...
		return null;
	}

	void sort(int[] a) {
		for (int i = 1; i < a.length; i++) {
			int current = a[i];
			int j = i;
			while (j > 0 && a[j - 1] > current) {
				a[j] = a[j - 1];
				j = j - 1;
			}
			a[j] = current;
		}
	}

	public void writeFile(String outPath) {
		File outFile = new File(outPath);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));

			// validate if all black nodes read are present
			List<String> bnNotIdentified = new ArrayList<String>();
			for (String name : blackMap.keySet()) {
				if( map.get(name) == null ) {
					bnNotIdentified.add(name);
				}
			}			
			for(String name: bnNotIdentified) {
				System.err.println("ERROR: black node "+name+" not found on the network.");
				blackMap.remove(name);
			}
			
			bw.write(numNodes + " " + blackMap.size() + "\n");
			int b[] = new int[blackMap.size()];
			int i = 0;
			for (String name : blackMap.keySet()) {
				if( map.get(name) != null ) {
					b[i] = map.get(name);
					i = i + 1;					
				}
			}

			sort(b);
			for (i = 0; i < b.length; i++) {
				bw.write(b[i] + "\n");
			}
			for (i = 0; i < numNodes; i++) {
				bw.write(i + " " + outDegree[i] + " " + inDegree[i] + " "
						+ nodeName[i]+";\n");
			}
			for (i = 0; i < numNodes; i++) {
				for (int j = 0; j < numNodes; j++) {
					if (adjacency[i][j]) {
						bw.write(i + " " + j +" "+nodeName[i]+";"+nodeName[j]+"\n");
					}
				}
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public String getNodeName(int index) {
		return nodeName[index];
	}
	
	public int getIdFromNodeName(String name) {
		for(int i = 0; i < nodeName.length; i++) {
			String[] names = nodeName[i].split(";");
			for(int j = 0; j < names.length; j++)
			if( names[j].equals(name) ) {
				return i;
			}
		}
		return -1;
	}
	
	public int getInDegree(int u) {
		return inDegree[u];
	}

	public int getOutDegree(int u) {
		return outDegree[u];
	}

	public HashMap<String, Double> getBlackMap() {
		return blackMap;
	}

	public void setBlackMap(HashMap<String, Double> blackMap) {
		this.blackMap = blackMap;
	}

	public int getNumNodes() {
		return numNodes;
	}

	public void setNumNodes(int numNodes) {
		this.numNodes = numNodes;
	}
	
	/*
	 * It computes the number of edges.
	 */
	public int numberOfEdges() {
		int result = 0;
		for (int i = 0; i < numNodes; i++) {
			result = result + outDegree[i];
		}
		return result;
	}	
	
}
