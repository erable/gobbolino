/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package network;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import utils.Utility;
import application.InputParameters;

/*
 * This class models a metabolic network by means of an adjacency matrix.
 */
public class NELNetwork {
	protected boolean[][] adjacency;
	protected String[][] labels;
	protected int[] inDegree;
	protected int[] outDegree;
	protected boolean[] isBlack;
	protected boolean[] nodeAlive;
	protected int[] oldNew;
	protected String[] newName;
	protected String[] nodeName;
	protected int nn, ne;
	
	public NELNetwork() {}
	
	public NELNetwork(NELNetwork net) {
		nn = net.nn;
		ne = net.ne;
		nodeName = new String[nn];
		nodeAlive = new boolean[nn];
		isBlack = new boolean[nn];
		outDegree = new int[nn];
		inDegree = new int[nn];
		labels = new String[nn][nn];
		adjacency = new boolean[nn][nn];
		
		for(int i = 0; i < nn; i++) {
			nodeName[i] = net.nodeName[i];
			nodeAlive[i] = net.nodeAlive[i];
			isBlack[i] = net.isBlack[i];
			outDegree[i] = net.outDegree[i];
			inDegree[i] = net.inDegree[i];
			for(int j = 0; j < nn; j++) {
				labels[i][j] = net.labels[i][j];
				adjacency[i][j] = net.adjacency[i][j];
			}
		}
	}
	
	
	/*
	 * All nodes with no incoming and no outgoing edges are logically deleted.
	 */
	public void computeAliveNodes() {
		nodeAlive = new boolean[nn];
		for (int i = 0; i < nn; i++) {
			nodeAlive[i] = false;
			for (int j = 0; j < nn; j++) {
				if (adjacency[i][j]) {
					nodeAlive[i] = true;
				}
				if (adjacency[j][i]) {
					nodeAlive[i] = true;
				}
			}
		}
	}

	/*
	 * It computes the degrees of the nodes according to the adjacency matrix.
	 */
	public void computeDegrees() {
		inDegree = new int[nn];
		outDegree = new int[nn];
		for (int i = 0; i < nn; i++) {
			inDegree[i] = 0;
			outDegree[i] = 0;
			for (int j = 0; j < nn; j++) {
				if (adjacency[i][j]) {
					outDegree[i]++;
				}
				if (adjacency[j][i]) {
					inDegree[i]++;
				}
			}
		}
	}

	/*
	 * It computes the number of nodes which have not been logically deleted.
	 */
	public int numberOfAlives() {
		int b = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i]) {
				b++;
			}
		}
		return b;
	}

	/*
	 * It computes the number of edges.
	 */
	public int numberOfEdges() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			result = result + outDegree[i];
		}
		return result;
	}

	/*
	 * It computes the number of self-loops involving nodes which have not been
	 * logically deleted.
	 */
	int numberOfSelfLoops() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i] && adjacency[i][i]) {
				result = result + 1;
			}
		}
		return result;
	}

	/*
	 * It computes the number of white nodes which have no incoming edges and
	 * have not been logically deleted.
	 */
	public int numberOfSources(boolean black) {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i] && inDegree[i] == 0 && isBlack[i] == black) {
				result = result + 1;
			}
		}
		return result;
	}

	/*
	 * It computes the number of white nodes which have no outgoing edges and
	 * have not been logically deleted.
	 */
	public int numberOfTargets(boolean black) {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i] && outDegree[i] == 0 && isBlack[i] == black) {
				result = result + 1;
			}
		}
		return result;
	}

	/*
	 * It computes the number of black nodes which have not been logically
	 * deleted.
	 */
	public int numberOfBlackNodes() {
		int b = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i] && isBlack[i]) {
				b++;
			}
		}
		return b;
	}

	/*
	 * It checks whether all logically deleted nodes have the degrees equal to 0
	 * and that the degrees of all the other nodes are consistent with the
	 * adjacency matrix.
	 */
	void checkDegrees() {
		for (int i = 0; i < nn; i++) {
			if (!nodeAlive[i] && (inDegree[i] != 0 || outDegree[i] != 0)) {
				System.out.println("Warning: dead node " + i
						+ " has degree greater than 0");
				System.exit(-1);
			}
			int in = 0;
			int out = 0;
			for (int j = 0; j < nn; j++) {
				if (adjacency[i][j])
					out++;
				if (adjacency[j][i])
					in++;
			}
			if (inDegree[i] != in) {
				System.out
						.println("Warning: node " + i + " has wrong indegree");
				System.exit(-1);
			}
			if (outDegree[i] != out) {
				System.out.println("Warning: node " + i
						+ " has wrong outdegree");
				System.exit(-1);
			}
		}
	}

	/*
	 * Logically delete a vertex
	 */
	public void delete(int v)
	{
		for(int i = 0; i < nn; i++) {
			if( adjacency[i][v] ) {
				adjacency[i][v] = false;
				outDegree[i] = outDegree[i] - 1;
			}
			
			if( adjacency[v][i] ) {
				adjacency[v][i] = false;
				inDegree[i] = inDegree[i] - 1;
			}
		}		
		nodeAlive[v] = false;
		inDegree[v] = 0;
		outDegree[v] = 0;
	}
	
	/*
	 * Deletes an arc
	 * 
	 */
	public void deleteArc(int u, int v) {
		if( adjacency[u][v] ) {
			adjacency[u][v] = false;
			outDegree[u] = outDegree[u] - 1;
			inDegree[v] = inDegree[v] - 1;
		}
	}
	
	/*
	 * It prints some basic statistics of the network.
	 */
	public void printStatistics(String method) {
		if (InputParameters.verbose) {
			System.out.println("=== Graph statistics after " + method + " ===");
			System.out.println("Number of alive nodes: " + numberOfAlives());
			System.out.println("Number of self-loops: " + numberOfSelfLoops());
			System.out.println("Number of white sources: " + numberOfSources(false));
			System.out.println("Number of black sources: " + numberOfSources(true));
			System.out.println("Number of white targets: " + numberOfTargets(false));
			System.out.println("Number of black targets: " + numberOfTargets(true));
		}
	}

	/*
	 * It reads the graph from a file in NEL format. This format is the
	 * following. The first line contains the number of nodes and the number of
	 * black nodes. The list of black nodes follows. Then, for each node, there
	 * is a line with the node index, its out-degree, its in-degree, and its
	 * name. Finally, the list of all edges follows.
	 */
	public void readFile(String inPath) {
		File inFile = new File(inPath);
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			int index = line.indexOf(' ');
			nn = Integer.parseInt(line.substring(0, index));
			int numberOfBlackNodes = Integer
					.parseInt(line.substring(index + 1));
			isBlack = new boolean[nn];
			nodeAlive = new boolean[nn];
			for (int i = 0; i < nn; i++) {
				isBlack[i] = false;
				nodeAlive[i] = true;
			}
			for (int i = 0; i < numberOfBlackNodes; i++) {
				isBlack[Integer.parseInt(br.readLine())] = true;
			}
			inDegree = new int[nn];
			outDegree = new int[nn];
			nodeName = new String[nn];
			for (int i = 0; i < nn; i++) {
				line = br.readLine();
				index = line.indexOf(' ');
				int secondIndex = line.indexOf(' ', index + 1);
				outDegree[i] = Integer.parseInt(line.substring(index + 1,
						secondIndex));
				index = line.indexOf(' ', secondIndex + 1);
				inDegree[i] = Integer.parseInt(line.substring(secondIndex + 1,
						index));
				nodeName[i] = line.substring(index + 1);
			}
			ne = numberOfEdges();
			adjacency = new boolean[nn][nn];
			labels = new String[nn][nn];
			for(int i = 0; i < nn; i++) {
				for(int j = 0; j < nn; j++) {
					labels[i][j] = "";
				}
			}
			line = br.readLine();
			while (line != null) {
				index = line.indexOf(' ');
				int secondIndex = line.indexOf(' ', index + 1);
				int s = Integer.parseInt(line.substring(0, index));
				int t = Integer.parseInt(line.substring(index + 1, secondIndex));
				adjacency[s][t] = true;
				labels[s][t] = line.substring(secondIndex+1);
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/*
	 * It reads the graph from a array of strings following the NEL format. 
	 * This format is the following. The first line contains the number of nodes and the number of
	 * black nodes. The list of black nodes follows. Then, for each node, there
	 * is a line with the node index, its out-degree, its in-degree, and its
	 * name. Finally, the list of all edges follows.
	 */
	public void readStrings(String[] data) {
		int currentLine = 0;
		String line = data[currentLine++];
		int index = line.indexOf(' ');
		nn = Integer.parseInt(line.substring(0, index));
		int numberOfBlackNodes = Integer
				.parseInt(line.substring(index + 1));
		isBlack = new boolean[nn];
		nodeAlive = new boolean[nn];
		for (int i = 0; i < nn; i++) {
			isBlack[i] = false;
			nodeAlive[i] = true;
		}
		for (int i = 0; i < numberOfBlackNodes; i++) {
			isBlack[Integer.parseInt(data[currentLine++])] = true;
		}
		inDegree = new int[nn];
		outDegree = new int[nn];
		nodeName = new String[nn];
		for (int i = 0; i < nn; i++) {
			line = data[currentLine++];
			index = line.indexOf(' ');
			int secondIndex = line.indexOf(' ', index + 1);
			outDegree[i] = Integer.parseInt(line.substring(index + 1,
					secondIndex));
			index = line.indexOf(' ', secondIndex + 1);
			inDegree[i] = Integer.parseInt(line.substring(secondIndex + 1,
					index));
			nodeName[i] = line.substring(index + 1);
		}
		ne = numberOfEdges();
		adjacency = new boolean[nn][nn];
		labels = new String[nn][nn];
		for(int i = 0; i < nn; i++) {
			for(int j = 0; j < nn; j++) {
				labels[i][j] = "";
			}
		}
		while(currentLine < data.length) {
			line = data[currentLine++];
			index = line.indexOf(' ');
			int secondIndex = line.indexOf(' ', index + 1);
			int s = Integer.parseInt(line.substring(0, index));
			int t = Integer.parseInt(line.substring(index + 1, secondIndex));
			adjacency[s][t] = true;
			labels[s][t] = line.substring(secondIndex+1);
		}
	}
	
	/*
	 * It computes a mapping between the nodes which have not been logically
	 * deleted and the interval between 0 and the number of these nodes minus 1.
	 */
	public int reMap() {
		oldNew = new int[nn];
		newName = new String[nn];
		for (int i = 0; i < nn; i++) {
			oldNew[i] = -1;
		}
		int numberOfAlive = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i]) {
				oldNew[i] = numberOfAlive;
				newName[numberOfAlive] = nodeName[i];
				numberOfAlive = numberOfAlive + 1;
			}
		}		
		return numberOfAlive;
	}

	/*
	 * It writes the graph on a NEL file: to this aim nodes are first mapped to
	 * the interval of numbers between 0 and the number of nodes which have not
	 * been logically deleted.
	 */
	public void writeFile(String outPath, int numberOfAlive) {
		File outFile = new File(outPath);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			int nb = numberOfBlackNodes();
			bw.write(numberOfAlive + " " + nb + "\n");
			int b[] = new int[nb];
			int i = 0;
			for (int j = 0; j < nn; j++) {
				if (nodeAlive[j] && isBlack[j]) {
					b[i] = oldNew[j];
					i = i + 1;
				}
			}
			Utility.sort(b);
			for (i = 0; i < b.length; i++) {
				bw.write(b[i] + "\n");
			}
			for (i = 0; i < nn; i++) {
				if (nodeAlive[i]) {
					bw.write(oldNew[i] + " " + outDegree[i] + " " + inDegree[i]
							+ " " + nodeName[i] + "\n");
				}
			}
			for (i = 0; i < nn; i++) {
				for (int j = 0; j < nn; j++) {
					if (adjacency[i][j]) {
						bw.write(oldNew[i] + " " + oldNew[j] + " " + labels[i][j] + "\n");
					}
				}
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	/*
	 * It recomputes the label of the edges by removing repetitions, transforming it in a set of node-labels
	 * 
	 */
	public void simplifyLabels() {
		for(int i = 0; i < nn; i++)
		{
			for(int j = 0; j < nn; j++)
			{
				if( adjacency[i][j] )
				{
					String[] tokens = labels[i][j].split(";");
					LinkedList<String> newLabels = new LinkedList<String>();
					String newLabel = "";
					for(String token: tokens)
					{
						if( !token.equals("") &&  !newLabels.contains(token) )
						{
							newLabels.add(token);
							newLabel += token +";";
						}
					}
					labels[i][j] = newLabel;
				}
			}
		}
	}

	/*
	 * Returns the total number of nodes, including the ones compressed into meta-nodes or meta-edges
	 * 
	 */
	public int totalNumberOfNodes() {
		return allNodeNames().size();
	}

	
	/*
	 * Returns a list of all node names in the graph (including the compressed ones)
	 * 
	 */
	public List<String> allNodeNames() {
		List<String> names = new ArrayList<String>();
		for(int i = 0; i < nn; i++) {
			String[] nodes = nodeName[i].split(";");
			for(String node: nodes) {
				if( !names.contains(node) )
					names.add(node);
			}
		}
		
		for(int i = 0; i < nn; i++) {
			for(int j = 0; j < nn; j++) {
				if( adjacency[i][j] ) {
					String[] nodes = labels[i][j].split(";");
					for(String node: nodes) {
						if( !names.contains(node) )
							names.add(node);
					}									
				}				
			}
		}
		
		return names;
	}
	
	/*
	 * Returns the total number of black nodes (including the compressed ones)
	 * 
	 */
	public int totalNumberOfBlackNodes() {
		List<String> names = new ArrayList<String>();
		for(int i = 0; i < nn; i++) {
			if( isBlack[i] ) {
				String[] nodes = nodeName[i].split(";");
				for(String node: nodes) {
					if( !names.contains(node) )
						names.add(node);
				}
			}
		}
		return names.size();
	}

	public void setNumNodes(int n) {
		nn = n;
		isBlack = new boolean[nn];
		nodeAlive = new boolean[nn];
		for (int i = 0; i < nn; i++) {
			isBlack[i] = false;
			nodeAlive[i] = false;
		}
	}	
	
	public int getNumNodes() {
		return nn;
	}
	
	public boolean areAdjacent(int u, int v) {
		return adjacency[u][v];
	}
	
	public int getInDegree(int u) {
		return inDegree[u];
	}

	public int getOutDegree(int u) {
		return outDegree[u];
	}
	
	public String getLabel(int u, int v) {
		return labels[u][v];
	}
	
	public boolean isBlack(int c) {
		return isBlack[c];
	}
	
	public boolean isAlive(int u) {
		return nodeAlive[u];
	}
	
	public String getNodeName(int c) {
		return nodeName[c];
	}
	
	public void setNodeName(int u, String name) {
		nodeName[u] = name;
	}
	
	public int getIdFromNodeName(String name) {
		for(int i = 0; i < nodeName.length; i++) {
			String[] names = nodeName[i].split(";");
			for(int j = 0; j < names.length; j++)
			if( names[j].equals(name) ) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean[][] getAdjacencyMatrix() {
		return adjacency;
	}
	

}
