/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package network;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import application.InputParameters;

public class EL2NEL extends Convert2NEL {

	public void readELFile(String inPath) {
		File inFile = new File(inPath);
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine().trim();
			while (line != null && line.startsWith("#")) {
				line = br.readLine().trim();
			}
			
			// Identify the nodes
			Set<String> nodes = new HashSet<String>();
			while (line != null ) {
				String[] arc = line.split("\t");
				if( arc.length != 2 ) {
					System.out.println("ERROR: Line with wrong format. "+line);
				}
				if( !nodes.contains(arc[0]) ) {
					nodes.add(arc[0]);
				}
				if( !nodes.contains(arc[1]) ) {
					nodes.add(arc[1]);
				}
				line = br.readLine();
			}
			br.close();
			
			numNodes = nodes.size();
			adjacency = new boolean[numNodes][numNodes];
			edgeLabels=new String[numNodes][numNodes];
			inDegree = new int[numNodes];
			outDegree = new int[numNodes];
			map = new HashMap<String, Integer>();
			nodeName = new String[numNodes];
			
			// creates the map of vertices and node names
			int index = 0;
			for(String node: nodes) {
				map.put(node, index);
				nodeName[index++] = node;				
			}
			
			// starts again now to recover the arcs
			br = new BufferedReader(new FileReader(inFile));
			line = br.readLine().trim();
			while (line != null && line.startsWith("#")) {
				line = br.readLine().trim();
			}
			
			while ( line != null ) {
				String[] arc = line.split("\t");
				if( arc.length != 2 ) {
					System.out.println("ERROR: Line with wrong format. "+line);
				}
				adjacency[map.get(arc[0])][map.get(arc[1])] = true;
				edgeLabels[map.get(arc[0])][map.get(arc[1])] = map.get(arc[0]) +"->"+map.get(arc[1]);
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public String run(String nn) {
		readBlackNodesFile(nn + ".bn");
		readELFile(nn + ".xml");
		initDegrees();
		deleteSelfLoops();
		String outputFile = nn; 
		if (InputParameters.ignoreReversibility) {
			outputFile = nn + "_ONEWAY"; 
		}
		writeFile(outputFile+".nel");
		return outputFile;
	}

}
