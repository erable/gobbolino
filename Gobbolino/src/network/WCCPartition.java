/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package network;
import java.util.Stack;

/*
 * This class allows us to compute the weakly connected components of a graph stored in a NEL file. 
 */
public class WCCPartition {
	protected int[] component;
	protected int currentComponent;
	protected int numComponents;
	protected Stack<Integer> partial;
	protected NELNetwork network;
	
	public WCCPartition(NELNetwork network){
		this.network = network;
	}
	
	/*
	 * This is a recursive version of the depth-first search which is used in
	 * order to compute the weakly connected components of the graph. 
	 */
	void extendedRecursiveDFS(int u) {
		partial.push(u);
		component[u] = currentComponent;
		for (int v = 0; v < network.nn; v++) {
			if (network.adjacency[u][v]) {
				if (component[v] == -1) {
					extendedRecursiveDFS(v);
				} 
				else 
				{
					currentComponent = component[v];
					for(int i = 0; i < partial.size(); i++) {
						component[partial.get(i)] = currentComponent;
					}
				}
			}
		}
	}

	/*
	 * It reads the graph NEL file, computes the weakly connected componentes
	 */
	public void run(){
		weaklyConnectedComponents();
	}

	/*
	 * It computes the weakly connected components of the graph.
	 */
	void weaklyConnectedComponents() {
		component = new int[network.nn];
		for (int s = 0; s < network.nn; s++) {
			component[s] = -1;
		}
		partial = new Stack<Integer>();
		numComponents = 0;
		for (int s = 0; s < network.nn; s++) {
			if ( component[s] == -1) {
				currentComponent = numComponents;
				extendedRecursiveDFS(s);
				if( currentComponent == numComponents )
					numComponents = numComponents + 1;
				else
					numComponents = currentComponent + 1;
			}
		}
	}
	
	public int getNumComponents() {
		return numComponents;
	}
	
	public int getComponent(int c) {
		return component[c];
	}
}
