package analysis.experimentGenerator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;

public class ScriptGenerator2 {

	public static void main(String[] args) {

		String path = "./input/SEA-networks/";
		File[] pathwayFiles = new File(path).listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".nel"); 
			}
		});
		
		try {
			BufferedWriter bw_msG = new BufferedWriter(new FileWriter("./SEA/scripts2/2ndExperiment_gobbolino.sh"));
			BufferedWriter bw_msC = new BufferedWriter(new FileWriter("./SEA/scripts2/2ndExperiment_touche.sh"));
			for(int i = 0; i < pathwayFiles.length; i++) {
				System.out.println((i+1)+") "+pathwayFiles[i].getName());
	
				try {
					String scriptGobbolino = (i+1)+"_gobbolino_"+pathwayFiles[i].getName()+".sh";
					//bw_msG.write("qsub -q q1hour "+scriptGobbolino+"\n");
					bw_msG.write("./"+scriptGobbolino+"\n");
					BufferedWriter bw = new BufferedWriter(new FileWriter("./SEA/scripts2/"+scriptGobbolino));
					bw.write("java -jar -Xmx1500M gobbolino.jar -n=\""+pathwayFiles[i].getName().substring(0, pathwayFiles[i].getName().length()-4)+"\" -q=10000 -stop=TIME -stopTime=1 -stopTimeCondition=elapsed >gobbolino_"+pathwayFiles[i].getName()+".log");
					bw.close();
					
					String scriptTouche = (i+1)+"_touche_"+pathwayFiles[i].getName()+".sh";
					bw_msC.write("./"+scriptTouche+"\n");
					bw = new BufferedWriter(new FileWriter("./SEA/scripts2/"+scriptTouche));
					bw.write("java -jar -Xmx1500M touche.jar -f=\""+pathwayFiles[i].getName()+"\" -verb=0 -pruning=2 -threads=2 >touche_"+pathwayFiles[i].getName()+".log");
					bw.close();
				} catch (IOException e) {
					System.out.println("Error writing script for: "+pathwayFiles[i].getName());
				}
			}
			bw_msG.close();
			bw_msC.close();
		} catch(IOException e) {
			System.out.println("Error writing main script");			
		}
	}

}
