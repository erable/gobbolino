package analysis.experimentGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ScriptGenerator {

	public static void main(String[] args) {

		String path = "./output/pathway/pathways";
		File[] pathwayFiles = new File(path).listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith("_LP_S.nel"); 
			}
		});
		
		try {
			int countOK = 0;
			BufferedWriter bw_msG = new BufferedWriter(new FileWriter("./SEA/scripts/1stExperiment_gobbolino.sh"));
			BufferedWriter bw_msT = new BufferedWriter(new FileWriter("./SEA/scripts/1stExperiment_touche.sh"));
			for(int i = 0; i < pathwayFiles.length; i++) {
				System.out.println((i+1)+") "+pathwayFiles[i].getName());
				
				BufferedReader bbr;
				try {
					bbr = new BufferedReader(new FileReader(path+"/"+pathwayFiles[i].getName()));
					String line = bbr.readLine();
					String[] nn = line.split(" ");
					if( Integer.parseInt(nn[0]) <= 10 ) {
						countOK++;
						System.out.println(nn[0]+" -> OK");
	
						String scriptGobbolino = (i+1)+"_gobbolino_"+pathwayFiles[i].getName()+".sh";
						bw_msG.write("./"+scriptGobbolino+"\n");
						BufferedWriter bw = new BufferedWriter(new FileWriter("./SEA/scripts/"+scriptGobbolino));
						bw.write("java -jar -Xmx1000M gobbolino.jar -n=\""+pathwayFiles[i].getName().substring(0, pathwayFiles[i].getName().length()-4)+"\" -q=all >gobbolino_"+pathwayFiles[i].getName()+".log");
						bw.close();
						
						String scriptTouche = (i+1)+"_touche_"+pathwayFiles[i].getName()+".sh";
						bw_msT.write("./"+scriptTouche+"\n");
						bw = new BufferedWriter(new FileWriter("./SEA/scripts/"+scriptTouche));
						bw.write("java -jar -Xmx1000M touche.jar -f=\""+pathwayFiles[i].getName()+"\" -verbose=0 -pruning=2 -threads=2 >touche_"+pathwayFiles[i].getName()+".log");
						bw.close();
					}
					bbr.close();
				} catch (FileNotFoundException e) {
					System.out.println("File not found: "+pathwayFiles[i].getName());
				} catch (IOException e) {
					System.out.println("Error reading file: "+pathwayFiles[i].getName());
				}
			}
			System.out.println("#OK: "+countOK);
			bw_msG.close();
			bw_msT.close();
		} catch (FileNotFoundException e) {
			System.out.println("Main script file not found.");
		} catch (IOException e) {
			System.out.println("Error writing main script file.");
		}
		
	}

}
