package analysis.experimentGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ExperimentResultsE3 {

	public static void main(String[] args) {

		String path = "./input/SEA-Networks";
		File[] pathwayFiles = new File(path).listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".nel"); 
			}
		});

		int c = 1;
		try {			
			BufferedWriter bw = new BufferedWriter(new FileWriter("./SEA/exp3/results.csv"));
			BufferedWriter bw_texTable = new BufferedWriter(new FileWriter("./SEA/exp3/results.tex"));
			bw.write("pathway;nn;nbn;stories;no;sc;bc\n");
			for(int i = 0; i < pathwayFiles.length; i++) {
				System.out.println((i+1)+") "+pathwayFiles[i].getName());

				File fileCheck = new File("./SEA/exp3/ciak_none_"+pathwayFiles[i].getName()+".log");
				if( fileCheck.exists() ) {
					BufferedReader bbr;
					try {
						bbr = new BufferedReader(new FileReader(path+"/"+pathwayFiles[i].getName()));
						String line = bbr.readLine();
						String[] nn = line.split(" ");
	
						bw.write(pathwayFiles[i].getName()+";"+nn[0]+";"+nn[1]+";");
						bw_texTable.write((c++)+" & \\scriptsize{"+pathwayFiles[i].getName().substring(0,pathwayFiles[i].getName().indexOf('.'))+"} & "+nn[0]+" & "+nn[1]+" & ");
	
						// reading results for ciak with no pruning
						BufferedReader bbrLog = new BufferedReader(new FileReader("./SEA/exp3/ciak_none_"+pathwayFiles[i].getName()+".log"));
						String[] lines = bbrLog.readLine().split("\t");
						bw.write(lines[1]+";"+lines[2].replace(".", ",")+";");	// # stories + time
						bw_texTable.write(lines[1]+" & " + lines[2].replace(".", ",")+" & ");	// # stories + time
						
						// reading results for ciak with the sc pruning
						bbrLog = new BufferedReader(new FileReader("./SEA/exp3/ciak_sc_"+pathwayFiles[i].getName()+".log"));
						bbrLog.readLine();	// salta a primeira linha...
						lines = bbrLog.readLine().split("\t");
						bw.write(lines[2].replace(".", ",")+";");	// # stories + time
						bw_texTable.write(lines[2].replace(".", ",")+" & ");	// # stories + time
	
						// reading results for ciak with the biconnected component pruning
						bbrLog = new BufferedReader(new FileReader("./SEA/exp3/ciak_bc_"+pathwayFiles[i].getName()+".log"));
						lines = bbrLog.readLine().split("\t");
						bw.write(lines[2].replace(".", ",")+"\n");	// # stories + time
						bw_texTable.write(lines[2].replace(".", ",")+" \\\\\n\\hline\n");	// # stories + time
											
						bbrLog.close();
						bbr.close();
					} catch (FileNotFoundException e) {
						System.out.println("File not found: "+pathwayFiles[i].getName());
					} catch (IOException e) {
						System.out.println("Error reading file: "+pathwayFiles[i].getName());
					}
				}
			}
			bw.close();
			bw_texTable.close();
		} catch (IOException e) {
			System.out.println("Error writing file results.csv");
		}
		
	}

}
