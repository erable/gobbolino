package analysis.experimentGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ScriptGeneratorPruning2 {
	
	public static void main(String[] args) {

		String path = "./input/SEA-networks/";
		File[] pathwayFiles = new File(path).listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".nel"); 
			}
		});
		
		try {
			BufferedWriter bw_msNONE = new BufferedWriter(new FileWriter("./SEA/scripts6/3rdExperiment_ciak_2_none.sh"));
			BufferedWriter bw_msPRUNING = new BufferedWriter(new FileWriter("./SEA/scripts6/3rdExperiment_ciak_2_pruning.sh"));
			BufferedReader bbr;
		
			for(int i = 0; i < pathwayFiles.length; i++) {
				System.out.println((i+1)+") "+pathwayFiles[i].getName());
				
				try {
					bbr = new BufferedReader(new FileReader(path+"/"+pathwayFiles[i].getName()));
					String line = bbr.readLine();
					String[] nn = line.split(" ");
					if( Integer.parseInt(nn[0]) <= 30 ) {
						String scriptCiak_none = (i+1)+"_ciak_none_"+pathwayFiles[i].getName()+".sh";
						bw_msNONE.write("./"+scriptCiak_none+"\n");
						BufferedWriter bw = new BufferedWriter(new FileWriter("./SEA/scripts6/"+scriptCiak_none));
						bw.write("java -jar -Xmx1500M ciak.jar -f=\""+pathwayFiles[i].getName()+"\" -verb=0 -allpitches >ciak_none_"+pathwayFiles[i].getName()+".log");
						bw.close();
	
						String scriptCiak_sc = (i+1)+"_ciak_pruning_"+pathwayFiles[i].getName()+".sh";
						bw_msPRUNING.write("./"+scriptCiak_sc+"\n");
						bw = new BufferedWriter(new FileWriter("./SEA/scripts6/"+scriptCiak_sc));
						bw.write("java -jar -Xmx1500M ciak.jar -f=\""+pathwayFiles[i].getName()+"\" -verb=0 >ciak_pruning_"+pathwayFiles[i].getName()+".log");
						bw.close();
					}
				
				} catch (IOException e) {
					System.out.println("Error writing script for: "+pathwayFiles[i].getName());
				}
			}
			bw_msNONE.close();
			bw_msPRUNING.close();
		} catch(IOException e) {
			System.out.println("Error writing main script");			
		}
		
	}
	
}
