package analysis.experimentGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;

public class ExperimentResultsE2 {

	public static void main(String[] args) {

		String path = "./input/SEA-Networks";
		File[] pathwayFiles = new File(path).listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".nel"); 
			}
		});

		int c = 1;
		try {			
			BufferedWriter bw = new BufferedWriter(new FileWriter("./SEA/exp2/results.csv"));
			BufferedWriter bw_texTable = new BufferedWriter(new FileWriter("./SEA/exp2/results.tex"));
			bw.write("pathway;nn;nbn;gobbolino_stories;gobbolino_time;ciak_stories;ciak_time;touche_stories;touche_time\n");
			DecimalFormat df = new DecimalFormat("0.000");
			for(int i = 0; i < pathwayFiles.length; i++) {
				System.out.println((i+1)+") "+pathwayFiles[i].getName());
				
				BufferedReader bbr;
				try {
					bbr = new BufferedReader(new FileReader(path+"/"+pathwayFiles[i].getName()));
					String line = bbr.readLine();
					String[] nn = line.split(" ");

					bw.write(pathwayFiles[i].getName()+";"+nn[0]+";"+nn[1]+";");
					bw_texTable.write((c++)+" & \\scriptsize{"+pathwayFiles[i].getName().substring(0,pathwayFiles[i].getName().indexOf('.'))+"} & "+nn[0]+" & "+nn[1]+" & ");

					// reading results for gobbolino
					BufferedReader bbrLog = new BufferedReader(new FileReader("./SEA/exp2/gobbolino_"+pathwayFiles[i].getName()+".log"));
					bbrLog.readLine();
					String[] lines = bbrLog.readLine().split(" ");
					bw.write(lines[2]+";");	// # stories
					bw_texTable.write(lines[2]+" & ");
					
					bbrLog.readLine();
					lines = bbrLog.readLine().split(" ");
					String timeGobbolino = df.format((Integer.parseInt(lines[3])/1000.0)/60.0);
					bw.write(timeGobbolino.replace(".",",")+";");	// time
					bw_texTable.write(timeGobbolino.replace(".",",")+" & ");
					bbrLog.close();
					
					// reading results for ciak
					bbrLog = new BufferedReader(new FileReader("./SEA/exp2/ciak_"+pathwayFiles[i].getName()+".log"));
					lines = bbrLog.readLine().split("\t");
					String timeCiak = df.format((Double.parseDouble(lines[2])/60.0));
					bw.write(lines[1]+";"+timeCiak.replace(".", ",")+";");	// # stories + time
					
					// reading results for touch�
					bbrLog = new BufferedReader(new FileReader("./SEA/exp2/touche_"+pathwayFiles[i].getName()+".log"));
					line = bbrLog.readLine();
					if( line.isEmpty() ) {
						bbrLog.readLine();	// ignore the two first lines
						String time = bbrLog.readLine().split(" ")[2];
						String stories = bbrLog.readLine().split(" ")[4];
						bw.write(stories+";"+time.replace(".", ",")+"\n");	// # stories + time
						bw_texTable.write(time.replace(".", ",")+" \\\\\n\\hline\n");	// # stories + time												
					} else {
						lines = line.split("\t");
						String timeTouche = df.format((Double.parseDouble(lines[2])/60.0));
						bw.write(lines[1]+";"+timeTouche.replace(".", ",")+"\n");	// # stories + time
						bw_texTable.write(lines[1]+" & " + timeTouche.replace(".", ",")+" \\\\\n\\hline\n");	// # stories + time
					}
					bbrLog.close();
					bbr.close();
				} catch (FileNotFoundException e) {
					System.out.println("File not found: "+pathwayFiles[i].getName());
				} catch (IOException e) {
					System.out.println("Error reading file: "+pathwayFiles[i].getName());
				}
			}
			bw.close();
			bw_texTable.close();
		} catch (IOException e) {
			System.out.println("Error writing file results.csv");
		}
		
	}

}
