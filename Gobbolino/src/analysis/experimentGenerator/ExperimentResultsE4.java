package analysis.experimentGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ExperimentResultsE4 {

	public static void main(String[] args) {

		String path = "./output/pathway/pathways";
		File[] pathwayFiles = new File(path).listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith("_LP_S.nel"); 
			}
		});
		
		String path2 = "./input/SEA-Networks";
		File[] networkFiles = new File(path2).listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".nel"); 
			}
		});
		

		int c = 1;
		try {			
			BufferedWriter bw = new BufferedWriter(new FileWriter("./SEA/exp4/results.csv"));
			BufferedWriter bw_texTable = new BufferedWriter(new FileWriter("./SEA/exp4/results.tex"));
			bw.write("pathway;nn;nbn;stories_no;time_no;stories_pruned;time_pruned\n");
			for(int i = 0; i < pathwayFiles.length; i++) {
				System.out.println((i+1)+") "+pathwayFiles[i].getName());

				BufferedReader bbr;
				try {
					bbr = new BufferedReader(new FileReader(path+"/"+pathwayFiles[i].getName()));
					String line = bbr.readLine();
					String[] nn = line.split(" ");

					if( Integer.parseInt(nn[0]) <= 10 ) {
						bw.write(pathwayFiles[i].getName()+";"+nn[0]+";"+nn[1]+";");
						String nameForTex = pathwayFiles[i].getName().substring(0,pathwayFiles[i].getName().indexOf('.'));
						nameForTex = nameForTex.replace("wholeYeast_for_", "");
						nameForTex = nameForTex.replace("_LP_S", "");
						bw_texTable.write((c++)+" & \\scriptsize{"+nameForTex+"} & "+nn[0]+" & "+nn[1]+" & ");
	
						// reading results for ciak with no pruning
						BufferedReader bbrLog = new BufferedReader(new FileReader("./SEA/exp4/ciak_none_"+pathwayFiles[i].getName()+".log"));
						String[] lines = bbrLog.readLine().split("\t");
						bw.write(lines[1]+";"+lines[2].replace(".", ",")+";");	// # stories + time
						bw_texTable.write(lines[1]+" & " + lines[2].replace(".", ",")+" & ");	// # stories + time
						
						// reading results for ciak with the pruning
						bbrLog = new BufferedReader(new FileReader("./SEA/exp4/ciak_pruning_"+pathwayFiles[i].getName()+".log"));
						lines = bbrLog.readLine().split("\t");
						bw.write(lines[1]+";"+lines[2].replace(".", ",")+"\n");	// # stories + time
						bw_texTable.write(lines[2].replace(".", ",")+" \\\\\n\\hline\n");	// # stories + time
											
						bbrLog.close();
					}
					bbr.close();
				} catch (FileNotFoundException e) {
					System.out.println("File not found: "+pathwayFiles[i].getName());
				} catch (IOException e) {
					System.out.println("Error reading file: "+pathwayFiles[i].getName());
				}
			}
			/*
			for(int i = 0; i < networkFiles.length; i++) {
				System.out.println((i+1)+") "+networkFiles[i].getName());

				BufferedReader bbr;
				try {
					bbr = new BufferedReader(new FileReader(path2+"/"+networkFiles[i].getName()));
					String line = bbr.readLine();
					String[] nn = line.split(" ");

					if( Integer.parseInt(nn[0]) <= 30 ) {
						bw.write(networkFiles[i].getName()+";"+nn[0]+";"+nn[1]+";");
						String nameForTex = networkFiles[i].getName().substring(0,networkFiles[i].getName().indexOf('.'));
						nameForTex = nameForTex.replace("wholeYeast_for_", "");
						nameForTex = nameForTex.replace("_LP_S", "");
						bw_texTable.write((c++)+" & \\scriptsize{"+nameForTex+"} & "+nn[0]+" & "+nn[1]+" & ");
	
						// reading results for ciak with no pruning
						BufferedReader bbrLog = new BufferedReader(new FileReader("./SEA/exp4/ciak_none_"+networkFiles[i].getName()+".log"));
						String[] lines = bbrLog.readLine().split("\t");
						bw.write(lines[1]+";"+lines[2].replace(".", ",")+";");	// # stories + time
						bw_texTable.write(lines[1]+" & " + lines[2].replace(".", ",")+" & ");	// # stories + time
						
						// reading results for ciak with the pruning
						bbrLog = new BufferedReader(new FileReader("./SEA/exp4/ciak_pruning_"+networkFiles[i].getName()+".log"));
						lines = bbrLog.readLine().split("\t");
						bw.write(lines[1]+";"+lines[2].replace(".", ",")+"\n");	// # stories + time
						bw_texTable.write(lines[2].replace(".", ",")+" \\\\\n\\hline\n");	// # stories + time
											
						bbrLog.close();
					}
					bbr.close();
				} catch (FileNotFoundException e) {
					System.out.println("File not found: "+networkFiles[i].getName());
				} catch (IOException e) {
					System.out.println("Error reading file: "+networkFiles[i].getName());
				}
			}
			*/
			bw.close();
			bw_texTable.close();
		} catch (IOException e) {
			System.out.println("Error writing file results.csv");
		}
		
	}

}
