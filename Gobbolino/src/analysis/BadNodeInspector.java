package analysis;

import network.BlackSCCComputer;
import network.NELNetwork;

public class BadNodeInspector {

	public static void main(String[] args) {
		NELNetwork network = new NELNetwork();
		network.readFile(args[0]);
		
		// one "easy" way to try to find one bad node is to decompose the network into its sccs
		// and to see if a white node lies inside one of them in the presence of black nodes.
		BlackSCCComputer scc = new BlackSCCComputer(network);
		scc.run(-1, false);
	}

}
