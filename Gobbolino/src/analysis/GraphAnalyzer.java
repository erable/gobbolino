/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package analysis;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.LinkedList;
import java.util.Stack;

import network.NELNetwork;

public class GraphAnalyzer extends NELNetwork {
	int backwardDiameter;
	int forwardDiameter;
	boolean[] isBad;
	int[] dfsNumber;
	boolean[] complete;
	int counter;
	Stack<Integer> partial;
	Stack<Integer> representative;
	Stack<Integer> marked;
	Stack<Integer> point;
	boolean[] mark;
	int lastComponent;
	int[] component, componentSize, numberOfBadNodesInSCC,
			numberOfBlackNodesInSCC;
	int numberOfBadNodes, numberOfCycles;

	boolean backtrack(int s, int v) {
		boolean f = false;
		point.push(v);
		mark[v] = true;
		marked.push(v);
		for (int w = 0; w < nn; w++) {
			if (adjacency[v][w]) {
				if (w < s) {
					adjacency[v][w] = false;
				} else if (w == s) {
					printCircuit(point);
					f = true;
				} else if (!mark[w]) {
					boolean g = backtrack(s, w);
					f = f || g;
				}
			}
		}
		if (f) {
			while (marked.peek() != v) {
				int u = marked.pop();
				mark[u] = false;
			}
			marked.pop();
			mark[v] = false;
		}
		point.pop();
		return f;
	}

	int blackEccentricity(int s, boolean forward) {
		boolean[] reached = new boolean[nn];
		int[] distance = new int[nn];
		for (int i = 0; i < nn; i++) {
			reached[i] = false;
			distance[i] = 0;
		}
		LinkedList<Integer> queue = new LinkedList<Integer>();
		queue.offer(s);
		while (!queue.isEmpty()) {
			int u = queue.poll();
			if (!reached[u]) {
				reached[u] = true;
				for (int x = 0; x < nn; x++) {
					if (forward) {
						if (adjacency[u][x]) {
							if (distance[x] == 0) {
								distance[x] = distance[u] + 1;
							}
							queue.offer(x);
						}
					} else {
						if (adjacency[x][u]) {
							if (distance[x] == 0) {
								distance[x] = distance[u] + 1;
							}
							queue.offer(x);
						}
					}
				}
			}
		}
		int max = 0;
		for (int i = 0; i < nn; i++) {
			if (isBlack[i]) {
				if (distance[i] > max) {
					max = distance[i];
				}
			}
		}
		return max;
	}

	boolean checkSCC(boolean onlyWhite) {
		boolean[] reached = new boolean[nn];
		for (int c = 0; c < lastComponent; c++) {
			for (int i = 0; i < nn; i++) {
				if ((!onlyWhite || (onlyWhite && !isBlack[i]))
						&& component[i] == c) {
					visit(i, true, reached);
					for (int j = 0; j < nn; j++) {
						if ((!onlyWhite || (onlyWhite && !isBlack[j]))
								&& component[j] == c) {
							if (!reached[j]) {
								return false;
							}
						}
					}
				}
			}
		}
		return true;
	}

	void computeBadNodes() {
		numberOfBadNodes = 0;
		isBad = new boolean[nn];
		for (int i = 0; i < nn; i++) {
			isBad[i] = false;
		}
		for (int i = 0; i < nn; i++) {
			if (!isBlack[i]) {
				isBad[i] = isBad(i);
				if (isBad[i]) {
					numberOfBadNodes = numberOfBadNodes + 1;
				}
			}
		}
	}

	void computeBadNodesInSCCs() {
		numberOfBadNodesInSCC = new int[lastComponent];
		for (int i = 0; i < lastComponent; i++) {
			numberOfBadNodesInSCC[i] = 0;
		}
		for (int i = 0; i < nn; i++) {
			if (isBad[i]) {
				numberOfBadNodesInSCC[component[i]] = numberOfBadNodesInSCC[component[i]] + 1;
			}
		}
	}

	void computeBlackNodesInSCCs() {
		numberOfBlackNodesInSCC = new int[lastComponent];
		for (int i = 0; i < lastComponent; i++) {
			numberOfBlackNodesInSCC[i] = 0;
		}
		for (int i = 0; i < nn; i++) {
			if (isBlack[i]) {
				numberOfBlackNodesInSCC[component[i]] = numberOfBlackNodesInSCC[component[i]] + 1;
			}
		}
	}

	void computeComponentSizes(boolean onlyWhite) {
		componentSize = new int[lastComponent];
		for (int i = 0; i < lastComponent; i++) {
			componentSize[i] = 0;
		}
		for (int i = 0; i < nn; i++) {
			if (!onlyWhite || (onlyWhite && !isBlack[i])) {
				componentSize[component[i]] = componentSize[component[i]] + 1;
			}
		}
	}

	void computeDiameter(boolean forward) {
		int diameter = 0;
		for (int i = 0; i < nn; i++) {
			if (isBlack[i]) {
				int ecc = blackEccentricity(i, forward);
				if (ecc > diameter) {
					diameter = ecc;
				}
			}
		}
		if (forward) {
			forwardDiameter = diameter;
		} else {
			backwardDiameter = diameter;
		}
	}

	void enumerateAllCircuits() {
		mark = new boolean[nn];
		for (int i = 0; i < nn; i++) {
			mark[i] = false;
		}
		marked = new Stack<Integer>();
		point = new Stack<Integer>();
		for (int s = 0; s < nn; s++) {
			backtrack(s, s);
			while (!marked.isEmpty()) {
				int u = marked.pop();
				mark[u] = false;
			}
		}
	}

	void extendedRecursiveDFS(int u, boolean onlyWhite) {
		dfsNumber[u] = counter;
		counter = counter + 1;
		partial.push(u);
		representative.push(u);
		for (int v = 0; v < nn; v++) {
			if ((!onlyWhite || (onlyWhite && !isBlack[v])) && adjacency[u][v]) {
				if (dfsNumber[v] == -1) {
					extendedRecursiveDFS(v, onlyWhite);
				} else if (!complete[v]) {
					while (dfsNumber[representative.peek()] > dfsNumber[v]) {
						representative.pop();
					}
				}
			}
		}
		if (u == representative.peek()) {
			int z = partial.pop();
			component[z] = lastComponent;
			complete[z] = true;
			while (z != u) {
				z = partial.pop();
				component[z] = lastComponent;
				complete[z] = true;
			}
			representative.pop();
			lastComponent = lastComponent + 1;
		}
	}

	boolean isBad(int i) {
		int[] pred = new int[inDegree[i]];
		int[] succ = new int[outDegree[i]];
		int j = 0;
		for (int p = 0; p < nn; p++) {
			if (adjacency[p][i]) {
				adjacency[p][i] = false;
				inDegree[i] = inDegree[i] - 1;
				outDegree[p] = outDegree[p] - 1;
				pred[j] = p;
				j = j + 1;
			}
		}
		j = 0;
		for (int s = 0; s < nn; s++) {
			if (adjacency[i][s]) {
				adjacency[i][s] = false;
				inDegree[s] = inDegree[s] - 1;
				outDegree[i] = outDegree[i] - 1;
				if (j == succ.length) {
					System.out.println(i);
					System.exit(-1);
				}
				succ[j] = s;
				j = j + 1;
			}
		}
		boolean[] reached = new boolean[nn];
		boolean res = true;
		for (int s = 0; res && s < succ.length; s++) {
			visit(succ[s], true, reached);
			for (int p = 0; p < pred.length; p++) {
				if (!reached[pred[p]]) {
					res = false;
					break;
				}
			}
		}
		for (int p = 0; p < pred.length; p++) {
			adjacency[pred[p]][i] = true;
			inDegree[i] = inDegree[i] + 1;
			outDegree[pred[p]] = outDegree[pred[p]] + 1;
		}
		for (int s = 0; s < succ.length; s++) {
			adjacency[i][succ[s]] = true;
			inDegree[succ[s]] = inDegree[succ[s]] + 1;
			outDegree[i] = outDegree[i] + 1;
		}
		return res;
	}

	int numberOfBlackTargets() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			if (outDegree[i] == 0 && isBlack[i]) {
				result = result + 1;
			}
		}
		return result;
	}

	int numberOfBlackSources() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			if (inDegree[i] == 0 && isBlack[i]) {
				result = result + 1;
			}
		}
		return result;
	}

	void printBadNodes() {
		System.out.print("=== List of bad nodes: ");
		for (int i = 0; i < nn; i++) {
			if (isBad[i]) {
				System.out.print(i + " ");
			}
		}
		System.out.println();
	}

	void printBadNodesInSCCs() {
		System.out.print("=== List of numbers of bad nodes in SCCs: ");
		for (int i = 0; i < lastComponent - 1; i++) {
			System.out.print(numberOfBadNodesInSCC[i] + ", ");
		}
		System.out.print(numberOfBadNodesInSCC[lastComponent - 1] + "\n");
	}

	void printBlackNodesInSCCs() {
		System.out.print("=== List of numbers of black nodes in SCCs: ");
		for (int i = 0; i < lastComponent - 1; i++) {
			System.out.print(numberOfBlackNodesInSCC[i] + ", ");
		}
		System.out.print(numberOfBlackNodesInSCC[lastComponent - 1] + "\n");
	}

	void printCircuit(Stack<Integer> stack) {
		// System.out.print("New circuit: ");
		// for (int i = 0; i < stack.size(); i++) {
		// System.out.print(stack.elementAt(i) + " ");
		// }
		// System.out.println();
		numberOfCycles++;
	}

	void printSCC(String networkName) {
		for (int c = 0; c < lastComponent; c++) {
			File outFile = new File(networkName + "SCC" + c + ".nel");
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
				bw.write(componentSize[c] + "\n");
				for (int i = 0; i < nn; i++) {
					if (component[i] == c) {
						int outDegree = 0;
						int inDegree = 0;
						for (int j = 0; j < nn; j++) {
							if (adjacency[i][j] && component[j] == c) {
								outDegree = outDegree + 1;
							}
							if (adjacency[j][i] && component[j] == c) {
								inDegree = inDegree + 1;
							}
						}
						bw.write(i + " " + outDegree + " " + inDegree + "\n");
					}
				}
				for (int i = 0; i < nn; i++) {
					if (component[i] == c) {
						for (int j = 0; j < nn; j++) {
							if (adjacency[i][j] && component[j] == c) {
								bw.write(i + " " + j + "\n");
							}
						}
					}
				}
				bw.close();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
	}

	void printSCCSizes(boolean onlyWhite) {
		System.out.print("=== List of SCC sizes: ");
		for (int i = 0; i < lastComponent - 1; i++) {
			System.out.print(componentSize[i] + ", ");
		}
		System.out.print(componentSize[lastComponent - 1] + "\n");
	}

	public void run(String networkName, boolean verbose, boolean onlyWhite) {
		readFile(networkName + ".nel");
		if (verbose) {
			System.out.println("=== Network name: " + networkName);
			System.out.println("=== Number of nodes: " + nn);
		}
		if (verbose) {
			System.out.println("=== Number of black sources: "
					+ numberOfBlackSources());
		}
		if (verbose) {
			System.out.println("=== Number of black targets: "
					+ numberOfBlackTargets());
		}
		computeDiameter(true);
		if (verbose) {
			System.out.println("=== Forward diameter: " + forwardDiameter);
		}
		computeDiameter(false);
		if (verbose) {
			System.out.println("=== Backward diameter: " + backwardDiameter);
		}
		computeBadNodes();
		if (verbose) {
			System.out.println("=== Number of bad nodes: " + numberOfBadNodes);
			printBadNodes();
		}
		stronglyConnectedComponents(onlyWhite);
		if (!checkSCC(onlyWhite)) {
			System.out.println("WARNING: SCCs are not correct");
		}
		computeComponentSizes(onlyWhite);
		if (verbose) {
			System.out.println("=== Number of SCCs: " + lastComponent);
		}
		if (verbose) {
			printSCCSizes(onlyWhite);
		}
		if (!onlyWhite) {
			computeBlackNodesInSCCs();
			if (verbose) {
				printBlackNodesInSCCs();
			}
		}
		computeBadNodesInSCCs();
		if (verbose) {
			printBadNodesInSCCs();
		}
		if (verbose) {
			printSCC(networkName);
		}
		numberOfCycles = 0;
		enumerateAllCircuits();
		if (verbose) {
			System.out.println("=== Number of cycles: " + numberOfCycles);
		}
	}

	void stronglyConnectedComponents(boolean onlyWhite) {
		dfsNumber = new int[nn];
		complete = new boolean[nn];
		component = new int[nn];
		for (int s = 0; s < nn; s++) {
			dfsNumber[s] = -1;
			complete[s] = false;
			component[s] = -1;
		}
		partial = new Stack<Integer>();
		representative = new Stack<Integer>();
		lastComponent = 0;
		counter = 0;
		for (int s = 0; s < nn; s++) {
			if ((!onlyWhite || (onlyWhite && !isBlack[s]))
					&& dfsNumber[s] == -1) {
				extendedRecursiveDFS(s, onlyWhite);
			}
		}
	}

	void visit(int s, boolean forward, boolean[] reached) {
		for (int j = 0; j < nn; j++) {
			reached[j] = false;
		}
		LinkedList<Integer> queue = new LinkedList<Integer>();
		queue.offer(s);
		while (!queue.isEmpty()) {
			int u = queue.poll();
			if (!reached[u]) {
				reached[u] = true;
				for (int x = 0; x < nn; x++) {
					if (forward) {
						if (adjacency[u][x]) {
							queue.offer(x);
						}
					} else {
						if (adjacency[x][u]) {
							queue.offer(x);
						}
					}
				}
			}
		}
	}
}
