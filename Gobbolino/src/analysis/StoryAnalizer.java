/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package analysis;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utils.Utility;
import application.InputParameters;

public class StoryAnalizer {
	File storyFile;
	
	HashMap<String, Boolean> blackNodes;		// 
	List<String>  storyNodeNames;	// as there are metanodes (using ";") I have to keep the names of the nodes (that may be composed)
	HashMap<String, String>  arcSource;			// as well as the sources and targets of each arc, instead of just assuming that the first and last
	HashMap<String, String>  arcTarget;			// indexes identify them (they could also be metanodes)
	
	HashMap<String, Integer> edgeFrequency;
	HashMap<String, Double>  storyEdgeFrequencyScore;
	HashMap<String, Double>  storyScore;

	private void computeStoryScores() {
		DecimalFormat df = new DecimalFormat("0.000");
		storyEdgeFrequencyScore = new HashMap<String, Double>();
		storyScore = new HashMap<String, Double>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(storyFile));
			String line = br.readLine();
			while (line != null && line.length() > 0) {
				int storyIndex = Integer.parseInt(line.split(" ")[0]
						.substring(1));
				double score = Double.parseDouble(line.split(" ")[2]);
				storyScore.put(String.valueOf(storyIndex), score);
				
				int edgeSum = 0;
				int ne = 0;
				line = br.readLine();	// consumes the line with the id of the story
				line = br.readLine();	// consumes the line with the order that generated the story 
				String storyString = line;
				String[] x = line.split(" ");
				int nl = Integer.parseInt(x[0]) + Integer.parseInt(x[1]);
				for (int i = 0; i < nl; i++) {
					line = br.readLine();
					storyString = storyString + "\n" + line;
				}
				line = br.readLine();
				storyString = storyString + "\n" + line;
				while (line != null && line.length() > 0
						&& !line.startsWith("#")) {
					storyString = storyString + "\n" + line;
					x = line.split(" ");
					String e = x[2];
					int f = edgeFrequency.get(e);
					edgeSum = edgeSum + f;
					ne = ne + 1;
					line = br.readLine();
				}
				storyString = "#" + storyIndex + " score:"
						+ df.format(edgeSum / (double) ne).replace(",", ".")
						+ "\n" + storyString;
				storyEdgeFrequencyScore.put(storyString, edgeSum / (double) ne);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * The method iterates twice over the stories output file. 
	 * The first is to identify the highest score and the second is to filter out
	 * only these ones and output them into a separate file (outputfile). 
	 * 
	 */
	private void extractTopScoreStories(String outputFile) {
		double highestScore = Double.MIN_VALUE;
		double lowestScore = Double.MAX_VALUE;
		try {
			BufferedReader br = new BufferedReader(new FileReader(storyFile));
			String line = br.readLine();
			while (line != null && line.length() > 0) {
				double score = Double.parseDouble(line.split(" ")[2]);
				
				if( score > highestScore ) { 
					highestScore = score;
				}
				if( score < lowestScore ) {
					lowestScore = score;
				}
				line = br.readLine();
				while (line != null && line.length() > 0 && !line.startsWith("#")) {
					line = br.readLine();
				}
			}
			br.close();
			
			InputParameters.storiesThresholdScore = highestScore - InputParameters.rangeHighestScore;
			DecimalFormat df = new DecimalFormat("##.###");
			System.out.println("Highest score: "+df.format(highestScore));

			// Prepares the histogram of the score distribution
			int scoreCases = (int)Math.floor(highestScore) + 2;
			int[] scores = new int[scoreCases];
			for(int i = 0; i < scoreCases; i++) {
				scores[i] = 0;
			}
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
			br = new BufferedReader(new FileReader(storyFile));
			line = br.readLine();
			int numberTopScoreStories = 0;
			while (line != null && line.length() > 0) {
				double score = Double.parseDouble(line.split(" ")[2]);
				
				if( score < 0 ) {
					scores[0]++;
				} else {
					int intScore = (int)Math.floor(score) + 1;
					scores[intScore]++;
				}
				
				if( score >= InputParameters.storiesThresholdScore ) {
					numberTopScoreStories++;
					bw.write(line+"\n");
					line = br.readLine();
					while (line != null && line.length() > 0 && !line.startsWith("#")) {
						bw.write(line+"\n");
						line = br.readLine();
					}
				} else {
					line = br.readLine();
					while (line != null && line.length() > 0 && !line.startsWith("#")) {
						line = br.readLine();
					}
				}
			}
			br.close();
			bw.close();

			// Prints the histogram
			System.out.println("\n\nHistogram of the Scores\n");
			System.out.println("Negative scores: "+scores[0]);
			int total = scores[0];
			for(int i = 1; i < scoreCases; i++) {
				System.out.println("From "+(i-1)+" to "+i+": "+scores[i]);
				total += scores[i];
			}
			System.out.println("\nTotal: "+total);
			System.out.println("\nNumber of top score stories: "+numberTopScoreStories);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	private void computeAnthology(String fn) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(InputParameters.outputFile+".anthology"));

			computeEdgeFrequencies(fn, false);			
			int nBN = 0, nN = 0;
			for(String nodeId: blackNodes.keySet()) {
				if( blackNodes.get(nodeId) ) {
					nBN++;
				}
				nN++;			
			}
			// first, outputs number of nodes and number of black nodes
			bw.write(nN+" "+nBN+"\n");

			// then output the list of black nodes
			for(String nodeId: blackNodes.keySet()) {
				if( blackNodes.get(nodeId) ) {
					bw.write(nodeId+"\n");
				}
			}
			
			Map<String, Integer> outgoing = new HashMap<String, Integer>();
			Map<String, Integer> incoming = new HashMap<String, Integer>();
			
			for(String edgeId : edgeFrequency.keySet()) {
				// we can infer the source and target of this arc by looking at first and last identifiers
				String source = arcSource.get(edgeId);
				if( outgoing.containsKey(source) ) {
					Integer out = outgoing.get(source);
					outgoing.put(source, out+1);
				} 
				else {
					outgoing.put(source, 1);
				}
				
				String target = arcTarget.get(edgeId);				
				if( incoming.containsKey(target) ) {
					Integer out = incoming.get(target);
					incoming.put(target, out+1);
				} 
				else {
					incoming.put(target, 1);
				}
			}
			
			// now outputs the list of nodes, its outdegree and indegree and its label
			for(String nodeId: blackNodes.keySet()) {
				bw.write(nodeId+" "+(outgoing.get(nodeId) == null ? 0 : outgoing.get(nodeId))+" "+
								    (incoming.get(nodeId) == null ? 0 : incoming.get(nodeId))+" "+nodeId+"\n");
			}
			
			// and finally outputs the arcs, enriched with their frequency among the computed stories
			for(String edgeId : edgeFrequency.keySet()) {
				// we can infer the source and target of this arc by looking at first and last identifiers
				Integer frequency = edgeFrequency.get(edgeId);
				bw.write(arcSource.get(edgeId)+" "+arcTarget.get(edgeId)+" "+edgeId+" "+(frequency == null ? 0 : frequency)+"\n");
			}			
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void computeArcWeightFiles() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(storyFile));
			String line = br.readLine();

			while (line != null && line.length() > 0) {
				int storyIndex = Integer.parseInt(line.split(" ")[0]
						.substring(1));
				double score = Double.parseDouble(line.split(" ")[2]);

				if( score > InputParameters.storiesThresholdScore ) {
					BufferedWriter bw = new BufferedWriter(new FileWriter("story"+storyIndex+".arcWeights"));
					Map<String, String> nodeNames = new HashMap<String, String>();

					line = br.readLine();	// consumes the line with the id of the story
					line = br.readLine();	// consumes the line with the order that generated the story 
					String[] x = line.split(" ");
					int nBN = Integer.parseInt(x[1]);
					// ignore the list of black nodes
					for (int i = 0; i < nBN; i++) {
						line = br.readLine();
					}
					// and for the nodes, save its names
					int nN = Integer.parseInt(x[0]);
					for (int i = 0; i < nN; i++) {
						line = br.readLine();
						x = line.split(" ");
						nodeNames.put(x[0], x[3]);
					}
					line = br.readLine();
					while (line != null && line.length() > 0
							&& !line.startsWith("#")) {
						x = line.split(" ");
						bw.write(nodeNames.get(x[0]) + "\t" + nodeNames.get(x[1]) + "\t" + x[2].split(";").length+"\n");
						line = br.readLine();
					}
					bw.close();
				}
				else { 
					// consumes the id of the story
					line = br.readLine();
					// goes until the next story
					while (line != null && line.length() > 0
							&& !line.startsWith("#")) {
						line = br.readLine();						
					}
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	private void computeEdgeFrequencies(String fn, boolean filterStories) {
		edgeFrequency = new HashMap<String, Integer>();
		blackNodes = new HashMap<String, Boolean>();
		storyNodeNames = new ArrayList<String>();
		arcSource = new HashMap<String, String>();
		arcTarget = new HashMap<String, String>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(fn)));
			int lineNumber = 1;
			String line = br.readLine();
			lineNumber++;

			while (line != null && line.length() > 0) {
				boolean ignoreStory = false;

				if( filterStories ) {
					double score = Double.parseDouble(line.split(" ")[2]);
					if( score < InputParameters.storiesThresholdScore ) {
						ignoreStory = true;
						// consumes the id of the story
						line = br.readLine();
						// goes until the next story
						while (line != null && line.length() > 0
								&& !line.startsWith("#")) {
							line = br.readLine();						
						}
					}
				}
				
				if( !ignoreStory ) {
					line = br.readLine();	// consumes the line with the id of the story
					line = br.readLine();	// consumes the line with the order that generated the story 
					lineNumber += 2;
					String[] x = line.split(" ");
					int nN = Integer.parseInt(x[0]);
					int nBN =  Integer.parseInt(x[1]);
					List<String> bnIds = new ArrayList<String>();
					HashMap<String, String> storyNodes = new HashMap<String,String>();
					for (int i = 0; i < nBN; i++) {
						line = br.readLine();
						lineNumber++;
						bnIds.add(line.split(" ")[0]);
					}
					for (int i = 0; i < nN; i++) {
						line = br.readLine();
						lineNumber++;
						String nodeId = line.split(" ")[0];
						String nodeName = line.split(" ")[3];
						nodeName = nodeName.substring(0, nodeName.length()-1);
						if( !blackNodes.containsKey(nodeName)) {
							if( bnIds.contains(nodeId) ) {
								blackNodes.put(nodeName, true);
							}
							else {
								blackNodes.put(nodeName, false);
							}
						}
						if( !storyNodeNames.contains(nodeName) ) {
							storyNodeNames.add(nodeName);
						}
						storyNodes.put(nodeId, nodeName);
					}
					line = br.readLine();
					lineNumber++;
					while (line != null && line.length() > 0
							&& !line.startsWith("#")) {
						x = line.split(" ");
						if( x.length < 3 ) {
							System.out.println("there is no name for this arc... #line: "+lineNumber);
						}
						String source = x[0];
						String target = x[1];
						String edgeId = x[2];
						if (edgeFrequency.containsKey(edgeId)) {
							int f = edgeFrequency.get(edgeId);
							edgeFrequency.put(edgeId, f + 1);
						} else {
							edgeFrequency.put(edgeId, 1);
						}
						arcSource.put(edgeId, storyNodes.get(source));
						arcTarget.put(edgeId, storyNodes.get(target));
						
						line = br.readLine();
						lineNumber++;
					}
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void run(String fn) {
		storyFile = new File(fn);

		if( InputParameters.analyseEdgeFrequencies ) {
			computeEdgeFrequencies(fn, false);
			computeStoryScores();
	
			// outputs the edge frequency ordered file 
			String[] si = new String[storyEdgeFrequencyScore.size()];
			double[] ss = new double[storyEdgeFrequencyScore.size()];
			int i = 0;
			for (String s : storyEdgeFrequencyScore.keySet()) {
				si[i] = s;
				ss[i++] = storyEdgeFrequencyScore.get(s);
			}
			Utility.sort(ss, si);
			File sortedStory = new File(fn + "-OrderedByEdgeFrequency.sto");
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(sortedStory));
				for (i = ss.length - 1; i >= 0; i--) {
					bw.write(si[i]+"\n");
				}
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			// outputs the score analytical file
			String[] si2 = new String[storyScore.size()];
			double[] ss2 = new double[storyScore.size()];
			i = 0;
			for (String s : storyScore.keySet()) {
				si2[i] = s;
				ss2[i++] = storyScore.get(s);
			}
			Utility.sort(ss2, si2);
			sortedStory = new File(fn + ".csv");
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(sortedStory));
				for (i = ss2.length - 1; i >= 0; i--) {
					bw.write(si2[i]+";"+ss2[i]+"\n");
				}
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
			
		if( InputParameters.generateArcWeightsFile ) {
			computeArcWeightFiles();			
		}
		
		if( InputParameters.generateAnthology ) {
			//extractTopScoreStories(fn+"_topScore");
			computeAnthology(fn);
		}
	}
}
