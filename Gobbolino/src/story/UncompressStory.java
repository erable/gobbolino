package story;

import java.util.HashSet;
import java.util.Set;

import network.NELNetwork;
import utils.Utility;

public class UncompressStory {

	protected static int[][] compare;
	
	public static NELNetwork uncompress(NELNetwork reference, NELNetwork compressed) {
		NELNetwork result = new NELNetwork(reference);
		
		Set<String> nodes = new HashSet<String>();
		// nodes from the compressed network will be nodes from the resulting network
		// they may also be meta nodes containing hidden nodes.
		for(int i = 0; i < compressed.getNumNodes(); i++) {
			String[] nodeNames = compressed.getNodeName(i).split(";");
			for(String node: nodeNames) {
				if( !nodes.contains(node) ) {
					nodes.add(node);
				}				
			}
			
			// check also the edges and its compressed paths (whose participants are stored in the label)
			for(int j = 0; j < compressed.getNumNodes(); j++) {
				if( compressed.areAdjacent(i, j) ) {
					nodeNames = compressed.getLabel(i, j).split(";");
					for(String node: nodeNames) {
						if( !nodes.contains(node) ) {
							nodes.add(node);
						}				
					}
				}
			}
		}
		
		// pass through the copy of the reference network deleting nodes that are not in the compressed network
		for(int i = 0; i < result.getNumNodes(); i++) {
			String name = result.getNodeName(i).split(";")[0];
			if( !nodes.contains(name) ) {
				result.delete(i);
			} 
			else {
				result.setNodeName(i, name);
			}
		}
		
		// pass through the subgraph obtained until now checking if edges respect the partial order given by the
		// original story. If not, then erase them because they do not correspond correctly to the paths encoded
		// in the arcs.

		// Initializes a partial order infered by the story
		compare = Utility.partialOrder(compressed.getAdjacencyMatrix(), compressed.getNumNodes());

		for(int i = 0; i < result.getNumNodes(); i++) {
			if( result.isAlive(i) )
			{
				for(int j = 0; j < result.getNumNodes(); j++) {
					if( result.isAlive(j) && result.areAdjacent(i, j) 
						&& !storyArcIsOK(compressed, result.getNodeName(i), result.getNodeName(j))) {
						Utility.printMsg("The uncompressed arc between "+result.getNodeName(i)+" and "+result.getNodeName(j)+" is not OK.");
						result.deleteArc(i, j);
					}
				}
			}
		}
		result.computeAliveNodes();
		result.computeDegrees();
		//if( InputParameters.verbose ) {
			result.writeFile("uncompressedStory.nel", result.reMap());
		//}
		
		return result;
	}
	
	/*
	 * An arc A -> B is not OK only if both nodes are explicitly present in the story (i.e, not hidden in metanodes or arc labels)
	 * and their relative order differs from A -> B
	 */
	protected static boolean storyArcIsOK(NELNetwork story, String nodeA, String nodeB) {
		int u = story.getIdFromNodeName(nodeA);
		int v = story.getIdFromNodeName(nodeB);
		
		if( u == -1 || v == -1 ) {
			return true;
		}
		
		if( compare[u][v] <= 0 ) {
			return true;
		}
		
		// the arc A -> B does not respect the partial order defined by the story.
		return false;
	}
	
}
