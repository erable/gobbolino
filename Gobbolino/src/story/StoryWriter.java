/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package story;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

import utils.Utility;
import application.InputParameters;

public class StoryWriter {

	File storiesFile;
	BufferedWriter bw;
	HashMap<String, Integer> groups = new HashMap<String, Integer>();
	Double[] maxScore;
	Double[] currentScore;
	DecimalFormat df = new DecimalFormat("0.000");
	int nMax;

	public StoryWriter() {
		storiesFile = new File(InputParameters.outputFile);
	}
	
	public void closeFile() {
		if (storiesFile != null && bw != null ) {
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
			bw = null;
			storiesFile = null;
			maxScore = null;
		}
	}

	public void groupSASBySourceTargetSets(Story story, int nsas) {
		LinkedList<Integer> targets = new LinkedList<Integer>();
		for (int i = 0; i < story.getNumNodes(); i++) {
			if (story.getOutDegree(i) == 0 && story.isBlack(i)) {
				targets.add(i);
			}
		}

		LinkedList<Integer> sources = new LinkedList<Integer>();
		for (int i = 0; i < story.getNumNodes(); i++) {
			if (story.getInDegree(i) == 0 && story.isBlack(i)) {
				sources.add(i);
			}
		}
		Collections.sort(sources);
		Collections.sort(targets);

		String id = "[";
		for (int i = 0; i < sources.size(); i++) {
			id = id + sources.get(i) + ";";
		}
		id = id.substring(0, id.length() - 1) + "][";
		for (int i = 0; i < targets.size(); i++) {
			id = id + targets.get(i) + ";";
		}
		id = id.substring(0, id.length() - 1) + "]";
		if (!groups.containsKey(id)) {
			groups.put(id, 1);
		} else {
			groups.put(id, groups.get(id) + 1);
		}
	}
	
	/*
	 * It writes the graph on a NEL file: to this aim nodes are first mapped to
	 * the interval of numbers between 0 and the number of nodes which have not
	 * been logically deleted.
	 */
	 public void writeFile(int n, Story story, Double[] score, int[] rank) {
		// converts the current score to Double[]
		currentScore = score;
		 
		try {
			// first, considers the first story being written
			if (maxScore == null) {
				maxScore = currentScore;
				nMax = 1;
				bw = new BufferedWriter(new FileWriter(storiesFile));
				writeStory(n, story, currentScore, rank);
			} 
			else {
				// if it is not the first, let us check wether ithas a bigger score the the maximum seen until now.
				boolean isGreater = false;
				boolean isEqual = false;
				int i = 0;
				while (i < currentScore.length && maxScore[i] == currentScore[i]) {
					i = i + 1;
				}
				if (i == currentScore.length) {
					isEqual = true;
				} else if (maxScore[i] < currentScore[i]) {
					isGreater = true;
				}
				if (isGreater || isEqual) {
					maxScore = currentScore;
					if (isGreater) {
						nMax = 1;
						if(InputParameters.outputOnlyMaxScore) {
							bw.close();
							bw = new BufferedWriter(new FileWriter(storiesFile));
						}
					} else {
						nMax = nMax + 1;
					}
					Utility.printMsg("\nMaximum current score (obtained in story " + n + "): ");
					for (i = 0; i < currentScore.length; i++) {
						Utility.printMsg(df.format(currentScore[i]).replace(",", ".")
								+ " ");
					}
					writeStory(n, story, currentScore, rank);
				}
				else if( !InputParameters.outputOnlyMaxScore ) {
					writeStory(n, story, currentScore, rank);					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	 }
	
	 /*
	  * writeStory writes down the story number, its score and the story itself
	  */
	 private void writeStory(int n, Story story, Double[] score, int[] rank) throws IOException {
		// write the score of the story
		bw.write("#" + n + " score: ");
		for (int i = 0; i < score.length; i++) {
			bw.write(df.format(score[i]).replace(",", ".") + " ");
		}
		bw.write("\n");

		// write the rank that originated the story
		bw.write("PI={");
		for(int i = 0; i < rank.length; i++) {
			bw.write(new Integer(rank[i]).toString());
			if( i < rank.length-1 ) { // if there is some next element
				bw.write(",");
			}
		}
		bw.write("}\n");
			
		story.writeStory(bw);
	 }
	 
}
