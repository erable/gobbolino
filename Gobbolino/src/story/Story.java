/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package story;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;

import preprocessing.GraphSimplifier;

public class Story extends GraphSimplifier {

	protected String[] role;
	protected int[] b;
	
	/*
	 * Constructor: only copies information to build the network without reading it from a file 
	 */
	public Story(boolean[][] adj, boolean[] isBlack, String[] nodeName,
			LinkedList<Integer>[] listAdj, LinkedList<String>[] outLabels) {
		adjacency = adj;
		this.isBlack = isBlack;
		nn = isBlack.length;
		importLabels(nodeName, listAdj, outLabels);
	}

	/*
	 * Reading the labels on the nodes and arcs
	 */
	public void importLabels(String[] nodeName,
			LinkedList<Integer>[] adjacency, LinkedList<String>[] outLabels) {
		this.nodeName = nodeName;
		labels = new String[outLabels.length][outLabels.length];
		for (int i = 0; i < labels.length; i++) {
			for (int j = 0; j < labels.length; j++) {
				labels[i][j] = "";
			}
		}

		for (int i = 0; i < outLabels.length; i++) {
			for (int j = 0; j < outLabels[i].size(); j++)
				labels[i][adjacency[i].get(j)] = outLabels[i].get(j);
		}
	}

	/*
	 * simplify consists on removing bottlenecks and self-loops
	 */
	public void simplify() {
		computeAliveNodes();
		computeDegrees();

		boolean changed = true;
		while (changed) {
			changed = false;
			if (fb()) {
				changed = true;
			}
			if (deleteSelfLoops()) {
				changed = true;
			}
			if (bb()) {
				changed = true;
			}
			if (deleteSelfLoops()) {
				changed = true;
			}
		}
		computeAliveNodes();
		computeDegrees();
		simplifyLabels();
		reMap();
		
		// computing the black nodes and its roles
		b = new int[numberOfBlackNodes()];
		role = new String[b.length];
		int i = 0;
		for (int j = 0; j < getNumNodes(); j++) {
			if (nodeAlive[j] && isBlack[j]) {
				b[i] = oldNew[j];
				if (inDegree[j] == 0 && outDegree[j] > 0) {
					role[i] = "S";
				} else if (inDegree[j] > 0 && outDegree[j] == 0) {
					role[i] = "T";
				} else {
					role[i] = "I";
				}
				i = i + 1;
			}
		}
		//Utility.sort(b);
	}

	public int[] getBlackNodes() {
		return b;
	}
	
	public String[] getRoles() {
		return role;
	}

	public void writeStory(BufferedWriter bw) throws IOException {
		int nb = numberOfBlackNodes();
		bw.write(numberOfAlives() + " " + nb + "\n");
		
		for (int i = 0; i < b.length; i++) {
			bw.write(b[i] + " " + role[i] + "\n");
		}
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i]) {
				bw.write(oldNew[i] + " " + outDegree[i] + " " + inDegree[i]
						+ " " + nodeName[i] + "\n");
			}
		}
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (adjacency[i][j]) {
					bw.write(oldNew[i] + " " + oldNew[j] + " " + labels[i][j]
							+ "\n");
				}
			}
		}
	}	
}
