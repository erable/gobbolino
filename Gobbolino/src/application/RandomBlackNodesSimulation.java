/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolc networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;

import java.io.File;
import java.io.FileFilter;

import experiment.statistics.Simulation;
import experiment.statistics.Simulation.InputFormat;

public class RandomBlackNodesSimulation {

	public void run() {
		
		File folder = new File(InputParameters.inputFile);
		if( folder.isDirectory() ) {
			File[] pathwayFiles = folder.listFiles(new FileFilter() {
				public boolean accept(File pathname) {
					return pathname.isFile() && pathname.getName().endsWith(".xml"); 
				}
			});
			
			System.out.println("Computing BNS+stories for "+pathwayFiles.length+" files in the folder "+folder.getName());
			for(File network: pathwayFiles) {
				(new Simulation()).run(InputParameters.inputFile+"/"+network.getName(), InputFormat.SBML); 				
			}
			
			File[] analysisFiles = folder.listFiles(new FileFilter() {
				public boolean accept(File pathname) {
					return pathname.isFile() && pathname.getName().endsWith(".cn"); 
				}
			});
			System.out.println("Analysing the results for "+analysisFiles.length+" files in the folder "+folder.getName());
			new Simulation().analyseResultFiles(analysisFiles);
			return;
		}
		else {
			File fileCheck = new File(InputParameters.inputFile + ".xml");
			if (fileCheck.exists()) {
				(new Simulation()).run(InputParameters.inputFile+".xml", InputFormat.SBML);
				return;
			}
			
			fileCheck = new File(InputParameters.inputFile + ".el");
			if( fileCheck.exists()) {
				(new Simulation()).run(InputParameters.inputFile+".xml", InputFormat.EL);
				return;
			}
		}
		System.out.println(InputParameters.inputFile + " does not exist in the recognized input formats (SBML/XML file or EL-edge list)");
		System.exit(0);
	}
}
