/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;

import java.io.File;

import network.BlackSCCComputer;
import network.NELNetwork;
import analysis.GraphAnalyzer;
import analysis.StoryAnalizer;

public class ComputeStatistics {

	public void run() {

		// if the NEL file is present, analyze it
		File fileCheck = new File(InputParameters.inputFile+ ".nel");
		if (!fileCheck.exists() ) {
			System.out.println(InputParameters.inputFile + ".nel not found.\n");
		}
		else {
			(new GraphAnalyzer()).run(InputParameters.inputFile, true, false);
			
			if( InputParameters.stronglyConnectedComponents ) {
				NELNetwork network = new NELNetwork();
				network.readFile(InputParameters.inputFile + ".nel");
				(new BlackSCCComputer(network)).run(InputParameters.component, true);
			}		
		}

		// if the STO file is present, analyze it
		fileCheck = new File(InputParameters.outputFile);
		if (!fileCheck.exists() ) {
			System.out.println(InputParameters.outputFile + " not found.\n");
		}
		else {
			(new StoryAnalizer()).run(InputParameters.outputFile);
		}
		
	}
	
}
