/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;

import java.io.File;

import network.XML2NEL;
import preprocessing.AtomExchangeWeight;
import preprocessing.GraphSimplifier;
import preprocessing.LightestPathCreator;
import application.InputParameters.EnumOperationMode;
import cytoscape.CytoscapeFilesCreator;

public class ComputeBlackNodeScenario {

	public String run() {

		File XMLFileCheck = new File(InputParameters.inputFile+".xml");
		
		// first check if the XML file exists
		if( XMLFileCheck.exists() ) {
			// converts it into a NEL file
			File BNFileCheck = new File(InputParameters.inputFile+".bn");
			if (BNFileCheck.exists()) {
				InputParameters.inputFile = (new XML2NEL()).run(InputParameters.inputFile);
			}
		}

		File NELFileCheck = new File(InputParameters.inputFile+".nel");
		if (!NELFileCheck.exists() ){
			System.out.println(InputParameters.inputFile + ".nel not found.\n");
			System.exit(0);
		}

		// with the NEL file defined, creates the path-graph
		String pathGraph = "";
		switch(InputParameters.paths) {
			case LIGHTEST_PATH: {
				pathGraph = (new LightestPathCreator()).run(InputParameters.inputFile, true, null);
				break;
			}
			case SHORTEST_PATH: {
				pathGraph = (new LightestPathCreator()).run(InputParameters.inputFile, false, null);
				break;				
			}
			case ALL_PATH: {
				break;
			}
			case ATOM_MAPPING_PATH: {
				pathGraph = (new LightestPathCreator()).run(InputParameters.inputFile, true, new AtomExchangeWeight());
				break;
			}
		}

		// now with the path graph, apply the simplification rules to build the black node scenario
		GraphSimplifier g = new GraphSimplifier();
		pathGraph = g.run(pathGraph, InputParameters.createMetaNode);
		
		if( InputParameters.verbose && InputParameters.mode == EnumOperationMode.COMPUTE_BNS ) {
			g.printStatistics("after simplification");
		}
		
		if (InputParameters.cytoscape) {
			CytoscapeFilesCreator.createFiles(pathGraph, ".nel");
		}
		
		return pathGraph;
	}
	
}
