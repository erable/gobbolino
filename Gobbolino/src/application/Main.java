/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;
import java.util.Date;

import utils.ArgumentParser;
import application.InputParameters.EnumBlackNodes;
import application.InputParameters.EnumOperationMode;
import application.InputParameters.EnumPathsMode;
import application.InputParameters.EnumPathwayComputationMode;
import application.InputParameters.EnumRandomBlackNodesMode;
import application.InputParameters.EnumScoreMode;
import application.InputParameters.EnumStopCondition;
import application.InputParameters.EnumTimeCondition;

public class Main {
	
	/*
	 * The first parameter is always the name of the file (in XML or NEL format)
	 * without the extension. According to the options specified by the user, it
	 * then performs one of the following operations: convert XML to NEL; create
	 * the lightest path graph; create the shortest path graph; simplify the
	 * graph by deleting forward and backward bottlenecks; convert from NEL to
	 * Cytoscape; compute a specific number of stories; compute the black
	 * strongly connected components.
	 */
	public static void main(String[] args) throws InterruptedException {
				
		ArgumentParser p = new ArgumentParser(args);
        if( args.length == 0 || p.hasOption("h") || p.hasOption("help") ) { 
        	printUsage();
        	System.exit(0);
        }
        processOptions(p);

        Date start = new Date();
        System.out.println("Started at "+start);

        switch(InputParameters.mode) {
	        case COMPUTE_BNS: {
	        	new ComputeBlackNodeScenario().run();
	        	break;
	        }
	        case COMPUTE_STORIES: {
	        	new ComputeStories().run(null, null);
	        	break;
	        }
	        case STATISTICS: {
	        	new ComputeStatistics().run();
	        	break;
	        }
	        case PATHWAY_SIMULATION: {
	        	new PathwaySimulation().run();
	        	break;
	        }
	        case RANDOM_BN_SIMULATION: {
	        	new RandomBlackNodesSimulation().run();
	        	break;
	        }
        }

        Date finish = new Date();
        System.out.println("Finished at "+finish);
        System.out.println("Elapsed time (ms): "+(finish.getTime()-start.getTime()));
	}

	/*
	 * It prints a short description of how the program has to be used.
	 */
	private static void printUsage() {
		System.out.println("GOBBOLINO has different modes of operation: \n");
		System.out.println("1) Generating a black node scenario. Consists on (optionally) converting a SBML (XML) metabolic network into a NEL graph format file, selecting (all/shortest/lightest/atom) paths between black nodes and, finally, compacting the network appyling compression rules (bottleneck removals/dead-end removals/self-loop removals).\n");
		
		System.out.println("2) Computing stories. Computes (all/a defined quantity of) stories and gives them a score according to a selected score function.\n");	
		
		System.out.println("3) Statistics. Compute statistics on the input network or the stories generated\n");
		
		System.out.println("4) Black nodes experiment. Computes stories by choosing black nodes randomly\n");

		System.out.println("5) Pathway covering experiment. Given a set of metabolic pathways and one metabolic pathways from which these pathways were extracted, it computes stories for each pathway (black nodes chosen specifically for the pathway) and checks whether one/some stories fit the pathway.\n");

		System.out.println("\nThe first step is to choose in which mode you want to operate and then set the other parameters." );
		System.out.println("Default options are shown in capital letters. For numeric parameters, the default value is shown, but it can be replaced by any other non-negative number.");
		System.out.println("\nThe options are: ");
		
		System.out.println("-mode=bns|STORIES|stat|random|pathway");
		
		System.out.println("\n\nBNS mode:");
 		 
		System.out.println("-n=<filename>. Input file name with no extension. If there is a XML file it will be first converted. Then the path-file will be created and simplified.");
		System.out.println("-paths=all|shortest|LIGHTEST|atom. Select the paths between black nodes to be preserved (all/shortest paths only/lightest paths only/atom mapping paths only).");
		System.out.println("-reversibility=ON|off. The user may choose to ignore the reversibility of the reactions.");
		System.out.println("-cyto: Computes also 3 text files (nodes, edges and attributes) that may be open in Cytoscape.");

		System.out.println("\n\nSTORIES mode:");
		
		System.out.println("-n=<filename>. Input file name with no extension (NEL format is assumed).");
		System.out.println("-o=<filename>. Output file containing the stories generated (./output/stories/stories.sto is the default output)");
		System.out.println("-q=10|all. Maximum number of stories to be generated. 10 is the default. The value <all> indicates that there is no limit on the number of stories to be generated.");
		System.out.println("-order=RANDOM|all. Chooses between randomly inspecting the space of orders or to generate all orders in a deterministic way.");
		System.out.println("-score=distance|EDGESUM|isolation|mst|partialOrder|metabolomics. Chooses the score function to be used: average distance between black nodes, sum up of edge weights on the story, level of isolation of black nodes, partial order accordance of stories (needs an additional file with the prefered partial orders) and metabolomics intensities (needs an additional information on the black nodes file with the concentrations of black nodes)");
		System.out.println("OBS: Scores may be combined using semicolon (;) as a separator. For instance, -score=METABOLOMICS;EDGESUM will use two score functions to classify the stories.");
		System.out.println("-scoreFile=<filename>. Input filename for the score functions that need additional data.");
		System.out.println("-maxScore: Outputs only the maximum scored stories. Default is to output all of them.");
		System.out.println("-orderFile=<filename>. Input filename containing total order of the nodes (1 per line, node ids separated with 1 space) to be used before passing to the random enumeration.");
		System.out.println("-stop=MAXSTORIES|bernouilli|time. By default the system stops only when all stories are computed or the defined maximum number of stories is reached. Two other options are to stop based on a time basis (specified in minutes) or on statistical measurements on the number of new stories generated trying to estimate the number of new stories still to be found.");
		System.out.println("-stopTime=10. By default, the time stop condition is set to 10 minutes.");
		System.out.println("-stopSampleSize=10000. By default, the sample size for the Bernouilli stop condition is set to 10000.");
		System.out.println("-stopTimeCondition=NO_NEW|elapsed. By default, gobbolino computes stories until now new stories are found after [stopTime] minutes. Elpased makes it stop after [stopTime] min with no additional condition.");
		System.out.println("OBS: The input files are searched on the <input> folder and the generated stories are saved on <output/stories>");		
		
		System.out.println("\n\nSTAT mode:");
		
		System.out.println("-o=<filename>. Output file containing the stories generated (Stories.sto is the default). If the file is present, several statistical analysis will be generated.");
		System.out.println("-n=<filename>. Input file name with no extension (NEL format is assumed).");
		System.out.println("-scc=ALL|1. Decomposes the input network into strongly connected components showing the contents of each scc (ALL, which is the default) or a specific component.");
		
		System.out.println("\n\nRANDOM mode:");
		System.out.println("For the computation of stories, the same parameters defined for the STORIES mode.");
		System.out.println("-rbnMin=5. Number of random black nodes to be chosen (min).");
		System.out.println("-rbnMax=15. Number of random black nodes to be chosen (max).");
		System.out.println("-rbnIncrement=5. Increment at each step from min to max.");
		System.out.println("-rbnMode=PERCENT|absolut. Consider the values as absolute numbers or as a percentage of the number of nodes on the network.");
		
		System.out.println("-runs=3. Number of simulation runs.");
		
		System.out.println("\n\nPATHWAY mode:");
		System.out.println("For the computation of stories, the same parameters defined for the STORIES mode. But the input file must be the original one (XML format) and without any preprocessing/transformation.");
		System.out.println("The system looks for the list of metabolic pathway files (XML format) in the folder <input/pathway/pathways>.");
		System.out.println("The system looks for the black nodes files in the folder <input/pathway/BNs>.");
		System.out.println("The system looks for the score parameters files in the folder <input/pathway/score>.");
		System.out.println("By default, the system will use the black nodes defined in the predefined folders. The user may want also to automatically choose black nodes using:");
		System.out.println("-autoBN=sourceTarget|borensteinAll|borensteinOne|borensteinRG. The first option corresponds to topological source/targets, the other 3 applies the Borenstein et. al method identifying source/target strongly connected componentes. The first one takes all compounds inside a scc as source/target, the second one chooses one as representant while the third applies the method on a reaction graph representation of the network");
		System.out.println("-tas=10. Defines a threshold on the number of black nodes. For pathways with less than <tas> black nodes, all stories are computed. For the others, the maximum number of stories defined (with the selected stop condition) is searched.");
		System.out.println("-computationMode=ALL|stories|accuracy. Chooses between computing the stories for the pathways or computing the accuracy of the method based on the results or both (FULL), which is the default option.");
		System.out.println("OBS: The automatic generated information is persisted in the following output folders: <output/pathways/automaticBNs> and <output/pathways/automaticScore>.");
		
		System.out.println("\n\nAll modes:");
		System.out.println("-v or -verbose: verbose mode on. Default is off.");
		
		System.exit(0);
	}

	/*
	 * It looks for the used options and sets the corresponding variables.
	 */
	private static void processOptions(ArgumentParser p) {
		
		// defines the operation mode: computing stories, pathway covering experiment or computing statistics
		if( p.hasOption("mode") ) {
			if( p.getOption("mode").equalsIgnoreCase("pathway") )
				InputParameters.mode = EnumOperationMode.PATHWAY_SIMULATION;
			else if (p.getOption("mode").equalsIgnoreCase("stat") )
				InputParameters.mode = EnumOperationMode.STATISTICS;
			else if (p.getOption("mode").equalsIgnoreCase("stories") )
				InputParameters.mode = EnumOperationMode.COMPUTE_STORIES;
			else if (p.getOption("mode").equalsIgnoreCase("bns") )
				InputParameters.mode = EnumOperationMode.COMPUTE_BNS;
			else if (p.getOption("mode").equalsIgnoreCase("random") )
				InputParameters.mode = EnumOperationMode.RANDOM_BN_SIMULATION;
			else {
				System.out.println("The specified mode <"+p.getOption("mode")+"> is not correct and will be ignored.");
			}
		}

		// general options
		InputParameters.verbose = p.hasOption("v") || p.hasOption("verbose");
			
		// now reads the input parameters specific for each operation mode
		if( InputParameters.mode == EnumOperationMode.COMPUTE_STORIES || 
			InputParameters.mode == EnumOperationMode.RANDOM_BN_SIMULATION 	|| 
			InputParameters.mode == EnumOperationMode.PATHWAY_SIMULATION ) {
			// the goal is to compute stories
			// gets the name of the input file
			if( !p.hasOption("n") ) {
				printUsage();
			}
			// reads the input file name
			InputParameters.inputFile = "./input/"+p.getOption("n");
			// reads the output file name (if defined)
			if( p.hasOption("o") ) {
				InputParameters.outputFile = "./output/stories/" + p.getOption("o");
			}
			// reads the order option
			if( p.hasOption("order") ) {
				if( p.getOption("order").equalsIgnoreCase("all")) {
					InputParameters.generateAllOrders = true;
				}
			}
			
			// reads the amount of stories to be generated
			if( p.hasOption("q") ) {
				
				if( p.getOption("q").equalsIgnoreCase("all") ) {
					InputParameters.generateAllOrders = true;
					InputParameters.stories = -1;
				}
				else {
					InputParameters.generateAllOrders = false;
					try 
					{
						InputParameters.stories = new Integer(p.getOption("q"));
					}
					catch(Exception e) {
						System.out.println("Error: option <q> should have an integer value for the quantity of stories to be generated.");
						System.exit(0);
					}
				}
			}
			
			// reads the score function to be used	
			if( p.hasOption("score") ) {
				String[] scores = p.getOption("score").split(";");
				InputParameters.scoreMode = new EnumScoreMode[scores.length];
				for(int i = 0; i < scores.length; i++) {
					if( scores[i].equalsIgnoreCase("distance") )
						InputParameters.scoreMode[i] = EnumScoreMode.BN_DISTANCE;
					else if (scores[i].equalsIgnoreCase("edgeSum") )
						InputParameters.scoreMode[i] = EnumScoreMode.EDGE_SUM;
					else if (scores[i].equalsIgnoreCase("isolation") )
						InputParameters.scoreMode[i] = EnumScoreMode.ISOLATION_LEVEL;
					else if (scores[i].equalsIgnoreCase("partialOrder") )
						InputParameters.scoreMode[i] = EnumScoreMode.PARTIAL_ORDER;
					else if (scores[i].equalsIgnoreCase("metabolomics") )
						InputParameters.scoreMode[i] = EnumScoreMode.METABOLOMICS_INTENSITY;
				}
			}			
			// checks if the user wants to output only maximum score stories
			if( p.hasOption("maxScore")) {
				InputParameters.outputOnlyMaxScore = true;
			}
			if( p.hasOption("scoreFile") ) {
				InputParameters.scoreFile = "./input/"+p.getOption("scoreFile");
			}

			if( p.hasOption("orderFile") ) {
				InputParameters.orderFile = "./input/"+p.getOption("orderFile");
			}
			
			// reads the stop conditions
			if( p.hasOption("stop") ) {
				if( p.getOption("stop").equalsIgnoreCase("maxStories") ) {
					InputParameters.stop = EnumStopCondition.MAX_STORIES;
				}
				else if (p.getOption("stop").equalsIgnoreCase("time") ) {
					InputParameters.stop = EnumStopCondition.TIME;
					if( p.hasOption("stopTimeCondition") ) {
						if( p.getOption("stopTimeCondition").equalsIgnoreCase("elapsed") ) {
							InputParameters.timeCondition = EnumTimeCondition.ELAPSED;							
						} else if ( p.getOption("stopTimeCondition").equalsIgnoreCase("no_new") ) {							
							InputParameters.timeCondition = EnumTimeCondition.NO_NEW;							
						}
					}
				}
				else if (p.getOption("stop").equalsIgnoreCase("bernouili") ) {
					InputParameters.stop = EnumStopCondition.BERNOUILLI;
				}
			}			
			// reads the amount of time to spend while searching for new stories
			if( p.hasOption("stopTime") ) {
				try 
				{
					InputParameters.stopTime = new Integer(p.getOption("stopTime")) * 60 * 1000;
				}
				catch(Exception e) {
					System.out.println("Error: option <stopTime> should have an integer value for the number of minutes to spend computing stories.");
					System.exit(0);
				}
			} 				
			// reads the size of the sample for using on the Bernouilli race control
			if( p.hasOption("stopSampleSize") ) {
				try 
				{
					InputParameters.stopSampleSize = new Integer(p.getOption("stopSampleSize"));
				}
				catch(Exception e) {
					System.out.println("Error: option <stopSampleSize> should have an integer value for the size of the sample size for the Bernouilli race control.");
					System.exit(0);
				}
			} 				
			
			// options specific for random black nodes simulations
			if( InputParameters.mode == EnumOperationMode.RANDOM_BN_SIMULATION ) {
				// reads the amount of random black nodes to be chosen (min)
				if( p.hasOption("rbnMin") ) {
					try 
					{
						InputParameters.randomBlackNodesMin = new Integer(p.getOption("rbnMin"));
					}
					catch(Exception e) {
						System.out.println("Error: option <rbnMin> should have an integer value for the quantity of random black nodes to be chosen.");
						System.exit(0);
					}
				} 

				// reads the amount of random black nodes to be chosen (max)
				if( p.hasOption("rbnMax") ) {
					try 
					{
						InputParameters.randomBlackNodesMax = new Integer(p.getOption("rbnMax"));
					}
					catch(Exception e) {
						System.out.println("Error: option <rbnMax> should have an integer value for the quantity of random black nodes to be chosen.");
						System.exit(0);
					}
				} 
				
				// reads the amount of random black nodes to be chosen (max)
				if( p.hasOption("rbnIncrement") ) {
					try 
					{
						InputParameters.randomBlackNodesIncrement = new Integer(p.getOption("rbnIncrement"));
					}
					catch(Exception e) {
						System.out.println("Error: option <rbnIncrement> should have an integer value for the quantity of random black nodes to be chosen.");
						System.exit(0);
					}
				} 
				
				// reads the mode option
				if( p.hasOption("rbnMode") ) {
					if( p.getOption("rbnMode").equalsIgnoreCase("percent") ) {
						InputParameters.randomblackNodesMode = EnumRandomBlackNodesMode.PERCENTAGE;
					}
					else if( p.getOption("rbnMode").equalsIgnoreCase("absolut") ) {
						InputParameters.randomblackNodesMode = EnumRandomBlackNodesMode.ABSOLUT;
					}
				}

				// reads the number of runs
				if( p.hasOption("runs") ) {
					try 
					{
						InputParameters.simulationRuns = new Integer(p.getOption("runs"));
					}
					catch(Exception e) {
						System.out.println("Error: option <runs> should have an integer value for the number of runs of the simulation.");
						System.exit(0);
					}
				} 				
			}
			
			// options specific for pathway covering simulations
			if( InputParameters.mode == EnumOperationMode.PATHWAY_SIMULATION ) {
				if( p.hasOption("autoBN") ) {
					if( p.getOption("autoBN").equalsIgnoreCase("sourceTarget") ) {
						InputParameters.blackNodesMode = EnumBlackNodes.AUTO_SOURCE_TARGET;
					}
					else if( p.getOption("autoBN").equalsIgnoreCase("borensteinAll") ) {
						InputParameters.blackNodesMode = EnumBlackNodes.AUTO_BORENSTEIN_ALL;
					}
					else if( p.getOption("autoBN").equalsIgnoreCase("borensteinOne") ) {
						InputParameters.blackNodesMode = EnumBlackNodes.AUTO_BORENSTEIN_ONE;
					}
					else if( p.getOption("autoBN").equalsIgnoreCase("borensteinRG") ) {
						InputParameters.blackNodesMode = EnumBlackNodes.AUTO_REACTION_GRAPH_BORENSTEIN;
					}
				}
				
				// reads the threshold on the number of black nodes for turning on the computation of all stories
				if( p.hasOption("tas") ) {
					try 
					{
						InputParameters.thresholdForComputingAllStories = new Integer(p.getOption("tas"));
					}
					catch(Exception e) {
						System.out.println("Error: option <tas> should have an integer value for the threshold on the number of black nodes for computing all stories.");
						System.exit(0);
					}
				}
				
				// reads the option on what is going to be computed: stories, accuracy or both
				if( p.hasOption("computationMode") ) {
					try {
						if( "ALL".equalsIgnoreCase(p.getOption("computationMode")) ) {
							InputParameters.computationMode = EnumPathwayComputationMode.ALL;							
						} 
						else if( "STORIES".equalsIgnoreCase(p.getOption("computationMode")) ) {
							InputParameters.computationMode = EnumPathwayComputationMode.STORIES;							
						} 
						else if( "ACCURACY".equalsIgnoreCase(p.getOption("computationMode")) ) {
							InputParameters.computationMode = EnumPathwayComputationMode.ACCURACY;							
						} 						
					}
					catch(Exception e) {
						System.out.println("Error: option <computationMode> should specify one of the following values: ALL|STORIES|ACCURACY.");
						System.exit(0);						
					}
					
				}
			}
		}
		else if( InputParameters.mode == EnumOperationMode.COMPUTE_BNS ) {
			// the goal is to compute a black node scenario
			// gets the name of the input file
			if( !p.hasOption("n") ) {
				printUsage();
			}
			InputParameters.inputFile = "./input/"+p.getOption("n");
			if( p.hasOption("paths") ) {
				if( p.getOption("paths").equalsIgnoreCase("all") )
					InputParameters.paths = EnumPathsMode.ALL_PATH;
				else if (p.getOption("paths").equalsIgnoreCase("shortest") )
					InputParameters.paths = EnumPathsMode.SHORTEST_PATH;
				else if (p.getOption("paths").equalsIgnoreCase("lightest") )
					InputParameters.paths = EnumPathsMode.LIGHTEST_PATH;
				else if (p.getOption("paths").equalsIgnoreCase("atom") )
					InputParameters.paths = EnumPathsMode.ATOM_MAPPING_PATH;
			}
			InputParameters.ignoreReversibility = p.hasOption("reversibility") && p.getOption("reversibility").equalsIgnoreCase("off");
			InputParameters.cytoscape = p.hasOption("cyto");
		}
		else if( InputParameters.mode == EnumOperationMode.STATISTICS ) {
			// the goal is to compute statistics or more information on the network/stories
			// gets the name of the input file
			if( !p.hasOption("n") ) {
				printUsage();
			}
			InputParameters.inputFile = "./input/"+p.getOption("n");
			// reads the output file name (if defined)
			if( p.hasOption("o") ) {
				InputParameters.outputFile = "./output/stories/"+p.getOption("o");
			}
			
			if( p.hasOption("scc") ) {
				InputParameters.stronglyConnectedComponents = true;
				if( p.getOption("scc").equalsIgnoreCase("all") ) {
					InputParameters.component = -1;
				}
				else {
					try 
					{
						InputParameters.component = new Integer(p.getOption("scc"));
					}
					catch(Exception e) {
						System.out.println("Error: option <scc> should have an integer value for the s.c.component to be inspected.");
						System.exit(0);
					}
				}
			}
		}
	}
}