/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;

import java.io.File;

import score.ComputeScore;
import stop.BernouilliRaceControl;
import stop.RaceControl;
import stop.TimeRaceControl;
import application.InputParameters.EnumTimeCondition;
import enumerator.AllOrdersGenerator;
import enumerator.FileFirstRandomOrdersGenerator;
import enumerator.OrderBasedEnumerator;
import enumerator.OrderGenerator;
import enumerator.RandomOrdersGenerator;
import enumerator.StoriesEnumeratorListener;

public class ComputeStories {

	
	public void run(ComputeScore score, StoriesEnumeratorListener listener) {
		
		File NELFileCheck = new File(InputParameters.inputFile+".nel");
		if (!NELFileCheck.exists() ){
			System.out.println(InputParameters.inputFile + ".nel not found.\n");
			System.exit(0);
		}
		
		// defines the stop method to be used (default: none)
		RaceControl rc = null;
		switch( InputParameters.stop ) {
			case MAX_STORIES: {
				rc = new RaceControl();
				break;
			}
			case TIME: {
				rc = new TimeRaceControl(InputParameters.stopTime, InputParameters.timeCondition == EnumTimeCondition.NO_NEW);
				break;
			}
			case BERNOUILLI: {
				rc = new BernouilliRaceControl(InputParameters.stopSampleSize, 0.001);
				break;
			}
		}

		OrderGenerator orderGen = null;
		if( InputParameters.generateAllOrders ) {
			orderGen = new AllOrdersGenerator();
		}
		else {
			if( !(InputParameters.orderFile == null || InputParameters.orderFile.isEmpty()) ) {
				orderGen = new FileFirstRandomOrdersGenerator(InputParameters.orderFile);
			}
			else {
				orderGen = new RandomOrdersGenerator();
			}
		}
		
		OrderBasedEnumerator storiesEnumerator = new OrderBasedEnumerator(rc, orderGen);
		if( listener != null ) {
			storiesEnumerator.setListener(listener);
		}
		if( score != null ) {
			storiesEnumerator.setScoreEvaluator(score);
		}
		storiesEnumerator.run(InputParameters.inputFile, InputParameters.stories);
	}
	
}
