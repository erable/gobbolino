/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;

import java.io.File;

import application.InputParameters.EnumPathwayComputationMode;
import experiment.pathways.PathwayCoveringSimulation;
import experiment.pathways.PathwayCoveringSimulationAccuracy;

public class PathwaySimulation {

	public void run() {
		File fileCheck = new File(InputParameters.inputFile + ".xml");
		if( !fileCheck.exists() ) {
			System.out.println(InputParameters.inputFile + ".xml not found.\n");
			System.exit(0);
		}

		if( InputParameters.computationMode == EnumPathwayComputationMode.ALL || 
			InputParameters.computationMode == EnumPathwayComputationMode.STORIES ) {
			(new PathwayCoveringSimulation()).run(InputParameters.inputFile, "./input/pathway");
		} 
		else if( InputParameters.computationMode == EnumPathwayComputationMode.ALL || 
			InputParameters.computationMode == EnumPathwayComputationMode.ACCURACY ) {
			(new PathwayCoveringSimulationAccuracy()).run(InputParameters.inputFile, "./input/pathway");
		} 

	}
	
}
