/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;

public class InputParameters {

	public enum EnumOperationMode { COMPUTE_BNS, COMPUTE_STORIES, PATHWAY_SIMULATION, RANDOM_BN_SIMULATION, STATISTICS }
	public enum EnumPathsMode { ALL_PATH, SHORTEST_PATH, LIGHTEST_PATH, ATOM_MAPPING_PATH }
	public enum EnumScoreMode { BN_DISTANCE, ISOLATION_LEVEL, EDGE_SUM, PARTIAL_ORDER, METABOLOMICS_INTENSITY }
	public enum EnumBlackNodes { FILE, AUTO_SOURCE_TARGET, AUTO_BORENSTEIN_ALL, AUTO_BORENSTEIN_ONE, AUTO_REACTION_GRAPH_BORENSTEIN } 
	public enum EnumStopCondition { MAX_STORIES, TIME, BERNOUILLI }
	public enum EnumTimeCondition {NO_NEW, ELAPSED }
	public enum EnumPathwayComputationMode { ALL, STORIES, ACCURACY }
	public enum EnumRandomBlackNodesMode { PERCENTAGE, ABSOLUT }
	
	// general parameters
	public static EnumOperationMode mode = EnumOperationMode.COMPUTE_STORIES;
	public static String  inputFile;
	public static boolean verbose = false;
	public static boolean countStoriesMode = false;		// in this mode, gobbolino only outputs the total number of stories and the time spent to produce them
	
	// parameters for computing the black node stage 
	public static EnumPathsMode paths = EnumPathsMode.LIGHTEST_PATH;
	public static boolean ignoreReversibility = false;
	public static boolean cytoscape = false;
	public static boolean createMetaNode = true;

	// parameters for computing stories
	public static String  outputFile = "./output/stories/stories.sto";
	public static int     stories = 10;
	public static boolean generateAllOrders = false;
	public static EnumScoreMode[] scoreMode = {EnumScoreMode.EDGE_SUM};
	public static boolean outputOnlyMaxScore = true;
	public static String  scoreFile;
	public static String  orderFile;
	public static EnumStopCondition stop = EnumStopCondition.MAX_STORIES;
	public static int stopTime = 10 * 60 * 1000; // 10 minutes (in milliseconds)
	public static int stopSampleSize = 10000;
	public static EnumTimeCondition timeCondition = EnumTimeCondition.NO_NEW;

	// parameters for random black nodes simulation
	public static int     randomBlackNodesMin = 5;
	public static int     randomBlackNodesMax = 15;
	public static int     randomBlackNodesIncrement = 5;
	public static int     simulationRuns = 3;
	public static EnumRandomBlackNodesMode randomblackNodesMode = EnumRandomBlackNodesMode.PERCENTAGE;
	
	// parameters for pathway covering simulation
	public static EnumBlackNodes blackNodesMode = EnumBlackNodes.FILE;
	public static int     thresholdForComputingAllStories = 8;
	public static EnumPathwayComputationMode computationMode = EnumPathwayComputationMode.ALL;

	// parameters for generating statistics on the stories generated
	public static boolean stronglyConnectedComponents = false;
	public static int     component = -1;
	
	public static double  storiesThresholdScore = 8.3;
	public static double  rangeHighestScore = 0.0;
	public static boolean analyseEdgeFrequencies = false;
	public static boolean generateArcWeightsFile = false;
	public static boolean generateAnthology = true;
	
	
}
