/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package score;

public class IsolationLevelScore extends ScoreEval {
	int[][] dist;

	private void floydWarshall() {
		dist = new int[story.getNumNodes()][story.getNumNodes()];
		for (int u = 0; u < story.getNumNodes(); u = u + 1) {
			for (int v = 0; v < story.getNumNodes(); v = v + 1) {
				dist[u][v] = computeWeight(u, v);
			}
		}
		for (int k = 1; k <= story.getNumNodes(); k = k + 1)
			for (int u = 0; u < story.getNumNodes(); u = u + 1) {
				for (int v = 0; v < story.getNumNodes(); v = v + 1) {
					if (dist[u][v] > dist[u][k - 1] + dist[k - 1][v]) {
						dist[u][v] = dist[u][k - 1] + dist[k - 1][v];
					}
				}
			}
	}

	public double[] computeScore() {
		floydWarshall();
		int[] ow = new int[story.getNumNodes()];
		for (int b = 0; b < story.getNumNodes(); b++) {
			if (story.isBlack(b)) {
				ow[b] = Integer.MAX_VALUE;
				for (int bb = 0; bb < story.getNumNodes(); bb++) {
					if (story.isBlack(bb)) {
						if (dist[b][bb] < ow[b]) {
							ow[b] = dist[b][bb];
						}
						if (dist[bb][b] < ow[b]) {
							ow[b] = dist[bb][b];
						}
					}
				}
			}
		}
		double sum = 0.0;
		int nb = 0;
		for (int b = 0; b < story.getNumNodes(); b++) {
			if (story.isBlack(b)) {
				sum = sum + ow[b];
				nb = nb + 1;
			}
		}
		return new double[] { nb / sum };
	}

	private int computeWeight(int u, int v) {
		if (story.areAdjacent(u,v)) {
//			return story.labels[u][v].split(";").length * story.labels[u][v].split(";").length;
			return story.getLabel(u,v).split(";").length;
		} else {
			return Integer.MAX_VALUE/4;
		}
	}

}
