/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package score;
import java.util.LinkedList;

import preprocessing.LightestPath;

public class SimpleDistanceScore extends ScoreEval {

	double[][] dist;

	public double[] computeScore() {
		LightestPath lp = new LightestPath(story, false, null);
		dist = lp.computeDistanceMatrix(false);

		LinkedList<Integer> sources = new LinkedList<Integer>();
		LinkedList<Integer> targets = new LinkedList<Integer>();
		for (int i = 0; i < story.getNumNodes(); i++) {
			if (story.getInDegree(i) == 0 && story.isBlack(i)) {
				sources.add(i);
			}
			if (story.getOutDegree(i) == 0 && story.isBlack(i)) {
				targets.add(i);
			}
		}

		double sumDistSourceTargets = 0;
		double nPairs = 0;
		double maxDist = 0;
		for (int i : sources) {
			for (int j : targets) {
				if (dist[i][j] > 0 && dist[i][j] < Integer.MAX_VALUE) {
					if (dist[i][j] > maxDist)
						maxDist = dist[i][j];
					nPairs++;
					sumDistSourceTargets += dist[i][j];
				}
			}
		}

		double avgDist = (double) (sumDistSourceTargets / (nPairs));
		// to get a normalized value, we consider the average distance divided
		// by the longest path
		return new double[] { (double) (avgDist / maxDist) };
	}

}
