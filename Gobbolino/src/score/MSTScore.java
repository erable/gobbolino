/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package score;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MSTScore extends ScoreEval {

	@SuppressWarnings("rawtypes")
	class Arc implements Comparable {
		int u, v;
		int weight;

		public Arc(int u, int v, int weight) {
			this.u = u;
			this.v = v;
			this.weight = weight;
		}

		public int compareTo(Object arg0) {
			if (arg0 instanceof Arc) {
				return weight - ((Arc) arg0).weight;
			}
			return -1;
		}
	}

	private List<Arc> arcs = new ArrayList<Arc>();

	@SuppressWarnings("unchecked")
	public double[] computeScore() {
		// build the list of edges of the story
		for (int i = 0; i < story.getNumNodes(); i++) {
			for (int j = 0; j < story.getNumNodes(); j++) {
				if (story.areAdjacent(i,j)) {
					arcs.add(new Arc(i, j, computeWeight(i, j)));
				}
			}
		}
		Collections.sort(arcs);
		return new double[] { 0.0 };
	}

	private int computeWeight(int u, int v) {
		return story.getLabel(u,v).split(";").length;
	}

}
