/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package score;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import network.NELNetwork;
import utils.SBMLTools;

public class PartialOrder {
	private int[][] partialOrder;

	public PartialOrder(int n) {
		init(n);
		
		// applying application specific partial orders (yeast)
		/*
		 * Derived from Fabien's slide
		 */
		partialOrder[1][3] = 1;
		partialOrder[1][5] = 1;
		partialOrder[1][11] = -1;
		partialOrder[1][18] = 1;
		partialOrder[1][20] = 1;
		partialOrder[3][11] = -1;
		partialOrder[3][14] = -1;
		partialOrder[3][15] = -1;
		partialOrder[5][11] = -1;
		partialOrder[5][14] = -1;
		partialOrder[5][15] = -1;
		partialOrder[11][14] = 1;
		partialOrder[11][15] = 1;
		partialOrder[11][18] = 1;
		partialOrder[11][20] = 1;
		partialOrder[14][18] = 1;
		partialOrder[14][20] = 1;
		partialOrder[15][18] = 1;
		partialOrder[15][20] = 1;
		/*
		 * Added by Fabien
		 */
		partialOrder[1][14] = -1;
		partialOrder[14][15] = 1;
		/*
		 * Added by Christophe
		 */
		partialOrder[1][2] = 1;
		partialOrder[2][14] = -1;
		partialOrder[2][15] = -1;
		
		symmetricRelations(n);
	}
	
	/*
	 * Initialization
	 */
	private void init(int n) {
		partialOrder = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				partialOrder[i][j] = 0;
			}
		}		
	}
	
	/*
	 * Definition of symmetric relations
	 */
	private void symmetricRelations(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (partialOrder[i][j] == 1) {
					partialOrder[j][i] = -1;
				}
				if (partialOrder[i][j] == -1) {
					partialOrder[j][i] = 1;
				}
			}
		}
	}

	public PartialOrder(NELNetwork bns, File partialOrderFile) {
		init(bns.getNumNodes());
		
		try {
			BufferedReader bbr = new BufferedReader(new FileReader(partialOrderFile));
			String line = bbr.readLine();
			while (line != null) {
				String smaller = line.split("<")[0].trim();
				String greater = line.split("<")[1].trim();
				
				String smallerNodeName = SBMLTools.sbmlEncode(smaller);
				String greaterNodeName = SBMLTools.sbmlEncode(greater);
				
				int smallerIndex = findNode(bns, smallerNodeName);
				int greaterIndex = findNode(bns, greaterNodeName);
				if( smallerIndex >= 0 && greaterIndex >= 0) {
					partialOrder[smallerIndex][greaterIndex] = -1;
					partialOrder[greaterIndex][smallerIndex] = 1;
				}
				line = bbr.readLine();
			}
			bbr.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private int findNode(NELNetwork bns, String name) {
		for(int i = 0; i < bns.getNumNodes(); i++) {
			if( bns.getNodeName(i).substring(0, bns.getNodeName(i).length()-1).equals(name) )
				return i;
		}
		return -1;
	}
	
	/*
	 * Element i is greater than element j (that is, i follows j) if the
	 * corresponding value is 1
	 */
	public boolean isGreater(int i, int j) {
		return partialOrder[i][j] == 1;
	}

	/*
	 * Element i is smaller than element j (that is, i precedes j) if the
	 * corresponding value is -1
	 */
	public boolean isSmaller(int i, int j) {
		return partialOrder[i][j] == -1;
	}
}
