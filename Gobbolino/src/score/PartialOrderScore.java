/* 
GOBBOLINO - A software tool to enumerate metabolic stories inside metabolic networks.

Copyright (C) 2011 Paulo V. Milreu (paulovieira@milreu.com.br), Pierluigi Crescenzi (pierluigi.crescenzi@unifi.it), Andrea Marino (andrea.marino@unifi.it),
Fabien Jourdan (fjourdan@toulouse.inra.fr ), Ludovic Cottret (l.cottret@gmail.com) and Vicente Acuna (l.cottret@gmail.com)      

This file is part of GOBBOLINO.

GOBBOLINO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOBBOLINO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOBBOLINO.  If not, see <http://www.gnu.org/licenses/>.
*/
package score;

import utils.Utility;

public class PartialOrderScore extends ScoreEval {
	private int[][] compare;
	private PartialOrder po;
	private int l = 0;
	private int offset;
	private boolean inversionCounter = true;
	private boolean whiteNodeCounter = false;

	public void setPartialOrder(PartialOrder po) {
		this.po = po;
	}
	
	public PartialOrder getPartialOrder() {
		return po;
	}

	public double[] computeScore() {
		compare = Utility.partialOrder(story.getAdjacencyMatrix(), story.getNumNodes());
		offset = 1;
		if (whiteNodeCounter) {
			offset = 2;
		}
		double[] score = new double[l + offset];
		if (inversionCounter) {
			/*
			 * The first score is the number of inversions
			 */
			double inversions = 0.0;
			for (int i = 0; i < story.getNumNodes(); i++) {
				for (int j = i + 1; j < story.getNumNodes(); j++) {
					if (po.isGreater(i, j) && compare[i][j] == -1) {
						inversions = inversions + 1;
					}
					if (po.isSmaller(i, j) && compare[i][j] == 1) {
						inversions = inversions + 1;
					}
				}
			}
			score[0] = 1 / (inversions + 1);
		} else {
			/*
			 * The first score is the number of errors
			 */
			double errors = 0.0;
			for (int i = 0; i < story.getNumNodes(); i++) {
				for (int j = i + 1; j < story.getNumNodes(); j++) {
					if (po.isGreater(i, j) && compare[i][j] != 1) {
						errors = errors + 1;
					}
					if (po.isSmaller(i, j) && compare[i][j] != -1) {
						errors = errors + 1;
					}
				}
			}
			score[0] = 1 / (errors + 1);
		}
		if (whiteNodeCounter) {
			/*
			 * The second score is the number of white nodes
			 */
			score[1] = 0;
			for (int u = 0; u < story.getNumNodes(); u++) {
				if (!story.isBlack(u) && story.isAlive(u)) {
					score[1] = score[1] + 1;
				}
			}
			score[1] = 1 / (1 + score[1]);
		}
		/*
		 * The remaining l scores are the number of edges of weight 2,...,l+1
		 */
		if (l > 0) {
			for (int w = offset; w < l + offset; w++) {
				score[w] = 0;
			}
			for (int u = 0; u < story.getNumNodes(); u++) {
				for (int v = 0; v < story.getNumNodes(); v++) {
					int weight = computeWeight(u, v);
					if (weight > 0) {
						if (weight <= l) {
							score[weight + offset - 2] = score[weight + offset
									- 2] + 1;
						} else {
							score[l + offset - 1] = score[l + offset - 1] + 1;
						}
					}
				}
			}
		}
		return score;
	}

	private int computeWeight(int u, int v) {
		if (story.areAdjacent(u, v)) {
			return story.getLabel(u,v).split(";").length;
		} else {
			return 0;
		}
	}
}
