	protected static double RED_OUTGOING_RED = 0.5;
	protected static double RED_OUTGOING_GREEN = 1.0;
	protected static double RED_INCOMING_RED = 0.25;
	protected static double RED_INCOMING_GREEN = -1.0;
	
	protected static double GREEN_OUTGOING_RED = -1.0;
	protected static double GREEN_OUTGOING_GREEN = 0;
	protected static double GREEN_INCOMING_RED = 1.0;
	protected static double GREEN_INCOMING_GREEN = 0.25;
	
	protected static double WHITE_OUTGOING_RED = 0;
	protected static double WHITE_OUTGOING_GREEN = 1.0;
	protected static double WHITE_INCOMING_RED = 0.5;
	protected static double WHITE_INCOMING_GREEN = -1.0;
