package fr.univ_lyon1.baobab.StoriesReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkWriterListener;

public class Story implements MetabolicNetworkWriterListener {

	String id;
	Map<Compound, String> compounds = new HashMap<Compound, String>();
	List<StoryArc> arcs = new ArrayList<StoryArc>();
	
	Story(String id)
	{
		this.id = id;
	}
	
	public void addCompound(StoryCompound c) {
		compounds.put(c, c.getRole());
	}
	
	public void addArc(StoryArc arc) {
		arcs.add(arc);
	}
	
	public void addCompoundFromId(String id, boolean isBlack, String idIntoStory, String role) {
		if( findById(id) == null ) {
			StoryCompound sc = new StoryCompound(id, id, null);
			sc.setBlack(isBlack);
			sc.setIdIntoStory(idIntoStory);
			sc.setRole(role);
			addCompound(sc);
		}
	}
	
	public Set<Compound> getCompounds() {
		return compounds.keySet();
	}
	
	public List<StoryArc> getArcs() {
		return arcs;
	}
	
	public Set<Compound> getCompoundsByRole(String role) {
		Set<Compound> cpds = new HashSet<Compound>();
		for(Compound c: compounds.keySet()) {
			if( compounds.get(c).equals(role) )
				cpds.add(c);
		}
		return cpds;
	}
	
	public Set<Compound> getNonSourceTargetsCompounds() {
		Set<Compound> cpds = new HashSet<Compound>();
		for(Compound c: compounds.keySet()) {
			if( (!compounds.get(c).equals("S")) && (!compounds.get(c).equals("T"))  )
				cpds.add(c);
		}
		return cpds;
	}	

	public List<Compound> getBlackCompounds() {
		List<Compound> blackNodes = new ArrayList<Compound>();
		for(Compound c: getCompounds()) {
			if( ((StoryCompound)c).getBlack() )
				blackNodes.add(c);
		}
		return blackNodes;
	}
	
	public String getId() {
		return id;
	}

	/* 
	 * Methods to act as a listener during the writing of the story as XML, it enables to add more information
	 * on the nodes (specific to stories), such as is the node is black and its role in the story.
	 * (non-Javadoc)
	 * @see fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkWriterListener#addPropertiesForCompound(fr.univ_lyon1.baobab.metabolicNetwork.Compound)
	 */
	public String addPropertiesForCompound(Compound c) {
		StoryCompound sc = findById(c.getId());
		if( sc != null )
			return "black=\""+sc.getBlack()+"\" idIntoStory=\""+sc.getIdIntoStory()+"\" role=\""+sc.getRole()+"\"";
		else
			return "black=\"false\" idIntoStory=\"absent\" role=\"none\"";
	}
	
	private StoryCompound findById(String id) {
		for(Compound sc: getCompounds()) {
			if( sc.getId().equals(id) )
				return (StoryCompound)sc;
		}
		return null;
	}

	public String addPropertiesForReaction(Reaction r) {
		return "";
	}
	
	public void exportProperties(String filename) {
		File propertiesFile;
		BufferedWriter bw;
		
		propertiesFile = new File(filename);
		try {
			if (propertiesFile != null) {
				bw = new BufferedWriter(new FileWriter(propertiesFile));
				bw.write("id \t idIntoStory \t black \t role");
				for(Compound c: getCompounds())
				{
					if( c instanceof StoryCompound )
					{
						StoryCompound sc = (StoryCompound)c;
						bw.write("\n"+c.getId()+" \t "+sc.getIdIntoStory()+" \t "+sc.getBlack()+" \t "+sc.getRole());
					}
					else
					{
						bw.write("\n"+c.getId()+" \t "+"NONE"+" \t "+"W"+" \t "+"NEW");						
					}
				}
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}		
	}
	
	
}
