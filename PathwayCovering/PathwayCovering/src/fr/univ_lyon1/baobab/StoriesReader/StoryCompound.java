package fr.univ_lyon1.baobab.StoriesReader;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;

public class StoryCompound extends Compound {

	private String idIntoStory;
	private Boolean black;
	private String role;
	
	public StoryCompound(String id, String name, String compartment) {
		super(id, name, compartment);
		// TODO Auto-generated constructor stub
	}

	public String getIdIntoStory() {
		return idIntoStory;
	}

	public void setIdIntoStory(String id) {
		this.idIntoStory = id;
	}

	public Boolean getBlack() {
		return black;
	}

	public void setBlack(Boolean black) {
		this.black = black;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	

}
