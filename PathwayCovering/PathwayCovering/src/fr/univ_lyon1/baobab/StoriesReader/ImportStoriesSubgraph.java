package fr.univ_lyon1.baobab.StoriesReader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImportStoriesSubgraph {

	public static List<Story> stories = new ArrayList<Story>();
	
	public static void importStories(String inFile)
	{
		stories = new ArrayList<Story>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			// 1st line - id of the story
			String line = br.readLine();
			while(line != null)
			{
				Story story = new Story(line.split(" ")[0]);
				
				// 2nd line - order that produced the story
				line = br.readLine();
				// 3rd line - number of nodes + number of black nodes
				line = br.readLine();
				String[] lineContent = line.split(" ");
				int nn = new Integer(lineContent[0]);
				int numberOfBlackNodes = new Integer(lineContent[1]);
				List<String> blackNodes = new ArrayList<String>(numberOfBlackNodes);
				String[] blackNodeRole = new String[numberOfBlackNodes];
				
				// ignore the attributes of the black nodes
				for (int i=1;i<numberOfBlackNodes+1;i++)
				{
					line = br.readLine();
					lineContent = line.split(" ");
					blackNodes.add(lineContent[0]);
					blackNodeRole[i-1] = lineContent[1];
				}
	
				for (int i=numberOfBlackNodes+1;i<numberOfBlackNodes+nn+1;i++)
				{
					line = br.readLine();
					lineContent = line.split(" ");
					String metanode = lineContent[3];
					String nodeId = lineContent[0];
					for(String c: metanode.split(";"))
						story.addCompoundFromId(c, blackNodes.contains(nodeId), nodeId, nodeRole(nodeId, blackNodes, blackNodeRole));
				}
				// now let us read the arcs of the story
				line = br.readLine();
				while(line != null && line.charAt(0) != '#')
				{
					lineContent = line.split(" ");
					String metanode = lineContent[2];
					
					for(String c: metanode.split(";"))
						story.addCompoundFromId(c, false, "Compressed", "C");
					line = br.readLine();
				}
				stories.add(story);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static String nodeRole(String nodeId, List<String> blackNodes, String[] roles) {
		int index = blackNodes.indexOf(nodeId);
		if( index >= 0 )
			return roles[index];
		return "W";
	}
}
