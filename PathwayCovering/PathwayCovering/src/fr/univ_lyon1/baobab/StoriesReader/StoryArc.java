package fr.univ_lyon1.baobab.StoriesReader;


public class StoryArc {

	private StoryCompound from;
	private StoryCompound to;
	
	public StoryArc(StoryCompound from, StoryCompound to) {
		this.setFrom(from);
		this.setTo(to);
	}

	public StoryCompound getFrom() {
		return from;
	}

	public void setFrom(StoryCompound from) {
		this.from = from;
	}

	public StoryCompound getTo() {
		return to;
	}

	public void setTo(StoryCompound to) {
		this.to = to;
	}


}
