package fr.univ_lyon1.baobab.applications;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.List;

public class PathwayCoveringOutput {

	private File outputFile = null;
	private BufferedWriter writer = null;
	private String experimentName;
	
	public PathwayCoveringOutput(String experimentName) {
		this.experimentName = experimentName;
	}

	public void finish() {
		if( writer != null ) {
			try {
				writer.close();
			}
			catch(Exception e) {
				System.out.println("Error trying to close file.");
			}
		}
	}
	
	private void writeHeader() {
		try {
			writer.write("experiment;" +
					"pathway;" +
					"#R;" +
					"bestStory;" +
					"#R;" +
					"TP;" +
					"FP;" +
					"FN;" +
					"Sn;" +
					"PPV;" +
					"Acc;" +
					"#BN\n");
		}
		catch(Exception e) {
			System.out.println("Error trying to write to the output file.");
		}		
	}
	
	public void register(String pathway, int numberReactionsPathway, String storyId, int numberReactionsStory,
			int TP, int FP, int FN, double Sn, double PPV, double Acc, int bn) {
		if( writer == null ) {
			prepareFile();
		}
		
		DecimalFormat df = new DecimalFormat("0.000");
		try {
			writer.write(experimentName + ";" +
					pathway + ";" +
					numberReactionsPathway + ";" +
					storyId + ";" +
					numberReactionsStory + ";" +
					TP + ";" +
					FP + ";" +
					FN + ";" +
					df.format(Sn) + ";" +
					df.format(PPV) + ";" +
					df.format(Acc) + ";" +
					bn + "\n"						 
					);
		}
		catch(Exception e) {
			System.out.println("Error trying to write to the output file.");
		}
	}
	
	private void prepareFile() {
		outputFile = new File(experimentName+".csv");
		try {
			writer = new BufferedWriter(new FileWriter(outputFile));
			writeHeader();
		}
		catch(Exception e) {
			System.out.println("Error trying to create output file "+experimentName+".csv");
		}
	}
	
}
