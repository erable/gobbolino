package fr.univ_lyon1.baobab.applications;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import org.xml.sax.SAXException;

import fr.univ_lyon1.baobab.StoriesReader.ImportStoriesSubgraph;
import fr.univ_lyon1.baobab.metabolicNetwork.Hypergraph;
import fr.univ_lyon1.baobab.metabolicNetwork.MetabolicNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkSBMLReader;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkSBMLWriter;

public class PathwayCovering {

	enum ComparisonModes {
		PATHWAY_TO_COMPOUND_GRAPH,
		STORY_TO_SUBHYPERGRAPH_STRICT,
		STORY_TO_SUBHYPERGRAPH_RELAXED,
		STORY_TO_SUBHYPERGRAPH_NODE_ROLES,
		STORY_TO_SUBHYPERGRAPH_EDGES
	}
	
	private static int TP;
	private static int FP;
	private static int FN;
	
	private static int bestTP;
	private static int bestFP;
	private static int bestFN;
	private static int bestStory;
	
	private static ComparisonModes mode = ComparisonModes.PATHWAY_TO_COMPOUND_GRAPH;
	private static boolean writeStoriesInXML = false;
	
	/**
	 * @param args
	 * @throws SAXException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, SAXException 
	{
		File[] pathways = new File("Pathways").listFiles(new FileFilter() {
			
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".xml");
			}
		});

		File[] stories = new File("Stories").listFiles(new FileFilter() {
			
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(".sto");
			}
		});
		
		if( args.length == 0 )
		{
			System.out.println("Metabolic network file name needed to proceed.");
			System.exit(0);
		}
		
		Hypergraph network = new Hypergraph();
		network.loadNetwork(new MetabolicNetworkSBMLReader(args[0]), false);
		System.out.println("Original network: #Reactions "+network.getReactions().size());

		PathwayCoveringOutput results = new PathwayCoveringOutput("pc-reaction-graph");
		try 
		{
			for(int i = 0; i < pathways.length; i++)
			{
				System.out.println("Checking the metabolic pathway "+pathways[i].getName());
				
				// Finds the stories file associated with the pathway
				String storiesFileName = findAssociatedStoriesFile(stories, pathways[i].getName());
				if( storiesFileName == null )
				{
					System.out.println("No metabolic stories file associated.");
				}
				else
				{
					// Loads the metabolic network of interest
					Hypergraph pathway = new Hypergraph();
					pathway.loadNetwork(new MetabolicNetworkSBMLReader("Pathways/"+pathways[i].getName()), false);
					
					ImportStoriesSubgraph.importStories("Stories/"+storiesFileName);
					System.out.println("#REACTIONS: Pathway = "+pathway.getReactions().size());
					
					bestTP = 0; bestFP = 0; bestFN = 0; bestStory = -1;
					int numReactions = 0;
					for(int story=0; story < ImportStoriesSubgraph.stories.size(); story++)
					{
						MetabolicNetwork subgraph = null;
						if( mode == ComparisonModes.STORY_TO_SUBHYPERGRAPH_NODE_ROLES ) {							
 							subgraph = network.subgraphFromCompoundsWithRoles(
								ImportStoriesSubgraph.stories.get(story).getCompoundsByRole("S"),
								ImportStoriesSubgraph.stories.get(story).getCompoundsByRole("T"),
								ImportStoriesSubgraph.stories.get(story).getNonSourceTargetsCompounds());
						}
						else if( mode == ComparisonModes.STORY_TO_SUBHYPERGRAPH_RELAXED ) {
							subgraph = network.subgraphFromCompounds(ImportStoriesSubgraph.stories.get(story).getCompounds());
						}
						else if( mode == ComparisonModes.PATHWAY_TO_COMPOUND_GRAPH ) {
							//CompoundNetwork compoundGraphPathway = new CompoundNetwork(new EdgeFactory());
							
						}
						
						if( writeStoriesInXML && subgraph != null ) {
							(new MetabolicNetworkSBMLWriter("StoriesInXML/"+storiesFileName+"_"+story+".xml")).write(subgraph, null);
						}
						
						//double intersection = intersection(subgraph, pathway);
						//System.out.println("Story "+story+". #Reaction = "+subgraph.getReactions().size() + " - |intersection|: "+intersection+". Covers "+df.format(intersection/pathway.getReactions().size()));
						compareReactionSets(subgraph, pathway);
						if( getAcc() > getBestAcc() ) {
							updateBest(story);
							numReactions = subgraph.getReactions().size();
						}
					}
					if( bestStory != -1 ) {
						results.register(pathways[i].getName(), pathway.getReactions().size(), ""+bestStory, 
								numReactions, bestTP, bestFP, bestFN, getBestSn(), getBestPPV(), getBestAcc(), ImportStoriesSubgraph.stories.get(bestStory).getBlackCompounds().size() );
					}
				}
				System.out.println("\n---------------\n");
			}
		} 
		catch(Exception e)
		{
			System.out.println("Error: "+e.getMessage());
		}
		results.finish();
	}
	
	private static String findAssociatedStoriesFile(File[] stories, String pathwayFile) {
		for(int i = 0; i < stories.length; i++) {
			if( stories[i].getName().replace(".sto", ".xml").equals(pathwayFile) )
				return stories[i].getName();
		}
		return null;
	}
	
	private static void compareReactionSets(MetabolicNetwork story, MetabolicNetwork pathway) {
		TP = 0; FP = 0;
		for(Reaction reaction1: story.getReactions())
		{
			boolean present = false;
			// is reaction 1 present in net2?
			for(Reaction reaction2: pathway.getReactions())
			{
				if( reaction2.getId().equals(reaction1.getId()) ) {
					present = true;
					TP++;
					break;
				}
			}
			if( !present ) {
				FP++;
			}
		}

		FN = 0;
		for(Reaction reaction2: pathway.getReactions())
		{
			boolean present = false;
			// is reaction 1 present in net2?
			for(Reaction reaction1: story.getReactions())
			{
				if( reaction2.getId().equals(reaction1.getId()) ) {
					present = true;
					break;
				}
			}
			if( !present ) {
				FN++;
			}
		}
	}
	
	public static double getSn() {
		if( (TP + FN) == 0)
			return 0;
		
		return (double)TP / (TP + FN);
	}
	
	public static double getPPV() {
		if( (TP + FP) == 0)
			return 0;

		return (double)TP / (TP + FP);
	}
	
	public static double getAcc() { 
		return Math.sqrt(getSn() * getPPV());
	}

	public static double getBestSn() {
		if( (bestTP + bestFN) == 0)
			return 0;
		
		return (double)bestTP / (bestTP + bestFN);
	}
	
	public static double getBestPPV() {
		if( (bestTP + bestFP) == 0)
			return 0;

		return (double)bestTP / (bestTP + bestFP);
	}
	
	public static double getBestAcc() {
		return Math.sqrt(getBestSn() * getBestPPV());
	}
	
	private static void updateBest(int story) {
		bestTP = TP;
		bestFP = FP;
		bestFN = FN;
		bestStory = story;
	}
	
}
