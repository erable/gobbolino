import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class TimeDataReader {
	void run(String fn) {
		File inFile = new File(fn+".time");
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			int index = line.indexOf("\t");
			int previousFirstColumn = Integer
					.parseInt(line.substring(0, index));
			System.out.println(line);
			line = br.readLine();
			while (line != null) {
				index = line.indexOf("\t");
				int currentFirstColumn = Integer.parseInt(line.substring(0,
						index));
				while (previousFirstColumn == currentFirstColumn) {
					line = br.readLine();
					index = line.indexOf("\t");
					currentFirstColumn = Integer.parseInt(line.substring(0,
							index));
				}
				System.out.println(line);
				previousFirstColumn = currentFirstColumn;
				line = br.readLine();
			}
		} catch (Exception e) {

		}
	}
}
