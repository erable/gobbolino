import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;

public class NELWithALNetwork {
	LinkedList<Integer>[] adjacencyList;
	LinkedList<Integer>[] incidencyList;
	int[] inDegree;
	int[] outDegree;
	boolean[] isBlack;
	String[] nodeName;
	int nn, ne;

	int numberOfEdges() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			result = result + outDegree[i];
		}
		return result;
	}

	int numberOfBlackNodes() {
		int b = 0;
		for (int i = 0; i < nn; i++) {
			if (isBlack[i]) {
				b++;
			}
		}
		return b;
	}

	void printStatistics(boolean verbose, String method) {
		if (verbose) {
			System.out.println("=== Graph statistics after " + method + " ===");
			System.out.println("Number of nodes: " + nn);
			System.out.println("Number of edges: " + ne);
		}
	}

	@SuppressWarnings("unchecked")
	void readFile(String inPath) {
		File inFile = new File(inPath);
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			int index = line.indexOf(' ');
			nn = Integer.parseInt(line.substring(0, index));
			int numberOfBlackNodes = Integer
					.parseInt(line.substring(index + 1));
			isBlack = new boolean[nn];
			for (int i = 0; i < nn; i++) {
				isBlack[i] = false;
			}
			for (int i = 0; i < numberOfBlackNodes; i++) {
				isBlack[Integer.parseInt(br.readLine())] = true;
			}
			inDegree = new int[nn];
			outDegree = new int[nn];
			nodeName = new String[nn];
			for (int i = 0; i < nn; i++) {
				line = br.readLine();
				index = line.indexOf(' ');
				int secondIndex = line.indexOf(' ', index + 1);
				outDegree[i] = Integer.parseInt(line.substring(index + 1,
						secondIndex));
				index = line.indexOf(' ', secondIndex + 1);
				inDegree[i] = Integer.parseInt(line.substring(secondIndex + 1,
						index));
				nodeName[i] = line.substring(index + 1);
			}
			ne = numberOfEdges();
			adjacencyList = new LinkedList[nn];
			incidencyList = new LinkedList[nn];
			for (int i = 0; i < nn; i++) {
				adjacencyList[i] = new LinkedList<Integer>();
				incidencyList[i] = new LinkedList<Integer>();
			}
			line = br.readLine();
			while (line != null) {
				index = line.indexOf(' ');
				int s = Integer.parseInt(line.substring(0, index));
				int t = Integer.parseInt(line.substring(index + 1));
				adjacencyList[s].add(t);
				incidencyList[t].add(s);
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
