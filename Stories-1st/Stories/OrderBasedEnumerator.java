import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

public class OrderBasedEnumerator extends NELWithALNetwork {
	private int[][] compare;
	private boolean[][] pitchAdj;
	private boolean[] isInPitch;
	private int[] rank, order;
	private boolean randomOrder = true;
	private HashMap<String, Integer> sasSet = new HashMap<String, Integer>();
	private HashMap<String, Integer> edgeIndex = new HashMap<String, Integer>();
	private int nsas = 0, maxNSAS;
	private int[][] storyIncidency;
	private String[][] nodeKind;
	private Random rnd = new Random();
	private boolean pitchChanged;

	private void addPath(int v, int u, int[] pred) {
		while (pred[u] != u) {
			pitchAdj[pred[u]][u] = true;
			isInPitch[u] = true;
			u = pred[u];
		}
	}

	private boolean areDifferent(int[][] X, int[][] Y) {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (X[i][j] != Y[i][j]) {
					return true;
				}
			}
		}
		return false;
	}

	private void bfs(int v) {
		int[] pred = new int[nn];
		boolean[] visited = new boolean[nn];
		for (int i = 0; i < nn; i++) {
			pred[i] = -1;
			visited[i] = false;
		}
		LinkedList<Integer> queue = new LinkedList<Integer>();
		pred[v] = v;
		queue.offer(v);
		while (!queue.isEmpty()) {
			int x = queue.poll();
			visited[x] = true;
			for (int u = 0; u < nn; u++) {
				if (adjacencyList[x].contains(u) && !pitchAdj[x][u]
						&& compare[x][u] <= 0) {
					if (!visited[u] && isInPitch[u] && compare[u][v] >= 0) {
						pred[u] = x;
						addPath(v, u, pred);
						setCompare();
						pitchChanged = true;
					}
					if (!isInPitch[u] && !visited[u]) {
						pred[u] = x;
						queue.offer(u);
					}
				}
			}
		}
	}

	void computePitch() {
		for (int u = 0; u < nn; u++) {
			for (int v : adjacencyList[u]) {
				if (rank[u] > rank[v]) {
					pitchAdj[u][v] = false;
				} else {
					isInPitch[u] = true;
					isInPitch[v] = true;
					pitchAdj[u][v] = true;
				}
			}
		}
		boolean changed = true;
		while (changed) {
			changed = false;
			for (int u = 0; u < nn; u++) {
				if (!isBlack[u]
						&& isInPitch[u]
						&& (successors(u, pitchAdj).size() == 0 || predecessors(
								u, pitchAdj).size() == 0)) {
					isInPitch[u] = false;
					for (int v = 0; v < nn; v++) {
						pitchAdj[u][v] = false;
						pitchAdj[v][u] = false;
					}
					changed = true;
				}
			}
		}
	}

	private void computeSAS() {
		isInPitch = new boolean[nn];
		pitchAdj = new boolean[nn][nn];
		for (int i = 0; i < nn; i++) {
			isInPitch[i] = false;
			if (isBlack[i]) {
				isInPitch[i] = true;
			}
			for (int j = 0; j < nn; j++) {
				pitchAdj[i][j] = false;
			}
		}
		computePitch();
		compare = new int[nn][nn];
		setCompare();
		pitchChanged = true;
		while (pitchChanged) {
			pitchChanged = false;
			for (int v = 0; v < nn; v++) {
				if (isInPitch[order[v]]) {
					bfs(order[v]);
				}
			}
		}
		String sas = sasFromPitchAdj();
		if (!sasSet.containsKey(sas)) {
			sasSet.put(sas, 1);
			if (randomOrder) {
				newSAS();
			}
			nsas = nsas + 1;
		} else {
			int m = sasSet.get(sas);
			m = m + 1;
			sasSet.put(sas, m);
		}
	}

	private void copy(int[][] X, int[][] Y) {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				Y[i][j] = X[i][j];
			}
		}
	}
	
	private void initOutputMatrices() {
		storyIncidency = new int[ne][maxNSAS];
		nodeKind = new String[nn][maxNSAS];
		for (int i = 0; i < ne; i++) {
			for (int j = 0; j < maxNSAS; j++) {
				storyIncidency[i][j] = 0;
			}
		}
		int e = 0;
		for (int i = 0; i < nn; i++) {
			for (int j : adjacencyList[i]) {
				edgeIndex.put(toString(new int[] { i, j }), e);
				e = e + 1;
			}
		}
	}

	private int[][] multiply(int[][] X, int[][] Y) {
		int[][] R = new int[nn][nn];
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				R[i][j] = 0;
				for (int k = 0; k < nn; k++) {
					R[i][j] = R[i][j] + X[i][k] * Y[k][j];
				}
				if (R[i][j] > 0) {
					R[i][j] = 1;
				}
			}
		}
		return R;
	}

	private void newSAS() {
		for (int i = 0; i < nn; i++) {
			for (int j : adjacencyList[i]) {
				if (pitchAdj[i][j]) {
					storyIncidency[edgeIndex.get(toString(new int[] { i, j }))][nsas] = 1;
				}
			}
		}
		for (int i = 0; i < nn; i++) {
			if (!isBlack[i]) {
				nodeKind[i][nsas] = "W";
			} else {
				boolean isSource = true;
				for (int j = 0; j < nn; j++) {
					if (adjacencyList[j].contains(i) && pitchAdj[j][i]) {
						isSource = false;
						break;
					}
				}
				if (isSource) {
					nodeKind[i][nsas] = "S";
				} else {
					boolean isTarget = true;
					for (int j = 0; j < nn; j++) {
						if (adjacencyList[i].contains(j) && pitchAdj[i][j]) {
							isTarget = false;
							break;
						}
					}
					if (isTarget) {
						nodeKind[i][nsas] = "T";
					} else {
						nodeKind[i][nsas] = "I";
					}
				}
			}
		}
	}

	private LinkedList<Integer> predecessors(int u, boolean[][] adj) {
		LinkedList<Integer> rst = new LinkedList<Integer>();
		for (int v = 0; v < nn; v++) {
			if (adj[v][u]) {
				rst.add(v);
			}
		}
		return rst;
	}

	void print(boolean[] x) {
		for (int i = 0; i < nn; i++) {
			System.out.print(x[i] + " ");
		}
		System.out.println();
		System.out.println();
	}

	private void printNodeKind() {
		for (int i = 0; i < nn; i++) {
			System.out.print(i);
			for (int s = 0; s < maxNSAS; s++) {
				System.out.print("\t" + nodeKind[i][s]);
			}
			System.out.println();
		}
	}

	private void printSASSet() {
		for (String sas : sasSet.keySet()) {
			System.out.println(sas + ": " + sasSet.get(sas));
		}
	}

	private void printStoryIncidency() {
		for (int i = 0; i < nn; i++) {
			for (int j : adjacencyList[i]) {
				System.out.print(i + " (pp) " + j);
				for (int s = 0; s < maxNSAS; s++) {
					System.out.print("\t"
							+ storyIncidency[edgeIndex.get(toString(new int[] {
									i, j }))][s]);
				}
				System.out.println();
			}
		}
	}

	private void randomOrder() {
		for (int i = 0; i < nn; ++i) {
			int j = rnd.nextInt(i + 1);
			rank[i] = rank[j];
			rank[j] = i;
		}
		for (int i = 0; i < nn; i++) {
			order[rank[i]] = i;
		}
	}

	void run(String networkName) {
		randomOrder = false;
		readFile(networkName + ".nel");
		rank = new int[nn];
		for (int i = 0; i < nn; i++) {
			rank[i] = i;
		}
		runAll(nn);
		printSASSet();
	}

	void run(String networkName, int maxNSAS) {
		this.maxNSAS = maxNSAS;
		readFile(networkName + ".nel");
		rank = new int[nn];
		order = new int[nn];
		initOutputMatrices();
		while (nsas < maxNSAS) {
			randomOrder();
			computeSAS();
		}
		printSASSet();
		System.out.println();
		printNodeKind();
		System.out.println();
		printStoryIncidency();
	}

	private void runAll(int p) {
		if (p == 0) {
			computeSAS();
		} else {
			for (int i = p - 1; i >= 0; i--) {
				swap(i, p - 1);
				runAll(p - 1);
				swap(i, p - 1);
			}
		}
	}

	private String sasFromPitchAdj() {
		boolean[][] isInSAS = new boolean[nn][nn];
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				if (adjacencyList[u].contains(v) && !pitchAdj[u][v]) {
					isInSAS[u][v] = true;
				} else {
					isInSAS[u][v] = false;
				}
			}
		}
		return toString(isInSAS);
	}

	private void setCompare() {
		int[][] A = new int[nn][nn];
		int[][] AStar = new int[nn][nn];
		int[][] B = new int[nn][nn];
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (pitchAdj[i][j]) {
					A[i][j] = 1;
					AStar[i][j] = 1;
				} else {
					A[i][j] = 0;
					AStar[i][j] = 0;
				}
			}
			A[i][i] = 1;
			AStar[i][i] = 1;
		}
		do {
			copy(AStar, B);
			AStar = multiply(B, B);
		} while (areDifferent(AStar, B));
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				compare[i][j] = 0;
			}
		}
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (AStar[i][j] == 1) {
					compare[i][j] = -1;
					compare[j][i] = 1;
				}
			}
		}
	}

	private LinkedList<Integer> successors(int u, boolean[][] adj) {
		LinkedList<Integer> rst = new LinkedList<Integer>();
		for (int v = 0; v < nn; v++) {
			if (adj[u][v]) {
				rst.add(v);
			}
		}
		return rst;
	}

	private void swap(int i, int j) {
		int t = rank[i];
		rank[i] = rank[j];
		rank[j] = t;
	}

	private String toString(boolean[][] a) {
		String rst = "";
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				if (adjacencyList[u].contains(v) && a[u][v]) {
					String s = toString(new int[] { u, v });
					rst = rst + s;
				}
			}
		}
		return rst;
	}

	private String toString(int[] edge) {
		return "[" + edge[0] + "," + edge[1] + "]";
	}

}
