public class GraphSimplifier extends NELNetwork {
	boolean bb() {
		boolean answer = false;
		for (int i = 0; i < nn; i++) {
			if (!isBlack[i] && inDegree[i] == 1) {
				int p;
				for (p = 0; p < nn; p++) {
					if (adjacency[p][i]) {
						adjacency[p][i] = false;
						inDegree[i] = inDegree[i] - 1;
						outDegree[p] = outDegree[p] - 1;
						break;
					}
				}
				for (int s = 0; s < nn; s++) {
					if (adjacency[i][s]) {
						adjacency[i][s] = false;
						outDegree[i] = outDegree[i] - 1;
						inDegree[s] = inDegree[s] - 1;
						if (!adjacency[p][s]) {
							adjacency[p][s] = true;
							outDegree[p] = outDegree[p] + 1;
							inDegree[s] = inDegree[s] + 1;
						}
					}
				}
				nodeAlive[i] = false;
				answer = true;
			}
		}
		return answer;
	}

	boolean deleteSelfLoops() {
		boolean answer = false;
		for (int i = 0; i < nn; i++) {
			if (adjacency[i][i]) {
				adjacency[i][i] = false;
				inDegree[i] = inDegree[i] - 1;
				outDegree[i] = outDegree[i] - 1;
				answer = true;
			}
		}
		return answer;
	}

	boolean deleteStars() {
		boolean answer = false;
		boolean changed = true;
		while (changed) {
			changed = false;
			for (int i = 0; i < nn; i++) {
				if (nodeAlive[i] && !isBlack[i] && inDegree[i] == outDegree[i]) {
					int d = inDegree[i];
					int[] pred = new int[d];
					int h = 0;
					for (int p = 0; p < nn; p++) {
						if (adjacency[p][i]) {
							pred[h] = p;
							h = h + 1;
						}
					}
					boolean canBeDeleted = true;
					for (int p = 0; canBeDeleted && p < d; p++) {
						if (!adjacency[i][pred[p]]) {
							canBeDeleted = false;
						}
					}
					if (canBeDeleted) {
						for (int p = 0; p < d; p++) {
							adjacency[pred[p]][i] = false;
							adjacency[i][pred[p]] = false;
							inDegree[i] = inDegree[i] - 1;
							outDegree[pred[p]] = outDegree[pred[p]] - 1;
							outDegree[i] = outDegree[i] - 1;
							inDegree[pred[p]] = inDegree[pred[p]] - 1;
						}
						nodeAlive[i] = false;
						for (int p1 = 0; p1 < d - 1; p1++) {
							for (int p2 = p1 + 1; p2 < d; p2++) {
								if (!adjacency[pred[p1]][pred[p2]]) {
									adjacency[pred[p1]][pred[p2]] = true;
									outDegree[pred[p1]] = outDegree[pred[p1]] + 1;
									inDegree[pred[p2]] = inDegree[pred[p2]] + 1;
								}
								if (!adjacency[pred[p2]][pred[p1]]) {
									adjacency[pred[p2]][pred[p1]] = true;
									outDegree[pred[p2]] = outDegree[pred[p2]] + 1;
									inDegree[pred[p1]] = inDegree[pred[p1]] + 1;
								}
							}
						}
						changed = true;
						answer = true;
					}
				}
			}
		}
		return answer;
	}

	boolean fb() {
		boolean answer = false;
		for (int i = 0; i < nn; i++) {
			if (!isBlack[i] && outDegree[i] == 1) {
				int s;
				for (s = 0; s < nn; s++) {
					if (adjacency[i][s]) {
						adjacency[i][s] = false;
						inDegree[s] = inDegree[s] - 1;
						outDegree[i] = outDegree[i] - 1;
						break;
					}
				}
				for (int p = 0; p < nn; p++) {
					if (adjacency[p][i]) {
						adjacency[p][i] = false;
						outDegree[p] = outDegree[p] - 1;
						inDegree[i] = inDegree[i] - 1;
						if (!adjacency[p][s]) {
							adjacency[p][s] = true;
							outDegree[p] = outDegree[p] + 1;
							inDegree[s] = inDegree[s] + 1;
						}
					}
				}
				nodeAlive[i] = false;
				answer = true;
			}
		}
		return answer;
	}

	void run(String nn, boolean verbose) {
		readFile(nn + ".nel");
		printStatistics(verbose, "readFile");
		boolean changed = true;
		while (changed) {
			changed = false;
			if (fb()) {
				printStatistics(verbose, "fb");
				changed = true;
			}
			if (deleteSelfLoops()) {
				printStatistics(verbose, "deleteSelfLoops");
				changed = true;
			}
			if (bb()) {
				printStatistics(verbose, "bb");
				changed = true;
			}
			if (deleteSelfLoops()) {
				printStatistics(verbose, "deleteSelfLoops");
				changed = true;
			}
		}
		computeAliveNodes();
		computeDegrees();
		int numberOfAlive = reMap();
		writeFile(nn + "S.nel", numberOfAlive);
	}

}
