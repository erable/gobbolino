public class BlackGraphGenerator extends NELNetwork {
	void run(String fn) {
		readFile(fn + ".nel");
		int nb = numberOfBlackNodes();
		int[] map = new int[nn];
		int b = 0;
		for (int u = 0; u < nn; u++) {
			if (isBlack[u]) {
				map[u] = b;
				b = b + 1;
			}
		}
		boolean[] newIsBlack = new boolean[nb];
		String[] newNodeName = new String[nb];
		for (int u = 0; u < nn; u++) {
			if (isBlack[u]) {
				newIsBlack[map[u]] = true;
				newNodeName[map[u]] = nodeName[u];
			}
		}
		boolean[][] newAdj = new boolean[nb][nb];
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				if (isBlack[u] && isBlack[v]) {
					newAdj[map[u]][map[v]] = adjacency[u][v];
				}
			}
		}
		adjacency = newAdj;
		isBlack = newIsBlack;
		nn = nb;
		computeDegrees();
		oldNew = new int[nb];
		nodeAlive = new boolean[nb];
		for (int u = 0; u < nb; u++) {
			oldNew[u] = u;
			nodeAlive[u] = true;
		}
		nodeName = newNodeName;
		writeFile(fn + "Black.nel", nn);
	}

}
