import java.util.LinkedList;
import java.util.Stack;

public class ShortestPathGraphCreator extends NELNetwork {
	boolean[][] backupAdjacency;
	boolean[][] edgeAlive;
	Stack<Integer> marked;
	Stack<Integer> point;
	boolean[] mark, backupBlack;
	int numberOfPaths, numberOfActiveEdges, numberOfEdges;

	private void bardemAlgorithm() {
		for (int s = 0; s < nn; s++) {
			if (isBlack[s]) {
				for (int t = 0; t < nn; t++) {
					if (t != s && isBlack[t]) {
						bardemAlgorithm(s, t);
					}
					// restore();
				}
			}
		}
	}

	private void bardemAlgorithm(int s, int t) {
		int[] dist = bfs(s, t);
		if (dist != null) {
			LinkedList<Integer> queue = new LinkedList<Integer>();
			queue.offer(t);
			while (!queue.isEmpty()) {
				int u = queue.poll();
				for (int v = 0; v < nn; v++) {
					if (adjacency[v][u] && dist[v] == dist[u] - 1) {
						edgeAlive[v][u] = true;
						queue.offer(v);
					}
				}
			}
		}
	}

	private int[] bfs(int s, int t) {
		int[] dist = new int[nn];
		boolean[] reached = new boolean[nn];
		for (int i = 0; i < nn; i++) {
			reached[i] = false;
			dist[i] = Integer.MAX_VALUE;
		}
		LinkedList<Integer> queue = new LinkedList<Integer>();
		queue.offer(s);
		reached[s] = true;
		dist[s] = 0;
		while (!queue.isEmpty()) {
			int u = queue.poll();
			if (u == t) {
				break;
			} else {
				for (int x = 0; x < nn; x++) {
					if (!reached[x] && adjacency[u][x]) {
						queue.offer(x);
						reached[x] = true;
						dist[x] = dist[u] + 1;
					}
				}
			}
		}
		if (dist[t] == Integer.MAX_VALUE) {
			return null;
		}
		return dist;
	}

	private void initEdgeLiveness() {
		edgeAlive = new boolean[nn][nn];
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				edgeAlive[i][j] = false;
			}
		}
		numberOfActiveEdges = 0;
	}

	void run(String nn, boolean verbose) {
		readFile(nn + ".nel");
		initEdgeLiveness();
		bardemAlgorithm();
		updateAdjacency();
		computeAliveNodes();
		computeDegrees();
		int numberOfAlive = reMap();
		writeFile(nn + "SP.nel", numberOfAlive);
	}

	private void updateAdjacency() {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				adjacency[i][j] = edgeAlive[i][j];
			}
		}
	}
}
