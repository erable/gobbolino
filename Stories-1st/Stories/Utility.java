
public class Utility {
	static void print(boolean[][] X) {
		for (int i = 0; i < X.length; i++) {
			for (int j = 0; j < X[0].length; j++) {
				if (X[i][j]) {
					System.out.print(1 + " ");
				} else {
					System.out.print(0 + " ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	static void print(int[] x) {
		for (int i = 0; i < x.length; i++) {
			System.out.print(x[i] + " ");
		}
		System.out.println();
		System.out.println();
	}

	static void print(int[][] X) {
		for (int i = 0; i < X.length; i++) {
			for (int j = 0; j < X[0].length; j++) {
				System.out.print(X[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	void printCompare(int[][] compare) {
		System.out.println("Compare matrix");
		for (int i = 0; i < compare.length; i++) {
			for (int j = 0; j < compare[0].length; j++) {
				if (compare[i][j] > 0) {
					System.out.print("+1 ");
				} else if (compare[i][j] < 0) {
					System.out.print("-1 ");
				} else {
					System.out.print(" 0 ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}
}
