public class Main {
	public static void main(String[] args) {
		String fn = "yeastLPS";
		boolean all = false;
		boolean allOrderBaseEnumerate = false;
		boolean analyze = false;
		boolean blackGraphGenerate = false;
		boolean cycles = true;
		boolean etienneEnumerate = false;
		boolean fasEnumerate = false;
		boolean fromXML = false;
		boolean lightestPathCreate = false;
		boolean oneWay = false;
		boolean randomOrderBaseEnumerate = false;
		boolean readTime = false;
		boolean shortestPathCreate = false;
		boolean simplify = false;
		boolean verbose = false;
		boolean vichoConvert = false;
		int maxNSAS = 20;

		if (fromXML) {
			(new XML2NEL()).run(fn, verbose, oneWay);
		}
		if (lightestPathCreate || all) {
			(new LightestPathGraphCreator()).run(fn);
		}
		if (shortestPathCreate || all) {
			(new ShortestPathGraphCreator()).run(fn, verbose);
		}
		if (simplify || all) {
			(new GraphSimplifier()).run(fn, verbose);
		}
		if (analyze) {
			(new GraphAnalyzer()).run(fn, true, false);
		}
		if (cycles) {
			GraphAnalyzer ga = new GraphAnalyzer();
			ga.readFile(fn + ".nel");
			ga.numberOfCycles = 0;
			ga.enumerateAllCircuits();
			System.out.println(ga.numberOfCycles);
		}
		if (fasEnumerate) {
			(new FASEnumerator()).run(fn, verbose);
		}
		if (randomOrderBaseEnumerate) {
			(new OrderBasedEnumerator()).run(fn, maxNSAS);
		}
		if (allOrderBaseEnumerate) {
			(new OrderBasedEnumerator()).run(fn);
		}
		if (etienneEnumerate) {
			(new EtienneEnumerator()).run(fn);
		}
		if (readTime) {
			(new TimeDataReader()).run(fn);
		}
		if (vichoConvert) {
			(new VichoConverter()).run(fn);
		}
		if (blackGraphGenerate) {
			(new BlackGraphGenerator()).run(fn);
		}
	}
}
