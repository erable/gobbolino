import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;


public class NELNetwork {
	boolean[][] adjacency;
	int[] inDegree;
	int[] outDegree;
	boolean[] isBlack;
	boolean[] nodeAlive;
	int[] oldNew;
	String[] nodeName;
	int nn, ne;

	void computeAliveNodes() {
		nodeAlive = new boolean[nn];
		for (int i = 0; i < nn; i++) {
			nodeAlive[i] = false;
			for (int j = 0; j < nn; j++) {
				if (adjacency[i][j]) {
					nodeAlive[i] = true;
				}
				if (adjacency[j][i]) {
					nodeAlive[i] = true;
				}
			}
		}
	}

	void computeDegrees() {
		inDegree = new int[nn];
		outDegree = new int[nn];
		for (int i = 0; i < nn; i++) {
			inDegree[i] = 0;
			outDegree[i] = 0;
			for (int j = 0; j < nn; j++) {
				if (adjacency[i][j]) {
					outDegree[i]++;
				}
				if (adjacency[j][i]) {
					inDegree[i]++;
				}
			}
		}
	}

	int numberOfAlives() {
		int b = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i]) {
				b++;
			}
		}
		return b;
	}
	
	int numberOfEdges() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			result = result+outDegree[i];
		}
		return result;
	}

	int numberOfSelfLoops() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i] && adjacency[i][i]) {
				result = result + 1;
			}
		}
		return result;
	}

	int numberOfSources() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i] && inDegree[i] == 0 && !isBlack[i]) {
				result = result + 1;
			}
		}
		return result;
	}

	int numberOfTargets() {
		int result = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i] && outDegree[i] == 0 && !isBlack[i]) {
				result = result + 1;
			}
		}
		return result;
	}
	
	int numberOfBlackNodes() {
		int b = 0;
		for (int i = 0; i < nn; i++) {
			if (isBlack[i]) {
				b++;
			}
		}
		return b;
	}

	void checkDegrees() {
		for (int i = 0; i < nn; i++) {
			if (!nodeAlive[i] && (inDegree[i] != 0 || outDegree[i] != 0)) {
				System.out.println("Warning: dead node " + i
						+ " has degree greater than 0");
				System.exit(-1);
			}
			int in = 0;
			int out = 0;
			for (int j = 0; j < nn; j++) {
				if (adjacency[i][j])
					out++;
				if (adjacency[j][i])
					in++;
			}
			if (inDegree[i] != in) {
				System.out
						.println("Warning: node " + i + " has wrong indegree");
				System.exit(-1);
			}
			if (outDegree[i] != out) {
				System.out.println("Warning: node " + i
						+ " has wrong outdegree");
				System.exit(-1);
			}
		}
	}

	void printStatistics(boolean verbose, String method) {
		if (verbose) {
			System.out.println("=== Graph statistics after " + method + " ===");
			System.out.println("Number of alive nodes: " + numberOfAlives());
			System.out.println("Number of self-loops: " + numberOfSelfLoops());
			System.out.println("Number of white sources: " + numberOfSources());
			System.out.println("Number of white targets: " + numberOfTargets());
		}
	}

	void readFile(String inPath) {
		File inFile = new File(inPath);
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			int index = line.indexOf(' ');
			nn = Integer.parseInt(line.substring(0, index));
			int numberOfBlackNodes = Integer
					.parseInt(line.substring(index + 1));
			isBlack = new boolean[nn];
			nodeAlive = new boolean[nn];
			for (int i = 0; i < nn; i++) {
				isBlack[i] = false;
				nodeAlive[i] = true;
			}
			for (int i = 0; i < numberOfBlackNodes; i++) {
				isBlack[Integer.parseInt(br.readLine())] = true;
			}
			inDegree = new int[nn];
			outDegree = new int[nn];
			nodeName = new String[nn];
			for (int i = 0; i < nn; i++) {
				line = br.readLine();
				index = line.indexOf(' ');
				int secondIndex = line.indexOf(' ', index + 1);
				outDegree[i] = Integer.parseInt(line.substring(index + 1,
						secondIndex));
				index = line.indexOf(' ', secondIndex + 1);
				inDegree[i] = Integer.parseInt(line.substring(secondIndex + 1, index));
				nodeName[i] = line.substring(index + 1);
			}
			ne = numberOfEdges();
			adjacency = new boolean[nn][nn];
			line = br.readLine();
			while (line != null) {
				index = line.indexOf(' ');
				int s = Integer.parseInt(line.substring(0, index));
				int t = Integer.parseInt(line.substring(index + 1));
				adjacency[s][t] = true;
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	int reMap() {
		oldNew = new int[nn];
		for (int i = 0; i < nn; i++) {
			oldNew[i] = -1;
		}
		int numberOfAlive = 0;
		for (int i = 0; i < nn; i++) {
			if (nodeAlive[i]) {
				oldNew[i] = numberOfAlive;
				numberOfAlive = numberOfAlive + 1;
			}
		}
		return numberOfAlive;
	}

	void sort(int[] a) {
		for (int i = 1; i < a.length; i++) {
			int current = a[i];
			int j = i;
			while (j > 0 && a[j - 1] > current) {
				a[j] = a[j - 1];
				j = j - 1;
			}
			a[j] = current;
		}
	}

	void writeFile(String outPath, int numberOfAlive) {
		File outFile = new File(outPath);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			int nb = numberOfBlackNodes();
			bw.write(numberOfAlive + " " + nb + "\n");
			int b[] = new int[nb];
			int i = 0;
			for (int j = 0; j < nn; j++) {
				if (isBlack[j]) {
					b[i] = oldNew[j];
					i = i + 1;
				}
			}
			sort(b);
			for (i = 0; i < b.length; i++) {
				bw.write(b[i] + "\n");
			}
			for (i = 0; i < nn; i++) {
				if (nodeAlive[i]) {
					bw.write(oldNew[i] + " " + outDegree[i] + " " + inDegree[i]
							+ " " + nodeName[i] + "\n");
				}
			}
			for (i = 0; i < nn; i++) {
				for (int j = 0; j < nn; j++) {
					if (adjacency[i][j]) {
						bw.write(oldNew[i] + " " + oldNew[j] + "\n");
					}
				}
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

}
