import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.LinkedList;
import java.util.Stack;

public class EtienneEnumerator extends NELWithALNetwork {
	int blackDiameter = 1;
	boolean[] bfsed;
	int[][] compare;
	boolean[][] isInA;
	boolean[] isInR;
	boolean verbose = false;

	void addPath(int v, int u, int[] pred) {
		while (pred[u] != u) {
			isInA[pred[u]][u] = false;
			if (verbose)
				System.out.println(pred[u] + " " + u);
			isInR[u] = true;
			u = pred[u];
		}
	}

	void apac(int s) {
		LinkedList<Stack<Integer>> pathSet = new LinkedList<Stack<Integer>>();
		Stack<Stack<Integer>> S = new Stack<Stack<Integer>>();
		for (int v : adjacencyList[s]) {
			Stack<Integer> P = new Stack<Integer>();
			P.push(s);
			P.push(v);
			if (isBlack[v]) {
				pathSet.add(P);
			} else if (P.size() <= blackDiameter) {
				S.push(P);
			}
		}
		while (!S.isEmpty()) {
			Stack<Integer> P = S.pop();
			int u = P.peek();
			for (int v : adjacencyList[u]) {
				if (!P.contains(v)) {
					if (isBlack[v]) {
						P.push(v);
						pathSet.add(P);
						P.pop();
					} else {
						Stack<Integer> V = new Stack<Integer>();
						copy(P, V);
						V.push(v);
						S.push(V);
					}
				}
			}
		}
		if (verbose)
			System.out.println(s + ": " + pathSet.size());
	}

	boolean areDifferent(int[][] X, int[][] Y) {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (X[i][j] != Y[i][j]) {
					return true;
				}
			}
		}
		return false;
	}

	void bfs(int v) {
		int[] pred = new int[nn];
		boolean[] visited = new boolean[nn];
		for (int i = 0; i < nn; i++) {
			pred[i] = -1;
			visited[i] = false;
		}
		LinkedList<Integer> queue = new LinkedList<Integer>();
		bfsed[v] = true;
		if (verbose)
			System.out.println(v + " is bsfed");
		visited[v] = true;
		pred[v] = v;
		queue.offer(v);
		while (!queue.isEmpty()) {
			int x = queue.poll();
			for (int u = 0; u < nn; u++) {
				if (adjacencyList[x].contains(u) && isInA[x][u]
						&& compare[x][u] <= 0) {
					if (!visited[u]) {
						if (isInR[u] && compare[u][v] >= 0) {
							pred[u] = x;
							addPath(v, u, pred);
							setCompare();
							if (verbose)
								printCompare();
						} else if (!isInR[u]) {
							bfsed[u] = true;
							visited[u] = true;
							if (verbose)
								System.out.println(u + " is bsfed");
							pred[u] = x;
							queue.offer(u);
						}
					}
				}
			}
		}
	}

	void copy(int[][] X, int[][] Y) {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				Y[i][j] = X[i][j];
			}
		}
	}

	void copy(Stack<Integer> source, Stack<Integer> target) {
		Stack<Integer> tmp = new Stack<Integer>();
		while (!source.isEmpty()) {
			int x = source.pop();
			tmp.push(x);
		}
		while (!tmp.isEmpty()) {
			int x = tmp.pop();
			source.push(x);
			target.push(x);
		}
	}
	
	void computeBlackDiameter() {
		blackDiameter = 0;
		int[] dist = new int[nn];
		for (int v = 0; v < nn; v++) {
			int[] curPred = new int[nn];
			boolean[] reached = new boolean[nn];
			for (int i = 0; i < nn; i++) {
				reached[i] = false;
				dist[i] = Integer.MAX_VALUE;
				curPred[i] = -1;
			}
			LinkedList<Integer> queue = new LinkedList<Integer>();
			queue.offer(v);
			reached[v] = true;
			dist[v] = 0;
			while (!queue.isEmpty()) {
				int u = queue.poll();
				if (isBlack[u]) {
					if (blackDiameter < dist[u]) {
						blackDiameter = dist[u];
						int last = u;
						while (curPred[last] != -1) {
							System.out.print(last + " ");
							last = curPred[last];
						}
						System.out.println(last);
					}
				} else {
					for (int x : adjacencyList[u]) {
						if (!reached[x]) {
							queue.offer(x);
							reached[x] = true;
							dist[x] = dist[u] + 1;
							curPred[x] = u;
						}
					}
				}
			}
		}
	}

	String firstSAS() {
		isInR = new boolean[nn];
		bfsed = new boolean[nn];
		compare = new int[nn][nn];
		isInA = new boolean[nn][nn];
		for (int i = 0; i < nn; i++) {
			bfsed[i] = false;
			isInR[i] = false;
			if (isBlack[i]) {
				isInR[i] = true;
			}
			for (int j = 0; j < nn; j++) {
				isInA[i][j] = true;
				compare[i][j] = 0;
			}
			compare[i][i] = 1;
		}
		for (int iter = 0; iter < nn; iter++) {
			int v = minimum();
			if (v < 0) {
				break;
			}
			bfs(v);
		}
		return toString(isInA);
	}

	int minimum() {
		if (verbose) {
			System.out.print("isInR: ");
			for (int i = 0; i < nn; i++) {
				if (isInR[i]) {
					System.out.print(i + " ");
				}
			}
			System.out.println();
		}
		for (int i = 0; i < nn; i++) {
			if (isBlack[i] && !bfsed[i]) {
				boolean isMinimum = true;
				if (verbose)
					System.out.println("Check (total order) " + i);
				for (int j = 0; j < nn; j++) {
					if (j != i && !bfsed[j] && isBlack[j] && compare[j][i] < 0) {
						if (verbose)
							System.out.println(i
									+ " is not minimum because of " + j);
						isMinimum = false;
						break;
					}
				}
				if (isMinimum) {
					if (verbose)
						System.out.println(i);
					return i;
				}
			}
		}
		return -1;
	}

	int[][] multiply(int[][] X, int[][] Y) {
		int[][] R = new int[nn][nn];
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				R[i][j] = 0;
				for (int k = 0; k < nn; k++) {
					R[i][j] = R[i][j] + X[i][k] * Y[k][j];
				}
				if (R[i][j] > 0) {
					R[i][j] = 1;
				}
			}
		}
		return R;
	}

	void print(int[][] X) {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				System.out.print(X[i][j]);
			}
			System.out.println();
		}
	}

	void printCompare() {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (compare[i][j] > 0) {
					System.out.print("+1 ");
				} else if (compare[i][j] < 0) {
					System.out.print("-1 ");
				} else {
					System.out.print(" 0 ");
				}
			}
			System.out.println();
		}
	}

	void run(String networkName) {
		readFile(networkName + ".nel");
		computeBlackDiameter();
		System.out.println(blackDiameter);
		System.exit(1);
		File outFile = new File(networkName + ".esas");
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(outFile));
			String sas = firstSAS();
			bw.write(sas + "\n");
			// enumerate(fas, bw);
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (int s = 0; s < nn; s++) {
			if (!isBlack[s]) {
				apac(s);
			}
		}
	}

	void setCompare() {
		int[][] A = new int[nn][nn];
		int[][] AStar = new int[nn][nn];
		int[][] B = new int[nn][nn];
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (adjacencyList[i].contains(j) && !isInA[i][j]) {
					A[i][j] = 1;
					AStar[i][j] = 1;
				} else {
					A[i][j] = 0;
					AStar[i][j] = 0;
				}
			}
			A[i][i] = 1;
			AStar[i][i] = 1;
		}
		do {
			copy(AStar, B);
			AStar = multiply(B, B);
		} while (areDifferent(AStar, B));
		if (verbose) {
			print(AStar);
		}
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				compare[i][j] = 0;
			}
		}
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				if (AStar[i][j] == 1) {
					compare[i][j] = -1;
					compare[j][i] = 1;
				}
			}
		}
		if (verbose)
			printCompare();
	}

	String toString(int[] edge) {
		return "[" + edge[0] + "," + edge[1] + "]";
	}

	String toString(boolean[][] a) {
		String rst = "";
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				if (adjacencyList[u].contains(v) && a[u][v]) {
					String s = toString(new int[] { u, v });
					rst = rst + s;
				}
			}
		}
		return rst;
	}
}
