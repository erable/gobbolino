import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;

public class Tmp {
	private HashMap<String, Integer> blackMap;
	private HashMap<String, Integer> map;
	private String nodeName[];
	private boolean[][] adjacency;
	private int[] inDegree;
	private int[] outDegree;
	private boolean[] alive;
	private int[] oldNew;
	private int numberOfSpecies;
	private int numberOfAlive;

	private boolean bb() {
		boolean answer = false;
		for (int i = 0; i < numberOfSpecies; i++) {
			if (inDegree[i] == 1) {
				int p;
				for (p = 0; p < numberOfSpecies; p++) {
					if (adjacency[p][i]) {
						adjacency[p][i] = false;
						inDegree[i] = inDegree[i] - 1;
						outDegree[p] = outDegree[p] - 1;
						break;
					}
				}
				for (int s = 0; s < numberOfSpecies; s++) {
					if (adjacency[i][s]) {
						adjacency[i][s] = false;
						outDegree[i] = outDegree[i] - 1;
						inDegree[s] = inDegree[s] - 1;
						if (!adjacency[p][s]) {
							adjacency[p][s] = true;
							outDegree[p] = outDegree[p] + 1;
							inDegree[s] = inDegree[s] + 1;
						}
					}
				}
				alive[i] = false;
				if (!isWhite(i)) {
					blackMap.remove(nodeName[i]);
					if (!blackMap.containsKey(nodeName[p])) {
						blackMap.put(nodeName[p], 0);
					}
				}
				answer = true;
			}
		}
		return answer;
	}

	private boolean bfs(int s, boolean forward) {
		boolean[] reached = new boolean[numberOfSpecies];
		for (int i = 0; i < numberOfSpecies; i++) {
			reached[i] = false;
		}
		LinkedList<Integer> queue = new LinkedList<Integer>();
		queue.offer(s);
		boolean blackFound = false;
		while (!blackFound && !queue.isEmpty()) {
			int u = queue.poll();
			if (!reached[u]) {
				reached[u] = true;
				if (blackMap.containsKey(nodeName[u])) {
					blackFound = true;
				} else {
					for (int x = 0; x < numberOfSpecies; x++) {
						if (forward) {
							if (adjacency[u][x]) {
								queue.offer(x);
							}
						} else {
							if (adjacency[x][u]) {
								queue.offer(x);
							}
						}
					}
				}
			}
		}
		return blackFound;
	}

	private boolean deleteBackwardDeadlocks() {
		boolean answer = false;
		for (int i = 0; i < numberOfSpecies; i++) {
			if (alive[i] && isWhite(i)) {
				boolean blackFound = bfs(i, false);
				if (!blackFound && isWhite(i)) {
					deleteWhiteNode(i);
					answer = true;
				} else if (!blackFound && !isWhite(i)) {
					deleteIncomingEdges(i);
					answer = true;
				}
			}
		}
		return answer;
	}

	private boolean deleteForwardDeadlocks() {
		boolean answer = false;
		for (int i = 0; i < numberOfSpecies; i++) {
			if (alive[i]) {
				boolean blackFound = bfs(i, true);
				if (!blackFound && isWhite(i)) {
					deleteWhiteNode(i);
					answer = true;
				} else if (!blackFound && !isWhite(i)) {
					deleteOutgoingEdges(i);
					answer = true;
				}
			}
		}
		return answer;
	}

	private void deleteIncomingEdges(int i) {
		for (int j = 0; j < numberOfSpecies; j++) {
			if (adjacency[j][i]) {
				adjacency[j][i] = false;
				outDegree[j] = outDegree[j] - 1;
				inDegree[i] = inDegree[i] - 1;
			}
		}
	}

	private void deleteOutgoingEdges(int i) {
		for (int j = 0; j < numberOfSpecies; j++) {
			if (adjacency[i][j]) {
				adjacency[i][j] = false;
				outDegree[i] = outDegree[i] - 1;
				inDegree[j] = inDegree[j] - 1;
			}
		}
	}

	private boolean deleteSelfLoops() {
		boolean answer = false;
		for (int i = 0; i < numberOfSpecies; i++) {
			if (adjacency[i][i]) {
				adjacency[i][i] = false;
				inDegree[i] = inDegree[i] - 1;
				outDegree[i] = outDegree[i] - 1;
				answer = true;
			}
		}
		return answer;
	}

	private boolean deleteStars() {
		boolean answer = false;
		boolean changed = true;
		while (changed) {
			changed = false;
			for (int i = 0; i < numberOfSpecies; i++) {
				if (alive[i] && isWhite(i) && inDegree[i] == outDegree[i]) {
					int d = inDegree[i];
					int[] pred = new int[d];
					int h = 0;
					for (int p = 0; p < numberOfSpecies; p++) {
						if (adjacency[p][i]) {
							pred[h] = p;
							h = h + 1;
						}
					}
					boolean canBeDeleted = true;
					for (int p = 0; canBeDeleted && p < d; p++) {
						if (!adjacency[i][pred[p]]) {
							canBeDeleted = false;
						}
					}
					if (canBeDeleted) {
						for (int p = 0; p < d; p++) {
							adjacency[pred[p]][i] = false;
							adjacency[i][pred[p]] = false;
							inDegree[i] = inDegree[i] - 1;
							outDegree[pred[p]] = outDegree[pred[p]] - 1;
							outDegree[i] = outDegree[i] - 1;
							inDegree[pred[p]] = inDegree[pred[p]] - 1;
							alive[i] = false;
						}
						for (int p1 = 0; p1 < d - 1; p1++) {
							for (int p2 = p1 + 1; p2 < d; p2++) {
								if (!adjacency[pred[p1]][pred[p2]]) {
									adjacency[pred[p1]][pred[p2]] = true;
									outDegree[pred[p1]] = outDegree[pred[p1]] + 1;
									inDegree[pred[p2]] = inDegree[pred[p2]] + 1;
								}
								if (!adjacency[pred[p2]][pred[p1]]) {
									adjacency[pred[p2]][pred[p1]] = true;
									outDegree[pred[p2]] = outDegree[pred[p2]] + 1;
									inDegree[pred[p1]] = inDegree[pred[p1]] + 1;
								}
							}
						}
						changed = true;
						answer = true;
					}
				}
			}
		}
		return answer;
	}

	private void deleteWhiteNode(int i) {
		for (int j = 0; j < numberOfSpecies; j++) {
			if (adjacency[i][j]) {
				adjacency[i][j] = false;
				inDegree[j] = inDegree[j] - 1;
			}
			if (adjacency[j][i]) {
				adjacency[j][i] = false;
				outDegree[j] = outDegree[j] - 1;
			}
		}
		inDegree[i] = 0;
		outDegree[i] = 0;
		alive[i] = false;
	}

	private boolean fb() {
		boolean answer = false;
		for (int i = 0; i < numberOfSpecies; i++) {
			if (outDegree[i] == 1) {
				int s;
				for (s = 0; s < numberOfSpecies; s++) {
					if (adjacency[i][s]) {
						adjacency[i][s] = false;
						inDegree[s] = inDegree[s] - 1;
						outDegree[i] = outDegree[i] - 1;
						break;
					}
				}
				for (int p = 0; p < numberOfSpecies; p++) {
					if (adjacency[p][i]) {
						adjacency[p][i] = false;
						outDegree[p] = outDegree[p] - 1;
						inDegree[i] = inDegree[i] - 1;
						if (!adjacency[p][s]) {
							adjacency[p][s] = true;
							outDegree[p] = outDegree[p] + 1;
							inDegree[s] = inDegree[s] + 1;
						}
					}
				}
				alive[i] = false;
				if (!isWhite(i)) {
					blackMap.remove(nodeName[i]);
					if (!blackMap.containsKey(nodeName[s])) {
						blackMap.put(nodeName[s], 0);
					}
				}
				answer = true;
			}
		}
		return answer;
	}

	private void initAlives() {
		alive = new boolean[numberOfSpecies];
		for (int j = 0; j < numberOfSpecies; j++) {
			alive[j] = true;
		}
	}

	private void initDegrees() {
		for (int i = 0; i < numberOfSpecies; i++) {
			outDegree[i] = 0;
			inDegree[i] = 0;
			for (int j = 0; j < numberOfSpecies; j++) {
				if (adjacency[i][j]) {
					outDegree[i] = outDegree[i] + 1;
				}
				if (adjacency[j][i]) {
					inDegree[i] = inDegree[i] + 1;
				}
			}
		}
	}

	private boolean isWhite(int i) {
		return !blackMap.containsKey(nodeName[i]);
	}

	private int numberOfAlives() {
		int result = 0;
		for (int i = 0; i < numberOfSpecies; i++) {
			if (alive[i]) {
				result = result + 1;
			}
		}
		return result;
	}

	private int numberOfSelfLoops() {
		int result = 0;
		for (int i = 0; i < numberOfSpecies; i++) {
			if (alive[i] && adjacency[i][i]) {
				result = result + 1;
			}
		}
		return result;
	}

	private int numberOfSources() {
		int result = 0;
		for (int i = 0; i < numberOfSpecies; i++) {
			if (alive[i] && inDegree[i] == 0 && isWhite(i)) {
				result = result + 1;
			}
		}
		return result;
	}

	private int numberOfTargets() {
		int result = 0;
		for (int i = 0; i < numberOfSpecies; i++) {
			if (alive[i] && outDegree[i] == 0 && isWhite(i)) {
				result = result + 1;
			}
		}
		return result;
	}

	private void printBlackNodes(boolean verbose) {
		if (verbose) {
			int b[] = new int[blackMap.size()];
			int i = 0;
			for (String name : blackMap.keySet()) {
				b[i] = oldNew[map.get(name)];
				i = i + 1;
			}
			sort(b);
			System.out.println("=== List of black nodes ===");
			for (i = 0; i < b.length - 1; i++) {
				System.out.print(b[i] + ",");
			}
			System.out.print(b[b.length - 1] + "\n");
		}
	}

	private void printStatistics(boolean verbose, String method) {
		if (verbose) {
			System.out.println("=== Graph changed by " + method + " ===");
			System.out.println("Number of alive nodes: " + numberOfAlives());
			System.out.println("Number of self-loops: " + numberOfSelfLoops());
			System.out.println("Number of sources: " + numberOfSources());
			System.out.println("Number of targets: " + numberOfTargets());
		}
		for (int i = 0; i < numberOfSpecies; i++) {
			if (!alive[i] && (inDegree[i] != 0 || outDegree[i] != 0)) {
				System.out.println("Warning: dead node " + i
						+ " has degree greater than 0");
				System.exit(-1);
			}
			int in = 0;
			int out = 0;
			for (int j = 0; j < numberOfSpecies; j++) {
				if (adjacency[i][j])
					out++;
				if (adjacency[j][i])
					in++;
			}
			if (inDegree[i] != in) {
				System.out
						.println("Warning: node " + i + " has wrong indegree");
				System.exit(-1);
			}
			if (outDegree[i] != out) {
				System.out.println("Warning: node " + i
						+ " has wrong outdegree");
				System.exit(-1);
			}
		}
	}

	private void readFile(String inPath, String blackPath) {
		File inFile = new File(inPath);
		File blackFile = new File(blackPath);
		try {
			BufferedReader bbr = new BufferedReader(new FileReader(blackFile));
			String line = bbr.readLine();
			blackMap = new HashMap<String, Integer>();
			while (line != null) {
				blackMap.put(line, 0);
				line = bbr.readLine();
			}
			bbr.close();
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			line = br.readLine();
			while (!line.equals("<listOfSpecies>")) {
				line = br.readLine();
			}
			line = br.readLine();
			numberOfSpecies = 0;
			while (!line.equals("</listOfSpecies>")) {
				numberOfSpecies = numberOfSpecies + 1;
				line = br.readLine();
			}
			br.close();
			adjacency = new boolean[numberOfSpecies][numberOfSpecies];
			inDegree = new int[numberOfSpecies];
			outDegree = new int[numberOfSpecies];
			map = new HashMap<String, Integer>();
			nodeName = new String[numberOfSpecies];
			br = new BufferedReader(new FileReader(inFile));
			line = br.readLine();
			while (!line.equals("<listOfSpecies>")) {
				line = br.readLine();
			}
			line = br.readLine();
			int index = 0;
			while (!line.equals("</listOfSpecies>")) {
				int firstIndex = line.indexOf('=');
				int secondIndex = line.indexOf(' ', firstIndex);
				String species = line
						.substring(firstIndex + 2, secondIndex - 1);
				map.put(species, index);
				nodeName[index] = species;
				line = br.readLine();
				index = index + 1;
			}
			line = br.readLine();
			line = br.readLine();
			while (line.indexOf("/listOfReactions") == -1) {
				index = line.indexOf("reversible=");
				index = index + "reversible=".length() + 1;
				boolean reversible = false;
				if (line.charAt(index) == 't') {
					reversible = true;
				}
				while (line.indexOf("listOfReactants") == -1) {
					line = br.readLine();
				}
				line = br.readLine();
				Vector<String> reactants = new Vector<String>();
				while (line.indexOf("/listOfReactants") == -1) {
					int firstIndex = line.indexOf('=');
					int secondIndex = line.indexOf(' ', firstIndex);
					String species = line.substring(firstIndex + 2,
							secondIndex - 1);
					reactants.add(species);
					line = br.readLine();
				}
				line = br.readLine();
				line = br.readLine();
				Vector<String> products = new Vector<String>();
				while (line.indexOf("/listOfProducts") == -1) {
					int firstIndex = line.indexOf('=');
					int secondIndex = line.indexOf(' ', firstIndex);
					String species = line.substring(firstIndex + 2,
							secondIndex - 1);
					products.add(species);
					line = br.readLine();
				}
				for (String reactant : reactants) {
					for (String product : products) {
						adjacency[map.get(reactant)][map.get(product)] = true;
						if (reversible) {
							adjacency[map.get(product)][map.get(reactant)] = true;
						}
					}
				}
				line = br.readLine();
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private void reMap() {
		oldNew = new int[numberOfSpecies];
		for (int j = 0; j < numberOfSpecies; j++) {
			oldNew[j] = -1;
		}
		numberOfAlive = 0;
		for (int j = 0; j < numberOfSpecies; j++) {
			if (alive[j]) {
				oldNew[j] = numberOfAlive;
				numberOfAlive = numberOfAlive + 1;
			}
		}
	}

	protected void run(String nn, boolean verbose) {
		readFile(nn + ".xml", nn + ".bn");
		initDegrees();
		initAlives();
		printStatistics(verbose, "readFile");
		boolean changed = true;

		while (changed) {
			changed = false;
			if (deleteSelfLoops()) {
				printStatistics(verbose, "deleteSelfLoops");
				changed = true;
			}
			if (deleteForwardDeadlocks()) {
				printStatistics(verbose, "deleteForwardDeadlocks");
				changed = true;
			}
			if (deleteBackwardDeadlocks()) {
				printStatistics(verbose, "deleteBackwardDeadlocks");
				changed = true;
			}
			if (fb()) {
				printStatistics(verbose, "fb");
				changed = true;
			}
			if (deleteSelfLoops()) {
				printStatistics(verbose, "deleteSelfLoops");
				changed = true;
			}
			if (bb()) {
				printStatistics(verbose, "bb");
				changed = true;
			}
			if (deleteSelfLoops()) {
				printStatistics(verbose, "deleteSelfLoops");
				changed = true;
			}
			if (deleteStars()) {
				printStatistics(verbose, "deleteTwoCycles");
				changed = true;
			}
		}
		reMap();
		printBlackNodes(verbose);
		writeFile(nn + ".nel");
	}

	private void sort(int[] a) {
		for (int i = 1; i < a.length; i++) {
			int current = a[i];
			int j = i;
			while (j > 0 && a[j - 1] > current) {
				a[j] = a[j - 1];
				j = j - 1;
			}
			a[j] = current;
		}
	}

	private void writeFile(String outPath) {
		File outFile = new File(outPath);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			bw.write(numberOfAlive + " " + blackMap.size() + "\n");
			int b[] = new int[blackMap.size()];
			int i = 0;
			for (String name : blackMap.keySet()) {
				b[i] = oldNew[map.get(name)];
				i = i + 1;
			}
			sort(b);
			for (i = 0; i < b.length; i++) {
				bw.write(b[i] + "\n");
			}
			for (i = 0; i < numberOfSpecies; i++) {
				if (alive[i]) {
					bw.write(oldNew[i] + " " + outDegree[i] + " " + inDegree[i]
							+ "\n");
				}
			}
			for (i = 0; i < numberOfSpecies; i++) {
				for (int j = 0; j < numberOfSpecies; j++) {
					if (adjacency[i][j]) {
						bw.write(oldNew[i] + " " + oldNew[j] + "\n");
					}
				}
			}
			bw.close();
		} catch (Exception e) {
			System.out.println("ARG!");
			System.exit(-1);
		}
	}
}
