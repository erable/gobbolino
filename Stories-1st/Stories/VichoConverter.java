public class VichoConverter extends NELNetwork {
	void run(String fn) {
		readFile(fn + ".nel");
		int nb = numberOfBlackNodes();
		int[] map = new int[nn];
		int b = 0;
		boolean[] newIsBlack = new boolean[nn + nb];
		String[] newNodeName = new String[nn+nb];
		for (int u = 0; u < nn; u++) {
			newNodeName[u] = nodeName[u];
			newIsBlack[u] = isBlack[u];
			if (isBlack[u]) {
				map[u] = nn + b;
				newIsBlack[map[u]] = true;
				newNodeName[map[u]] = nodeName[u];
				b = b + 1;
			}
		}
		boolean[][] newAdj = new boolean[nn + nb][nn + nb];
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				if (adjacency[u][v]) {
					if (isBlack[v]) {
						newAdj[u][v] = false;
						newAdj[u][map[v]] = true;
					} else {
						newAdj[u][v] = adjacency[u][v];
					}
				}
			}
		}
		adjacency = newAdj;
		isBlack = newIsBlack;
		nn = nn + nb;
		computeDegrees();
		oldNew = new int[nn];
		nodeAlive = new boolean[nn];
		for (int u = 0; u < nn; u++) {
			oldNew[u] = u;
			nodeAlive[u] = true;
		}
		nodeName = newNodeName;
		writeFile(fn + "Vicho.nel", nn);
	}

}
