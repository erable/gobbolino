import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Vector;

public class XML2NEL {
	HashMap<String, Integer> blackMap;
	HashMap<String, Integer> map;
	String nodeName[];
	boolean[][] adjacency;
	int[] inDegree;
	int[] outDegree;
	int numberOfSpecies;
	boolean oneWay;

	void deleteSelfLoops() {
		for (int i = 0; i < numberOfSpecies; i++) {
			if (adjacency[i][i]) {
				adjacency[i][i] = false;
				inDegree[i] = inDegree[i] - 1;
				outDegree[i] = outDegree[i] - 1;
			}
		}
	}

	void initDegrees() {
		for (int i = 0; i < numberOfSpecies; i++) {
			outDegree[i] = 0;
			inDegree[i] = 0;
			for (int j = 0; j < numberOfSpecies; j++) {
				if (adjacency[i][j]) {
					outDegree[i] = outDegree[i] + 1;
				}
				if (adjacency[j][i]) {
					inDegree[i] = inDegree[i] + 1;
				}
			}
		}
	}

	void readFile(String inPath, String blackPath) {
		File inFile = new File(inPath);
		File blackFile = new File(blackPath);
		try {
			BufferedReader bbr = new BufferedReader(new FileReader(blackFile));
			String line = bbr.readLine();
			blackMap = new HashMap<String, Integer>();
			while (line != null) {
				blackMap.put(line, 0);
				line = bbr.readLine();
			}
			bbr.close();
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			line = br.readLine();
			while (!line.equals("<listOfSpecies>")) {
				line = br.readLine();
			}
			line = br.readLine();
			numberOfSpecies = 0;
			while (!line.equals("</listOfSpecies>")) {
				numberOfSpecies = numberOfSpecies + 1;
				line = br.readLine();
			}
			br.close();
			adjacency = new boolean[numberOfSpecies][numberOfSpecies];
			inDegree = new int[numberOfSpecies];
			outDegree = new int[numberOfSpecies];
			map = new HashMap<String, Integer>();
			nodeName = new String[numberOfSpecies];
			br = new BufferedReader(new FileReader(inFile));
			line = br.readLine();
			while (!line.equals("<listOfSpecies>")) {
				line = br.readLine();
			}
			line = br.readLine();
			int index = 0;
			while (!line.equals("</listOfSpecies>")) {
				int firstIndex = line.indexOf('=');
				int secondIndex = line.indexOf(' ', firstIndex);
				String species = line
						.substring(firstIndex + 2, secondIndex - 1);
				map.put(species, index);
				nodeName[index] = species;
				line = br.readLine();
				index = index + 1;
			}
			System.out.println(map.size());
			line = br.readLine();
			line = br.readLine();
			while (line.indexOf("/listOfReactions") == -1) {
				index = line.indexOf("reversible=");
				index = index + "reversible=".length() + 1;
				boolean reversible = false;
				if (line.charAt(index) == 't') {
					reversible = true;
				}
				while (line.indexOf("listOfReactants") == -1) {
					line = br.readLine();
				}
				line = br.readLine();
				Vector<String> reactants = new Vector<String>();
				while (line.indexOf("/listOfReactants") == -1) {
					int firstIndex = line.indexOf('=');
					int secondIndex = line.indexOf(' ', firstIndex);
					String species = line.substring(firstIndex + 2,
							secondIndex - 1);
					reactants.add(species);
					line = br.readLine();
				}
				line = br.readLine();
				line = br.readLine();
				Vector<String> products = new Vector<String>();
				while (line.indexOf("/listOfProducts") == -1) {
					int firstIndex = line.indexOf('=');
					int secondIndex = line.indexOf(' ', firstIndex);
					String species = line.substring(firstIndex + 2,
							secondIndex - 1);
					products.add(species);
					line = br.readLine();
				}
				for (String reactant : reactants) {
					for (String product : products) {
						adjacency[map.get(reactant)][map.get(product)] = true;
						if (reversible && !oneWay) {
							adjacency[map.get(product)][map.get(reactant)] = true;
						}
					}
				}
				line = br.readLine();
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	void run(String nn, boolean verbose, boolean oneWay) {
		this.oneWay = oneWay;
		readFile(nn + ".xml", nn + ".bn");
		initDegrees();
		deleteSelfLoops();
		if (oneWay) {
			writeFile(nn + "OW.nel");
		} else {
			writeFile(nn + ".nel");
		}
	}

	void sort(int[] a) {
		for (int i = 1; i < a.length; i++) {
			int current = a[i];
			int j = i;
			while (j > 0 && a[j - 1] > current) {
				a[j] = a[j - 1];
				j = j - 1;
			}
			a[j] = current;
		}
	}

	void writeFile(String outPath) {
		File outFile = new File(outPath);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			bw.write(numberOfSpecies + " " + blackMap.size() + "\n");
			int b[] = new int[blackMap.size()];
			int i = 0;
			for (String name : blackMap.keySet()) {
				b[i] = map.get(name);
				i = i + 1;
			}
			sort(b);
			for (i = 0; i < b.length; i++) {
				bw.write(b[i] + "\n");
			}
			for (i = 0; i < numberOfSpecies; i++) {
				bw.write(i + " " + outDegree[i] + " " + inDegree[i] + " "
						+ nodeName[i] + "\n");
			}
			for (i = 0; i < numberOfSpecies; i++) {
				for (int j = 0; j < numberOfSpecies; j++) {
					if (adjacency[i][j]) {
						bw.write(i + " " + j + "\n");
					}
				}
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
