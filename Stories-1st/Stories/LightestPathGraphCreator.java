
public class LightestPathGraphCreator extends NELNetwork {
	int[][] edgeMatrix, distanceMatrix;
	boolean[][] edgeAlive;

	void initMatrices() {
		edgeMatrix = new int[nn][nn];
		edgeAlive = new boolean[nn][nn];
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				edgeAlive[u][v] = false;
				if (!adjacency[u][v]) {
					edgeMatrix[u][v] = Integer.MAX_VALUE;
				} else {
					edgeMatrix[u][v] = outDegree[v];
				}
			}
		}
	}

	void computeDistanceMatrix() {
		distanceMatrix = new int[nn][nn];
		for (int u = 0; u < nn; u++) {
			distanceMatrix[u] = bellmanFord(u, edgeMatrix);
		}
	}

	int[] bellmanFord(int s, int[][] edge) {
		int[] dist = new int[nn];
		for (int u = 0; u < nn; u = u + 1) {
			dist[u] = Integer.MAX_VALUE;
		}
		dist[s] = outDegree[s];
		for (int i = 0; i < nn; i = i + 1) {
			for (int v = 0; v < nn; v = v + 1) {
				for (int u = 0; u < nn; u = u + 1) {
					if (adjacency[v][u] && dist[v] < Integer.MAX_VALUE
							&& edge[v][u] < Integer.MAX_VALUE) {
						if (dist[u] > dist[v] + edge[v][u]) {
							dist[u] = dist[v] + edge[v][u];
						}
					}
				}
			}
		}
		return dist;
	}

	void cruzAlgorithm() {
		for (int s = 0; s < nn; s++) {
			if (isBlack[s]) {
				for (int t = 0; t < nn; t++) {
					if (t != s && isBlack[t]) {
						for (int u = 0; u < nn; u++) {
							for (int v = 0; v < nn; v++) {
								if (adjacency[u][v]
										&& distanceMatrix[s][u] < Integer.MAX_VALUE
										&& distanceMatrix[v][t] < Integer.MAX_VALUE) {
									if (distanceMatrix[s][u]
											+ distanceMatrix[v][t] == distanceMatrix[s][t]) {
										edgeAlive[u][v] = true;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	void run(String fn) {
		readFile(fn + ".nel");
		initMatrices();
		computeDistanceMatrix();
		cruzAlgorithm();
		updateAdjacency();
		computeAliveNodes();
		computeDegrees();
		int numberOfAlive = reMap();
		writeFile(fn + "LP.nel", numberOfAlive);
	}

	private void updateAdjacency() {
		for (int i = 0; i < nn; i++) {
			for (int j = 0; j < nn; j++) {
				adjacency[i][j] = edgeAlive[i][j];
			}
		}
	}
}
