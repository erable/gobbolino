import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

public class FASEnumerator extends NELWithALNetwork {
	HashMap<String, Integer> copy(HashMap<String, Integer> fas) {
		HashMap<String, Integer> rst = new HashMap<String, Integer>();
		for (String s : fas.keySet()) {
			rst.put(s, 0);
		}
		return rst;
	}

	int[] edge(String s) {
		int i1 = s.indexOf('[');
		int i2 = s.indexOf(',');
		int i3 = s.indexOf(']');
		int[] rst = new int[2];
		rst[0] = Integer.parseInt(s.substring(i1 + 1, i2));
		rst[1] = Integer.parseInt(s.substring(i2 + 1, i3));
		return rst;
	}

	void enumerate(HashMap<String, Integer> fas, BufferedWriter bw) throws IOException {
		int ns = 0, nf = 0;
		HashMap<String, Integer> F = copy(fas);
		LinkedList<HashMap<String, Integer>> Q = new LinkedList<HashMap<String, Integer>>();
		HashMap<String, Integer> D = new HashMap<String, Integer>();
		Q.add(F);
		D.put(toString(F), 0);
		while (!Q.isEmpty()) {
			F = Q.removeFirst();
			if (isSAS(F)) {
				ns++;
				nf++;
				bw.write("SAS " + ns + " (over " + nf + "): " + toString(F) + "\n");
			} else {
				nf++;
				System.out.println("FAS " + nf + ": " + toString(F));
			}
			for (String s : F.keySet()) {
				int[] edge = edge(s);
				HashMap<String, Integer> FPrime = copy(F);
				mu(FPrime, edge[0], edge[1]);
				minimize(FPrime);
				if (!D.containsKey(toString(FPrime))) {
					Q.add(FPrime);
					D.put(toString(FPrime), 0);
				}
			}
		}
		System.out.println("Number of found SAS: " + ns);
		System.out.println("Number of found FAS: " + nf);
	}

	HashMap<String, Integer> firstFAS() {
		HashMap<String, Integer> inFAS = new HashMap<String, Integer>();
		HashMap<String, Integer> tmpFAS = new HashMap<String, Integer>();
		int[] currentInDegree = new int[nn];
		for (int u = 0; u < nn; u++) {
			currentInDegree[u] = 0;
			for (Integer v : adjacencyList[u]) {
				inFAS.put(toString(new int[] { u, v }), 0);
				tmpFAS.put(toString(new int[] { u, v }), 0);
			}
		}

		Set<String> edgeSet = tmpFAS.keySet();
		for (String e : edgeSet) {
			int[] edge = edge(e);
			inFAS.remove(e);
			currentInDegree[edge[1]]++;
			if (isCyclic(inFAS, currentInDegree)) {
				inFAS.put(e, 0);
				currentInDegree[edge[1]]--;
			}
		}
		return inFAS;
	}

	boolean isCyclic(HashMap<String, Integer> inFAS, int[] inDegree) {
		int[] inCounter = new int[nn];
		int[] number = new int[nn];
		LinkedList<Integer> queue = new LinkedList<Integer>();
		for (int u = 0; u < nn; u++) {
			number[u] = -1;
			inCounter[u] = inDegree[u];
			if (inCounter[u] == 0) {
				queue.add(u);
			}
		}
		int i = 0;
		while (!queue.isEmpty()) {
			int u = queue.removeFirst();
			number[u] = i;
			i = i + 1;
			for (Integer w : adjacencyList[u]) {
				if (!inFAS.containsKey(toString(new int[] { u, w }))) {
					inCounter[w] = inCounter[w] - 1;
					if (inCounter[w] == 0) {
						queue.add(w);
					}
				}
			}
		}
		for (int u = 0; u < nn; u++) {
			if (number[u] < 0) {
				return true;
			}
		}
		return false;
	}

	boolean isSAS(HashMap<String, Integer> fas) {
		for (int u = 0; u < nn; u++) {
			if (!isBlack[u]) {
				boolean in = false;
				boolean out = false;
				for (Integer v : adjacencyList[u]) {
					if (!fas.containsKey(toString(new int[] { u, v }))) {
						out = true;
					}
				}
				for (Integer v : incidencyList[u]) {
					if (!fas.containsKey(toString(new int[] { v, u }))) {
						in = true;
					}
				}
				if ((!in && out) || (in && !out)) {
					return false;
				}
			}
		}
		return true;
	}

	void minimize(HashMap<String, Integer> fas) {
		int[] currentInDegree = new int[nn];
		for (int u = 0; u < nn; u++) {
			currentInDegree[u] = 0;
			for (Integer v : incidencyList[u]) {
				if (!fas.containsKey(toString(new int[] { v, u }))) {
					currentInDegree[u]++;
				}
			}
		}
		for (int u = 0; u < nn; u++) {
			for (Integer v : adjacencyList[u]) {
				int[] edge = new int[] { u, v };
				String s = toString(edge);
				if (fas.containsKey(s)) {
					fas.remove(s);
					currentInDegree[v]++;
					if (isCyclic(fas, currentInDegree)) {
						fas.put(s, 0);
						currentInDegree[v]--;
					}
				}
			}
		}
	}

	void mu(HashMap<String, Integer> fas, int u, int v) {
		fas.remove(toString(new int[] { u, v }));
		for (Integer x : adjacencyList[v]) {
			fas.put(toString(new int[] { v, x }), 0);
		}
	}

	void run(String networkName, boolean verbose) {
		readFile(networkName + ".nel");
		File outFile = new File(networkName + ".sas");
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(outFile));
			HashMap<String, Integer> fas = firstFAS();
			enumerate(fas, bw);
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	int size(HashMap<String, Integer> fas) {
		return fas.size();
	}

	String toShortString(HashMap<String, Integer> fas) {
		String rst;
		boolean firstFound = false;
		int[] first = new int[2];
		int[] last = new int[2];
		int size = 0;
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				String s = toString(new int[] { u, v });
				if (fas.containsKey(s)) {
					size++;
					if (!firstFound) {
						firstFound = true;
						first[0] = u;
						first[1] = v;
					}
					last[0] = u;
					last[1] = v;
				}
			}
		}
		rst = "size " + size + " (" + "[" + first[0] + "," + first[1] + "]--["
				+ last[0] + "," + last[1] + "])";
		return rst;
	}

	String toString(HashMap<String, Integer> fas) {
		String rst = "";
		for (int u = 0; u < nn; u++) {
			for (int v = 0; v < nn; v++) {
				String s = toString(new int[] { u, v });
				if (fas.containsKey(s)) {
					rst = rst + s;
				}
			}
		}
		return rst;
	}

	String toString(int[] edge) {
		return "[" + edge[0] + "," + edge[1] + "]";
	}
}
