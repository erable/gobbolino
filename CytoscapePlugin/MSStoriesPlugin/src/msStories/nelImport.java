package msStories;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import cytoscape.CyEdge;
import cytoscape.CyNetwork;
import cytoscape.CyNode;
import cytoscape.Cytoscape;
import cytoscape.layout.CyLayouts;
import cytoscape.util.FileUtil;
import cytoscape.view.CyNetworkView;

public class nelImport {
	
	//protected static double[] storiesScore;
	
	protected static CyNetwork originalNetwork = null;
	protected static CyNetwork compressedNetwork = null;
	
	public static void importNelNetwork(String inFile, CyNetwork network, boolean originalNet)
	{
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			int nn = new Integer(line.split(" ")[0]);
			int numberOfBlackNodes = new Integer(line.split(" ")[1]);
			
			//Black nodes attributes
			Vector<String> bnNodes=new Vector<String>();
			for (int i=1;i<numberOfBlackNodes+1;i++)
			{
				line = br.readLine();
				bnNodes.add(line);
			}
			/**
			 * Create Nodes
			 */
			for (int i=numberOfBlackNodes+1;i<numberOfBlackNodes+nn+1;i++)
			{
				line = br.readLine();
				CyNode node=Cytoscape.getCyNode(line.split(" ")[0],true);				
				network.addNode(node);
				Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(),"SubGraph",line.split(" ")[3]);
				if(originalNet)
					Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(),"Name",line.split(" ")[3].split(";")[0]);
				if(bnNodes.contains(node.getIdentifier()))
						Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(),"bn","true");
				else
					Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(),"bn","false");
			}
			line = br.readLine();
			double maxValues[] = null;
			List<String> modifiedEdges = new ArrayList<String>();
			
			while(line!=null)
			{
				String edgeAttribs[] = line.split(" ");
				CyNode source=Cytoscape.getCyNode(edgeAttribs[0],false);
				CyNode target=Cytoscape.getCyNode(edgeAttribs[1],false);
				CyEdge edge=Cytoscape.getCyEdge(source.getIdentifier(),"name",target.getIdentifier(),"pp");
				Cytoscape.getEdgeAttributes().setAttribute(edge.getIdentifier(),"SubGraph",edgeAttribs[2]);

				if( maxValues == null ) {
					maxValues = new double[edgeAttribs.length-3];	// the 3 fix parameters are name, subgraph and bn
					for(int j = 0; j < maxValues.length; j++) {
						maxValues[j] = 0.0;
					}
				}
				
				// deals with the frequency (if present)
				if( edgeAttribs.length == 4 ) { // if there is a forth parameter, it is the frequency of the edge over all stories (anthology)
					double value = Double.valueOf(edgeAttribs[3]);
					Cytoscape.getEdgeAttributes().setAttribute(edge.getIdentifier(),"QtFrequency", value);
					modifiedEdges.add(edge.getIdentifier());
					if( value > maxValues[0] ) {
						maxValues[0] = value;
					}
				}
				
				// deals with other "unidentified" attributes
				for(int j = 4; j < edgeAttribs.length; j++) {
					// tries to import it as a double
					try {
						double value = Double.valueOf(edgeAttribs[j]);
						Cytoscape.getEdgeAttributes().setAttribute(edge.getIdentifier(),"Attrib["+j+"]", value);
						if( value > maxValues[j-3] ) {
							maxValues[j-3] = value;
						}
					}
					catch(Exception eDouble) {
						// tries to import it as an integer
						try {
							Cytoscape.getEdgeAttributes().setAttribute(edge.getIdentifier(),"Attrib["+j+"]",Integer.valueOf(edgeAttribs[j]));					
						}
						catch(Exception eInteger) {
							// import it as a string
							Cytoscape.getEdgeAttributes().setAttribute(edge.getIdentifier(),"Attrib["+j+"]",edgeAttribs[j]);					
						}
					}
				}
				network.addEdge(edge);
				line = br.readLine();
			}
			
			DecimalFormat df = new DecimalFormat("###.##");
			for(String edge: modifiedEdges) {
				double value = (Double) Cytoscape.getEdgeAttributes().getAttribute(edge, "QtFrequency");
				Cytoscape.getEdgeAttributes().setAttribute(edge,"FrequencyPerc", df.format((value / maxValues[0]) * 100));
			}
			
			if( originalNet ) {
				originalNetwork = network;
			} 
			else {
				compressedNetwork = network;
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static HashMap<Integer,Double> storyScoreMap=new HashMap<Integer, Double>();
	public static int getNumberOfStoriesAndScores(String inFile)
	{
		int nb =0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			String line = br.readLine();
			while (line!=null)
			{
				if (line.contains("#"))
					nb++;
				line = br.readLine();
			}
			//storiesScore =new double[nb];
			br = new BufferedReader(new FileReader(inFile));
			line = br.readLine();
			int cpt=0;
			while (line!=null)
			{
				if (line.contains("#"))
				{
					Double score=new Double(line.split(": ")[1].split(" ")[0]);
					storyScoreMap.put(new Integer(line.split(":")[0].split(" ")[0].split("#")[1]),score);
					cpt++;
				}
				line = br.readLine();
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("File contains "+nb+" stories.");
		return nb;
	}
	
	public static void storiesFileReader(int story)
	{
		CyNetwork storyNetwork=Cytoscape.createNetwork("Story "+story);
		try {
			BufferedReader br = new BufferedReader(new FileReader(storiesFile));
			
			String line = br.readLine();
			while(!line.contains("#"+story+" "))
			{
				line = br.readLine();
			}
			line = br.readLine();	// ignoring the line with the order originating the story
			line = br.readLine();
			int numberOfBlackNodes = new Integer(line.split(" ")[1]);
			int nn = new Integer(line.split(" ")[0]);
			//Black nodes attributes
			Vector<String> bnNodes=new Vector<String>();
			HashMap<Integer, String> roleMap=new HashMap<Integer, String>();
			for (int i=1;i<numberOfBlackNodes+1;i++)
			{
				line = br.readLine();
				bnNodes.add(line.split(" ")[0]);
				roleMap.put(new Integer(line.split(" ")[0]),line.split(" ")[1]);
			}
			/**
			 * Create Nodes
			 */
			for (int i=numberOfBlackNodes+1;i<numberOfBlackNodes+nn+1;i++)
			{
				line = br.readLine();
				String nodeID="S"+story+"-"+line.split(" ")[0];
				CyNode node=Cytoscape.getCyNode(nodeID,true);				
				storyNetwork.addNode(node);
				Cytoscape.getNodeAttributes().setAttribute(nodeID,"SubGraph",line.split(" ")[3]);
				Cytoscape.getNodeAttributes().setAttribute(nodeID,"Name",line.split(" ")[3].split(";")[0]);
				if(bnNodes.contains(node.getIdentifier().split("-")[1]))
				{
					Cytoscape.getNodeAttributes().setAttribute(nodeID,"bn","true");
					Cytoscape.getNodeAttributes().setAttribute(nodeID,"role",roleMap.get(new Integer(line.split(" ")[0])));
				}
				else
					Cytoscape.getNodeAttributes().setAttribute(nodeID,"bn","false");
			}
			line = br.readLine();
			while(!line.contains("#"))
			{
				String nodeIDSource="S"+story+"-"+line.split(" ")[0];
				String nodeIDTarget="S"+story+"-"+line.split(" ")[1];
				CyNode source=Cytoscape.getCyNode(nodeIDSource,false);
				CyNode target=Cytoscape.getCyNode(nodeIDTarget,false);
				CyEdge edge=Cytoscape.getCyEdge(source.getIdentifier(),"name",target.getIdentifier(),"pp");
				Cytoscape.getEdgeAttributes().setAttribute(edge.getIdentifier(),"SubGraph",line.split(" ")[2]);
				Cytoscape.getEdgeAttributes().setAttribute(edge.getIdentifier(),"Weight",line.split(" ")[2].split(";").length);
				storyNetwork.addEdge(edge);
				line = br.readLine();
			}
	        Cytoscape.firePropertyChange(Cytoscape.VIZMAP_LOADED, null, nelImport.class.getResource("stories.props"));
	        Cytoscape.getVisualMappingManager().setVisualStyle("stories");
			//String vsName = "default";
        	//Cytoscape.getVisualMappingManager().setVisualStyle(vsName);  
	        Cytoscape.getNetworkView(storyNetwork.getIdentifier()).applyLayout(CyLayouts.getLayout("force-directed")); 
	        //Cytoscape.getDesktop().getCyMenus().getMenuBar().getMenu("yFiles").getItem(1).doClick();
	    	CyNetworkView cynv =Cytoscape.getNetworkView(storyNetwork.getIdentifier());
	    	cynv.fitContent(); 
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void computePitch()
	{
		String orderStr = JOptionPane.showInputDialog(null, "Enter a total order of the nodes: ", "Compute pitch", 1);
		if(orderStr != null) {			
			System.out.println(orderStr);
			
			CyNetwork pitchNetwork=Cytoscape.createNetwork("Pitch for "+orderStr);
			String[] order = orderStr.split(" ");
			
			/**
			 * Create Nodes
			 */
			Iterator<CyNode> nodes = compressedNetwork.nodesIterator();
			while( nodes.hasNext() ) {
				CyNode node= nodes.next();
				// consistent arcs
				Iterator<CyNode> targetNodes = compressedNetwork.nodesIterator();
				while( targetNodes.hasNext() ) {
					CyNode target = targetNodes.next();
					if( target != node ) {
						// check if there is an edge
						if( compressedNetwork.edgeExists(node, target) ) {
							// check if it is consistent with the given order, if it is, keep it.
							if( consistent_arc(node.getIdentifier(), target.getIdentifier(), order) ) {
								CyEdge edge=Cytoscape.getCyEdge(node.getIdentifier(),"name",target.getIdentifier(),"pp");
								//System.out.println("Keeping consistent arc from "+node.getIdentifier()+" to "+target.getIdentifier());
								pitchNetwork.addEdge(edge);
							}
							else {
								System.out.println("Inconsistent arc from "+node.getIdentifier()+" to "+target.getIdentifier());								
							}
						}
					}
				}
			}
			
			// now apply clean to get a pitch
			boolean isAPitch = false;
			while( !isAPitch ) {
				isAPitch = true;
				Iterator<CyNode> pitchNodes = pitchNetwork.nodesIterator();
				while( pitchNodes.hasNext() ) {
					CyNode node = pitchNodes.next();
					// check if it is a white node
					if( Cytoscape.getNodeAttributes().getAttribute(node.getIdentifier(),"bn").equals("false")) {
						// check if it is a source
						if( pitchNetwork.getInDegree(node) == 0 && pitchNetwork.getOutDegree(node) > 0 ) {
							List<CyNode> neighbours = pitchNetwork.neighborsList(node);
							for(int i = 0; i < neighbours.size(); i++) {
								System.out.println("Cleaning arc from "+node.getIdentifier()+" to "+neighbours.get(i).getIdentifier());
								pitchNetwork.removeEdge(pitchNetwork.getIndex(Cytoscape.getCyEdge(node.getIdentifier(),"name",neighbours.get(i).getIdentifier(),"pp")), false);
							}
							isAPitch = false;
						}

						// check if it is a target
						if( pitchNetwork.getOutDegree(node) == 0 && pitchNetwork.getInDegree(node) > 0 ) {
							List<CyNode> neighbours = pitchNetwork.neighborsList(node);
							for(int i = 0; i < neighbours.size(); i++) {
								System.out.println("Cleaning arc from "+node.getIdentifier()+" to "+neighbours.get(i).getIdentifier());
								pitchNetwork.removeEdge(pitchNetwork.getIndex(Cytoscape.getCyEdge(neighbours.get(i).getIdentifier(),"name",node.getIdentifier(),"pp")), false);
							}
							isAPitch = false;
						}
					}
					
				}
			}
	        Cytoscape.firePropertyChange(Cytoscape.VIZMAP_LOADED, null, nelImport.class.getResource("original.props"));
	        Cytoscape.getVisualMappingManager().setVisualStyle("original");
	        Cytoscape.getNetworkView(pitchNetwork.getIdentifier()).applyLayout(CyLayouts.getLayout("force-directed")); 
	    	CyNetworkView cynv =Cytoscape.getNetworkView(pitchNetwork.getIdentifier());
	    	cynv.fitContent(); 
		}
	}
	
	// check if source appears before target in the given order array
	protected static boolean consistent_arc(String source, String target, String[] order) {
		for(int i = 0; i < order.length; i++) {
			if( order[i].equals(source) ) {
				return true;
			}
			else if( order[i].equals(target) ) {
				return false;
			}
		}
		return false;
	}
	
	protected static File storiesFile=null;
	public static void importStory()
	{
		if(storiesFile==null)
			storiesFile=FileUtil.getFile("File containing the stories",0);
		int nbStories=getNumberOfStoriesAndScores(storiesFile.getAbsolutePath());
		String choice=null;
		JFrame frame=new JFrame();
		String[] params=new String[nbStories];
		List<Integer> allStories = new ArrayList<Integer>(storyScoreMap.keySet());
		Collections.sort(allStories);
		int i = 0;
		for(Integer storyNumber: allStories)
		{
			params[i++]=new String(""+storyNumber+" score : "+storyScoreMap.get(storyNumber));
		}
		choice = (String)JOptionPane.showInputDialog(frame,"Which story do you want to see ?","",JOptionPane.PLAIN_MESSAGE,null,params,"...");
		storiesFileReader(new Integer(choice.split(" ")[0]));
		//return choice;
	}
	
	/*
	 * Imports a file containing a list of attributes
	 * id;attr1;attrb2;...
	 * and so on
	 * 
	 */
	public static void importAttributesData()
	{
		File dataFile=FileUtil.getFile("File containing the attributes to load",0);
		try {
			BufferedReader br = new BufferedReader(new FileReader(dataFile));
			
			String header[] = br.readLine().split(";");  // the 1st line is the header
			String line = br.readLine();
			while(line != null) {
				String[] data = line.split(";");
				
				// looks for a node with the same name as in the file
				for(int i = 0; i < Cytoscape.getCyNodesList().size(); i++) {
					CyNode node = (CyNode)Cytoscape.getCyNodesList().get(i);
					if( data[0].equals(Cytoscape.getNodeAttributes().getAttribute(node.getIdentifier(), "Name")) ) {
						for(int j = 1; j < header.length; j++) {
							// tries to import it as a double
							try {
								Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), header[j], Double.valueOf(data[j]));
							}
							catch(Exception eDouble) {
								// tries to import it as an integer
								try {
									Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), header[j], Integer.valueOf(data[j]));
								}
								catch(Exception eInteger) {
									// import it as a string
									Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), header[j], data[j]);
								}
							}
						}
						break;
					}
				}
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Imports a file containing a list of black nodes and their concentration change intensity (from a metabolomics experiment)
	 * 
	 */
	public static void importMetabolomicsData()
	{
		File dataFile=FileUtil.getFile("File containing the metabolomics data",0);
		try {
			BufferedReader br = new BufferedReader(new FileReader(dataFile));
			
			String line = br.readLine();
			while(line != null) {
				String[] data = line.split(" ");
				
				// looks for a node with the same name as in the file
				for(int i = 0; i < Cytoscape.getCyNodesList().size(); i++) {
					CyNode node = (CyNode)Cytoscape.getCyNodesList().get(i);
					/*String[] nodeNames = node.getIdentifier().split(";");
					for(int j = 0; j < nodeNames.length; j++) {
						if( nodeNames[j].equalsIgnoreCase(data[0]) ) {
							double intensity = new Double(data[1]);
							Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), "intensity", intensity);
							if( intensity > 1 ) {
								Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), "color", "G");
							} else {
								Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), "color", "R");							
							}
						}
					}*/
					if( data[0].equals( Cytoscape.getNodeAttributes().getAttribute(node.getIdentifier(), "Name") )) {
						double intensity = new Double(data[1]);
						Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), "intensity", intensity);
						if( intensity > 1 ) {
							Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), "color", "G");
						} else {
							Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), "color", "R");
						}
					}
				}
				line = br.readLine();
			}
			
			// looks for the white nodes (the ones without intensity defined)
			for(int i = 0; i < Cytoscape.getCyNodesList().size(); i++) {
				CyNode node = (CyNode)Cytoscape.getCyNodesList().get(i);
				if( Cytoscape.getNodeAttributes().getAttribute(node.getIdentifier(), "intensity") == null ) {
					Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), "intensity", 1.0);
					Cytoscape.getNodeAttributes().setAttribute(node.getIdentifier(), "color", "W");
				}
			}
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// applies the visual mapping for anthologies
        Cytoscape.firePropertyChange(Cytoscape.VIZMAP_LOADED, null, nelImport.class.getResource("anthology.props"));
        Cytoscape.getVisualMappingManager().setVisualStyle("anthology");
        Cytoscape.getNetworkView(compressedNetwork.getIdentifier()).applyLayout(CyLayouts.getLayout("force-directed")); 
        //Cytoscape.getDesktop().getCyMenus().getMenuBar().getMenu("yFiles").getItem(1).doClick();
    	CyNetworkView cynv =Cytoscape.getNetworkView(compressedNetwork.getIdentifier());
    	cynv.fitContent(); 
		
	}

	public static CyNetwork run(String fileSelectorText,boolean isOriginalNetwork)
	{
		File file=FileUtil.getFile(fileSelectorText,0);
		CyNetwork network=null;
		if(file!=null)
		{
			String inFile=file.getAbsolutePath();
			network=Cytoscape.createNetwork(file.getName());
			importNelNetwork(inFile, network,isOriginalNetwork);
	        Cytoscape.firePropertyChange(Cytoscape.VIZMAP_LOADED, null, nelImport.class.getResource("original.props"));
	        Cytoscape.getVisualMappingManager().setVisualStyle("original");
	        Cytoscape.getNetworkView(network.getIdentifier()).applyLayout(CyLayouts.getLayout("force-directed")); 
	        //Cytoscape.getDesktop().getCyMenus().getMenuBar().getMenu("yFiles").getItem(1).doClick();
	    	CyNetworkView cynv =Cytoscape.getNetworkView(network.getIdentifier());
	    	cynv.fitContent(); 
		}

		return network;			
	}

}
