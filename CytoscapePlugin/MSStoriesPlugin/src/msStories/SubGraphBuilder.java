package msStories;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import cytoscape.CyEdge;
import cytoscape.CyNetwork;
import cytoscape.CyNode;
import cytoscape.Cytoscape;
import cytoscape.layout.CyLayouts;
import cytoscape.view.CyNetworkView;

public class SubGraphBuilder {
	public static void build(CyNetwork referenceNetwork)
	{
		// TODO Auto-generated method stub
    	System.err.println("build network based on "+referenceNetwork);
    	
    	Iterator<CyEdge> allSelectedEdges=Cytoscape.getCurrentNetwork().getSelectedEdges().iterator();
    	Vector<CyNode> selectedNodes=new Vector<CyNode>();
    	while(allSelectedEdges.hasNext())
    	{
    		CyEdge edge=allSelectedEdges.next();
    		String subgraph=Cytoscape.getEdgeAttributes().getStringAttribute(edge.getIdentifier(),"SubGraph");
    		String[] nodes=subgraph.split(";");
    		for (int i=0;i<nodes.length;i++)
    		{
    			String nodeName=nodes[i];
    			/**
    			 * search for this node in the original network
    			 */
    			Iterator<CyNode> allNodes=referenceNetwork.nodesIterator();
    			while(allNodes.hasNext())
    			{
    				CyNode node=allNodes.next();
    				String label= Cytoscape.getNodeAttributes().getStringAttribute(node.getIdentifier(),"Name");
    				if(label.equals(nodeName))
    				{
       					selectedNodes.add(Cytoscape.getCyNode(node.getIdentifier(),false));
       					System.err.println("Node Name "+nodeName);
       					System.err.println("Label "+label);
       					System.err.println("node identifier "+node.getIdentifier());
    				}
    			}
    		}
    	}
    	
    	System.err.println(selectedNodes);
    	
    	//CyNetwork network=Cytoscape.createNetwork("subnet");
    	Iterator<CyNode> allSelectedNodes=Cytoscape.getCurrentNetwork().getSelectedNodes().iterator();
    	while(allSelectedNodes.hasNext())
    	{
    		CyNode node=allSelectedNodes.next();
    		String subgraph=Cytoscape.getNodeAttributes().getStringAttribute(node.getIdentifier(),"SubGraph");
    		String[] nodes=subgraph.split(";");
    		for (int i=0;i<nodes.length;i++)
    		{
    			String nodeName=nodes[i];
    			Iterator<CyNode> allNodes=referenceNetwork.nodesIterator();
    			while(allNodes.hasNext())
    			{
    				CyNode node2=allNodes.next();
    				String label= Cytoscape.getNodeAttributes().getStringAttribute(node2.getIdentifier(),"Name");
    				if(label.equals(nodeName))
    					selectedNodes.add(Cytoscape.getCyNode(node2.getIdentifier(),false));
    			}
    		}
    	}
    	
    	System.err.println(selectedNodes);
    	
    	Set nodes = new HashSet<CyNode>(selectedNodes);
    	
    	Set edges = new HashSet<CyEdge>(referenceNetwork.getConnectingEdges(new ArrayList(selectedNodes)));
    	CyNetwork new_network = Cytoscape.createNetwork(nodes, edges,"subnet",referenceNetwork);
    	CyNetworkView new_network_view = Cytoscape.createNetworkView(new_network, "subnet");
    	 String vsName = "default";

	        //Cytoscape.firePropertyChange(Cytoscape.VIZMAP_LOADED, null, nelImport.class.getResource("original.props"));
	        //Cytoscape.getVisualMappingManager().setVisualStyle("original");
	        Cytoscape.getNetworkView(new_network.getIdentifier()).applyLayout(CyLayouts.getLayout("hierarchical")); 
	        //Cytoscape.getDesktop().getCyMenus().getMenuBar().getMenu("yFiles").getItem(1).doClick();
	    	CyNetworkView cynv =Cytoscape.getNetworkView(new_network.getIdentifier());
	    	cynv.fitContent(); 
	}

}
