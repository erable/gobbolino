package msStories;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import cytoscape.CyNetwork;
import cytoscape.Cytoscape;
import cytoscape.plugin.CytoscapePlugin;
import cytoscape.util.CytoscapeAction;
import cytoscape.view.CytoscapeDesktop;
import cytoscape.view.cytopanels.CytoPanel;

public class MSStories extends CytoscapePlugin{
	public MSStories()
	{
		MSStoriesAction action = new MSStoriesAction();
        action.setPreferredMenu("Plugins");
        Cytoscape.getDesktop().getCyMenus().addAction(action);
	}
	
    public String describe() {
        StringBuffer sb = new StringBuffer();
        sb.append("Metabolic Stories");
        return sb.toString();
    }
    
	public class MSStoriesAction extends CytoscapeAction 
    {    
		private static final long serialVersionUID = -7659165643695704040L;

		/**
         * The constructor sets the text that should appear on the menu item.
         */
        public MSStoriesAction() 
        {super("Metabolic Stories");}
        /**
         * This method is called when the user selects the menu item.
         */
        
        CyNetwork referenceNetwork;
        public void actionPerformed(ActionEvent ae) 
        {
        	JPanel overallPanel1=new JPanel();
   		 	overallPanel1.setLayout(new GridLayout());
    		CytoscapeDesktop desktop = Cytoscape.getDesktop();
            CytoPanel cytoPanel = desktop.getCytoPanel (SwingConstants.SOUTH);
            cytoPanel.add("MetabolicStories",overallPanel1);
            int index=cytoPanel.indexOfComponent(overallPanel1);
            cytoPanel.setSelectedIndex(index);

        	JButton button=new JButton("Subgraph");
        	button.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
					if( referenceNetwork == null ) {
						JOptionPane.showMessageDialog(null, "No reference network was loaded.");
					}
					SubGraphBuilder.build(referenceNetwork);					
				}
			});
        	
        	JButton importNelButton=new JButton("NEL Network");
        	importNelButton.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
					referenceNetwork=nelImport.run("File containing the original Network",true);
				}
			});

        	JButton importOtherNelButton=new JButton("NEL Compressed");
        	importOtherNelButton.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
					nelImport.run("File containing the modified NEL Network",false);
				}
			});
        	
        	JButton computePitchButton=new JButton("Pitch");
        	computePitchButton.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
					nelImport.computePitch();
				}
			});        	
        	
        	JButton getStoryButton=new JButton("Story");
        	getStoryButton.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
					nelImport.importStory();
				}
			});

        	JButton getMetabolomicsDataButton =new JButton("Metabolomics data");
        	getMetabolomicsDataButton.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
					nelImport.importMetabolomicsData();
				}
			});
        	
        	
        	overallPanel1.add(importNelButton);        	
        	overallPanel1.add(importOtherNelButton);
        	overallPanel1.add(computePitchButton);
        	overallPanel1.add(getStoryButton);
        	overallPanel1.add(getMetabolomicsDataButton);
        	overallPanel1.add(button);
        };
     }

}
