#
# Giapetto's problem
#
# This finds the optimal solution for maximizing Giapetto's profit
#
# Giapetto's Woodcarving Inc. manufactures two types of wooden toys: soldiers and trains. 
# A soldier sells for $27 and uses $10 worth of raw materials. Each soldier that is manufactured 
# increases Giapetto's variable labor and overhead costs by $14. A train sells for $21 and uses $9 worth 
# of raw materials. Each train built increases Giapetto's variable labor and overhead costs by $10. 
# The manufacture of wooden soldiers and trains requires two types of skilled labor: carpentry and finishing. 
# A soldier requires 2 hours of finishing labor and 1 hour of carpentry labor. A train requires 1 hour of 
# finishing and 1 hour of carpentry labor. Each week, Giapetto can obtain all the needed raw material but 
# only 100 finishing hours and 80 carpentry hours. Demand for trains is unlimited, but at most 40 soldier
# are bought each week. Giapetto wants to maximize weekly profits (revenues - costs).
#
# To summarize the important information and assumptions about this problem:
#
# There are two types of wooden toys: soldiers and trains.
# A soldier sells for $27, uses $10 worth of raw materials, and increases variable labor and overhead costs by $14.
# A train sells for $21, uses $9 worth of raw materials, and increases variable labor and overhead costs by $10.
# A soldier requires 2 hours of finishing labor and 1 hour of carpentry labor.
# A train requires 1 hour of finishing labor and 1 hour of carpentry labor.
# At most, 100 finishing hours and 80 carpentry hours are available weekly.
# The weekly demand for trains is unlimited, while, at most, 40 soldiers will be sold.
# The goal is to find the numbers of soldiers and trains that will maximize the weekly profit.

/* Set of toys */
set TOY;

/* Parameters */
param Finishing_hours {i in TOY};
param Carpentry_hours {i in TOY};
param Demand_toys     {i in TOY};
param Profit_toys     {i in TOY};

/* Decision variables */
var x {i in TOY} >=0;

/* Objective function */
maximize z: sum{i in TOY} Profit_toys[i]*x[i];

/* Constraints */
s.t. Fin_hours : sum{i in TOY} Finishing_hours[i]*x[i] <= 100;
s.t. Carp_hours : sum{i in TOY} Carpentry_hours[i]*x[i] <= 80;
s.t. Dem {i in TOY} : x[i] <= Demand_toys[i];

data;
/* data  section */

set TOY := soldier train;
param Finishing_hours:=
	soldier         2
    train           1;

param Carpentry_hours:=
    soldier         1
    train           1;

param Demand_toys:=
    soldier        40
    train    6.02E+23;

param Profit_toys:=
    soldier         3
    train           2;
end;