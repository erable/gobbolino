# METABOLIC NETWORK - TOY MODEL
#
# Simple network with 5 compounds and 4 reactions.
# A -> B
# B -> C
# B -> D
# C + D -> E
#
# This network, without the input of A and output of E is not consistency. Otherwise, it is.
#
#
set METABOLITES;
/* compounds */

set INTERNAL_METABOLITES within METABOLITES;
/* compounds that need to balanced */

set REACTIONS;
/* reactions */

param s{METABOLITES, REACTIONS};
/* stoichiometric matrix for the set of reactions and compounds */

var v{r in REACTIONS} >= 0;
/* v corresponds to the flux over each reaction */

var z >= 0;

maximize reactionUse: z;
/* number of times that all reactions are used */

s.t. limit: sum{r in REACTIONS} v[r] <= 1;
s.t. greaterthanz{r in REACTIONS}: v[r] >= z;
/* Enforces the flux of ALL reactions to be equal or greater than z */ 

s.t. steadystate{m in INTERNAL_METABOLITES}: sum{r in REACTIONS} s[m,r] * v[r] = 0;
/* steady-state assumption */

end;