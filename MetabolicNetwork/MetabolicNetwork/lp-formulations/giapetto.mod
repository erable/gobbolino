#
# Giapetto's problem
#
# This finds the optimal solution for maximizing Giapetto's profit
#
# Giapetto's Woodcarving Inc. manufactures two types of wooden toys: soldiers and trains. 
# A soldier sells for $27 and uses $10 worth of raw materials. Each soldier that is manufactured 
# increases Giapetto's variable labor and overhead costs by $14. A train sells for $21 and uses $9 worth 
# of raw materials. Each train built increases Giapetto's variable labor and overhead costs by $10. 
# The manufacture of wooden soldiers and trains requires two types of skilled labor: carpentry and finishing. 
# A soldier requires 2 hours of finishing labor and 1 hour of carpentry labor. A train requires 1 hour of 
# finishing and 1 hour of carpentry labor. Each week, Giapetto can obtain all the needed raw material but 
# only 100 finishing hours and 80 carpentry hours. Demand for trains is unlimited, but at most 40 soldier
# are bought each week. Giapetto wants to maximize weekly profits (revenues - costs).
#
# To summarize the important information and assumptions about this problem:
#
# There are two types of wooden toys: soldiers and trains.
# A soldier sells for $27, uses $10 worth of raw materials, and increases variable labor and overhead costs by $14.
# A train sells for $21, uses $9 worth of raw materials, and increases variable labor and overhead costs by $10.
# A soldier requires 2 hours of finishing labor and 1 hour of carpentry labor.
# A train requires 1 hour of finishing labor and 1 hour of carpentry labor.
# At most, 100 finishing hours and 80 carpentry hours are available weekly.
# The weekly demand for trains is unlimited, while, at most, 40 soldiers will be sold.
# The goal is to find the numbers of soldiers and trains that will maximize the weekly profit.
#
/* Decision variables */
var x1 >=0;  /* soldier */
var x2 >=0;  /* train */

/* Objective function */
maximize z: 3*x1 + 2*x2;

/* Constraints */
s.t. Finishing : 2*x1 + x2 <= 100;
s.t. Carpentry : x1 + x2 <= 80;
s.t. Demand    : x1 <= 40;

end;
