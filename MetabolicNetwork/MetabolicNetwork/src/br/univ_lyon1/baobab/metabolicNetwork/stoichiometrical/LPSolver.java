package br.univ_lyon1.baobab.metabolicNetwork.stoichiometrical;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.SWIGTYPE_p_double;
import org.gnu.glpk.SWIGTYPE_p_int;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_smcp;

public class LPSolver 
{
	static
	{ 
	    try
	    { 
	    	System.out.println(System.getProperty("java.library.path"));
	    	System.loadLibrary("glpk_java"); 
	    }
		catch(UnsatisfiedLinkError e)
		{ 
			System.err.println("Native code library failed to load. See the chapter on Dynamic Linking Problems in the SWIG Java documentation for help.\n"+e); 
			System.exit(1);
		} 
	} 	

	public static void solveExample()
	{
		SWIGTYPE_p_int ia = GLPK.new_intArray(1+1000); 
		SWIGTYPE_p_int ja = GLPK.new_intArray(1+1000); 
		SWIGTYPE_p_double ar = GLPK.new_doubleArray(1+1000); 
		double z, x1, x2, x3; 
		glp_prob lp = GLPK.glp_create_prob(); 
		GLPK.glp_set_prob_name(lp, "sample"); 
		GLPK.glp_set_obj_dir(lp, GLPK.GLP_MAX); 
		GLPK.glp_add_rows(lp, 3); 
		GLPK.glp_set_row_name(lp, 1, "p"); 
		GLPK.glp_set_row_bnds(lp, 1, GLPK.GLP_UP, 0.0, 100.0); 
		GLPK.glp_set_row_name(lp, 2, "q"); 
		GLPK.glp_set_row_bnds(lp, 2, GLPK.GLP_UP, 0.0, 600.0); 
		GLPK.glp_set_row_name(lp, 3, "r"); 
		GLPK.glp_set_row_bnds(lp, 3, GLPK.GLP_UP, 0.0, 300.0); 
		GLPK.glp_add_cols(lp, 3); 
		GLPK.glp_set_col_name(lp, 1, "x1"); 
		GLPK.glp_set_col_bnds(lp, 1, GLPK.GLP_LO, 0.0, 0.0); 
		GLPK.glp_set_obj_coef(lp, 1, 10.0); 
		GLPK.glp_set_col_name(lp, 2, "x2"); 
		GLPK.glp_set_col_bnds(lp, 2, GLPK.GLP_LO, 0.0, 0.0); 
		GLPK.glp_set_obj_coef(lp, 2, 6.0); 
		GLPK.glp_set_col_name(lp, 3, "x3"); 
		GLPK.glp_set_col_bnds(lp, 3, GLPK.GLP_LO, 0.0, 0.0);		
		GLPK.glp_set_obj_coef(lp,3,4.0); 
		
		GLPK.intArray_setitem(ia,1,1);GLPK.intArray_setitem(ja,1,1); 
		GLPK.doubleArray_setitem(ar,1,1.0);/*a[1,1]=1*/ 
		GLPK.intArray_setitem(ia,2,1);GLPK.intArray_setitem(ja,2,2); 
		GLPK.doubleArray_setitem(ar,2,1.0);/*a[1,2]=1*/ 
		GLPK.intArray_setitem(ia,3,1);GLPK.intArray_setitem(ja,3,3); 
		GLPK.doubleArray_setitem(ar,3,1.0);/*a[1,3]=1*/ 
		GLPK.intArray_setitem(ia,4,2);GLPK.intArray_setitem(ja,4,1); 
		GLPK.doubleArray_setitem(ar,4,10.0);/*a[2,1]=10*/ 
		GLPK.intArray_setitem(ia,5,3);GLPK.intArray_setitem(ja,5,1); 
		GLPK.doubleArray_setitem(ar,5,2.0);/*a[3,1]=2*/ 
		GLPK.intArray_setitem(ia,6,2);GLPK.intArray_setitem(ja,6,2); 
		GLPK.doubleArray_setitem(ar,6,4.0);/*a[2,2]=4*/ 
		GLPK.intArray_setitem(ia,7,3);GLPK.intArray_setitem(ja,7,2); 
		GLPK.doubleArray_setitem(ar,7,2.0);/*a[3,2]=2*/ 
		GLPK.intArray_setitem(ia,8,2);GLPK.intArray_setitem(ja,8,3); 
		GLPK.doubleArray_setitem(ar,8,5.0);/*a[2,3]=5*/ 
		GLPK.intArray_setitem(ia,9,3);GLPK.intArray_setitem(ja,9,3); 
		GLPK.doubleArray_setitem(ar,9,6.0);/*a[3,3]=6*/ 
		GLPK.glp_load_matrix(lp,9,ia,ja,ar); 
		
		glp_smcp param = new glp_smcp();//pourpasserdesparam¸tres 
		GLPK.glp_init_smcp(param);//pourinitialiserlesparam¸tres 
		GLPK.glp_simplex(lp,param); 
		z=GLPK.glp_get_obj_val(lp); 
		x1=GLPK.glp_get_col_prim(lp,1); 
		x2=GLPK.glp_get_col_prim(lp,2); 
		x3=GLPK.glp_get_col_prim(lp,3); 
		System.out.printf("\nz=%g;x1=%g;x2=%g;x3=%g\n",z,x1,x2,x3); 
		GLPK.glp_delete_prob(lp); 
		GLPK.delete_intArray(ia); 
		GLPK.delete_intArray(ja); 
		GLPK.delete_doubleArray(ar);		
	}
	
}
