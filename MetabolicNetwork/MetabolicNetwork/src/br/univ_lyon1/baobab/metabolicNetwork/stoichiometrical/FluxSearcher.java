package br.univ_lyon1.baobab.metabolicNetwork.stoichiometrical;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_smcp;
import org.gnu.glpk.glp_tran;

public class FluxSearcher extends LPSolver
{
	public static Double solveGNUMathModel(String lpModel, String data, boolean printColValues)
	{
		// To work with GNU Math models a Translator is needed
		glp_tran trans = GLPK.glp_mpl_alloc_wksp();
		// Allocates the problem instance
		glp_prob lp = GLPK.glp_create_prob();
		// Opens it with the translator help
		if( GLPK.glp_mpl_read_model(trans, lpModel, 0) != 0 )
		{
			System.out.println("Error on translating model.");
			return null;
		}
		if( data != null)
		{
			// Opens it with the translator help
			if( GLPK.glp_mpl_read_data(trans, data) != 0 )
			{
				System.out.println("Error on reading the data.");
				return null;
			}
		}
		// Generates the appropriate model
		if( GLPK.glp_mpl_generate(trans, null) != 0 )
		{
			System.out.println("Error on generating model.");
			return null;
		}
		// And now builds the problem instance
		GLPK.glp_mpl_build_prob(trans, lp);
		
		// And executes it
		glp_smcp param = new glp_smcp(); 
		GLPK.glp_init_smcp(param); 
		GLPK.glp_simplex(lp,param);
		
		double z=GLPK.glp_get_obj_val(lp);
		
		if(printColValues)
		{
			for(int i = 1; i <= GLPK.glp_get_num_cols(lp); i++)
			{
				if( GLPK.glp_get_col_prim(lp, i) == 0.0 )
					System.out.println(GLPK.glp_get_col_name(lp, i)+" = "+GLPK.glp_get_col_prim(lp, i));
			}
		}
		//System.out.printf("\nz=%g;\n",z);		
		GLPK.glp_delete_prob(lp);
		GLPK.glp_mpl_free_wksp(trans);
		return z;
	}	
	
	public static Double solveCPLEXModel(String model)
	{
		glp_prob lp = GLPK.glp_create_prob();
		GLPK.glp_read_lp(lp, null, model);
		glp_smcp param = new glp_smcp(); 
		GLPK.glp_init_smcp(param); 
		GLPK.glp_simplex(lp,param);
		
		double z=GLPK.glp_get_obj_val(lp); 
		//System.out.printf("\nz=%g;\n",z);		
		GLPK.glp_delete_prob(lp);
		return z;
	}		
}
