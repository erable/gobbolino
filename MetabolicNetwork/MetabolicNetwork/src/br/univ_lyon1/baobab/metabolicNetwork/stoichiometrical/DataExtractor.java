package br.univ_lyon1.baobab.metabolicNetwork.stoichiometrical;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.MetabolicNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.Stoichiometry;

public class DataExtractor 
{
	/*
	 * data;
	 * 
	 * set METABOLITES := A B C D E;
	 * set INTERNAL_METABOLITES := A B C D;
	 * set REACTIONS := R1 R2 R3 R4;
	 * 
	 * param s default 0
	 * :    R1  R2  R3  R4 := 
	 *      A    -1  0   0   0   
	 *      B    1   -1  -1  0
	 *      C    0   1   0   -1
	 *      D    0   0   1   -1
	 *      E    0   0   0   1;
	 *      
	 *      end;
	 */	
	public static void extractFromMetabolicNetworkForCheckingConsistency(MetabolicNetwork network, List<Compound> externalMetabolites, Reaction reactionToCheck, String filename, boolean codeIds) throws IOException
	{
		FileWriter writer = new FileWriter(filename);
		// tells that the file corresponds to the data section
		writer.write("data;\n\n");
		// and defines the set of metabolites
		writer.write("set METABOLITES := ");
		Compound compounds[] = network.getCompounds().toArray(new Compound[0]);
		for(int i = 0; i < compounds.length; i++)
		{
			if( codeIds ) 
				writer.write("C"+i+" ");  // instead of compounds[i].getId()
			else
				writer.write(compounds[i].getId()+" ");
		}
		writer.write(";\n");
		
		// and defines the set of internal metabolites (the ones that need to be balanced)
		writer.write("set INTERNAL_METABOLITES := ");
		for(int i = 0; i < compounds.length; i++)
		{
			if( !externalMetabolites.contains(compounds[i]) )
			{
				if( codeIds )
					writer.write("C"+i+" ");
				else
					writer.write(compounds[i].getId() +" ");
			}
		}
		writer.write(";\n");
		
		// and defines the set of reactions
		writer.write("set REACTIONS := ");
		Reaction reactions[] = network.getReactions().toArray(new Reaction[0]);
		int indexOfReactionToCheck = -1;
		for(int j = 0; j < reactions.length; j++)
		{
			writer.write("R"+j+" ");  // instead of reactions[j].getId()
			if( reactions[j].getReversible() )
				writer.write("R"+j+"rev ");
			
			if( reactionToCheck != null && reactionToCheck.equals(reactions[j]) )
				indexOfReactionToCheck = j;
		}
		writer.write(";\n\n");
		
		if( reactionToCheck != null ) {
			writer.write("param reaction_to_check := R"+indexOfReactionToCheck+";\n");
		}
		
		// Finally, extracts the stoichiometric matrix compounds x reactions
		writer.write("param s default 0 \n: ");
		// the list of reactions
		for(int j = 0; j < reactions.length; j++)
		{
			writer.write("R"+j+" ");
			if( reactions[j].getReversible() )
				writer.write("R"+j+"rev ");
		}
		writer.write(" := \n");	
		// and the stoichiometric coefficients of each compound
		for(int i = 0; i < compounds.length; i++)
		{
			if( codeIds ) 
				writer.write("C"+i+" ");
			else
				writer.write(compounds[i].getId()+ " ");
			for(int j = 0; j < reactions.length; j++)
			{
				writer.write(reactions[j].getProduction(compounds[i]) - reactions[j].getConsumption(compounds[i]) + " ");
				if( reactions[j].getReversible() )
					writer.write(reactions[j].getConsumption(compounds[i]) - reactions[j].getProduction(compounds[i]) + " ");
			}
			writer.write("\n");
		}
		writer.write(";\n\n end;");
		writer.close();
	}

	public static void extractStoichiometryMatrixFromMetabolicNetwork(MetabolicNetwork network, String filename, List<Compound> seedsAndTargets) throws IOException
	{
		FileWriter writer = new FileWriter(filename);

		Compound compounds[] = network.getCompounds().toArray(new Compound[0]);
		Reaction reactions[] = network.getReactions().toArray(new Reaction[0]);
		writer.write(compounds.length + " " +reactions.length+"\n");
		writer.write(compounds.length-seedsAndTargets.size()+"\n");
		for(int i = 0; i < compounds.length; i++)
		{
			if( !seedsAndTargets.contains(compounds[i]) ) {
				writer.write(i+"\n");
			}
		}
		
		// and the stoichiometric coefficients of each compound
		for(int i = 0; i < compounds.length; i++)
		{
			for(int j = 0; j < reactions.length; j++)
			{
				writer.write( reactions[j].getProduction(compounds[i]) - reactions[j].getConsumption(compounds[i]) + ".0 ");
				if( reactions[j].getReversible() )
					writer.write(reactions[j].getConsumption(compounds[i]) - reactions[j].getProduction(compounds[i]) + ".0 ");
			}
			writer.write("\n");
		}
		writer.close();
	}

	public static void extractAdjListWithStoichiometryFromMetabolicNetwork(MetabolicNetwork network, String filename, List<Compound> seedsAndTargets) throws IOException
	{
		FileWriter writer = new FileWriter(filename);

		// gets a list of internal compounds (not in the seeds/targets list)
		Compound compounds[] = new Compound[network.getCompounds().size() - seedsAndTargets.size()];
		int k = 0;
		for(Compound compound: network.getCompounds()) {
			if( !seedsAndTargets.contains( compound ) ) {
				compounds[k++] = compound;
			}
		}
		
		Reaction reactions[] = network.getReactions().toArray(new Reaction[0]);
		int nbReactions = 0;
		// ignore reactions that involve only sources/targets
		List<Integer> reactionsToIgnore = new ArrayList<Integer>();
		for(int i = 0; i < reactions.length; i++) {
			boolean hasInternal = false;
			for(Stoichiometry s: reactions[i].getSubstrates()) {
				if( !seedsAndTargets.contains(s.getCompound())) {
					hasInternal = true;
					break;
				}
			}
			if( !hasInternal ) {
				for(Stoichiometry s: reactions[i].getProducts()) {
					if( !seedsAndTargets.contains(s.getCompound())) {
						hasInternal = true;
						break;
					}
				}				
			}
			if( !hasInternal ) {
				reactionsToIgnore.add(i);
			}
			else {
				nbReactions++;
				if( reactions[i].getReversible() ) 
					nbReactions++;
			}
		}
		writer.write(compounds.length + " " +nbReactions+"\n");
		
		// and the adjacency list of each reaction with stoichiometric coefficients 
		for(int j = 0; j < reactions.length; j++)
		{
			if( !reactionsToIgnore.contains(j) ) {
				
				int qtt = 0;
				StringBuffer adj = new StringBuffer("");
				StringBuffer adjRev = new StringBuffer("");
				DecimalFormat df = new DecimalFormat("0.000");

				for(Stoichiometry s: reactions[j].getSubstrates()) {
					if( !seedsAndTargets.contains(s.getCompound())) {
						qtt++;
						adj.append(getIndexOfCompound(compounds, s.getCompound())+" "+df.format(getStoichiometry(reactions[j], s.getCompound()))+" ");
						adjRev.append(getIndexOfCompound(compounds, s.getCompound())+" "+df.format(-1.0 * getStoichiometry(reactions[j], s.getCompound()))+" ");
					}
				}
				for(Stoichiometry s: reactions[j].getProducts()) {
					if( !seedsAndTargets.contains(s.getCompound())) {
						qtt++;
						adj.append(getIndexOfCompound(compounds, s.getCompound())+" "+df.format(getStoichiometry(reactions[j], s.getCompound()))+" ");
						adjRev.append(getIndexOfCompound(compounds, s.getCompound())+" "+df.format(-1.0 * getStoichiometry(reactions[j], s.getCompound()))+" ");
					}
				}				
				writer.write(qtt+" "+adj.toString()+"\n");
				if( reactions[j].getReversible() )
					writer.write(qtt+" "+adjRev.toString()+"\n");
			}
		}
		writer.close();
	}
	
	private static int getIndexOfCompound(Compound[] cpds, Compound c) {
		for(int i = 0; i < cpds.length; i++) {
			if( cpds[i].equals(c) )
				return i;
		}
		return -1;
	}
	
	private static Double getStoichiometry(Reaction r, Compound c) {
		return r.getProduction(c) - r.getConsumption(c);
	}

	
	/*
	 * data;
	 * 
	 * set METABOLITES := A B C D E;
	 * set INTERNAL_METABOLITES := A B C D;
	 * set REACTIONS := R1 R2 R3 R4;
	 * set TRANSPORT_REACTIONS := R1 R4;
	 * 
	 * param s default 0
	 * :    R1  R2  R3  R4 := 
	 *      A    -1  0   0   0   
	 *      B    1   -1  -1  0
	 *      C    0   1   0   -1
	 *      D    0   0   1   -1
	 *      E    0   0   0   1;
	 *      
	 *      end;
	 */	
	public static void extractFromMetabolicNetworkForCheckingSourceTargetConsistency(MetabolicNetwork network, List<Compound> externalMetabolites, Reaction reactionToCheck, String filename, boolean codeIds) throws IOException
	{
		FileWriter writer = new FileWriter(filename);
		// tells that the file corresponds to the data section
		writer.write("data;\n\n");
		// and defines the set of metabolites
		writer.write("set METABOLITES := ");
		Compound compounds[] = network.getCompounds().toArray(new Compound[0]);
		for(int i = 0; i < compounds.length; i++)
		{
			if( codeIds ) 
				writer.write("C"+i+" ");  // instead of compounds[i].getId()
			else
				writer.write(compounds[i].getId()+" ");
		}
		writer.write(";\n");
		
		// and defines the set of internal metabolites (the ones that need to be balanced)
		writer.write("set INTERNAL_METABOLITES := ");
		for(int i = 0; i < compounds.length; i++)
		{
			if( !externalMetabolites.contains(compounds[i]) )
			{
				if( codeIds )
					writer.write("C"+i+" ");
				else
					writer.write(compounds[i].getId() +" ");
			}
		}
		writer.write(";\n");
		
		// and defines the set of reactions
		writer.write("set REACTIONS := ");
		Reaction reactions[] = network.getReactions().toArray(new Reaction[0]);
		int indexOfReactionToCheck = -1;
		for(int j = 0; j < reactions.length; j++)
		{
			writer.write("R"+j+" ");  // instead of reactions[j].getId()
			if( reactions[j].getReversible() )
				writer.write("R"+j+"rev ");
			
			if( reactionToCheck != null && reactionToCheck.equals(reactions[j]) )
				indexOfReactionToCheck = j;
		}
		writer.write(";\n\n");
		
		// and defines the set of transport reactions
		writer.write("set TRANSPORT_REACTIONS := ");
		for(int j = 0; j < reactions.length; j++)
		{
			// checks if the reaction involves any of the external metabolites
			for(Compound c: externalMetabolites)
			{
				if( reactions[j].isSubstrate(c) || reactions[j].isProduct(c) )
				{
					writer.write("R"+j+" ");  // instead of reactions[j].getId()
					if( reactions[j].getReversible() )
						writer.write("R"+j+"rev "); 
					break;
				}
			}
		}
		writer.write(";\n\n");

		if( reactionToCheck != null ) {
			writer.write("param reaction_to_check := R"+indexOfReactionToCheck+";\n");
		}
		
		// Finally, extracts the stoichiometric matrix compounds x reactions
		writer.write("param s default 0 \n: ");
		// the list of reactions
		for(int j = 0; j < reactions.length; j++)
		{
			writer.write("R"+j+" ");
			if( reactions[j].getReversible() )
				writer.write("R"+j+"rev ");
		}
		writer.write(" := \n");	
		// and the stoichiometric coefficients of each compound
		for(int i = 0; i < compounds.length; i++)
		{
			if( codeIds ) 
				writer.write("C"+i+" ");
			else
				writer.write(compounds[i].getId()+ " ");
			for(int j = 0; j < reactions.length; j++)
			{
				writer.write(reactions[j].getProduction(compounds[i]) - reactions[j].getConsumption(compounds[i]) + " ");
				if( reactions[j].getReversible() )
					writer.write(reactions[j].getConsumption(compounds[i]) - reactions[j].getProduction(compounds[i]) + " ");
			}
			writer.write("\n");
		}
		writer.write(";\n\n end;");
		writer.close();
	}
	
}