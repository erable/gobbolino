package br.univ_lyon1.baobab.metabolicNetwork.stoichiometrical;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.MetabolicNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;

public class ConsistencyCheck 
{
	public static boolean checkConsistency(MetabolicNetwork network, List<Compound> externalMetabolites, boolean codeIds)
	{
		// Extracts the data from the metabolic network in the appropriate format for the LP solver
		try 
		{
			DataExtractor.extractFromMetabolicNetworkForCheckingConsistency(network, externalMetabolites, null, "data/network.data", codeIds);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// and applies the LP formulation to check for consistency
		return FluxSearcher.solveGNUMathModel("lp-formulations/network-consistency-check.lp", "data/network.data", false) > 0;
	}
	
	public static void printReactionCut(MetabolicNetwork network, List<Compound> externalMetabolites, String outputFile)
	{
		File outFile = new File(outputFile);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));

			// Assumes that "network" is not consistent and checks, for each reaction, if it can hold a positive flux
			// if the answer is NO then this reaction should be cut.
			for(Reaction r: network.getReactions())
			{
				// Extracts the data from the metabolic network in the appropriate format for the LP solver
				DataExtractor.extractFromMetabolicNetworkForCheckingConsistency(network, externalMetabolites, r, "data/network.dataReaction", false);
				if( FluxSearcher.solveGNUMathModel("lp-formulations/network-consistency-check-reaction.lp", "data/network.dataReaction", false) == 0 )
					bw.write(r.getId()+"\n");
			}
			bw.close();
		}
		catch(Exception e) {
			System.out.println("Error searching for reaction cut.");
			e.printStackTrace();
		}
	}
	
	public static boolean checkSourceTargetConsistency(MetabolicNetwork network, List<Compound> externalMetabolites, boolean codeIds)
	{
		// Extracts the data from the metabolic network in the appropriate format for the LP solver
		try 
		{
			DataExtractor.extractFromMetabolicNetworkForCheckingSourceTargetConsistency(network, externalMetabolites, null, "data/network.data", codeIds);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// and applies the LP formulation to check for consistency
		return FluxSearcher.solveGNUMathModel("lp-formulations/network-source-target-consistency-check.lp", "data/network.data", false) > 0;
	}
	
	
}
