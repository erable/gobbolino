package fr.univ_lyon1.baobab.metabolicNetwork;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jgraph.graph.DefaultEdge;

/**
 * The Reaction class represents a chemical reaction, mapping the transformation of a set of substrate compounds
 * into a set of product compounds. The stoichiometry of each compound in the substrates and products list is also
 * represented.
 * 
 * @author paulomilreu
 */
public class Reaction extends DefaultEdge 
{	
	private static final long serialVersionUID = -5025125791778134682L;
	
	private String id;
	private String name;
	private Boolean reversible = false;
	private Boolean topologicalSeed = false;
	private Boolean topologicalTarget = false;
	
	private Set<Stoichiometry> substrates = new HashSet<Stoichiometry>();
	private Set<Stoichiometry> products   = new HashSet<Stoichiometry>();
	
	public Reaction()
	{
		id = "None";
		name = "Undefined";
	}
	
	public Reaction(String id, String name, Boolean reversible)
	{
		this.setId(id);
		this.setName(name);
		this.setReversible(reversible);
	}
	
	/*@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null ) ? 0 : id.hashCode());
		return result;
	}*/
	
	@Override
	public boolean equals(Object arg0) {
		if( arg0 == null )
			return false;
		if( this == arg0 )
			return true;
		if( ! (arg0 instanceof Reaction) )
			return false;
		
		Reaction r = (Reaction)arg0;
		if( id == null )
			return r.id == null;
		
		return id.equals(r.id);
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setReversible(Boolean reversible) {
		this.reversible = reversible;
	}

	public Boolean getReversible() {
		return reversible;
	}

	public void setSubstrates(Set<Stoichiometry> substrates) {
		this.substrates = substrates;
	}

	public Set<Stoichiometry> getSubstrates() {
		return substrates;
	}

	public void setProducts(Set<Stoichiometry> products) {
		this.products = products;
	}

	public Set<Stoichiometry> getProducts() {
		return products;
	}
	
	public void addSubstrate(Stoichiometry substrate)
	{
		getSubstrates().add(substrate);
	}
	
	public void addSubstrate(Compound substrate)
	{
		addSubstrate(new Stoichiometry(substrate, 1.0));
	}
	
	public void addSubstrate(Compound substrate, Double stoichiometry) {
		addSubstrate(new Stoichiometry(substrate, stoichiometry));
	}
	
	public void addSubstrates(List<Stoichiometry> substrates)
	{
		getSubstrates().addAll(substrates);
	}
		
	public void addProduct(Stoichiometry product)
	{
		getProducts().add(product);
	}
	
	public void addProduct(Compound product)
	{
		addProduct(new Stoichiometry(product, 1.0));
	}
	
	public void addProduct(Compound product, Double stoichiometry) {
		addProduct(new Stoichiometry(product, stoichiometry));
	}

	public void addProducts(List<Stoichiometry> products)
	{
		getSubstrates().addAll(substrates);
	}
	
	public boolean isProduct(Compound compound)
	{
		for(Stoichiometry s: getProducts())
			if( s.getCompound().equals(compound) )
				return true;
		return false;
	}

	public boolean isSubstrate(Compound compound)
	{
		for(Stoichiometry s: getSubstrates())
			if( s.getCompound().equals(compound) )
				return true;
		return false;
	}
	
	
	public Stoichiometry getProductionStoichiometry(Compound compound)
	{
		for(Stoichiometry s: getProducts())
			if( s.getCompound().equals(compound) )
				return s;
		return null;
	}

	public Double getProduction(Compound compound)
	{
		Stoichiometry s = getProductionStoichiometry(compound);
		return s == null ? 0 : s.getStoichiometry();
	}
	
	public Stoichiometry getConsumptionStoichiometry(Compound compound)
	{
		for(Stoichiometry s: getSubstrates())
			if( s.getCompound().equals(compound) )
				return s;
		return null;
	}	
	
	public Double getConsumption(Compound compound)
	{
		Stoichiometry s = getConsumptionStoichiometry(compound);
		return s == null ? 0 : s.getStoichiometry();
	}
	
	public Compound getSource()
	{
		return (Compound)super.getSource();
	}

	public Compound getTarget()
	{
		return (Compound)super.getTarget();
	}

	public Boolean getTopologicalSeed() {
		return topologicalSeed;
	}

	public void setTopologicalSeed(Boolean seed) {
		this.topologicalSeed = seed;
	}	

	public Boolean getTopologicalTarget() {
		return topologicalTarget;
	}

	public void setTopologicalTarget(Boolean seed) {
		this.topologicalTarget = seed;
	}
	
	public String toString() {
		return getId();
	}
	
	public void removeCompound(Compound c) {
		Stoichiometry s = getProductionStoichiometry(c);
		if( s != null ) {
			getProducts().remove(s);
		}
		s = getConsumptionStoichiometry(c);
		if( s != null ) {
			getSubstrates().remove(s);
		}
	}
}
