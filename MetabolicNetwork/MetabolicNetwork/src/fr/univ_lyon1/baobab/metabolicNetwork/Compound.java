package fr.univ_lyon1.baobab.metabolicNetwork;

/*
 * The Compound class represents a chemical compound.
 * 
 * @author paulomilreu
 */
public class Compound implements Comparable<Object> 
{	
	String id;
	String name;
	String compartment;
	boolean seed;
	boolean target;
	 
	public Compound(String id, String name, String compartment) {
		this.id = id;
		this.name = name;
		this.compartment = compartment;
		this.seed = false;
		this.target = false;
	}
	
	@Override
	public int hashCode() {
		return id == null ? super.hashCode() : id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if( id == null )
			return false;
		if( !(obj instanceof Compound) )
			return false;
		
		return id.equals(((Compound)obj).getId());
	}

	@Override
	public String toString() 
	{
		
		return "<" + id +">";
	}

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

	public int compareTo(Object c) 
	{
		return toString().compareTo(c.toString());
	}

	public boolean isSeed() {
		return seed;
	}

	public void setSeed(boolean seed) {
		this.seed = seed;
	}

	public boolean isTarget() {
		return target;
	}

	public void setTarget(boolean target) {
		this.target = target;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
}
