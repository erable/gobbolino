package fr.univ_lyon1.baobab.metabolicNetwork.IO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.univ_lyon1.baobab.metabolicNetwork.MetabolicNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.Stoichiometry;

public class MetabolicNetworkReactionGraphWriter implements MetabolicNetworkWriter
{
	String filename;
	File networkFile;
	BufferedWriter bw;
	Map<String, Integer> reactionPairs = new HashMap<String, Integer>();
	boolean selfLoops = false;
	boolean isolatedNodes = true;
	
	public MetabolicNetworkReactionGraphWriter(String filename, boolean selfLoops, boolean isolatedNodes) 
	{
		this.filename = filename;
		this.selfLoops = selfLoops;
		this.isolatedNodes = isolatedNodes;
	}
	
	private void closeFile() {
		if (networkFile != null) {
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
			bw = null;
			networkFile = null;
		}
	}
	
	@Override
	public void write(MetabolicNetwork network, MetabolicNetworkWriterListener listener) 
	{
		reactionPairs.clear();
		networkFile = new File(filename);
		try {
			bw = new BufferedWriter(new FileWriter(networkFile));
			writeListOfReactions(network, listener);
			closeFile();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}	
	}

	private void writeListOfReactions(MetabolicNetwork network, MetabolicNetworkWriterListener listener) throws IOException {
		List<Reaction> includedReactions = new ArrayList<Reaction>();
		
		for(Reaction r: network.getReactions()) {
			List<Stoichiometry> products = new ArrayList<Stoichiometry>();
			products.addAll(r.getProducts());
			if( r.getReversible() )
				products.addAll(r.getSubstrates());
			
			for(Stoichiometry product: products) {
				for(Reaction t: network.getReactionsThatConsume(product.getCompound())) {
					if( !r.equals(t) || selfLoops ) {
						String pair = r.getId()+" "+t.getId();
						if( !reactionPairs.containsKey(pair) )
						{
							if( !includedReactions.contains(r) )
								includedReactions.add(r);
							if( !includedReactions.contains(t) )
								includedReactions.add(t);
							reactionPairs.put(pair, 1);
							bw.write(pair+"\n");
						}
					}
				}
			}
		}
		
		if( isolatedNodes ) {
			for(Reaction r: network.getReactions()) {
				if( !includedReactions.contains(r)) {
					String pair = r.getId()+" ";
					if( !reactionPairs.containsKey(pair) )
					{
						reactionPairs.put(pair, 1);
						bw.write(pair+"\n");
					}
				}
			}
		}
		
	}	
    
}
