package fr.univ_lyon1.baobab.metabolicNetwork.IO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.xml.sax.SAXException;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.Stoichiometry;

public class XML2HNEL2 {

	
	public void run(String inputFile, String outputFile) throws IOException, SAXException {
		MetabolicNetworkSBMLReader reader = new MetabolicNetworkSBMLReader(inputFile);
		File outFile = new File(outputFile);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			List<Compound> compounds = reader.getCompounds();
			List<Reaction> reactions = reader.getReactions();
			bw.write(compounds.size()+"\n");
			for(int i = 0; i < compounds.size(); i++) {
				bw.write(i + " " + outDegree(reactions, compounds.get(i)) + " " + inDegree(reactions, compounds.get(i)) + " " + compounds.get(i).getId()+"\n");
			}
			for (int i = 0; i < reactions.size(); i++) {
				bw.write(listOfIds(reactions.get(i).getSubstrates(), compounds)+"-> "+listOfIds(reactions.get(i).getProducts(),compounds)+"\n");
				if( reactions.get(i).getReversible() )
					bw.write(listOfIds(reactions.get(i).getProducts(), compounds)+"-> "+listOfIds(reactions.get(i).getSubstrates(), compounds)+"\n");				
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}		
	}
	
	public int outDegree(List<Reaction> reactions, Compound c) {
		int degree = 0;
		for(Reaction r: reactions) {
			if( r.isProduct(c) )
				degree++;
			else if( r.isSubstrate(c) && r.getReversible() )
				degree++;		
		}
		return degree;
	}
	
	public int inDegree(List<Reaction> reactions, Compound c) {
		int degree = 0;
		for(Reaction r: reactions) {
			if( r.isSubstrate(c) )
				degree++;
			else if( r.isProduct(c) && r.getReversible() )
				degree++;		
		}
		return degree;
	}

	public String listOfIds(Set<Stoichiometry> metabolites, List<Compound> compounds) {
		String str = "";
		for(Stoichiometry s: metabolites) {
			str += compounds.indexOf(s.getCompound()) + " ";
		}
		
		return str;
	}
	
}