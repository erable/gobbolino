package fr.univ_lyon1.baobab.metabolicNetwork.IO;

import java.util.List;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;


public interface MetabolicNetworkReader 
{
	List<Compound> getCompounds();
	List<Reaction> getReactions();	
}