package fr.univ_lyon1.baobab.metabolicNetwork.IO;

import fr.univ_lyon1.baobab.metabolicNetwork.MetabolicNetwork;

public interface MetabolicNetworkWriter {

	public void write(MetabolicNetwork network, MetabolicNetworkWriterListener listener);
}
