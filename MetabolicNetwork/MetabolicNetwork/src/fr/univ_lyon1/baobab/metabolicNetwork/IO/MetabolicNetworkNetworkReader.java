package fr.univ_lyon1.baobab.metabolicNetwork.IO;

import java.util.ArrayList;
import java.util.List;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.MetabolicNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;


public class MetabolicNetworkNetworkReader implements MetabolicNetworkReader
{
	private MetabolicNetwork network;
	
	public MetabolicNetworkNetworkReader(MetabolicNetwork network)
	{	
		this.network = network;
	}
	
	@Override
	public List<Compound> getCompounds() 
	{
		List<Compound> compounds = new ArrayList<Compound>(network.getCompounds());
		return compounds;
	}

	@Override
	public List<Reaction> getReactions() 
	{
		List<Reaction> reactions = new ArrayList<Reaction>(network.getReactions());
		return reactions;
	}	
	
}
