package fr.univ_lyon1.baobab.metabolicNetwork.IO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;


public class MetabolicNetworkSBMLReader implements MetabolicNetworkReader
{
	private Document document;
	private List<Compound> compounds;
	
	public MetabolicNetworkSBMLReader(String inputFile) throws IOException, SAXException
	{
       	document = XMLUtils.open(inputFile);		
	}
	
	public Document getDocument()
	{
		return document;
	}
	
	@Override
	public List<Compound> getCompounds() 
	{
		compounds = new ArrayList<Compound>();
		parseSbmlListOfCompounds(getDocument());
		return compounds;
	}

	@Override
	public List<Reaction> getReactions() 
	{
		return parseSbmlListOfReactions(getDocument());
	}	
	
    public Compound addCompound(String id, String name, String compartment)
    {
        Compound c = new Compound(id, name, compartment);
        compounds.add(c);
        return c;
    }
    
    public Compound findCompoundById(String id)
    {
    	for(Compound c: compounds)
    	{
    		if( c.getId().equals(id) )
    			return c;
    	}
    	return null;
    }
	
    public void parseSbmlListOfCompounds(Document document)
    {
        // get the element "listOfSpecies"
        NodeList listOfSpecies = document.getElementsByTagName("listOfSpecies");
        
        // Check if everything is OK.
        if (listOfSpecies == null)
        	throw new RuntimeException("Incorrect Format: Can't find node [listOfSpecies]");

        if (listOfSpecies.getLength() > 1)
        	throw new RuntimeException("Incorrect Format: More than one node [listOfSpecies]");
        
        // print all child elements from listOfSpecies
        NodeList listOfCompounds = listOfSpecies.item(0).getChildNodes();
        int compoundCount = listOfCompounds.getLength();
        // IMPORTANT: The loop adds 2 at each iteration... I don't know why, but there are
        // some intermediate nodes that correspond to no compound.
        for (int i = 1; i < compoundCount; i = i+2) 
        {
            Element compound = (Element)listOfCompounds.item(i);
            addCompound(compound.getAttribute("id"), compound.getAttribute("name"), compound.getAttribute("compartment"));
        }
    }
		
    public List<Reaction> parseSbmlListOfReactions(Document document)
    {
    	List<Reaction> reactions = new ArrayList<Reaction>();
    	
        // get the element "listOfReactions"
        NodeList listOfReactions = document.getElementsByTagName("listOfReactions");
        
        // Check if everything is OK.
        if (listOfReactions == null)
        	throw new RuntimeException("Incorrect Format: Can't find node [listOfReactions]");

        if (listOfReactions.getLength() > 1)
        	throw new RuntimeException("Incorrect Format: More than one node [listOfReactions]");
        
        // print all child elements from listOfReactions
        NodeList reactionsList = listOfReactions.item(0).getChildNodes();
        int reactionCount = reactionsList.getLength();
        // IMPORTANT: The loop adds 2 at each iteration... I don't know why, but there are
        // some intermediate nodes that do not correspond to a reaction and must be ignored
        for (int i = 1; i < reactionCount; i = i+2) 
        {
            Element reaction = (Element)reactionsList.item(i);
            reactions.add( parseSbmlReaction(reaction, reaction.getAttribute("id"), reaction.getAttribute("name"), "true".equals(reaction.getAttribute("reversible"))) );
        }
        return reactions;
    }
	
    public Reaction parseSbmlReaction(Element reactionNode, String id, String name, Boolean reversible)
    {
    	Reaction reaction = new Reaction(id, name, reversible);
    	
        // print all child elements from listOfReactions
        NodeList listsFromReaction = reactionNode.getChildNodes();
       
        // There must be 2 children: List of Reactants and list of Products. 
        // But, there are some cases when appears some "Notes" list...
        int listsCount = listsFromReaction.getLength();        
        for(int j = 0; j < listsCount; j++)
        {
        	NodeList listOfReactants, listOfProducts;
	     
	        if( "listOfReactants".equals(listsFromReaction.item(j).getNodeName()))
	        {
	        	listOfReactants = listsFromReaction.item(j).getChildNodes();
		        int reactantsCount = listOfReactants.getLength();
		        
		        for (int i = 1; i < reactantsCount; i = i+2) {
		            Element reactant = (Element)listOfReactants.item(i);
		            
		            // Finds the compound
		            Compound c = findCompoundById(reactant.getAttribute("species"));
		            if( c != null)
		            	reaction.addSubstrate(c, new Double(reactant.getAttribute("stoichiometry")));
		        }
	        }

	        if( "listOfProducts".equals(listsFromReaction.item(j).getNodeName()))
	        {
	        	listOfProducts = listsFromReaction.item(j).getChildNodes();
		        int productsCount = listOfProducts.getLength();
		        
		        for (int i = 1; i < productsCount; i = i+2) {
		            Element product = (Element)listOfProducts.item(i);

		            // Finds the compound
		            Compound c = findCompoundById(product.getAttribute("species"));
		            if( c != null)
		            	reaction.addProduct(c, new Double(product.getAttribute("stoichiometry")));
		        }
	        }
        }
        return reaction;
    }
}
