package fr.univ_lyon1.baobab.metabolicNetwork.IO;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;

public interface MetabolicNetworkWriterListener {
	public String addPropertiesForCompound(Compound c);
	public String addPropertiesForReaction(Reaction r);
	
}
