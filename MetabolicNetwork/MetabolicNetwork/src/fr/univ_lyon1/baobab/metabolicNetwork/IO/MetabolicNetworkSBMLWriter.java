package fr.univ_lyon1.baobab.metabolicNetwork.IO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.MetabolicNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.Stoichiometry;

public class MetabolicNetworkSBMLWriter implements MetabolicNetworkWriter
{
	String filename;
	File networkFile;
	BufferedWriter bw;
	
	public MetabolicNetworkSBMLWriter(String filename) 
	{
		this.filename = filename;
	}
	
	private void closeFile() {
		if (networkFile != null) {
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
			bw = null;
			networkFile = null;
		}
	}
	
	@Override
	public void write(MetabolicNetwork network, MetabolicNetworkWriterListener listener) 
	{
		networkFile = new File(filename);
		try {
			bw = new BufferedWriter(new FileWriter(networkFile));
			writeHeader();
			writeListOfCompounds(network, listener);
			writeListOfReactions(network, listener);
			writeTail();
			closeFile();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}	
	}

	private void writeHeader() throws IOException {
		bw.write("<?xml version=\"1.0\"  encoding=\"UTF-8\"?>");
		bw.write("\n<sbml xmlns=\"http://www.sbml.org/sbml/level2\" version=\"1\" level=\"2\" xmlns:html=\"http://www.w3.org/1999/xhtml\">");
		bw.write("\n<model id=\"iMM904_metabolic_network\" name=\"iMM904\">");
	}
	
	private void writeListOfCompounds(MetabolicNetwork network, MetabolicNetworkWriterListener listener) throws IOException {
		bw.write("\n<listOfSpecies>");
//		  <species id="M_abt_e" name="M_L_Arabinitol_C5H12O5" initialAmount="0" compartment="cytoplasm" boundaryCondition="false" />
		for(Compound c: network.getCompounds()) {
			bw.write("\n\t<species id=\""+c.getId()+"\" name=\""+c.getName()+"\""+(listener == null? "":listener.addPropertiesForCompound(c))+"/>");
		}
		bw.write("\n</listOfSpecies>");	
	}
	
	private void writeListOfReactions(MetabolicNetwork network, MetabolicNetworkWriterListener listener) throws IOException {
		bw.write("\n<listOfReactions>");
//		  <reaction id="R_PANTS" name="R_pantothenate_synthase" reversible="false">
//		      <listOfReactants>
//		        <speciesReference species="M_pant_R_c" stoichiometry="1.000000"/>
//		        <speciesReference species="M_ala_B_c" stoichiometry="1.000000"/>
//		      </listOfReactants>
//		      <listOfProducts>
//		        <speciesReference species="M_pnto_R_c" stoichiometry="1.000000"/>
//		      </listOfProducts>
//		    </reaction>
		for(Reaction r: network.getReactions()) {
			bw.write("\n\t<reaction id=\""+r.getId()+"\" name=\""+r.getName()+"\" reversible=\""+r.getReversible()+"\">");

			bw.write("\n\t\t<listOfReactants>");
			for(Stoichiometry subs: r.getSubstrates()) {
				bw.write("\n\t\t\t<speciesReference species=\""+subs.getCompound().getId()+"\" stoichiometry=\""+subs.getStoichiometry()+"\"/>");				
			}
			bw.write("\n\t\t</listOfReactants>");

			bw.write("\n\t\t<listOfProducts>");
			for(Stoichiometry prod: r.getProducts()) {
				bw.write("\n\t\t\t<speciesReference species=\""+prod.getCompound().getId()+"\" stoichiometry=\""+prod.getStoichiometry()+"\"/>");				
			}
			bw.write("\n\t\t</listOfProducts>");

			bw.write("\n\t</reaction>");			
		}
		bw.write("\n</listOfReactions>");			
	}
	
	private void writeTail() throws IOException {
		bw.write("\n</model>");
		bw.write("\n</sbml>");
	}
    
}
