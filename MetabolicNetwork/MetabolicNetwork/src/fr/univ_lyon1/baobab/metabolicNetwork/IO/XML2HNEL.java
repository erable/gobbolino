package fr.univ_lyon1.baobab.metabolicNetwork.IO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.SAXException;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.Stoichiometry;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkSBMLReader;


public class XML2HNEL {
	private MetabolicNetworkSBMLReader network;
	private List<Compound> compounds;
	private List<Reaction> reactions;
	private List<String> nodeNames;
	private ArrayList[] hypergraph;
	private int[] outDegree;
	private int[] inDegree;
	private int nn;
	private int nr;
	
	public void run(String inputFile, String outputFile, boolean write) throws IOException, SAXException {
		network = new MetabolicNetworkSBMLReader(inputFile);
		this.convert();
		if(write) {
			this.writeFile(outputFile);
		}
	}
	
	@SuppressWarnings("unchecked")
	void convert() {
		hypergraph = new ArrayList[2];
		hypergraph[0] = new ArrayList<String>();
		hypergraph[1] = new ArrayList<String>();
		compounds = network.getCompounds();
		reactions = network.getReactions();
		nn = compounds.size();
		nr = reactions.size();
		this.initializeDegrees();
		this.initializeNodeNames();
		int i=0;
		int j=0;
		//========================
		//== for each reaction ===
		//========================
		for(i=0 ; i<nr ; i++) {
			String substrates = "";
			//=== for each set of substrates ===
			for(Stoichiometry s: reactions.get(i).getSubstrates()) {
				int index = nodeNames.indexOf(s.getCompound().getId());
				substrates = substrates + index + " ";
			}
			substrates = substrates.substring(0,substrates.lastIndexOf(" "));
			hypergraph[0].add(substrates);
			String products = "";
			//=== for each set of products ===
			for(Stoichiometry s: reactions.get(i).getProducts()) {
				int index = nodeNames.indexOf(s.getCompound().getId());
				products = products + index + " ";
			}
			products = products.substring(0,products.lastIndexOf(" "));
			hypergraph[1].add(products);
			//=== if the reaction is reversible ===
			if(reactions.get(i).getReversible()) {
				hypergraph[0].add(products);
				hypergraph[1].add(substrates);
			}
		}
		//========================
		//== for each compound ===
		//========================
		for(i=0 ; i<nn ; i++) {
			for(j=0 ; j<nr ; j++) {
				if(reactions.get(j).isSubstrate(compounds.get(i))) {
					outDegree[i]++;
					if(reactions.get(j).getReversible()) {
						inDegree[i]++;
					}
				}
				if(reactions.get(j).isProduct(compounds.get(i))) {
					inDegree[i]++;
					if(reactions.get(j).getReversible()) {
						outDegree[i]++;
					}
				}
			}
		}
	}
	
	void writeFile(String outputFile) {
		File outFile = new File(outputFile);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			bw.write(nn + "\n");
			int i=0;
			for (i=0 ; i<nn ; i++) {
				bw.write(i + " " + outDegree[i] + " " + inDegree[i] + " "
						+ nodeNames.get(i)+";\n");
			}
			for (i=0 ; i<hypergraph[0].size() ; i++) {
				bw.write(hypergraph[0].get(i) + " -> " + hypergraph[1].get(i) + "\n");
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	void initializeDegrees() {
		outDegree = new int[nn];
		inDegree = new int[nn];
		int i=0;
		for(i=0 ; i<nn ; i++) {
			inDegree[i] = 0;
			outDegree[i] = 0;
		}
	}
	
	void initializeNodeNames() {
		nodeNames = new ArrayList<String>();
		int i=0;
		for(i=0 ; i<nn ; i++) {
			nodeNames.add(compounds.get(i).getId());
		}
	}
	
}
