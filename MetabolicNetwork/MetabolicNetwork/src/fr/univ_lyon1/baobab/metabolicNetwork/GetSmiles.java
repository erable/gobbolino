package fr.univ_lyon1.baobab.metabolicNetwork;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetSmiles {

	// parameters to connect to database
	// may get the credentials from a file...
	static String dbUrl = "jdbc:mysql://localhost:3306/metacyc";
	static String user = "root";
	static String password = "";
	
	
	/**
	 * Get SMILES for each compound name of the given list. SMILES are retrieved from database.
	 * @param compounds
	 * @return
	 */
	public static Map<String, String> getSmiles(List<String> compounds){ // could be a list of <Compound>
		
		Map<String, String> smilesHash = new HashMap<String, String>();
		
		// connect to database
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(dbUrl, user, password);
			
			// query database...
			String query = "Select c.smiles FROM metacyc.compound as c WHERE c.name = ?";
			PreparedStatement prepStmt = con.prepareStatement(query);
		
			//...for each item in the given list
			for(String item : compounds){
				prepStmt.setString(1, item);
				ResultSet rs = prepStmt.executeQuery();
				smilesHash.put(item, null); // initialize entry
				while(rs.next()){
					smilesHash.put(item, rs.getString(1));
				}
				
			}
		
			// disconnect from database
			con.close();
		
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// return map
		return smilesHash;
	}
	
}
