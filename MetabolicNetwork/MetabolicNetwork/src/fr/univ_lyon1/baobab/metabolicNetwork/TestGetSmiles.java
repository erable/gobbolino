package fr.univ_lyon1.baobab.metabolicNetwork;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class TestGetSmiles {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<String> liste = new ArrayList<String>();
		liste.add("PROCOLLAGEN-3-HYDROXY-L-PROLINE");
		liste.add("PROCOLLAGEN-L-PROLINE");
		liste.add("Test");
		
		Map<String, String> smilesList = GetSmiles.getSmiles(liste);
		
		for(String key : smilesList.keySet()){
			System.out.println(key + " : " + smilesList.get(key));
		}
	}

	

}
