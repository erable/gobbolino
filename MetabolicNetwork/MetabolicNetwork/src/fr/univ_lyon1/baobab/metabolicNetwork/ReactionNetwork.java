package fr.univ_lyon1.baobab.metabolicNetwork;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jgraph.graph.DefaultEdge;
import org.jgrapht.EdgeFactory;
import org.jgrapht.ext.EdgeNameProvider;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.DefaultDirectedGraph;

import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkReader;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkWriterListener;

public class ReactionNetwork extends DefaultDirectedGraph<Reaction, DefaultEdge> implements VertexNameProvider<Reaction>, EdgeNameProvider<DefaultEdge>, MetabolicNetwork
{

	private static final long serialVersionUID = 8670878530507807239L;

	public ReactionNetwork(EdgeFactory<Reaction, DefaultEdge> arg0)
	{
		super(arg0);
	}
	
	/*
	 * Interface with some met. network reader
	 */
	public void loadNetwork(MetabolicNetworkReader reader, boolean duplicateReversibleReactions)
	{
		for(Reaction r: reader.getReactions())
			addReaction(r);
		
		createArcs(vertexSet(), duplicateReversibleReactions);
	}
	
	public void addReaction(Reaction reaction)
	{
		// Each reaction is a new vertex in the graph
		addVertex(reaction);
	}
	
	private void createArcs(Collection<Reaction> reactions, boolean duplicateReversibleReactions) {
		Map<String, Integer> reactionPairs = new HashMap<String, Integer>();		
		for(Reaction r: reactions) {
			List<Stoichiometry> products = new ArrayList<Stoichiometry>();
			products.addAll(r.getProducts());
			if( r.getReversible() && duplicateReversibleReactions )
				products.addAll(r.getSubstrates());
			
			for(Stoichiometry product: products) {
				for(Reaction t: getReactionsThatConsume(product.getCompound())) {
					if( !r.equals(t) ) {
						String pair = r.getId()+" "+t.getId();
						if( !reactionPairs.containsKey(pair) )
						{
							reactionPairs.put(pair, 1);
							DefaultEdge e1 = new DefaultEdge();
							e1.setSource(r);
							e1.setTarget(t);
							addEdge(r, t, e1);
						}
					}
				}
			}
		}
	}
	
	@Override
	public Collection<Compound> getCompounds() {
		return null;
	}

	@Override
	public Collection<Reaction> getReactions() {
		return vertexSet();
	}

	@Override
	public Collection<Reaction> getReactionsThatProduce(Compound compound) {
		List<Reaction> result = new ArrayList<Reaction>();
		for(Reaction r: vertexSet()) {
			if( r.getProduction(compound) > 0 ) {
				result.add(r);
				continue;
			}
			if( r.getReversible() && r.getConsumption(compound) > 0 ) {
				result.add(r);
			}
		}
		return result;	
	}

	@Override
	public Collection<Reaction> getReactionsThatConsume(Compound compound) {
		List<Reaction> result = new ArrayList<Reaction>();
		for(Reaction r: vertexSet()) {
			if( r.getConsumption(compound) > 0 ) {
				result.add(r);
				continue;
			}
			if( r.getReversible() && r.getProduction(compound) > 0 ) {
				result.add(r);
			}
		}
		return result;
	}

	@Override
	public Collection<Compound> reactionsFires(Reaction reaction, Collection<Compound> availableCompounds) 
	{
		return null;
	}

	@Override
	public MetabolicNetwork subgraphFromReactions(Collection<Reaction> reactions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MetabolicNetwork subgraphFromCompounds(Collection<Compound> compounds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MetabolicNetwork subgraphFromCompoundsWithRoles(
			Collection<Compound> sources, Collection<Compound> targets,
			Collection<Compound> compounds) {
		// TODO Auto-generated method stub
		return null;
	}	
	
	@Override
	public void exportAsXML(String filename, MetabolicNetworkWriterListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeCompounds(Collection<Compound> compounds) {
	}

	@Override
	public void removeReactions(Collection<Reaction> reactions) {
	}
	
	@Override
	public void union(MetabolicNetwork network) {
	}

	@Override
	public void addCompound(Compound compound) {
	}

	@Override
	public void addReaction(Reaction reaction,
			boolean duplicateReversibleReactions) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getVertexName(Reaction arg0) {
		return arg0.getName();
	}

	@Override
	public String getEdgeName(DefaultEdge arg0) {
		if( arg0 == null) 
			return "Null edge";
		else
			return arg0.getSource()+" ->"+arg0.getTarget();
				
	}

}
