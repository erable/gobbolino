package fr.univ_lyon1.baobab.metabolicNetwork;

import java.util.Collection;

import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkReader;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkWriterListener;

/*
 * MetabolicNetwork represents a generic metabolic network.
 * Since it is not yet defined if the network is going to be a compound, reaction or stoichiometric network
 * there is no data structures to kept information. The class simply interact with some metabolic data reader
 * (e.x: sbml parser) to extract the actual information and prepare it to be processed by the further subclasses.
 * 
 * 
 * @author paulomilreu 
 */
public interface MetabolicNetwork 
{
	public void loadNetwork(MetabolicNetworkReader reader, boolean duplicateReversibleReactions);
	
	public void addCompound(Compound compound);
	
	public void addReaction(Reaction reaction, boolean duplicateReversibleReactions);
	
	public Collection<Compound> getCompounds();
	
	public Collection<Reaction> getReactions();
	
	public Collection<Reaction> getReactionsThatProduce(Compound compound);
	
	public Collection<Reaction> getReactionsThatConsume(Compound compound);
	
	public Collection<Compound> reactionsFires(Reaction reaction, Collection<Compound> availableCompounds);
	
	public MetabolicNetwork subgraphFromReactions(Collection<Reaction> reactions);
	
	public MetabolicNetwork subgraphFromCompounds(Collection<Compound> compounds);
	
	public MetabolicNetwork subgraphFromCompoundsWithRoles(Collection<Compound> sources, Collection<Compound> targets, Collection<Compound> compounds);
	
	public void removeCompounds(Collection<Compound> compounds);
	
	public void removeReactions(Collection<Reaction> reactions);
	
	public void exportAsXML(String filename, MetabolicNetworkWriterListener listener);
	
	public void union(MetabolicNetwork network);
		
}
