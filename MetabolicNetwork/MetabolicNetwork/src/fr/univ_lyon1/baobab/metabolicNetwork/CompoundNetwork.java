package fr.univ_lyon1.baobab.metabolicNetwork;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.jgrapht.EdgeFactory;
import org.jgrapht.ext.EdgeNameProvider;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.DefaultDirectedGraph;

import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkReader;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkWriterListener;

public class CompoundNetwork extends DefaultDirectedGraph<Compound, Reaction> implements VertexNameProvider<Compound>, EdgeNameProvider<Reaction>, MetabolicNetwork
{
	private static final long serialVersionUID = 9186617366975611505L;

	public CompoundNetwork(EdgeFactory<Compound, Reaction> arg0)
	{
		super(arg0);
	}
	
	/*
	 * Interface with some met. network reader
	 */
	public void loadNetwork(MetabolicNetworkReader reader, boolean duplicateReversibleReactions)
	{
		for(Compound c: reader.getCompounds())
			addCompound(c);

		for(Reaction r: reader.getReactions())
			addReaction(r, duplicateReversibleReactions);
	}
	
	public void addCompound(Compound compound)
	{
		// Each compound is a new vertex in the graph
		addVertex(compound);
	}
	
	public void addReaction(Reaction reaction, boolean duplicateReversibleReactions)
	{
		// Each reaction creates s x p edges in the graph, one for each pair of (s)ubstrate and (p)roduct
		for(Stoichiometry substrate: reaction.getSubstrates())
		{
			if( !getCompounds().contains(substrate.getCompound()) ) {
				System.out.println("network does not contain substrate "+substrate.getCompound().id+" of reaction "+reaction.getId());
			}
			else
			{
				for(Stoichiometry product: reaction.getProducts())
				{
					if( !getCompounds().contains(product.getCompound()) )
						System.out.println("network does not contain product "+product.getCompound().id+" of reaction "+reaction.getId());
					else {
						addReaction(reaction.getId(), reaction.getName(), substrate.getCompound(), product.getCompound(), reaction.getReversible());
						if( duplicateReversibleReactions && reaction.getReversible() )
							addReaction(reaction.getId()+"_REV", reaction.getName(), product.getCompound(), substrate.getCompound(), reaction.getReversible());
					}
				}
			}
		}
	}
	
    private Reaction addReaction(String id, String name, Compound source, Compound target, Boolean reversible)
    {
    	Reaction r = addEdge(source, target);
    	if( r != null )
    	{
    		r.setId(id);
    		r.setName(name);
    		r.setReversible(reversible);
    	}
    	return r;
    }

	@Override
	public Collection<Compound> getCompounds() {
		return vertexSet();
	}

	@Override
	public Collection<Reaction> getReactions() {
		return edgeSet();
	}

	@Override
	public Collection<Reaction> getReactionsThatProduce(Compound compound) {
		return incomingEdgesOf(compound);
	}

	@Override
	public Collection<Reaction> getReactionsThatConsume(Compound compound) {
		return outgoingEdgesOf(compound);
	}

	@Override
	public Collection<Compound> reactionsFires(Reaction reaction, Collection<Compound> availableCompounds) 
	{
		List<Compound> products = new ArrayList<Compound>();
		
		for(Stoichiometry substrate: reaction.getSubstrates())
		{
			if(!availableCompounds.contains(substrate.getCompound()))
				return products;
		}

		Iterator<Stoichiometry> productsIterator = reaction.getProducts().iterator();
		// copies the products of the reaction to the result set
		while( productsIterator.hasNext() )
			products.add(productsIterator.next().getCompound());
		
		return products;
	}

	@Override
	public String getEdgeName(Reaction arg0) 
	{
		return arg0.getName();
	}

	@Override
	public String getVertexName(Compound arg0) 
	{
		return arg0.getName();
	}

	@Override
	public MetabolicNetwork subgraphFromReactions(Collection<Reaction> reactions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MetabolicNetwork subgraphFromCompounds(Collection<Compound> compounds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MetabolicNetwork subgraphFromCompoundsWithRoles(
			Collection<Compound> sources, Collection<Compound> targets,
			Collection<Compound> compounds) {
		// TODO Auto-generated method stub
		return null;
	}	
	
	@Override
	public void exportAsXML(String filename, MetabolicNetworkWriterListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeCompounds(Collection<Compound> compounds) {
		removeAllVertices(compounds);
	}

	@Override
	public void removeReactions(Collection<Reaction> reactions) {
		removeAllEdges(reactions);
	}
	
	@Override
	public void union(MetabolicNetwork network) {
		// add the compounds not present in the current network
		for(Compound c: network.getCompounds()) {
			if(!getCompounds().contains(c))
				getCompounds().add(c);
		}
		
		// add reactions not present in the current network
		for(Reaction r: network.getReactions()) {
			if( !getReactions().contains(r))
				getReactions().add(r);
		}
	}

}
