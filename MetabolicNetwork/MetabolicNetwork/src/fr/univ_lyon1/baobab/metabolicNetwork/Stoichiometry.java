package fr.univ_lyon1.baobab.metabolicNetwork;

import java.text.DecimalFormat;

public class Stoichiometry 
{
	private Compound compound;
	private Double	 stoichiometry;
	
	public Stoichiometry(Compound c, Double s)
	{
		setCompound(c);
		setStoichiometry(s);
	}

	public void setCompound(Compound compound) {
		this.compound = compound;
	}

	public Compound getCompound() {
		return compound;
	}

	public void setStoichiometry(Double stoichiometry) {
		this.stoichiometry = stoichiometry;
	}

	public Double getStoichiometry() {
		return stoichiometry;
	}
	
	public String toString() {
		DecimalFormat df = new DecimalFormat("##.##");
		return df.format(stoichiometry) + " " + compound.getId();
	}
	
}
