package fr.univ_lyon1.baobab.metabolicNetwork.topological;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.jgraph.graph.DefaultEdge;
import org.jgrapht.alg.StrongConnectivityInspector;
import org.jgrapht.graph.ClassBasedEdgeFactory;
import org.jgrapht.graph.DirectedSubgraph;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.CompoundNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.MetabolicNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.ReactionNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkNetworkReader;

public class SeedSearcher 
{	
	/*
	 * findTopologicalSources returns a list of compounds for which there is no reaction producing it
	 * 
	 * @author paulomilreu
	 */
	public static List<Compound> findTopologicalSources(MetabolicNetwork network)
	{
		List<Compound> inputs = new ArrayList<Compound>();
		for(Compound c: network.getCompounds())
		{
			if( network.getReactionsThatProduce(c).isEmpty() )
			{
				inputs.add(c);
				c.setSeed(true);
			}
		}
		return inputs;
	}

	/*
	 * findTopologicalTargets returns a list of compounds for which there is no reaction consuming it
	 * 
	 * @author paulomilreu
	 */
	public static List<Compound> findTopologicalTargets(MetabolicNetwork network)
	{
		List<Compound> targets = new ArrayList<Compound>();
		for(Compound c: network.getCompounds())
		{
			if( network.getReactionsThatConsume(c).isEmpty() )
			{
				targets.add(c);
				c.setTarget(true);
			}
		}
		return targets;
	}
	
	/*
	 * findTopologicalSourcesWithReversibility returns a list of compounds for which there is no reaction producing it
	 *     or if all reactions producing it are reversible (meaning that it could be imported)
	 * @author paulomilreu
	 */
	public static List<Compound> findTopologicalSourcesWithReversibility(MetabolicNetwork network)
	{
		List<Compound> inputs = new ArrayList<Compound>();
		for(Compound c: network.getCompounds())
		{
			Collection<Reaction> producing = network.getReactionsThatProduce(c);
			if( producing.isEmpty() )
			{
				inputs.add(c);
				c.setSeed(true);
			}
			else
			{
				Iterator<Reaction> reactions = producing.iterator();
				boolean allReversible = true;
				while(reactions.hasNext())
				{
					Reaction r = reactions.next();
					if( !r.getReversible() )
					{
						allReversible = false;
						break;
					}
				}
				if( allReversible )
				{
					inputs.add(c);
					c.setSeed(true);
				}
			}
		}
		return inputs;
	}
	
	/*
	 * findTopologicalSeedsAndTarges
	 * @author paulomilreu
	 */
	public static List<Compound> findTopologicalSeedsAndTargets(MetabolicNetwork network)
	{
		List<Compound> result = new ArrayList<Compound>();
		result.addAll(findTopologicalSources(network));		
		result.addAll(findTopologicalTargets(network));
		return result;
	}
	
	/*
	 * getStronglyConnectedComponents returns the list of strongly connected components of the network
	 * 
	 * @author paulomilreu
	 */
	public static List<DirectedSubgraph<Reaction,DefaultEdge>> getStronglyConnectedComponentsForRG(MetabolicNetwork network)
	{
		// the method is based on a compound graph representation of the network
		ReactionNetwork metNet = null;
		if( network instanceof ReactionNetwork )
			metNet = (ReactionNetwork) network;
		else
		{
			// if the network is not an instance of a compound network, then transform it into one
			metNet = new ReactionNetwork(new ClassBasedEdgeFactory<Reaction,DefaultEdge>(DefaultEdge.class));
			metNet.loadNetwork(new MetabolicNetworkNetworkReader(network), false);
		}

    	StrongConnectivityInspector<Reaction,DefaultEdge> sci = new StrongConnectivityInspector<Reaction,DefaultEdge>(metNet);
    	return sci.stronglyConnectedSubgraphs();
	}	
	
	/*
	 * getStronglyConnectedComponents returns the list of strongly connected components of the network
	 * 
	 * @author paulomilreu
	 */
	public static List<DirectedSubgraph<Compound,Reaction>> getStronglyConnectedComponents(MetabolicNetwork network)
	{
		// the method is based on a compound graph representation of the network
		CompoundNetwork metNet = null;
		if( network instanceof CompoundNetwork )
			metNet = (CompoundNetwork) network;
		else
		{
			// if the network is not an instance of a compound network, then transform it into one
			metNet = new CompoundNetwork(new ClassBasedEdgeFactory<Compound,Reaction>(Reaction.class));
			metNet.loadNetwork(new MetabolicNetworkNetworkReader(network), false);
		}

    	StrongConnectivityInspector<Compound,Reaction> sci = new StrongConnectivityInspector<Compound,Reaction>(metNet);
    	return sci.stronglyConnectedSubgraphs();
	}
	
	/*
	 * findSeeds returns a list of seeds based on Borenstein et al. (2008) algorithm.
	 * The idea is to apply a strongly connected components decomposition and identify as seeds the compounds
	 * in components that have no incoming edge
	 * 
	 * @author paulomilreu
	 */
	public static List<Compound> findSeeds(MetabolicNetwork network)
	{
		CompoundNetwork metNet = null;
		if( network instanceof CompoundNetwork )
			metNet = (CompoundNetwork) network;
		else
		{
			// if the network is not an instance of a compound network, then transform it into one
			metNet = new CompoundNetwork(new ClassBasedEdgeFactory<Compound,Reaction>(Reaction.class));
			metNet.loadNetwork(new MetabolicNetworkNetworkReader(network), false);
		}

		List<Compound> inputs = new ArrayList<Compound>();
    	List<DirectedSubgraph<Compound,Reaction>> components = getStronglyConnectedComponents(metNet);
    	// after the strongly connected component (scc) decomposition, let us search for scc's with no incoming arc
    	for(DirectedSubgraph<Compound,Reaction> subgraph: components)
    	{
    		boolean incomingFromOutside = false;
    		for(Compound c: subgraph.vertexSet())
    		{
    			for(Reaction r: metNet.incomingEdgesOf(c))
    			{
    				if( ! subgraph.containsVertex(r.getSource()) )
    				{
    					incomingFromOutside = true;
    					break;
    				}
    				
    			}
    			// checks if the compound contains a production reaction that uses something outside the scc
    			if( incomingFromOutside )
    				break;
    		}
    		// if no compound has a way to be produced from outside the scc, then all compounds in the scc are seeds
    		if( ! incomingFromOutside )
    		{
    			inputs.addAll(subgraph.vertexSet());
    		}	
    	}
			
    	// sets all inputs as seeds
    	for(Compound c: inputs) {
    		c.setSeed(true);
    	}
		return inputs;
	}

	/*
	 * findSeeds returns a list of seeds based on Borenstein et al. (2008) algorithm.
	 * The idea is to apply a strongly connected components decomposition and identify as seeds the compounds
	 * in components that have no incoming edge
	 * 
	 * @author paulomilreu
	 */
	public static List<Compound> findSeedsAndTargets(MetabolicNetwork network, String filename)
	{
		File propertiesFile = null;
		BufferedWriter bw = null;
		if( filename != null )
			propertiesFile = new File(filename);			
				
		// the method is based on a compound graph representation of the network
		CompoundNetwork metNet = null;
		if( network instanceof CompoundNetwork )
			metNet = (CompoundNetwork) network;
		else
		{
			// if the network is not an instance of a compound network, then transform it into one
			metNet = new CompoundNetwork(new ClassBasedEdgeFactory<Compound,Reaction>(Reaction.class));
			metNet.loadNetwork(new MetabolicNetworkNetworkReader(network), false);
		}

		List<Compound> inputsAndTargets = new ArrayList<Compound>();
		List<DirectedSubgraph<Compound,Reaction>> components = getStronglyConnectedComponents(metNet);

		try {
			if (propertiesFile != null) {
				bw = new BufferedWriter(new FileWriter(propertiesFile));
				bw.write("id \t consistencyRole");
			}
				
			// after the strongly connected component (scc) decomposition, let us search for scc's with no incoming arc
			for(DirectedSubgraph<Compound,Reaction> subgraph: components)
			{
	    		boolean incomingFromOutside = false;
	    		for(Compound c: subgraph.vertexSet())
	    		{
	    			for(Reaction r: metNet.incomingEdgesOf(c))
	    			{
	    				if( ! subgraph.containsVertex(r.getSource()) )
	    				{
	    					incomingFromOutside = true;
	    					break;
	    				}
	    				
	    			}
	    			// checks if the compound contains a production reaction that uses something outside the scc
	    			if( incomingFromOutside )
	    				break;
	    		}
	    		// if no compound has a way to be produced from outside the scc, then all compounds in the scc are seeds
	    		if( ! incomingFromOutside )
	    		{
    				inputsAndTargets.addAll(subgraph.vertexSet());
	    			for(Compound c: subgraph.vertexSet()) {
		    				c.setSeed(true);
	    			}
	    			
	    			if (propertiesFile != null) {
		    			for(Compound c: subgraph.vertexSet())
		    				bw.write("\n"+c.getId()+" \t SOURCE");
	    			}
	    			
	    		}
	    		else
	    		{
	    			// maybe it is not a source component but a target one.
	        		boolean outgoingToOutside = false;
	        		for(Compound c: subgraph.vertexSet())
	        		{
	        			for(Reaction r: metNet.outgoingEdgesOf(c))
	        			{
	        				if( ! subgraph.containsVertex(r.getTarget()) )
	        				{
	        					outgoingToOutside = true;
	        					break;
	        				}
	        				
	        			}
	        			// checks if the compound contains a production reaction that uses something outside the scc
	        			if( outgoingToOutside )
	        				break;
	        		}
	        		// if no compound has a way to be produced from outside the scc, then all compounds in the scc are seeds
	        		if( ! outgoingToOutside )
	        		{
	        			inputsAndTargets.addAll(subgraph.vertexSet());
		    			if (propertiesFile != null) {	        			
			    			for(Compound c: subgraph.vertexSet())
			    				bw.write("\n"+c.getId()+" \t TARGET");
		    			}
		    			for(Compound c: subgraph.vertexSet()) {
		    				c.setTarget(true);
		    			}		    			
	        		}
	    		}
			}
			if (propertiesFile != null) {
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}		
		return inputsAndTargets;
	}
	
	/*
	 * findSeeds returns a list of seeds based on Borenstein et al. (2008) algorithm.
	 * The idea is to apply a strongly connected components decomposition and identify as seeds the compounds
	 * in components that have no incoming edge
	 * 
	 * @author paulomilreu
	 */
	public static List<Reaction> findSeedsAndTargetsConvertingToReactionGraph(MetabolicNetwork network)
	{
		// the method is based on a reaction graph representation of the network
		ReactionNetwork metNet = null;
		if( network instanceof ReactionNetwork )
			metNet = (ReactionNetwork) network;
		else
		{
			// if the network is not an instance of a compound network, then transform it into one
			metNet = new ReactionNetwork(new ClassBasedEdgeFactory<Reaction,DefaultEdge>(DefaultEdge.class));
			metNet.loadNetwork(new MetabolicNetworkNetworkReader(network), false);
		}

		List<Reaction> inputsAndTargets = new ArrayList<Reaction>();
		List<DirectedSubgraph<Reaction,DefaultEdge>> components = getStronglyConnectedComponentsForRG(metNet);
		try {
			// after the strongly connected component (scc) decomposition, let us search for scc's with no incoming arc
			for(DirectedSubgraph<Reaction,DefaultEdge> subgraph: components)
			{
	    		boolean incomingFromOutside = false;
	    		for(Reaction r: subgraph.vertexSet())
	    		{
	    			for(DefaultEdge e: metNet.incomingEdgesOf(r))
	    			{
	    				if( ! subgraph.containsVertex((Reaction)e.getSource()) )
	    				{
	    					incomingFromOutside = true;
	    					break;
	    				}
	    				
	    			}
	    			// checks if the compound contains a production reaction that uses something outside the scc
	    			if( incomingFromOutside )
	    				break;
	    		}
	    		// if no compound has a way to be produced from outside the scc, then all compounds in the scc are seeds
	    		if( ! incomingFromOutside )
	    		{
    				inputsAndTargets.addAll(subgraph.vertexSet());
	    			for(Reaction r: subgraph.vertexSet()) {
		    				r.setTopologicalSeed(true);
	    			}
	    		}
	    		else
	    		{
	    			// maybe it is not a source component but a target one.
	        		boolean outgoingToOutside = false;
	        		for(Reaction r: subgraph.vertexSet())
	        		{
	        			for(DefaultEdge e: metNet.outgoingEdgesOf(r))
	        			{
	        				if( ! subgraph.containsVertex((Reaction)e.getTarget()) )
	        				{
	        					outgoingToOutside = true;
	        					break;
	        				}
	        				
	        			}
	        			// checks if the compound contains a production reaction that uses something outside the scc
	        			if( outgoingToOutside )
	        				break;
	        		}
	        		// if no compound has a way to be produced from outside the scc, then all compounds in the scc are seeds
	        		if( ! outgoingToOutside )
	        		{
	        			inputsAndTargets.addAll(subgraph.vertexSet());
		    			for(Reaction r: subgraph.vertexSet()) {
		    				r.setTopologicalTarget(true);
		    			}		    			
	        		}
	    		}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}		
		return inputsAndTargets;
	}
		
	
}
