package fr.univ_lyon1.baobab.metabolicNetwork.topological;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import fr.univ_lyon1.baobab.metabolicNetwork.Compound;
import fr.univ_lyon1.baobab.metabolicNetwork.MetabolicNetwork;
import fr.univ_lyon1.baobab.metabolicNetwork.Reaction;
import fr.univ_lyon1.baobab.metabolicNetwork.Stoichiometry;

public class ForwardPropagation 
{
	
	public ForwardPropagation()
	{}
	
	public static Collection<Compound> compute(MetabolicNetwork network, Collection<Compound> seeds)
	{
		// Prepare the collection to return. Initially is the seeds passed as argument.
		Collection<Compound> result = new HashSet<Compound>();
		result.addAll(seeds);

		// Copy the set of reactions to be analyzed
		Collection<Reaction> reactions = new HashSet<Reaction>();
		reactions.addAll(network.getReactions());
		
		Boolean reactionFired = true;
		List<Reaction> reactionsToRemove = new ArrayList<Reaction>();
		while(reactionFired)
		{
			reactionFired = false;
			reactionsToRemove.clear();
			for(Reaction r: reactions)
			{
				Collection<Compound> products = network.reactionsFires(r, result);
				if( products.size() > 0 )
				{
					result.addAll(products);
					reactionFired = true;
					reactionsToRemove.add(r);
				}
			}
			
			reactions.removeAll(reactionsToRemove);
		}		
		return result;
	}
	
	public static List<Compound> computeBlockingCompounds(Collection<Stoichiometry> substrates, Collection<Compound> availableCompounds)
	{
		List<Compound> result = new ArrayList<Compound>();
		Boolean useSubstrate = false, missesSubstrate = false;
		for(Stoichiometry substrate: substrates)
		{
			if(availableCompounds.contains(substrate.getCompound()))
				useSubstrate = true;
			else
			{
				result.add(substrate.getCompound());
				missesSubstrate = true;
			}
		}
		// the list of absent compounds is blocking only if at least one compound is already available
		if( useSubstrate && missesSubstrate )
			return result;
		// otherwise, an empty result must be returned indicating that there is no blocking compound.
		result.clear();
		return result;
	}
	
	public static List<Compound> blockingCompounds(MetabolicNetwork network, Collection<Compound> availableCompounds)
	{
		List<Compound> result = new ArrayList<Compound>();
		for(Reaction reaction: network.getReactions())
		{
			// for each reaction, checks the list of blocking compounds
			List<Compound> blockingCompounds = computeBlockingCompounds(reaction.getSubstrates(), availableCompounds);
			// if there is no blocking compound, check the other direction in the case of reversible reactions
			if( blockingCompounds.size() == 0 && reaction.getReversible() )
				blockingCompounds = computeBlockingCompounds(reaction.getProducts(), availableCompounds);
			// and add the identified blocking compounds to the result set
			result.addAll(blockingCompounds);
		}
		return result;
	}

}
