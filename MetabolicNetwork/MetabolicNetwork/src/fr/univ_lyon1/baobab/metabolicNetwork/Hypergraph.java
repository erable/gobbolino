package fr.univ_lyon1.baobab.metabolicNetwork;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import edu.uci.ics.jung.graph.SetHypergraph;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkReader;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkSBMLWriter;
import fr.univ_lyon1.baobab.metabolicNetwork.IO.MetabolicNetworkWriterListener;

public class Hypergraph extends SetHypergraph<Compound, Reaction> implements MetabolicNetwork
{
	private static final long serialVersionUID = 1879233100336318083L;

	public Hypergraph()
	{}
	
	/*
	 * Interface with some met. network reader
	 */
	public void loadNetwork(MetabolicNetworkReader reader, boolean duplicateReversibleReactions)
	{
		for(Compound c: reader.getCompounds())
			addCompound(c);
		
		for(Reaction r: reader.getReactions())
			addReaction(r, duplicateReversibleReactions);
	}
	
	public void addCompound(Compound compound)
	{
		// Each compound is a new vertex in the hypergraph
		addVertex(compound);
	}
	
	public void addReaction(Reaction reaction, boolean duplicateReversibleReactions)
	{
		// an hyperarc represented by a reaction connects a list of compounds (substrates and products)
		List<Compound> compounds = new ArrayList<Compound>();
		for(Stoichiometry s: reaction.getSubstrates())
			compounds.add(s.getCompound());
		for(Stoichiometry p: reaction.getProducts())
			compounds.add(p.getCompound());
		
		// Each reaction creates an hyperedge
		addEdge(reaction, compounds);		
	}

	@Override
	public Collection<Compound> getCompounds() {
		return getVertices();
	}

	@Override
	public Collection<Reaction> getReactions() {
		return getEdges();
	}
	
	@Override
	public Collection<Reaction> getReactionsThatProduce(Compound compound) 
	{
		Collection<Reaction> result = new ArrayList<Reaction>();
		for(Reaction r: getIncidentEdges(compound))
			if( r.isProduct(compound) || (r.isSubstrate(compound) && r.getReversible()) )
				result.add(r);
		return result;
	}

	@Override
	public Collection<Reaction> getReactionsThatConsume(Compound compound) {
		Collection<Reaction> result = new ArrayList<Reaction>();
		for(Reaction r: getIncidentEdges(compound))
			if( r.isSubstrate(compound) || (r.isProduct(compound) && r.getReversible()))
				result.add(r);
		return result;
	}
	
	@Override
	public Collection<Compound> reactionsFires(Reaction reaction, Collection<Compound> availableCompounds)
	{
		List<Compound> products = new ArrayList<Compound>();
		
		boolean containsAllSubstrates = true;
		for(Stoichiometry substrate: reaction.getSubstrates())
		{
			if(!availableCompounds.contains(substrate.getCompound()))
			{
				containsAllSubstrates = false;
				if( !reaction.getReversible() )
					return products;
				
				break;
			}
		}

		Iterator<Stoichiometry> productsIterator = null;
		// Or the reaction is reversible and we have to check for the other direction
		// or the reaction can happen in the given direction
		if( !containsAllSubstrates && reaction.getReversible())
		{
			// checks if the reaction fires
			for(Stoichiometry substrate: reaction.getProducts())
			{
				if(!availableCompounds.contains(substrate.getCompound()))
					return products;
			}
			productsIterator = reaction.getSubstrates().iterator();
		}
		else
			productsIterator = reaction.getProducts().iterator();
		
		// copies the products of the reaction to the result set
		while( productsIterator.hasNext() )
			products.add(productsIterator.next().getCompound());
		
		return products;
	}
	
	
	public Double[][] getStoichiometricMatrix()
	{
        // Transforms the hashMap of compounds into an ordered-array
        Compound[] compounds = null;
        compounds = getVertices().toArray(compounds);
        
        // Transforms the hashMap of reactions into an ordered-array
        Reaction[] reactions = null;
        reactions = getEdges().toArray(reactions);

        // Allocates the n x m matrix to represent the network
        int n = compounds.length;
        Double[][] w = new Double[n][];
        for(int i = 0; i < n; i++)
            w[i] = new Double[n];        
        
        // Fills the matrix w with the following rule:
        // 1) w(i,j) = 0, if reaction j does not involve compound i
        // 2) w(i,j) = prod(i) - cons(i), if reaction j involves compound i. prod(i) is the amount of i
        //				  produced by j and cons(i) is the amount consumed. ** this also could be 0, if the
        //				  metabolite is "catalytic", i.e, it is consumed and produced at the same rate.

        // The initialization step fills the main diagonal with 0 (rule no. 1)
        // and the rest with infinite (rule no. 3)
        for(int i = 0; i < n; i++)
            for(int j = 0; j < n; j++)
            {
            	Stoichiometry consumption = reactions[j].getConsumptionStoichiometry(compounds[i]);
            	Stoichiometry production  = reactions[j].getProductionStoichiometry(compounds[i]);
            	
            	w[i][j] = (production == null? 0: production.getStoichiometry()) - 
            	          (consumption == null? 0: consumption.getStoichiometry());
            }

        return w;		
	}

	@Override
	public MetabolicNetwork subgraphFromReactions(Collection<Reaction> reactions) {
		Hypergraph subgraph = new Hypergraph();
		for(Reaction r: reactions) 
			subgraph.addReaction(r, r.getReversible());		
		
		return subgraph;
	}

	@Override
	public MetabolicNetwork subgraphFromCompounds(Collection<Compound> compounds) {
		Hypergraph subgraph = new Hypergraph();
		
		for(Compound c: compounds)
			subgraph.addCompound(c);

		List<Compound> compoundsToAdd = new ArrayList<Compound>();		
		List<Reaction> reactionsToAdd = new ArrayList<Reaction>();
		for(Reaction r: getReactions()) {
			// First verify if the reaction is valid (there is at least one substrate and at least one product 
			// that are in the list of compounds in the story)
			boolean valid = false;
			for(Stoichiometry s: r.getSubstrates()) {
				if( compounds.contains(s.getCompound()) ) {
					valid = true;
					break; 
				}
			}
			if( valid )
			{
				valid = false;
				for(Stoichiometry p: r.getProducts()) {
					if( compounds.contains(p.getCompound()) ) {
						valid = true;
						break; 
					}
				}
			}
			// if it is valid, identify the compounds to add and mark also the reaction to be added
			if( valid )
			{
				for(Stoichiometry s: r.getSubstrates()) {
					if( !compounds.contains(s.getCompound()) && !compoundsToAdd.contains(s.getCompound()) )
						compoundsToAdd.add(s.getCompound());
				}
				for(Stoichiometry s: r.getProducts()) {
					if( !compounds.contains(s.getCompound()) && !compoundsToAdd.contains(s.getCompound()) )
						compoundsToAdd.add(s.getCompound());
				}
				reactionsToAdd.add(r);
			}
		}

		// add the compounds and the reactions to the induced graph
		for(Compound c: compoundsToAdd)
			subgraph.addCompound(c);
		for(Reaction r: reactionsToAdd)
			subgraph.addReaction(r, r.getReversible());
		
		return subgraph;
	}

	@Override
	public MetabolicNetwork subgraphFromCompoundsWithRoles(
			Collection<Compound> sources, Collection<Compound> targets,
			Collection<Compound> compounds) {
		Hypergraph subgraph = new Hypergraph();
		
		// all all the sources to the new subgraph
		for(Compound c: sources)
			subgraph.addCompound(c);

		// all all the targets to the new subgraph
		for(Compound c: targets)
			subgraph.addCompound(c);

		// all all the other compounds to the new subgraph
		for(Compound c: compounds)
			subgraph.addCompound(c);
		
		List<Compound> compoundsToAdd = new ArrayList<Compound>();		
		List<Reaction> reactionsToAdd = new ArrayList<Reaction>();
		for(Reaction r: getReactions()) {
			// First verify if the reaction is valid (there is at least one substrate and at least one product 
			// that are in the list of compounds in the story)
			boolean hasIntersection = false;
			for(Stoichiometry s: r.getSubstrates()) {
				if( subgraph.getCompounds().contains(s.getCompound()) ) {
					hasIntersection = true;
					break; 
				}
			}
			if( hasIntersection )
			{
				hasIntersection = false;
				for(Stoichiometry p: r.getProducts()) {
					if( subgraph.getCompounds().contains(p.getCompound()) ) {
						hasIntersection = true;
						break; 
					}
				}
			}
			// if it has an intersection with the list of compounds, identify the compounds to add and mark 
			// also the reaction to be added
			if( hasIntersection )
			{
				// now validate if the reaction (not reversible!) does not violate the roles of the compounds
				boolean valid = true;
				if( !r.getReversible() ) {
					for(Stoichiometry s: r.getSubstrates()) {
						if( targets.contains(s.getCompound()) ) {
							valid = false;
							break;
						}
					}
					if( valid ) {
						for(Stoichiometry s: r.getProducts()) {
							if( sources.contains(s.getCompound()) ) {
								valid = false;
								break;
							}
						}						
					}
				}
				if( valid ) {
					for(Stoichiometry s: r.getSubstrates()) {
						if( !compounds.contains(s.getCompound()) && !compoundsToAdd.contains(s.getCompound()) )
							compoundsToAdd.add(s.getCompound());
					}
					for(Stoichiometry s: r.getProducts()) {
						if( !compounds.contains(s.getCompound()) && !compoundsToAdd.contains(s.getCompound()) )
							compoundsToAdd.add(s.getCompound());
					}
					reactionsToAdd.add(r);					
				}
			}
		}

		// add the compounds and the reactions to the induced graph
		for(Compound c: compoundsToAdd)
			subgraph.addCompound(c);
		for(Reaction r: reactionsToAdd)
			subgraph.addReaction(r, r.getReversible());
		
		return subgraph;	
	}
	
	
	@Override
	public void exportAsXML(String filename, MetabolicNetworkWriterListener listener) {
		new MetabolicNetworkSBMLWriter(filename).write(this, listener);
	}

	@Override
	public void removeCompounds(Collection<Compound> compounds) {		
		for(Compound c: compounds) {
			// updates the reactions involving each compound
			for(Reaction r: getReactionsThatConsume(c)) {
				r.removeCompound(c);
			}
			for(Reaction r: getReactionsThatProduce(c)) {
				r.removeCompound(c);
			}
			removeVertex(c);		
		}
		
		// removes invalid reactions (empty side substrate/product)
		List<Reaction> toRemove = new ArrayList<Reaction>();
		for(Reaction r: getReactions()) {
			if( r.getSubstrates().size() == 0 || r.getProducts().size() == 0 )
				toRemove.add(r);
		}
		
		for(Reaction r: toRemove) {
			removeEdge(r);
		}
	}

	@Override
	public void removeReactions(Collection<Reaction> reactions) {
		for(Reaction r: reactions)
			removeEdge(r);
	}
	
	@Override
	public void union(MetabolicNetwork network) {
		// add the compounds not present in the current network
		for(Compound c: network.getCompounds()) {
			if(!getCompounds().contains(c))
				addCompound(c);
		}
		
		// add reactions not present in the current network
		for(Reaction r: network.getReactions()) {
			if( !getReactions().contains(r))
				addReaction(r, false);
		}
	}

}